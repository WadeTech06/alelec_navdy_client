package com.navdy.service.library.events.connection;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.ProtoField;

public final class DisconnectRequest extends Message {
    public static final Boolean DEFAULT_FORGET = Boolean.valueOf(false);
    private static final long serialVersionUID = 0;
    @ProtoField(tag = 1, type = Datatype.BOOL)
    public final Boolean forget;

    public static final class Builder extends com.squareup.wire.Message.Builder<DisconnectRequest> {
        public Boolean forget;

        public Builder(DisconnectRequest message) {
            super(message);
            if (message != null) {
                this.forget = message.forget;
            }
        }

        public Builder forget(Boolean forget) {
            this.forget = forget;
            return this;
        }

        public DisconnectRequest build() {
            return new DisconnectRequest(this);
        }
    }

    public DisconnectRequest(Boolean forget) {
        this.forget = forget;
    }

    private DisconnectRequest(Builder builder) {
        this(builder.forget);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (other instanceof DisconnectRequest) {
            return equals((Object) this.forget, (Object) ((DisconnectRequest) other).forget);
        }
        return false;
    }

    public int hashCode() {
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = this.forget != null ? this.forget.hashCode() : 0;
        this.hashCode = hashCode;
        return hashCode;
    }
}
