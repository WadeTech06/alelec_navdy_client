package com.navdy.service.library.events.hudcontrol;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.ProtoEnum;
import com.squareup.wire.ProtoField;

public final class AccelerateShutdown extends Message {
    public static final AccelerateReason DEFAULT_REASON = AccelerateReason.ACCELERATE_REASON_UNKNOWN;
    private static final long serialVersionUID = 0;
    @ProtoField(tag = 1, type = Datatype.ENUM)
    public final AccelerateReason reason;

    public enum AccelerateReason implements ProtoEnum {
        ACCELERATE_REASON_UNKNOWN(1),
        ACCELERATE_REASON_HFP_DISCONNECT(2),
        ACCELERATE_REASON_WALKING(3);
        
        private final int value;

        private AccelerateReason(int value) {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }
    }

    public static final class Builder extends com.squareup.wire.Message.Builder<AccelerateShutdown> {
        public AccelerateReason reason;

        public Builder(AccelerateShutdown message) {
            super(message);
            if (message != null) {
                this.reason = message.reason;
            }
        }

        public Builder reason(AccelerateReason reason) {
            this.reason = reason;
            return this;
        }

        public AccelerateShutdown build() {
            return new AccelerateShutdown(this);
        }
    }

    public AccelerateShutdown(AccelerateReason reason) {
        this.reason = reason;
    }

    private AccelerateShutdown(Builder builder) {
        this(builder.reason);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (other instanceof AccelerateShutdown) {
            return equals((Object) this.reason, (Object) ((AccelerateShutdown) other).reason);
        }
        return false;
    }

    public int hashCode() {
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = this.reason != null ? this.reason.hashCode() : 0;
        this.hashCode = hashCode;
        return hashCode;
    }
}
