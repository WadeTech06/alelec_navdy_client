package com.navdy.service.library.events.settings;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoField;
import java.util.Collections;
import java.util.List;

public final class ReadSettingsResponse extends Message {
    public static final List<Setting> DEFAULT_SETTINGS = Collections.emptyList();
    private static final long serialVersionUID = 0;
    @ProtoField(label = Label.REQUIRED, tag = 1)
    public final ScreenConfiguration screenConfiguration;
    @ProtoField(label = Label.REPEATED, messageType = Setting.class, tag = 2)
    public final List<Setting> settings;

    public static final class Builder extends com.squareup.wire.Message.Builder<ReadSettingsResponse> {
        public ScreenConfiguration screenConfiguration;
        public List<Setting> settings;

        public Builder(ReadSettingsResponse message) {
            super(message);
            if (message != null) {
                this.screenConfiguration = message.screenConfiguration;
                this.settings = Message.copyOf(message.settings);
            }
        }

        public Builder screenConfiguration(ScreenConfiguration screenConfiguration) {
            this.screenConfiguration = screenConfiguration;
            return this;
        }

        public Builder settings(List<Setting> settings) {
            this.settings = com.squareup.wire.Message.Builder.checkForNulls(settings);
            return this;
        }

        public ReadSettingsResponse build() {
            checkRequiredFields();
            return new ReadSettingsResponse(this, null);
        }
    }

    public ReadSettingsResponse(ScreenConfiguration screenConfiguration, List<Setting> settings) {
        this.screenConfiguration = screenConfiguration;
        this.settings = Message.immutableCopyOf(settings);
    }

    private ReadSettingsResponse(Builder builder) {
        this(builder.screenConfiguration, builder.settings);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof ReadSettingsResponse)) {
            return false;
        }
        ReadSettingsResponse o = (ReadSettingsResponse) other;
        if (equals((Object) this.screenConfiguration, (Object) o.screenConfiguration) && equals(this.settings, o.settings)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        result = ((this.screenConfiguration != null ? this.screenConfiguration.hashCode() : 0) * 37) + (this.settings != null ? this.settings.hashCode() : 1);
        this.hashCode = result;
        return result;
    }
}
