package com.navdy.service.library.events.glances;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.ProtoField;

public final class KeyValue extends Message {
    public static final String DEFAULT_KEY = "";
    public static final String DEFAULT_VALUE = "";
    private static final long serialVersionUID = 0;
    @ProtoField(tag = 1, type = Datatype.STRING)
    public final String key;
    @ProtoField(tag = 2, type = Datatype.STRING)
    public final String value;

    public static final class Builder extends com.squareup.wire.Message.Builder<KeyValue> {
        public String key;
        public String value;

        public Builder(KeyValue message) {
            super(message);
            if (message != null) {
                this.key = message.key;
                this.value = message.value;
            }
        }

        public Builder key(String key) {
            this.key = key;
            return this;
        }

        public Builder value(String value) {
            this.value = value;
            return this;
        }

        public KeyValue build() {
            return new KeyValue(this, null);
        }
    }

    public KeyValue(String key, String value) {
        this.key = key;
        this.value = value;
    }

    private KeyValue(Builder builder) {
        this(builder.key, builder.value);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof KeyValue)) {
            return false;
        }
        KeyValue o = (KeyValue) other;
        if (equals((Object) this.key, (Object) o.key) && equals((Object) this.value, (Object) o.value)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        if (this.key != null) {
            result = this.key.hashCode();
        } else {
            result = 0;
        }
        int i2 = result * 37;
        if (this.value != null) {
            i = this.value.hashCode();
        }
        result = i2 + i;
        this.hashCode = result;
        return result;
    }
}
