package com.navdy.service.library.events.obd;

import com.squareup.wire.Message;

public final class ObdStatusRequest extends Message {
    private static final long serialVersionUID = 0;

    public static final class Builder extends com.squareup.wire.Message.Builder<ObdStatusRequest> {
        public Builder(ObdStatusRequest message) {
            super(message);
        }

        public ObdStatusRequest build() {
            return new ObdStatusRequest(this);
        }
    }

    private ObdStatusRequest(Builder builder) {
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        return other instanceof ObdStatusRequest;
    }

    public int hashCode() {
        return 0;
    }
}
