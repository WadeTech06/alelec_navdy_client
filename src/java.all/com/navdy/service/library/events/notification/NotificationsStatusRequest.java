package com.navdy.service.library.events.notification;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.ProtoField;

public final class NotificationsStatusRequest extends Message {
    public static final NotificationsState DEFAULT_NEWSTATE = NotificationsState.NOTIFICATIONS_ENABLED;
    public static final ServiceType DEFAULT_SERVICE = ServiceType.SERVICE_ANCS;
    private static final long serialVersionUID = 0;
    @ProtoField(tag = 1, type = Datatype.ENUM)
    public final NotificationsState newState;
    @ProtoField(tag = 2, type = Datatype.ENUM)
    public final ServiceType service;

    public static final class Builder extends com.squareup.wire.Message.Builder<NotificationsStatusRequest> {
        public NotificationsState newState;
        public ServiceType service;

        public Builder(NotificationsStatusRequest message) {
            super(message);
            if (message != null) {
                this.newState = message.newState;
                this.service = message.service;
            }
        }

        public Builder newState(NotificationsState newState) {
            this.newState = newState;
            return this;
        }

        public Builder service(ServiceType service) {
            this.service = service;
            return this;
        }

        public NotificationsStatusRequest build() {
            return new NotificationsStatusRequest(this, null);
        }
    }

    public NotificationsStatusRequest(NotificationsState newState, ServiceType service) {
        this.newState = newState;
        this.service = service;
    }

    private NotificationsStatusRequest(Builder builder) {
        this(builder.newState, builder.service);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof NotificationsStatusRequest)) {
            return false;
        }
        NotificationsStatusRequest o = (NotificationsStatusRequest) other;
        if (equals((Object) this.newState, (Object) o.newState) && equals((Object) this.service, (Object) o.service)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        if (this.newState != null) {
            result = this.newState.hashCode();
        } else {
            result = 0;
        }
        int i2 = result * 37;
        if (this.service != null) {
            i = this.service.hashCode();
        }
        result = i2 + i;
        this.hashCode = result;
        return result;
    }
}
