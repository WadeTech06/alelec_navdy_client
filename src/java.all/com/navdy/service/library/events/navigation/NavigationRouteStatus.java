package com.navdy.service.library.events.navigation;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.ProtoField;

public final class NavigationRouteStatus extends Message {
    public static final String DEFAULT_HANDLE = "";
    public static final Integer DEFAULT_PROGRESS = Integer.valueOf(0);
    public static final String DEFAULT_REQUESTID = "";
    private static final long serialVersionUID = 0;
    @ProtoField(tag = 1, type = Datatype.STRING)
    public final String handle;
    @ProtoField(tag = 2, type = Datatype.INT32)
    public final Integer progress;
    @ProtoField(tag = 3, type = Datatype.STRING)
    public final String requestId;

    public static final class Builder extends com.squareup.wire.Message.Builder<NavigationRouteStatus> {
        public String handle;
        public Integer progress;
        public String requestId;

        public Builder(NavigationRouteStatus message) {
            super(message);
            if (message != null) {
                this.handle = message.handle;
                this.progress = message.progress;
                this.requestId = message.requestId;
            }
        }

        public Builder handle(String handle) {
            this.handle = handle;
            return this;
        }

        public Builder progress(Integer progress) {
            this.progress = progress;
            return this;
        }

        public Builder requestId(String requestId) {
            this.requestId = requestId;
            return this;
        }

        public NavigationRouteStatus build() {
            return new NavigationRouteStatus(this);
        }
    }

    public NavigationRouteStatus(String handle, Integer progress, String requestId) {
        this.handle = handle;
        this.progress = progress;
        this.requestId = requestId;
    }

    private NavigationRouteStatus(Builder builder) {
        this(builder.handle, builder.progress, builder.requestId);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof NavigationRouteStatus)) {
            return false;
        }
        NavigationRouteStatus o = (NavigationRouteStatus) other;
        if (equals((Object) this.handle, (Object) o.handle) && equals((Object) this.progress, (Object) o.progress) && equals((Object) this.requestId, (Object) o.requestId)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode;
        int hashCode2 = (this.handle != null ? this.handle.hashCode() : 0) * 37;
        if (this.progress != null) {
            hashCode = this.progress.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (hashCode2 + hashCode) * 37;
        if (this.requestId != null) {
            i = this.requestId.hashCode();
        }
        result = hashCode + i;
        this.hashCode = result;
        return result;
    }
}
