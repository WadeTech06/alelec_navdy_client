package com.navdy.service.library.events.dial;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.ProtoField;

public final class DialStatusResponse extends Message {
    public static final Integer DEFAULT_BATTERYLEVEL = Integer.valueOf(0);
    public static final Boolean DEFAULT_ISCONNECTED = Boolean.valueOf(false);
    public static final Boolean DEFAULT_ISPAIRED = Boolean.valueOf(false);
    public static final String DEFAULT_MACADDRESS = "";
    public static final String DEFAULT_NAME = "";
    private static final long serialVersionUID = 0;
    @ProtoField(tag = 5, type = Datatype.INT32)
    public final Integer batteryLevel;
    @ProtoField(tag = 2, type = Datatype.BOOL)
    public final Boolean isConnected;
    @ProtoField(tag = 1, type = Datatype.BOOL)
    public final Boolean isPaired;
    @ProtoField(tag = 4, type = Datatype.STRING)
    public final String macAddress;
    @ProtoField(tag = 3, type = Datatype.STRING)
    public final String name;

    public static final class Builder extends com.squareup.wire.Message.Builder<DialStatusResponse> {
        public Integer batteryLevel;
        public Boolean isConnected;
        public Boolean isPaired;
        public String macAddress;
        public String name;

        public Builder(DialStatusResponse message) {
            super(message);
            if (message != null) {
                this.isPaired = message.isPaired;
                this.isConnected = message.isConnected;
                this.name = message.name;
                this.macAddress = message.macAddress;
                this.batteryLevel = message.batteryLevel;
            }
        }

        public Builder isPaired(Boolean isPaired) {
            this.isPaired = isPaired;
            return this;
        }

        public Builder isConnected(Boolean isConnected) {
            this.isConnected = isConnected;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder macAddress(String macAddress) {
            this.macAddress = macAddress;
            return this;
        }

        public Builder batteryLevel(Integer batteryLevel) {
            this.batteryLevel = batteryLevel;
            return this;
        }

        public DialStatusResponse build() {
            return new DialStatusResponse(this);
        }
    }

    public DialStatusResponse(Boolean isPaired, Boolean isConnected, String name, String macAddress, Integer batteryLevel) {
        this.isPaired = isPaired;
        this.isConnected = isConnected;
        this.name = name;
        this.macAddress = macAddress;
        this.batteryLevel = batteryLevel;
    }

    private DialStatusResponse(Builder builder) {
        this(builder.isPaired, builder.isConnected, builder.name, builder.macAddress, builder.batteryLevel);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof DialStatusResponse)) {
            return false;
        }
        DialStatusResponse o = (DialStatusResponse) other;
        if (equals((Object) this.isPaired, (Object) o.isPaired) && equals((Object) this.isConnected, (Object) o.isConnected) && equals((Object) this.name, (Object) o.name) && equals((Object) this.macAddress, (Object) o.macAddress) && equals((Object) this.batteryLevel, (Object) o.batteryLevel)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode;
        int hashCode2 = (this.isPaired != null ? this.isPaired.hashCode() : 0) * 37;
        if (this.isConnected != null) {
            hashCode = this.isConnected.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.name != null) {
            hashCode = this.name.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.macAddress != null) {
            hashCode = this.macAddress.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (hashCode2 + hashCode) * 37;
        if (this.batteryLevel != null) {
            i = this.batteryLevel.hashCode();
        }
        result = hashCode + i;
        this.hashCode = result;
        return result;
    }
}
