package com.navdy.service.library.events.dial;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.ProtoField;

public final class DialBondResponse extends Message {
    public static final String DEFAULT_MACADDRESS = "";
    public static final String DEFAULT_NAME = "";
    public static final DialError DEFAULT_STATUS = DialError.DIAL_ERROR;
    private static final long serialVersionUID = 0;
    @ProtoField(tag = 3, type = Datatype.STRING)
    public final String macAddress;
    @ProtoField(tag = 2, type = Datatype.STRING)
    public final String name;
    @ProtoField(tag = 1, type = Datatype.ENUM)
    public final DialError status;

    public static final class Builder extends com.squareup.wire.Message.Builder<DialBondResponse> {
        public String macAddress;
        public String name;
        public DialError status;

        public Builder(DialBondResponse message) {
            super(message);
            if (message != null) {
                this.status = message.status;
                this.name = message.name;
                this.macAddress = message.macAddress;
            }
        }

        public Builder status(DialError status) {
            this.status = status;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder macAddress(String macAddress) {
            this.macAddress = macAddress;
            return this;
        }

        public DialBondResponse build() {
            return new DialBondResponse(this);
        }
    }

    public DialBondResponse(DialError status, String name, String macAddress) {
        this.status = status;
        this.name = name;
        this.macAddress = macAddress;
    }

    private DialBondResponse(Builder builder) {
        this(builder.status, builder.name, builder.macAddress);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof DialBondResponse)) {
            return false;
        }
        DialBondResponse o = (DialBondResponse) other;
        if (equals((Object) this.status, (Object) o.status) && equals((Object) this.name, (Object) o.name) && equals((Object) this.macAddress, (Object) o.macAddress)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode;
        int hashCode2 = (this.status != null ? this.status.hashCode() : 0) * 37;
        if (this.name != null) {
            hashCode = this.name.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (hashCode2 + hashCode) * 37;
        if (this.macAddress != null) {
            i = this.macAddress.hashCode();
        }
        result = hashCode + i;
        this.hashCode = result;
        return result;
    }
}
