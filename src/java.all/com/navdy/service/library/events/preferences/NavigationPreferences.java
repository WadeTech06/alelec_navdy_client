package com.navdy.service.library.events.preferences;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoEnum;
import com.squareup.wire.ProtoField;

public final class NavigationPreferences extends Message {
    public static final Boolean DEFAULT_ALLOWAUTOTRAINS = Boolean.valueOf(true);
    public static final Boolean DEFAULT_ALLOWFERRIES = Boolean.valueOf(true);
    public static final Boolean DEFAULT_ALLOWHIGHWAYS = Boolean.valueOf(true);
    public static final Boolean DEFAULT_ALLOWHOVLANES = Boolean.valueOf(true);
    public static final Boolean DEFAULT_ALLOWTOLLROADS = Boolean.valueOf(true);
    public static final Boolean DEFAULT_ALLOWTUNNELS = Boolean.valueOf(true);
    public static final Boolean DEFAULT_ALLOWUNPAVEDROADS = Boolean.valueOf(true);
    public static final RerouteForTraffic DEFAULT_REROUTEFORTRAFFIC = RerouteForTraffic.REROUTE_CONFIRM;
    public static final RoutingType DEFAULT_ROUTINGTYPE = RoutingType.ROUTING_FASTEST;
    public static final Long DEFAULT_SERIAL_NUMBER = Long.valueOf(0);
    public static final Boolean DEFAULT_SHOWTRAFFICINOPENMAP = Boolean.valueOf(true);
    public static final Boolean DEFAULT_SHOWTRAFFICWHILENAVIGATING = Boolean.valueOf(true);
    public static final Boolean DEFAULT_SPOKENCAMERAWARNINGS = Boolean.valueOf(true);
    public static final Boolean DEFAULT_SPOKENSPEEDLIMITWARNINGS = Boolean.valueOf(true);
    public static final Boolean DEFAULT_SPOKENTURNBYTURN = Boolean.valueOf(true);
    private static final long serialVersionUID = 0;
    @ProtoField(tag = 13, type = Datatype.BOOL)
    public final Boolean allowAutoTrains;
    @ProtoField(tag = 10, type = Datatype.BOOL)
    public final Boolean allowFerries;
    @ProtoField(tag = 14, type = Datatype.BOOL)
    public final Boolean allowHOVLanes;
    @ProtoField(tag = 8, type = Datatype.BOOL)
    public final Boolean allowHighways;
    @ProtoField(tag = 9, type = Datatype.BOOL)
    public final Boolean allowTollRoads;
    @ProtoField(tag = 11, type = Datatype.BOOL)
    public final Boolean allowTunnels;
    @ProtoField(tag = 12, type = Datatype.BOOL)
    public final Boolean allowUnpavedRoads;
    @ProtoField(tag = 2, type = Datatype.ENUM)
    public final RerouteForTraffic rerouteForTraffic;
    @ProtoField(tag = 3, type = Datatype.ENUM)
    public final RoutingType routingType;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.INT64)
    public final Long serial_number;
    @ProtoField(tag = 6, type = Datatype.BOOL)
    public final Boolean showTrafficInOpenMap;
    @ProtoField(tag = 7, type = Datatype.BOOL)
    public final Boolean showTrafficWhileNavigating;
    @ProtoField(tag = 15, type = Datatype.BOOL)
    public final Boolean spokenCameraWarnings;
    @ProtoField(tag = 5, type = Datatype.BOOL)
    public final Boolean spokenSpeedLimitWarnings;
    @ProtoField(tag = 4, type = Datatype.BOOL)
    public final Boolean spokenTurnByTurn;

    public static final class Builder extends com.squareup.wire.Message.Builder<NavigationPreferences> {
        public Boolean allowAutoTrains;
        public Boolean allowFerries;
        public Boolean allowHOVLanes;
        public Boolean allowHighways;
        public Boolean allowTollRoads;
        public Boolean allowTunnels;
        public Boolean allowUnpavedRoads;
        public RerouteForTraffic rerouteForTraffic;
        public RoutingType routingType;
        public Long serial_number;
        public Boolean showTrafficInOpenMap;
        public Boolean showTrafficWhileNavigating;
        public Boolean spokenCameraWarnings;
        public Boolean spokenSpeedLimitWarnings;
        public Boolean spokenTurnByTurn;

        public Builder(NavigationPreferences message) {
            super(message);
            if (message != null) {
                this.serial_number = message.serial_number;
                this.rerouteForTraffic = message.rerouteForTraffic;
                this.routingType = message.routingType;
                this.spokenTurnByTurn = message.spokenTurnByTurn;
                this.spokenSpeedLimitWarnings = message.spokenSpeedLimitWarnings;
                this.showTrafficInOpenMap = message.showTrafficInOpenMap;
                this.showTrafficWhileNavigating = message.showTrafficWhileNavigating;
                this.allowHighways = message.allowHighways;
                this.allowTollRoads = message.allowTollRoads;
                this.allowFerries = message.allowFerries;
                this.allowTunnels = message.allowTunnels;
                this.allowUnpavedRoads = message.allowUnpavedRoads;
                this.allowAutoTrains = message.allowAutoTrains;
                this.allowHOVLanes = message.allowHOVLanes;
                this.spokenCameraWarnings = message.spokenCameraWarnings;
            }
        }

        public Builder serial_number(Long serial_number) {
            this.serial_number = serial_number;
            return this;
        }

        public Builder rerouteForTraffic(RerouteForTraffic rerouteForTraffic) {
            this.rerouteForTraffic = rerouteForTraffic;
            return this;
        }

        public Builder routingType(RoutingType routingType) {
            this.routingType = routingType;
            return this;
        }

        public Builder spokenTurnByTurn(Boolean spokenTurnByTurn) {
            this.spokenTurnByTurn = spokenTurnByTurn;
            return this;
        }

        public Builder spokenSpeedLimitWarnings(Boolean spokenSpeedLimitWarnings) {
            this.spokenSpeedLimitWarnings = spokenSpeedLimitWarnings;
            return this;
        }

        public Builder showTrafficInOpenMap(Boolean showTrafficInOpenMap) {
            this.showTrafficInOpenMap = showTrafficInOpenMap;
            return this;
        }

        public Builder showTrafficWhileNavigating(Boolean showTrafficWhileNavigating) {
            this.showTrafficWhileNavigating = showTrafficWhileNavigating;
            return this;
        }

        public Builder allowHighways(Boolean allowHighways) {
            this.allowHighways = allowHighways;
            return this;
        }

        public Builder allowTollRoads(Boolean allowTollRoads) {
            this.allowTollRoads = allowTollRoads;
            return this;
        }

        public Builder allowFerries(Boolean allowFerries) {
            this.allowFerries = allowFerries;
            return this;
        }

        public Builder allowTunnels(Boolean allowTunnels) {
            this.allowTunnels = allowTunnels;
            return this;
        }

        public Builder allowUnpavedRoads(Boolean allowUnpavedRoads) {
            this.allowUnpavedRoads = allowUnpavedRoads;
            return this;
        }

        public Builder allowAutoTrains(Boolean allowAutoTrains) {
            this.allowAutoTrains = allowAutoTrains;
            return this;
        }

        public Builder allowHOVLanes(Boolean allowHOVLanes) {
            this.allowHOVLanes = allowHOVLanes;
            return this;
        }

        public Builder spokenCameraWarnings(Boolean spokenCameraWarnings) {
            this.spokenCameraWarnings = spokenCameraWarnings;
            return this;
        }

        public NavigationPreferences build() {
            checkRequiredFields();
            return new NavigationPreferences(this);
        }
    }

    public enum RerouteForTraffic implements ProtoEnum {
        REROUTE_CONFIRM(0),
        REROUTE_AUTOMATIC(1),
        REROUTE_NEVER(2);
        
        private final int value;

        private RerouteForTraffic(int value) {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }
    }

    public enum RoutingType implements ProtoEnum {
        ROUTING_FASTEST(0),
        ROUTING_SHORTEST(1);
        
        private final int value;

        private RoutingType(int value) {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }
    }

    public NavigationPreferences(Long serial_number, RerouteForTraffic rerouteForTraffic, RoutingType routingType, Boolean spokenTurnByTurn, Boolean spokenSpeedLimitWarnings, Boolean showTrafficInOpenMap, Boolean showTrafficWhileNavigating, Boolean allowHighways, Boolean allowTollRoads, Boolean allowFerries, Boolean allowTunnels, Boolean allowUnpavedRoads, Boolean allowAutoTrains, Boolean allowHOVLanes, Boolean spokenCameraWarnings) {
        this.serial_number = serial_number;
        this.rerouteForTraffic = rerouteForTraffic;
        this.routingType = routingType;
        this.spokenTurnByTurn = spokenTurnByTurn;
        this.spokenSpeedLimitWarnings = spokenSpeedLimitWarnings;
        this.showTrafficInOpenMap = showTrafficInOpenMap;
        this.showTrafficWhileNavigating = showTrafficWhileNavigating;
        this.allowHighways = allowHighways;
        this.allowTollRoads = allowTollRoads;
        this.allowFerries = allowFerries;
        this.allowTunnels = allowTunnels;
        this.allowUnpavedRoads = allowUnpavedRoads;
        this.allowAutoTrains = allowAutoTrains;
        this.allowHOVLanes = allowHOVLanes;
        this.spokenCameraWarnings = spokenCameraWarnings;
    }

    private NavigationPreferences(Builder builder) {
        this(builder.serial_number, builder.rerouteForTraffic, builder.routingType, builder.spokenTurnByTurn, builder.spokenSpeedLimitWarnings, builder.showTrafficInOpenMap, builder.showTrafficWhileNavigating, builder.allowHighways, builder.allowTollRoads, builder.allowFerries, builder.allowTunnels, builder.allowUnpavedRoads, builder.allowAutoTrains, builder.allowHOVLanes, builder.spokenCameraWarnings);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof NavigationPreferences)) {
            return false;
        }
        NavigationPreferences o = (NavigationPreferences) other;
        if (equals((Object) this.serial_number, (Object) o.serial_number) && equals((Object) this.rerouteForTraffic, (Object) o.rerouteForTraffic) && equals((Object) this.routingType, (Object) o.routingType) && equals((Object) this.spokenTurnByTurn, (Object) o.spokenTurnByTurn) && equals((Object) this.spokenSpeedLimitWarnings, (Object) o.spokenSpeedLimitWarnings) && equals((Object) this.showTrafficInOpenMap, (Object) o.showTrafficInOpenMap) && equals((Object) this.showTrafficWhileNavigating, (Object) o.showTrafficWhileNavigating) && equals((Object) this.allowHighways, (Object) o.allowHighways) && equals((Object) this.allowTollRoads, (Object) o.allowTollRoads) && equals((Object) this.allowFerries, (Object) o.allowFerries) && equals((Object) this.allowTunnels, (Object) o.allowTunnels) && equals((Object) this.allowUnpavedRoads, (Object) o.allowUnpavedRoads) && equals((Object) this.allowAutoTrains, (Object) o.allowAutoTrains) && equals((Object) this.allowHOVLanes, (Object) o.allowHOVLanes) && equals((Object) this.spokenCameraWarnings, (Object) o.spokenCameraWarnings)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode;
        int hashCode2 = (this.serial_number != null ? this.serial_number.hashCode() : 0) * 37;
        if (this.rerouteForTraffic != null) {
            hashCode = this.rerouteForTraffic.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.routingType != null) {
            hashCode = this.routingType.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.spokenTurnByTurn != null) {
            hashCode = this.spokenTurnByTurn.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.spokenSpeedLimitWarnings != null) {
            hashCode = this.spokenSpeedLimitWarnings.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.showTrafficInOpenMap != null) {
            hashCode = this.showTrafficInOpenMap.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.showTrafficWhileNavigating != null) {
            hashCode = this.showTrafficWhileNavigating.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.allowHighways != null) {
            hashCode = this.allowHighways.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.allowTollRoads != null) {
            hashCode = this.allowTollRoads.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.allowFerries != null) {
            hashCode = this.allowFerries.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.allowTunnels != null) {
            hashCode = this.allowTunnels.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.allowUnpavedRoads != null) {
            hashCode = this.allowUnpavedRoads.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.allowAutoTrains != null) {
            hashCode = this.allowAutoTrains.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.allowHOVLanes != null) {
            hashCode = this.allowHOVLanes.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (hashCode2 + hashCode) * 37;
        if (this.spokenCameraWarnings != null) {
            i = this.spokenCameraWarnings.hashCode();
        }
        result = hashCode + i;
        this.hashCode = result;
        return result;
    }
}
