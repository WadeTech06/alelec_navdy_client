package com.navdy.service.library.events.preferences;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.ProtoField;

public final class NotificationPreferencesRequest extends Message {
    public static final Long DEFAULT_SERIAL_NUMBER = Long.valueOf(0);
    private static final long serialVersionUID = 0;
    @ProtoField(tag = 1, type = Datatype.INT64)
    public final Long serial_number;

    public static final class Builder extends com.squareup.wire.Message.Builder<NotificationPreferencesRequest> {
        public Long serial_number;

        public Builder(NotificationPreferencesRequest message) {
            super(message);
            if (message != null) {
                this.serial_number = message.serial_number;
            }
        }

        public Builder serial_number(Long serial_number) {
            this.serial_number = serial_number;
            return this;
        }

        public NotificationPreferencesRequest build() {
            return new NotificationPreferencesRequest(this);
        }
    }

    public NotificationPreferencesRequest(Long serial_number) {
        this.serial_number = serial_number;
    }

    private NotificationPreferencesRequest(Builder builder) {
        this(builder.serial_number);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (other instanceof NotificationPreferencesRequest) {
            return equals((Object) this.serial_number, (Object) ((NotificationPreferencesRequest) other).serial_number);
        }
        return false;
    }

    public int hashCode() {
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = this.serial_number != null ? this.serial_number.hashCode() : 0;
        this.hashCode = hashCode;
        return hashCode;
    }
}
