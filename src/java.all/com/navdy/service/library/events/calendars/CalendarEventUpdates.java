package com.navdy.service.library.events.calendars;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoField;
import java.util.Collections;
import java.util.List;

public final class CalendarEventUpdates extends Message {
    public static final List<CalendarEvent> DEFAULT_CALENDAR_EVENTS = Collections.emptyList();
    private static final long serialVersionUID = 0;
    @ProtoField(label = Label.REPEATED, messageType = CalendarEvent.class, tag = 1)
    public final List<CalendarEvent> calendar_events;

    public static final class Builder extends com.squareup.wire.Message.Builder<CalendarEventUpdates> {
        public List<CalendarEvent> calendar_events;

        public Builder(CalendarEventUpdates message) {
            super(message);
            if (message != null) {
                this.calendar_events = Message.copyOf(message.calendar_events);
            }
        }

        public Builder calendar_events(List<CalendarEvent> calendar_events) {
            this.calendar_events = com.squareup.wire.Message.Builder.checkForNulls(calendar_events);
            return this;
        }

        public CalendarEventUpdates build() {
            return new CalendarEventUpdates(this);
        }
    }

    public CalendarEventUpdates(List<CalendarEvent> calendar_events) {
        this.calendar_events = Message.immutableCopyOf(calendar_events);
    }

    private CalendarEventUpdates(Builder builder) {
        this(builder.calendar_events);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (other instanceof CalendarEventUpdates) {
            return equals(this.calendar_events, ((CalendarEventUpdates) other).calendar_events);
        }
        return false;
    }

    public int hashCode() {
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = this.calendar_events != null ? this.calendar_events.hashCode() : 1;
        this.hashCode = hashCode;
        return hashCode;
    }
}
