package com.navdy.service.library.events;

import com.navdy.service.library.events.NavdyEventUtil.Initializer;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.util.IOUtils;
import com.squareup.wire.Message;
import com.squareup.wire.Wire;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class MessageStore {
    private static final Logger sLogger = new Logger(MessageStore.class);
    private File directory;
    private Wire wire = new Wire(Ext_NavdyEvent.class);

    public MessageStore(File directory) {
        this.directory = directory;
    }

    public static <T extends Message> T removeNulls(T message) {
        return NavdyEventUtil.applyDefaults(message);
    }

    public <T extends Message> T readMessage(String filename, Class<T> type) throws IOException {
        return readMessage(filename, type, null);
    }

    public <T extends Message> T readMessage(String filename, Class<T> type, Initializer<T> initializer) throws IOException {
        T result;
        Exception e;
        Throwable th;
        File messageFile = new File(this.directory, filename);
        InputStream stream = null;
        try {
            if (messageFile.length() == 0) {
                result = NavdyEventUtil.getDefault(type, initializer);
            } else {
                InputStream stream2 = new FileInputStream(messageFile);
                try {
                    result = this.wire.parseFrom(stream2, (Class) type);
                    stream = stream2;
                } catch (Exception e2) {
                    e = e2;
                    stream = stream2;
                    try {
                        result = NavdyEventUtil.getDefault(type, initializer);
                        sLogger.w("Failed to read message from " + filename, e);
                        IOUtils.closeStream(stream);
                        return removeNulls(result);
                    } catch (Throwable th2) {
                        th = th2;
                        IOUtils.closeStream(stream);
                        throw th;
                    }
                } catch (Throwable th3) {
                    th = th3;
                    stream = stream2;
                    IOUtils.closeStream(stream);
                    throw th;
                }
            }
            IOUtils.closeStream(stream);
        } catch (Exception e3) {
            e = e3;
            result = NavdyEventUtil.getDefault(type, initializer);
            sLogger.w("Failed to read message from " + filename, e);
            IOUtils.closeStream(stream);
            return removeNulls(result);
        }
        return removeNulls(result);
    }

    public void writeMessage(Message message, String filename) {
        String tmpFilename = filename + ".new";
        File tmpFile = new File(this.directory, tmpFilename);
        try {
            byte[] bytes = message.toByteArray();
            int bytesCopied = IOUtils.copyFile(tmpFile.getAbsolutePath(), bytes);
            if (bytesCopied != bytes.length) {
                sLogger.e("failed to write messages to " + tmpFilename + "wrote: " + bytesCopied + " bytes, length:" + bytes.length);
            } else if (!tmpFile.renameTo(new File(this.directory, filename))) {
                sLogger.e("could not replace messages in " + filename);
            }
        } catch (Exception e) {
            sLogger.e("could not write new messages to " + filename + " : ", e);
        }
    }
}
