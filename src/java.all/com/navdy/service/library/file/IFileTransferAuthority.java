package com.navdy.service.library.file;

import com.navdy.service.library.events.file.FileType;

public interface IFileTransferAuthority {
    String getDirectoryForFileType(FileType fileType);

    String getFileToSend(FileType fileType);

    boolean isFileTypeAllowed(FileType fileType);

    void onFileSent(FileType fileType);
}
