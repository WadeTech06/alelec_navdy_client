package com.navdy.service.library.network.http.services;

import android.text.TextUtils;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.network.http.HttpUtils;
import com.navdy.service.library.util.IOUtils;
import com.zendesk.service.HttpConstants;
import java.io.File;
import java.io.IOException;
import java.util.List;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request.Builder;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.json.JSONObject;

public class JiraClient {
    public static final String DESCRIPTION = "description";
    public static final String FIELDS = "fields";
    public static final String ISSUE_TYPE = "issuetype";
    public static final String ISSUE_TYPE_BUG = "Bug";
    private static final String JIRA_ATTACHMENTS = "/attachments/";
    private static final String JIRA_ISSUE_API_URL = "https://navdyhud.atlassian.net/rest/api/2/issue/";
    public static final String KEY = "key";
    public static final String NAME = "name";
    public static final String PROJECT = "project";
    public static final String SUMMARY = "summary";
    private static final Logger sLogger = new Logger(JiraClient.class);
    private String encodedCredentials;
    private OkHttpClient mClient;

    public interface ResultCallback {
        void onError(Throwable th);

        void onSuccess(Object obj);
    }

    public static class Attachment {
        public String filePath = "";
        public String mimeType = "";
    }

    public JiraClient(OkHttpClient httpClient) {
        this.mClient = httpClient;
    }

    public void submitTicket(String projectName, String issueTypeName, String summary, String description, ResultCallback callback) throws IOException {
        try {
            JSONObject data = new JSONObject();
            JSONObject fields = new JSONObject();
            JSONObject project = new JSONObject();
            JSONObject issueType = new JSONObject();
            project.put("key", projectName);
            fields.put("project", project);
            issueType.put("name", issueTypeName);
            fields.put("issuetype", issueType);
            fields.put("summary", summary);
            fields.put("description", description);
            data.put("fields", fields);
            Builder requestBuilder = new Builder().url(JIRA_ISSUE_API_URL).post(HttpUtils.getJsonRequestBody(data.toString()));
            if (!TextUtils.isEmpty(this.encodedCredentials)) {
                requestBuilder.addHeader("Authorization", "Basic " + this.encodedCredentials);
            }
            final ResultCallback resultCallback = callback;
            this.mClient.newCall(requestBuilder.build()).enqueue(new Callback() {
                public void onFailure(Call call, IOException e) {
                    if (resultCallback != null) {
                        resultCallback.onError(e);
                    }
                }

                public void onResponse(Call call, Response response) throws IOException {
                    try {
                        int responseCode = response.code();
                        if (responseCode == 200 || responseCode == HttpConstants.HTTP_CREATED) {
                            String key = new JSONObject(response.body().string()).getString("key");
                            if (resultCallback != null) {
                                resultCallback.onSuccess(key);
                            }
                        } else if (resultCallback != null) {
                            resultCallback.onError(new IOException("Jira request failed with " + responseCode));
                        }
                    } catch (Throwable t) {
                        if (resultCallback != null) {
                            resultCallback.onError(t);
                        }
                    }
                }
            });
        } catch (Throwable t) {
            callback.onError(t);
        } finally {
            IOUtils.closeStream(null);
        }
    }

    public void setEncodedCredentials(String encodedCredentials) {
        this.encodedCredentials = encodedCredentials;
    }

    public void attachFilesToTicket(String ticketId, List<Attachment> attachments, final ResultCallback callback) {
        MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);
        for (Attachment attachment : attachments) {
            File file = new File(attachment.filePath);
            if (!file.exists()) {
                throw new IllegalArgumentException("Attachment " + file.getName() + "File does not exists");
            } else if (file.canRead()) {
                builder.addFormDataPart(TransferTable.COLUMN_FILE, file.getName(), RequestBody.create(MediaType.parse(attachment.mimeType), file));
            } else {
                throw new IllegalArgumentException("Attachment " + file.getName() + "File cannot be read");
            }
        }
        Builder requestBuilder = new Builder().url(JIRA_ISSUE_API_URL + ticketId + JIRA_ATTACHMENTS).addHeader("X-Atlassian-Token", "nocheck").post(builder.build());
        if (!TextUtils.isEmpty(this.encodedCredentials)) {
            requestBuilder.addHeader("Authorization", "Basic " + this.encodedCredentials);
        }
        this.mClient.newCall(requestBuilder.build()).enqueue(new Callback() {
            public void onFailure(Call call, IOException e) {
                if (callback != null) {
                    callback.onError(e);
                }
            }

            public void onResponse(Call call, Response response) throws IOException {
                try {
                    int responseCode = response.code();
                    if ((responseCode == 200 || responseCode == HttpConstants.HTTP_CREATED) && callback != null) {
                        callback.onSuccess(null);
                    }
                } catch (Throwable t) {
                    if (callback != null) {
                        callback.onError(t);
                    }
                }
            }
        });
    }
}
