package com.navdy.service.library.network;

import com.navdy.service.library.device.NavdyDeviceId;
import com.navdy.service.library.device.connection.ConnectionInfo;
import com.navdy.service.library.device.connection.ConnectionType;
import com.navdy.service.library.device.connection.TCPConnectionInfo;
import com.navdy.service.library.log.Logger;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;

public class TCPSocketAcceptor implements SocketAcceptor {
    private static final Logger sLogger = new Logger(TCPSocketAcceptor.class);
    private final boolean local;
    private final int port;
    private ServerSocket serverSocket;

    public TCPSocketAcceptor(int port) {
        this(port, false);
    }

    public TCPSocketAcceptor(int port, boolean bindToLocalHost) {
        this.local = bindToLocalHost;
        this.port = port;
    }

    public SocketAdapter accept() throws IOException {
        if (this.serverSocket == null) {
            if (this.local) {
                this.serverSocket = new ServerSocket(this.port, 10, InetAddress.getByAddress(new byte[]{Byte.MAX_VALUE, (byte) 0, (byte) 0, (byte) 1}));
            } else {
                this.serverSocket = new ServerSocket(this.port, 10);
            }
            sLogger.d("Socket bound to " + (this.local ? "localhost" : "all interfaces") + ", port " + this.port);
        }
        return new TCPSocketAdapter(this.serverSocket.accept());
    }

    public void close() throws IOException {
        if (this.serverSocket != null) {
            this.serverSocket.close();
            this.serverSocket = null;
        }
    }

    public ConnectionInfo getRemoteConnectionInfo(SocketAdapter socket, ConnectionType connectionType) {
        if (socket instanceof TCPSocketAdapter) {
            TCPSocketAdapter adapter = (TCPSocketAdapter) socket;
            NavdyDeviceId deviceId = socket.getRemoteDevice();
            if (deviceId != null) {
                return new TCPConnectionInfo(deviceId, adapter.getRemoteAddress());
            }
        }
        return null;
    }
}
