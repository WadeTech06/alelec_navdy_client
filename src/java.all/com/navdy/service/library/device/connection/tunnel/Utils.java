package com.navdy.service.library.device.connection.tunnel;

import org.apache.commons.io.FilenameUtils;

public class Utils {
    static final char[] hexArray = "0123456789ABCDEF".toCharArray();

    public static String bytesToHex(byte[] bytes) {
        return bytesToHex(bytes, false);
    }

    public static String bytesToHex(byte[] bytes, boolean spaces) {
        int charsPerByte = spaces ? 3 : 2;
        char[] hexChars = new char[(bytes.length * charsPerByte)];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 255;
            hexChars[j * charsPerByte] = hexArray[v >>> 4];
            hexChars[(j * charsPerByte) + 1] = hexArray[v & 15];
            if (spaces) {
                hexChars[(j * charsPerByte) + 2] = ' ';
            }
        }
        return new String(hexChars);
    }

    public static String toASCII(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        for (byte aByte : bytes) {
            int v = aByte & 255;
            char c = (v < 32 || v >= 128) ? FilenameUtils.EXTENSION_SEPARATOR : (char) v;
            sb.append(c);
        }
        return sb.toString();
    }
}
