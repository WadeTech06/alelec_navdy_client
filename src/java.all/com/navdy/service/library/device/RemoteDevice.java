package com.navdy.service.library.device;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import androidx.annotation.Nullable;
import com.navdy.service.library.device.connection.Connection;
import com.navdy.service.library.device.connection.Connection.ConnectionFailureCause;
import com.navdy.service.library.device.connection.Connection.DisconnectCause;
import com.navdy.service.library.device.connection.Connection.Status;
import com.navdy.service.library.device.connection.ConnectionInfo;
import com.navdy.service.library.device.connection.ConnectionType;
import com.navdy.service.library.device.link.EventRequest;
import com.navdy.service.library.device.link.Link;
import com.navdy.service.library.device.link.LinkListener;
import com.navdy.service.library.device.link.LinkManager;
import com.navdy.service.library.events.DeviceInfo;
import com.navdy.service.library.events.Ext_NavdyEvent;
import com.navdy.service.library.events.NavdyEvent;
import com.navdy.service.library.events.NavdyEventUtil;
import com.navdy.service.library.events.connection.ConnectionStateChange;
import com.navdy.service.library.events.connection.ConnectionStateChange.ConnectionState;
import com.navdy.service.library.events.connection.NetworkLinkReady;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.network.SocketAdapter;
import com.navdy.service.library.util.Listenable;
import com.squareup.wire.Message;
import com.squareup.wire.Wire;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.atomic.AtomicBoolean;

public class RemoteDevice extends Listenable<Listenable.Listener> implements com.navdy.service.library.device.connection.Connection.Listener, LinkListener {
    public static final int MAX_PACKET_SIZE = 524288;
    private static final Object lock = new Object();
    public static final LastSeenDeviceInfo sLastSeenDeviceInfo = new LastSeenDeviceInfo();
    public static final Logger sLogger = new Logger(RemoteDevice.class);
    protected boolean convertToNavdyEvent;
    public long lastMessageSentTime;
    protected Connection mActiveConnection;
    protected ConnectionInfo mConnectionInfo;
    protected final Context mContext;
    protected NavdyDeviceId mDeviceId;
    protected DeviceInfo mDeviceInfo;
    protected final LinkedBlockingDeque<EventRequest> mEventQueue;
    protected final Handler mHandler;
    private Link mLink;
    private LinkStatus mLinkStatus;
    private volatile boolean mNetworkLinkReady;
    protected AtomicBoolean mNetworkReadyEventDispatched;
    private Wire mWire;

    public interface Listener extends com.navdy.service.library.util.Listenable.Listener {
        void onDeviceConnectFailure(RemoteDevice remoteDevice, ConnectionFailureCause connectionFailureCause);

        void onDeviceConnected(RemoteDevice remoteDevice);

        void onDeviceConnecting(RemoteDevice remoteDevice);

        void onDeviceDisconnected(RemoteDevice remoteDevice, DisconnectCause disconnectCause);

        void onNavdyEventReceived(RemoteDevice remoteDevice, NavdyEvent navdyEvent);

        void onNavdyEventReceived(RemoteDevice remoteDevice, byte[] bArr);
    }

    protected interface EventDispatcher extends EventDispatcher<RemoteDevice, Listener> {
    }

    public static class LastSeenDeviceInfo {
        private DeviceInfo mDeviceInfo;

        public DeviceInfo getDeviceInfo() {
            return this.mDeviceInfo;
        }
    }

    public enum LinkStatus {
        DISCONNECTED,
        LINK_ESTABLISHED,
        CONNECTED
    }

    public interface PostEventHandler {
        void onComplete(PostEventStatus postEventStatus);
    }

    public enum PostEventStatus {
        SUCCESS,
        DISCONNECTED,
        SEND_FAILED,
        UNKNOWN_FAILURE
    }

    public RemoteDevice(Context context, NavdyDeviceId deviceId, boolean convertToNavdyEvent) {
        this.mLinkStatus = LinkStatus.DISCONNECTED;
        this.mNetworkLinkReady = false;
        this.mNetworkReadyEventDispatched = new AtomicBoolean(false);
        this.mContext = context;
        this.mDeviceId = deviceId;
        this.mHandler = new Handler(Looper.getMainLooper());
        this.mEventQueue = new LinkedBlockingDeque();
        this.convertToNavdyEvent = convertToNavdyEvent;
        if (convertToNavdyEvent) {
            this.mWire = new Wire(Ext_NavdyEvent.class);
        }
    }

    public RemoteDevice(Context context, ConnectionInfo connectionInfo, boolean convertToNavdyEvent) {
        this(context, connectionInfo.getDeviceId(), convertToNavdyEvent);
        this.mConnectionInfo = connectionInfo;
    }

    public NavdyDeviceId getDeviceId() {
        return this.mDeviceId;
    }

    public boolean setActiveConnection(Connection connection) {
        boolean z;
        synchronized (lock) {
            this.mActiveConnection = connection;
            if (this.mActiveConnection != null) {
                this.mActiveConnection.addListener(this);
                this.mConnectionInfo = this.mActiveConnection.getConnectionInfo();
            }
            if (isConnected()) {
                dispatchConnectEvent();
            }
            z = this.mActiveConnection != null;
        }
        return z;
    }

    public void removeActiveConnection() {
        synchronized (lock) {
            if (this.mActiveConnection != null) {
                this.mActiveConnection.removeListener(this);
                this.mActiveConnection = null;
            }
        }
    }

    public ConnectionInfo getConnectionInfo() {
        return this.mConnectionInfo;
    }

    public DeviceInfo getDeviceInfo() {
        return this.mDeviceInfo;
    }

    public void setDeviceInfo(DeviceInfo deviceInfo) {
        this.mDeviceInfo = deviceInfo;
        if (deviceInfo != null) {
            sLastSeenDeviceInfo.mDeviceInfo = deviceInfo;
        }
    }

    public boolean connect() {
        if (getConnectionStatus() != Status.DISCONNECTED) {
            return false;
        }
        if (this.mConnectionInfo == null) {
            sLogger.e("can't connect without a connectionInfo");
            return false;
        } else if (setActiveConnection(Connection.instantiateFromConnectionInfo(this.mContext, this.mConnectionInfo))) {
            boolean shouldDispatch;
            synchronized (lock) {
                if (this.mActiveConnection.connect() && getConnectionStatus() == Status.CONNECTING) {
                    shouldDispatch = true;
                } else {
                    shouldDispatch = false;
                }
            }
            if (shouldDispatch) {
                dispatchConnectingEvent();
                sLogger.i("Starting connection");
                return true;
            }
            sLogger.e("Unable to connect");
            removeActiveConnection();
            return false;
        } else {
            sLogger.e("unable to find connection of type: " + this.mConnectionInfo.getType());
            return false;
        }
    }

    public synchronized boolean disconnect() {
        boolean z;
        if (getConnectionStatus() == Status.DISCONNECTED) {
            sLogger.e("can't disconnect; already disconnected");
            z = true;
        } else {
            Connection oldConnection;
            synchronized (lock) {
                oldConnection = this.mActiveConnection;
                this.mActiveConnection = null;
            }
            if (oldConnection != null) {
                z = oldConnection.disconnect();
            } else {
                z = false;
            }
        }
        return z;
    }

    public boolean postEvent(Message message) {
        return postEvent(NavdyEventUtil.eventFromMessage(message), null);
    }

    public boolean postEvent(NavdyEvent event) {
        return postEvent(event, null);
    }

    public boolean postEvent(NavdyEvent event, PostEventHandler postEventHandler) {
        return postEvent(event.toByteArray(), postEventHandler);
    }

    public boolean postEvent(byte[] eventData) {
        return postEvent(eventData, null);
    }

    public boolean postEvent(byte[] eventData, PostEventHandler postEventHandler) {
        boolean ret = this.mEventQueue.add(new EventRequest(this.mHandler, eventData, postEventHandler));
        this.lastMessageSentTime = SystemClock.elapsedRealtime();
        if (sLogger.isLoggable(2)) {
            sLogger.v("NAVDY-PACKET Event queue size [" + this.mEventQueue.size() + "]");
        }
        return ret;
    }

    public Status getConnectionStatus() {
        Status status;
        synchronized (lock) {
            if (this.mActiveConnection == null) {
                status = Status.DISCONNECTED;
            } else {
                status = this.mActiveConnection.getStatus();
            }
        }
        return status;
    }

    public boolean startLink() {
        if (this.mLink != null) {
            throw new IllegalStateException("Link already started");
        }
        sLogger.i("Starting link");
        this.mLink = LinkManager.build(getConnectionInfo());
        SocketAdapter adapter = null;
        synchronized (lock) {
            if (!(this.mLink == null || this.mActiveConnection == null)) {
                adapter = this.mActiveConnection.getSocket();
            }
        }
        if (adapter != null) {
            return this.mLink.start(adapter, this.mEventQueue, this);
        }
        return false;
    }

    public void setLinkBandwidthLevel(int bandwidthLevel) {
        if (this.mLink != null && this.mLinkStatus == LinkStatus.CONNECTED) {
            this.mLink.setBandwidthLevel(bandwidthLevel);
        }
    }

    public int getLinkBandwidthLevel() {
        if (this.mLink == null || this.mLinkStatus != LinkStatus.CONNECTED) {
            return -1;
        }
        return this.mLink.getBandWidthLevel();
    }

    public LinkStatus getLinkStatus() {
        return this.mLinkStatus;
    }

    private void setLinkStatus(LinkStatus state) {
        if (this.mLinkStatus != state) {
            LinkStatus oldState = this.mLinkStatus;
            this.mLinkStatus = state;
            dispatchStateChangeEvents(this.mLinkStatus, oldState);
        }
    }

    private void dispatchStateChangeEvents(LinkStatus newState, LinkStatus oldState) {
        String deviceId = getDeviceId().toString();
        switch (newState) {
            case CONNECTED:
                if (oldState == LinkStatus.DISCONNECTED) {
                    dispatchLocalEvent(new ConnectionStateChange(deviceId, ConnectionState.CONNECTION_LINK_ESTABLISHED));
                }
                dispatchLocalEvent(new ConnectionStateChange(deviceId, ConnectionState.CONNECTION_CONNECTED));
                if (this.mNetworkLinkReady && this.mNetworkReadyEventDispatched.compareAndSet(false, true)) {
                    dispatchLocalEvent(new NetworkLinkReady());
                    sLogger.d("dispatchStateChangeEvents: dispatching the Network Link Ready message");
                    return;
                }
                return;
            case DISCONNECTED:
                if (oldState == LinkStatus.CONNECTED) {
                    dispatchLocalEvent(new ConnectionStateChange(deviceId, ConnectionState.CONNECTION_DISCONNECTED));
                }
                dispatchLocalEvent(new ConnectionStateChange(deviceId, ConnectionState.CONNECTION_LINK_LOST));
                return;
            case LINK_ESTABLISHED:
                if (oldState == LinkStatus.CONNECTED) {
                    dispatchLocalEvent(new ConnectionStateChange(deviceId, ConnectionState.CONNECTION_DISCONNECTED));
                    return;
                } else {
                    dispatchLocalEvent(new ConnectionStateChange(deviceId, ConnectionState.CONNECTION_LINK_ESTABLISHED));
                    return;
                }
            default:
                return;
        }
    }

    public boolean stopLink() {
        sLogger.i("Stopping link");
        if (this.mLink != null) {
            sLogger.i("Closing link");
            this.mNetworkReadyEventDispatched.set(false);
            this.mNetworkLinkReady = false;
            this.mLink.close();
            this.mLink = null;
            this.mEventQueue.clear();
        }
        return true;
    }

    public synchronized void linkEstablished(ConnectionType type) {
        switch (type) {
            case BT_PROTOBUF:
            case TCP_PROTOBUF:
            case EA_PROTOBUF:
                setLinkStatus(LinkStatus.CONNECTED);
                if (this.mLink != null) {
                    this.mLink.setBandwidthLevel(1);
                    break;
                }
                break;
            case BT_IAP2_LINK:
                setLinkStatus(LinkStatus.LINK_ESTABLISHED);
                if (this.mLink != null) {
                    this.mLink.setBandwidthLevel(1);
                    break;
                }
                break;
            default:
                sLogger.e("Unknown connection type: " + type);
                break;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:? A:{SYNTHETIC, RETURN} */
    /* JADX WARNING: Removed duplicated region for block: B:6:0x0013  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onNavdyEventReceived(byte[] eventData) {
        NavdyEvent event;
        int eventTypeIndex;
        if (this.convertToNavdyEvent) {
            event = null;
            try {
                event = (NavdyEvent) this.mWire.parseFrom(eventData, NavdyEvent.class);
            } catch (Throwable th) {
            }
            if (event == null) {
                dispatchNavdyEvent(event);
                return;
            }
            return;
        }
        dispatchNavdyEvent(eventData);
        return;
        sLogger.e("Ignoring invalid navdy event[" + eventTypeIndex + "]", e);
        if (event == null) {
        }
    }

    public synchronized void linkLost(ConnectionType type, DisconnectCause cause) {
        if (getConnectionStatus() == Status.DISCONNECTED) {
            cause = DisconnectCause.NORMAL;
        }
        this.mNetworkReadyEventDispatched.set(false);
        this.mNetworkLinkReady = false;
        switch (type) {
            case BT_PROTOBUF:
            case TCP_PROTOBUF:
                setLinkStatus(LinkStatus.DISCONNECTED);
                markDisconnected(cause);
                break;
            case EA_PROTOBUF:
                if (getLinkStatus() == LinkStatus.CONNECTED) {
                    setLinkStatus(LinkStatus.LINK_ESTABLISHED);
                    break;
                }
                break;
            case BT_IAP2_LINK:
                setLinkStatus(LinkStatus.DISCONNECTED);
                markDisconnected(cause);
                break;
            default:
                sLogger.e("Unknown connection type: " + type);
                break;
        }
    }

    public void onNetworkLinkReady() {
        this.mNetworkLinkReady = true;
        if (getLinkStatus() != LinkStatus.CONNECTED) {
            sLogger.d("Network Link is ready, but state is not connected");
        } else if (this.mNetworkReadyEventDispatched.compareAndSet(false, true)) {
            dispatchLocalEvent(new NetworkLinkReady());
            sLogger.d("onNetworkLinkReady: dispatching the Network Link Ready message");
        }
    }

    private void dispatchLocalEvent(Message message) {
        NavdyEvent event = NavdyEventUtil.eventFromMessage(message);
        if (this.convertToNavdyEvent) {
            dispatchNavdyEvent(event);
        } else {
            dispatchNavdyEvent(event.toByteArray());
        }
    }

    public boolean isConnected() {
        return getConnectionStatus() == Status.CONNECTED;
    }

    public boolean isConnecting() {
        return getConnectionStatus() == Status.CONNECTING;
    }

    @Nullable
    public ConnectionInfo getActiveConnectionInfo() {
        synchronized (lock) {
            if (this.mActiveConnection != null) {
                ConnectionInfo connectionInfo = this.mActiveConnection.getConnectionInfo();
                return connectionInfo;
            }
            return null;
        }
    }

    private void markDisconnected(DisconnectCause cause) {
        removeActiveConnection();
        dispatchDisconnectEvent(cause);
    }

    protected void dispatchConnectingEvent() {
        dispatchToListeners(new EventDispatcher() {
            public void dispatchEvent(RemoteDevice source, Listener listener) {
                listener.onDeviceConnecting(source);
            }
        });
    }

    protected void dispatchConnectEvent() {
        dispatchToListeners(new EventDispatcher() {
            public void dispatchEvent(RemoteDevice source, Listener listener) {
                listener.onDeviceConnected(source);
            }
        });
    }

    protected void dispatchConnectFailureEvent(final ConnectionFailureCause cause) {
        dispatchToListeners(new EventDispatcher() {
            public void dispatchEvent(RemoteDevice source, Listener listener) {
                listener.onDeviceConnectFailure(source, cause);
            }
        });
    }

    protected void dispatchDisconnectEvent(final DisconnectCause cause) {
        dispatchToListeners(new EventDispatcher() {
            public void dispatchEvent(RemoteDevice source, Listener listener) {
                listener.onDeviceDisconnected(source, cause);
            }
        });
    }

    protected void dispatchNavdyEvent(final byte[] eventData) {
        dispatchToListeners(new EventDispatcher() {
            public void dispatchEvent(RemoteDevice source, Listener listener) {
                listener.onNavdyEventReceived(source, eventData);
            }
        });
    }

    protected void dispatchNavdyEvent(final NavdyEvent event) {
        dispatchToListeners(new EventDispatcher() {
            public void dispatchEvent(RemoteDevice source, Listener listener) {
                listener.onNavdyEventReceived(source, event);
            }
        });
    }

    public void onConnected(Connection connection) {
        synchronized (lock) {
            if (this.mActiveConnection == connection) {
                sLogger.i("Connected");
                dispatchConnectEvent();
            } else {
                sLogger.e("Received connect event for unknown connection");
            }
        }
    }

    public void onConnectionFailed(Connection connection, ConnectionFailureCause cause) {
        synchronized (lock) {
            if (this.mActiveConnection == connection) {
                removeActiveConnection();
            }
        }
        sLogger.e("Connection failed: " + cause);
        dispatchConnectFailureEvent(cause);
    }

    public void onDisconnected(Connection connection, DisconnectCause cause) {
        synchronized (lock) {
            if (this.mActiveConnection == connection) {
                removeActiveConnection();
            }
        }
        ConnectionInfo info = connection.getConnectionInfo();
        Logger logger = sLogger;
        StringBuilder append = new StringBuilder().append("Disconnected ");
        if (info == null) {
            info = "unknown";
        }
        logger.i(append.append(info).append(" - ").append(cause).toString());
        dispatchDisconnectEvent(cause);
    }
}
