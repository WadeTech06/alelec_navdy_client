package com.navdy.service.library.device.discovery;

import android.content.Context;
import com.navdy.service.library.device.connection.ConnectionInfo;
import com.navdy.service.library.util.Listenable;
import java.util.Arrays;
import java.util.List;

public abstract class RemoteDeviceScanner extends Listenable<RemoteDeviceScanner.Listener> {
    protected Context mContext;

    public interface Listener extends com.navdy.service.library.util.Listenable.Listener {
        void onDiscovered(RemoteDeviceScanner remoteDeviceScanner, List<ConnectionInfo> list);

        void onLost(RemoteDeviceScanner remoteDeviceScanner, List<ConnectionInfo> list);

        void onScanStarted(RemoteDeviceScanner remoteDeviceScanner);

        void onScanStopped(RemoteDeviceScanner remoteDeviceScanner);
    }

    protected interface ScanEventDispatcher extends EventDispatcher<RemoteDeviceScanner, Listener> {
    }

    public abstract boolean startScan();

    public abstract boolean stopScan();

    RemoteDeviceScanner(Context context) {
        this.mContext = context;
    }

    protected void dispatchOnScanStopped() {
        dispatchToListeners(new ScanEventDispatcher() {
            public void dispatchEvent(RemoteDeviceScanner source, Listener listener) {
                listener.onScanStopped(source);
            }
        });
    }

    protected void dispatchOnScanStarted() {
        dispatchToListeners(new ScanEventDispatcher() {
            public void dispatchEvent(RemoteDeviceScanner source, Listener listener) {
                listener.onScanStarted(source);
            }
        });
    }

    protected void dispatchOnDiscovered(ConnectionInfo connectionInfo) {
        dispatchOnDiscovered(Arrays.asList(new ConnectionInfo[]{connectionInfo}));
    }

    protected void dispatchOnDiscovered(final List<ConnectionInfo> devices) {
        dispatchToListeners(new ScanEventDispatcher() {
            public void dispatchEvent(RemoteDeviceScanner source, Listener listener) {
                listener.onDiscovered(source, devices);
            }
        });
    }

    protected void dispatchOnLost(final List<ConnectionInfo> devices) {
        dispatchToListeners(new ScanEventDispatcher() {
            public void dispatchEvent(RemoteDeviceScanner source, Listener listener) {
                listener.onLost(source, devices);
            }
        });
    }
}
