package com.navdy.client.app.service;

import com.navdy.client.app.framework.util.TTSAudioRouter;
import dagger.MembersInjector;
import dagger.internal.Binding;
import dagger.internal.Linker;
import java.util.Set;
import javax.inject.Provider;

public final class ActivityRecognizedCallbackService$$InjectAdapter extends Binding<ActivityRecognizedCallbackService> implements Provider<ActivityRecognizedCallbackService>, MembersInjector<ActivityRecognizedCallbackService> {
    private Binding<TTSAudioRouter> audioRouter;

    public ActivityRecognizedCallbackService$$InjectAdapter() {
        super("com.navdy.client.app.service.ActivityRecognizedCallbackService", "members/com.navdy.client.app.service.ActivityRecognizedCallbackService", false, ActivityRecognizedCallbackService.class);
    }

    public void attach(Linker linker) {
        this.audioRouter = linker.requestBinding("com.navdy.client.app.framework.util.TTSAudioRouter", ActivityRecognizedCallbackService.class, getClass().getClassLoader());
    }

    public void getDependencies(Set<Binding<?>> set, Set<Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.audioRouter);
    }

    public ActivityRecognizedCallbackService get() {
        ActivityRecognizedCallbackService result = new ActivityRecognizedCallbackService();
        injectMembers(result);
        return result;
    }

    public void injectMembers(ActivityRecognizedCallbackService object) {
        object.audioRouter = (TTSAudioRouter) this.audioRouter.get();
    }
}
