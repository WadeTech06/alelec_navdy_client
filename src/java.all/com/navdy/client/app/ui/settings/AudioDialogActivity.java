package com.navdy.client.app.ui.settings;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.BindView;
import butterknife.OnClick;
import butterknife.Optional;
import com.alelec.navdyclient.R;
import com.navdy.client.app.framework.util.BusProvider;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.framework.util.TTSAudioRouter.TTSAudioStatus;
import com.navdy.client.app.ui.base.BaseActivity;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

public class AudioDialogActivity extends BaseActivity {
    public static final String ACTION_HFP_NOT_CONNECTED = "TEST_HFP_NOT_CONNECTED";
    public static final String ACTION_TEST_AUDIO_STATUS = "TEST_AUDIO_STATUS";
    public static final int DELAY_MILLIS = 2000;
    public static final String EXTRA_TITLE = "EXTRA_TITLE";
    private AudioManager audioManager;
    Bus bus;
    private Runnable finishActivityRunnable;
    private boolean firstEvent = true;
    private Handler handler;
    boolean isMuted = false;
    boolean isShowingAudioStatus = false;
    @BindView(2131755316)
    @Nullable
    TextView outputDeviceName;
    @BindView(2131755315)
    @Nullable
    ImageView statusImage;
    @BindView(2131755135)
    @Nullable
    TextView statusTitle;
    @BindView(2131755317)
    @Nullable
    ProgressBar volumeProgress;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.handler = new Handler();
        this.audioManager = (AudioManager) getSystemService("audio");
        this.finishActivityRunnable = new Runnable() {
            public void run() {
                AudioDialogActivity.this.finish();
            }
        };
        this.bus = BusProvider.getInstance();
        Intent intent = getIntent();
        if (intent != null) {
            String action = intent.getAction();
            if (ACTION_TEST_AUDIO_STATUS.equals(action)) {
                String title;
                setContentView((int) R.layout.dialog_audio_status);
                View view = findViewById(16908290);
                ((ViewGroup) view.getParent()).setBackground(null);
                view.setBackground(null);
                ButterKnife.bind((Object) this, (Activity) this);
                Intent activityIntent = getIntent();
                if (activityIntent == null || !activityIntent.hasExtra(EXTRA_TITLE)) {
                    title = getString(R.string.settings_audio_status_playing_audio_test);
                } else {
                    title = activityIntent.getStringExtra(EXTRA_TITLE);
                }
                this.statusTitle.setText(title);
                this.volumeProgress.setProgress(0);
                this.isShowingAudioStatus = true;
                return;
            } else if (ACTION_HFP_NOT_CONNECTED.equals(action)) {
                setContentView((int) R.layout.settings_audio_hfp_not_connected);
                ButterKnife.bind((Object) this, (Activity) this);
                return;
            } else {
                setContentView((int) R.layout.dialog_audio_settings);
                return;
            }
        }
        setContentView((int) R.layout.dialog_audio_settings);
    }

    protected void onResume() {
        super.onResume();
        if (this.isShowingAudioStatus) {
            this.bus.register(this);
        }
    }

    protected void onPause() {
        super.onPause();
        if (this.isShowingAudioStatus) {
            this.bus.unregister(this);
        }
    }

    public void onCloseClick(View view) {
        onBackPressed();
    }

    private void updateAudioStatus(TTSAudioStatus audioStatus) {
        if (audioStatus.playingTTS || audioStatus.waitingForHfp) {
            this.handler.removeCallbacks(this.finishActivityRunnable);
            this.firstEvent = false;
            if (audioStatus.playingTTS && audioStatus.currentStreamVolume == 0) {
                this.isMuted = true;
                setContentView((int) R.layout.dialog_audio_muted);
                this.audioManager.adjustStreamVolume(audioStatus.streamType == 4 ? 3 : audioStatus.streamType, 0, 1);
                ((ImageButton) findViewById(R.id.btn_close)).setOnClickListener(new OnClickListener() {
                    public void onClick(View view) {
                        AudioDialogActivity.this.finish();
                    }
                });
                ((Button) findViewById(R.id.btn_confirm_audio_turned_up)).setOnClickListener(new OnClickListener() {
                    public void onClick(View view) {
                        AudioDialogActivity.this.finish();
                    }
                });
            }
            if (!this.isMuted) {
                if (audioStatus.throughBluetooth) {
                    this.statusImage.setImageResource(R.drawable.icon_bluetooth_output);
                    if (audioStatus.playingTTS && audioStatus.throughHFp) {
                        this.audioManager.adjustStreamVolume(6, 0, 1);
                    }
                } else {
                    this.statusImage.setImageResource(R.drawable.icon_audio_output);
                }
                this.outputDeviceName.setText(audioStatus.outputDeviceName);
            } else {
                return;
            }
        } else if (this.firstEvent) {
            this.firstEvent = false;
            this.handler.postDelayed(this.finishActivityRunnable, 2000);
        } else {
            this.handler.removeCallbacks(this.finishActivityRunnable);
            if (!this.isMuted) {
                finish();
            }
        }
        if (audioStatus.currentStreamMaxVolume != 0) {
            this.volumeProgress.setMax(audioStatus.currentStreamMaxVolume);
            this.volumeProgress.setProgress(audioStatus.currentStreamVolume);
        }
    }

    @OnClick({2131755908, 2131755909})
    @Nullable
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.settings /*2131755908*/:
                Intent intentOpenBluetoothSettings = new Intent();
                intentOpenBluetoothSettings.setAction("android.settings.BLUETOOTH_SETTINGS");
                startActivity(intentOpenBluetoothSettings);
                return;
            case R.id.ok /*2131755909*/:
                finish();
                return;
            default:
                return;
        }
    }

    @Subscribe
    public void onAudioStatus(TTSAudioStatus ttsAudioStatus) {
        if (this.isShowingAudioStatus) {
            updateAudioStatus(ttsAudioStatus);
        }
    }

    public static void startAudioStatusActivity(Context context) {
        startAudioStatusActivity(context, null);
    }

    public static void startAudioStatusActivity(Context context, String title) {
        Intent intent = new Intent(context, AudioDialogActivity.class);
        intent.setAction(ACTION_TEST_AUDIO_STATUS);
        if (!StringUtils.isEmptyAfterTrim(title)) {
            intent.putExtra(EXTRA_TITLE, title);
        }
        context.startActivity(intent);
    }

    public static void showHFPNotConnected(Context context) {
        Intent intent = new Intent(context, AudioDialogActivity.class);
        intent.setAction(ACTION_HFP_NOT_CONNECTED);
        context.startActivity(intent);
    }

    public boolean requiresBus() {
        return false;
    }
}
