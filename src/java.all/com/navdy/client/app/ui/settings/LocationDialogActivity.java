package com.navdy.client.app.ui.settings;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.provider.Settings.SettingNotFoundException;
import android.view.View;
import com.alelec.navdyclient.R;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.models.Destination;
import com.navdy.client.app.framework.navigation.NavdyRouteHandler;
import com.navdy.client.app.ui.base.BaseActivity;

public class LocationDialogActivity extends BaseActivity {
    public static final String PENDING_DESTINATION = "pending_destination";
    private Destination pendingDestination;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.dialog_location);
        this.pendingDestination = (Destination) getIntent().getExtras().getParcelable(PENDING_DESTINATION);
    }

    protected void onResume() {
        super.onResume();
        try {
            if (Secure.getInt(NavdyApplication.getAppContext().getContentResolver(), "location_mode") == 3) {
                finish();
            }
        } catch (SettingNotFoundException e) {
            this.logger.e("location setting not found: " + e);
        }
    }

    protected void onPause() {
        super.onPause();
    }

    public void onLocationSettingsClick(View view) {
        startActivity(new Intent("android.settings.LOCATION_SOURCE_SETTINGS"));
    }

    public void onCancelClick(View view) {
        NavdyRouteHandler.getInstance().requestNewRoute(this.pendingDestination);
        finish();
    }
}
