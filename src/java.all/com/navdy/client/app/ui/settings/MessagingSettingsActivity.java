package com.navdy.client.app.ui.settings;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import androidx.appcompat.widget.PopupMenu;
import androidx.appcompat.widget.PopupMenu.OnMenuItemClickListener;
import android.support.v7.widget.RecyclerView;
import androidx.recyclerview.widget.ItemTouchHelper;
import android.view.MenuItem;
import android.view.View;
import com.alelec.navdyclient.R;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.tracking.Tracker;
import com.navdy.client.app.tracking.TrackerConstants.Screen.Settings;
import com.navdy.client.app.ui.base.BaseActivity;
import com.navdy.client.app.ui.base.BaseEditActivity;
import com.navdy.client.app.ui.base.BaseToolbarActivity.ToolbarBuilder;
import java.util.ArrayList;
import java.util.Arrays;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MessagingSettingsActivity extends BaseEditActivity {
    private RepliesAdapter adapter;
    private RecyclerView repliesRecycler;
    private ReplyClickListener replyClickListener;

    public interface ReplyClickListener {
        void onReplyClick(String str, View view);
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.settings_messaging);
        new ToolbarBuilder().title((int) R.string.menu_messaging).build();
        this.repliesRecycler = (RecyclerView) findViewById(R.id.replies_list);
        ArrayList<String> replies = getRepliesFromSharedPrefs();
        this.replyClickListener = new ReplyClickListener() {
            public void onReplyClick(final String reply, View v) {
                final PopupMenu popup = new PopupMenu(MessagingSettingsActivity.this, v);
                popup.setOnMenuItemClickListener(new OnMenuItemClickListener() {
                    private static final int DELETE_MENU_ITEM = 2131756065;
                    private static final int EDIT_MENU_ITEM = 2131756064;

                    public boolean onMenuItemClick(MenuItem item) {
                        popup.dismiss();
                        switch (item.getItemId()) {
                            case R.id.menu_delete /*2131756065*/:
                                MessagingSettingsActivity.this.adapter.delete(reply);
                                MessagingSettingsActivity.this.somethingChanged = true;
                                break;
                            default:
                                Intent intent = new Intent(MessagingSettingsActivity.this.getApplicationContext(), MessagingSettingsEditDialogActivity.class);
                                intent.putExtra(MessagingSettingsEditDialogActivity.EXTRA_OLD_MESSAGE, reply);
                                MessagingSettingsActivity.this.startActivityForResult(intent, MessagingSettingsEditDialogActivity.EDIT_CODE);
                                break;
                        }
                        return true;
                    }
                });
                int menuRes = R.menu.menu_messaging_edit_delete;
                if (MessagingSettingsActivity.this.adapter.getItemCount() <= 3) {
                    menuRes = R.menu.menu_messaging_edit;
                }
                popup.getMenuInflater().inflate(menuRes, popup.getMenu());
                popup.show();
            }
        };
        this.adapter = new RepliesAdapter(replies, this.replyClickListener, new Runnable() {
            public void run() {
                MessagingSettingsActivity.this.somethingChanged = true;
            }
        }, this.imageCache);
        if (this.repliesRecycler != null) {
            this.repliesRecycler.setAdapter(this.adapter);
            new ItemTouchHelper(this.adapter.getItemTouchHelperCallback()).attachToRecyclerView(this.repliesRecycler);
            LinearLayoutManager layoutManager = new LinearLayoutManager(this);
            layoutManager.setOrientation(1);
            this.repliesRecycler.setLayoutManager(layoutManager);
        }
        Tracker.tagScreen(Settings.MESSAGING);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        String newMessage;
        if (resultCode == MessagingSettingsEditDialogActivity.EDIT_CODE) {
            newMessage = data.getStringExtra(MessagingSettingsEditDialogActivity.EXTRA_NEW_MESSAGE);
            if (!StringUtils.isEmptyAfterTrim(newMessage)) {
                this.adapter.replace(data.getStringExtra(MessagingSettingsEditDialogActivity.EXTRA_OLD_MESSAGE), newMessage);
                this.somethingChanged = true;
            } else {
                return;
            }
        } else if (resultCode == MessagingSettingsEditDialogActivity.ADD_CODE) {
            newMessage = data.getStringExtra(MessagingSettingsEditDialogActivity.EXTRA_NEW_MESSAGE);
            if (!StringUtils.isEmptyAfterTrim(newMessage)) {
                this.adapter.add(newMessage);
                this.somethingChanged = true;
                this.repliesRecycler.scrollToPosition(this.adapter.getItemCount() - 1);
            }
        }
        if (this.somethingChanged) {
            saveChanges();
        }
    }

    public static ArrayList<String> getRepliesFromSharedPrefs() {
        ArrayList<String> replies = new ArrayList();
        String repliesString = SettingsUtils.getSharedPreferences().getString(SettingsConstants.MESSAGING_REPLIES, null);
        Context context = NavdyApplication.getAppContext();
        if (!StringUtils.isEmptyAfterTrim(repliesString)) {
            return convertStringToArray(repliesString);
        }
        replies.addAll(Arrays.asList(context.getResources().getStringArray(R.array.default_replies_list)));
        replies.add("\ud83d\udc4d");
        replies.add("\ud83d\udc4e");
        replies.add("\ud83d\ude00");
        replies.add("\ud83d\ude1e");
        replies.add("\u2764");
        return replies;
    }

    @Nullable
    public static ArrayList<String> convertStringToArray(@Nullable String repliesString) {
        if (StringUtils.isEmptyAfterTrim(repliesString)) {
            return null;
        }
        ArrayList<String> replies = new ArrayList();
        try {
            JSONArray jsonArray = new JSONObject(repliesString).getJSONArray("replies");
            for (int i = 0; i < jsonArray.length(); i++) {
                replies.add((String) jsonArray.get(i));
            }
            return replies;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    private boolean saveRepliesToSharedPrefsAndSendToHud(ArrayList<String> replies) {
        SharedPreferences sharedPrefs = SettingsUtils.getSharedPreferences();
        String repliesString = convertArrayToString(replies);
        if (StringUtils.isEmptyAfterTrim(repliesString)) {
            return false;
        }
        sharedPrefs.edit().putString(SettingsConstants.MESSAGING_REPLIES, repliesString).apply();
        SettingsUtils.sendMessagingSettingsToTheHud(replies, Long.valueOf(SettingsUtils.incrementSerialNumber("nav_serial_number")));
        return true;
    }

    @Nullable
    private String convertArrayToString(ArrayList<String> replies) {
        String str = null;
        if (replies == null || replies.size() <= 0) {
            return str;
        }
        try {
            JSONArray array = new JSONArray(replies);
            JSONObject object = new JSONObject();
            object.put("replies", array);
            return object.toString();
        } catch (JSONException e) {
            e.printStackTrace();
            return str;
        }
    }

    public void onAddReplyClick(View view) {
        startActivityForResult(new Intent(getApplicationContext(), MessagingSettingsEditDialogActivity.class), MessagingSettingsEditDialogActivity.ADD_CODE);
    }

    protected void saveChanges() {
        if (saveRepliesToSharedPrefsAndSendToHud(this.adapter.getList())) {
            BaseActivity.showShortToast(R.string.settings_messaging_succeeded, new Object[0]);
        } else {
            BaseActivity.showShortToast(R.string.settings_messaging_failed, new Object[0]);
        }
    }
}
