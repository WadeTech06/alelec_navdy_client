package com.navdy.client.app.ui.settings;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import com.alelec.navdyclient.R;
import com.navdy.client.app.framework.util.ZendeskJWT;
import com.navdy.client.app.tracking.Tracker;
import com.navdy.client.app.tracking.TrackerConstants.Screen.Settings;
import com.navdy.client.app.ui.WebViewActivity;
import com.navdy.client.app.ui.base.BaseToolbarActivity;
import com.navdy.client.app.ui.base.BaseToolbarActivity.ToolbarBuilder;
import com.navdy.client.app.ui.firstlaunch.InstallCompleteActivity;
import com.navdy.service.library.task.TaskManager;

public class SupportActivity extends BaseToolbarActivity {
    public static final int SHOW_SUCCESS_DIALOG_FOR_SUPPORT_TICKET = 1;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.settings_support);
        new ToolbarBuilder().title((int) R.string.menu_support).build();
    }

    protected void onResume() {
        super.onResume();
        Tracker.tagScreen(Settings.SUPPORT);
    }

    public void onHelpCenterClick(View view) {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                Uri zendeskUri = ZendeskJWT.getZendeskUri();
                if (zendeskUri != null) {
                    Intent browserIntent = new Intent(SupportActivity.this.getApplicationContext(), WebViewActivity.class);
                    browserIntent.putExtra("type", WebViewActivity.ZENDESK);
                    browserIntent.putExtra(WebViewActivity.EXTRA_HTML, zendeskUri.toString());
                    SupportActivity.this.startActivity(browserIntent);
                }
            }
        }, 1);
    }

    public void onInstallationVideoClick(View view) {
        Intent i = new Intent(getApplicationContext(), InstallCompleteActivity.class);
        i.putExtra(InstallCompleteActivity.EXTRA_COMING_FROM_SETTINGS, true);
        startActivity(i);
    }

    public void onGuidedTourClick(View view) {
        startActivity(new Intent(getApplicationContext(), FeatureVideosActivity.class));
    }

    public void onContactUsClick(View view) {
        startActivityForResult(new Intent(getApplicationContext(), ContactUsActivity.class), 1);
    }
}
