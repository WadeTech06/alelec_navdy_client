package com.navdy.client.app.ui.settings;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.speech.tts.Voice;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.RadioButton;
import android.widget.Switch;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.BindView;
import butterknife.OnClick;
import com.alelec.navdyclient.R;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.framework.util.TTSAudioRouter;
import com.navdy.client.app.framework.util.TTSAudioRouter.AudioOutput;
import com.navdy.client.app.tracking.Tracker;
import com.navdy.client.app.tracking.TrackerConstants.Event;
import com.navdy.client.app.tracking.TrackerConstants.Screen.Settings;
import com.navdy.client.app.ui.base.BaseActivity;
import com.navdy.client.app.ui.base.BaseEditActivity;
import com.navdy.client.app.ui.base.BaseToolbarActivity.ToolbarBuilder;
import com.navdy.client.app.ui.customviews.MultipleChoiceLayout;
import com.navdy.client.app.ui.customviews.MultipleChoiceLayout.ChoiceListener;
import com.navdy.service.library.log.Logger;
import java.util.ArrayList;
import java.util.Random;
import java.util.Set;
import javax.inject.Inject;
import org.droidparts.contract.SQL.DDL;

public class AudioSettingsActivity extends BaseEditActivity implements OnCheckedChangeListener {
    public static final int DIALOG_VOICE_SELECTION = 1;
    public static final Logger sLogger = new Logger(AudioSettingsActivity.class);
    AudioOutput audioOutput = AudioOutput.BEST_AVAILABLE;
    int audioTestIndex = 0;
    String[] audioTests;
    @Inject
    TTSAudioRouter mAudioRouter;
    @BindView(R.id.bluetooth)
    RadioButton mBluetooth;
    @BindView(R.id.camera_warnings)
    Switch mCameraWarnings;
    boolean mCameraWarningsIsOn;
    Voice mInitialVoice;
    @BindView(R.id.)
    TextView mMainDescription;
    String mSelectedVoice;
    @Inject
    SharedPreferences mSharedPrefs;
    @BindView(2131755891)
    RadioButton mSmartBluetooth;
    @BindView(R.id.speaker)
    RadioButton mSpeaker;
    @BindView(R.id.preference_speech_delay)
    View mSpeechDelayPreference;
    boolean mSpeedWarningsIsOn;
    @BindView(2131755905)
    Switch mSppedLimitWarnings;
    boolean mTbtInstructionsIsOn;
    float mTtsLevel;
    @BindView(2131755903)
    Switch mTurnByTurnNavigation;
    @BindView(2131755898)
    TextView mTxtVoiceName;
    @BindView(R.id.preference_voice)
    View mVoicePreference;
    boolean mWelcomeMessage;
    @BindView(2131755904)
    Switch mWelcomeMessageSwitch;
    @BindView(2131755895)
    MultipleChoiceLayout speechLevelSettings;
    @BindView(2131755901)
    TextView tvSpeechLevel;

    static class VoiceLabel {
        String label;
        Voice voice;

        VoiceLabel() {
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_audio);
        Injector.inject(NavdyApplication.getAppContext(), this);
        new ToolbarBuilder().title((int) R.string.menu_audio).build();
        ButterKnife.bind((Activity) this);
        this.audioTests = getResources().getStringArray(R.array.test_audio);
        this.audioTestIndex = new Random().nextInt(this.audioTests.length);
        if (this.mSharedPrefs.contains(SettingsConstants.AUDIO_PHONE_VOLUME)) {
            int phoneVolumeLevel = this.mSharedPrefs.getInt(SettingsConstants.AUDIO_PHONE_VOLUME, 75);
            this.mSharedPrefs.edit().remove(SettingsConstants.AUDIO_PHONE_VOLUME).apply();
            if (!this.mSharedPrefs.contains(SettingsConstants.AUDIO_TTS_VOLUME)) {
                if (phoneVolumeLevel <= 50) {
                    this.mSharedPrefs.edit().putFloat(SettingsConstants.AUDIO_TTS_VOLUME, 0.3f).apply();
                } else if (phoneVolumeLevel <= 75) {
                    this.mSharedPrefs.edit().putFloat(SettingsConstants.AUDIO_TTS_VOLUME, 0.6f).apply();
                } else {
                    this.mSharedPrefs.edit().putFloat(SettingsConstants.AUDIO_TTS_VOLUME, 1.0f).apply();
                }
            }
        }
        this.mTtsLevel = this.mSharedPrefs.getFloat(SettingsConstants.AUDIO_TTS_VOLUME, 0.6f);
        this.mWelcomeMessage = this.mSharedPrefs.getBoolean(SettingsConstants.AUDIO_PLAY_WELCOME_MESSAGE, false);
        this.mTbtInstructionsIsOn = this.mSharedPrefs.getBoolean(SettingsConstants.AUDIO_TURN_BY_TURN_INSTRUCTIONS, true);
        this.mSpeedWarningsIsOn = this.mSharedPrefs.getBoolean(SettingsConstants.AUDIO_SPEED_WARNINGS, false);
        this.mCameraWarningsIsOn = this.mSharedPrefs.getBoolean(SettingsConstants.AUDIO_CAMERA_WARNINGS, true);
        try {
            this.mSelectedVoice = this.mSharedPrefs.getString(SettingsConstants.AUDIO_VOICE, null);
        } catch (ClassCastException e) {
            sLogger.e("Class cast exception while getting the saved voice");
            this.mSharedPrefs.edit().remove(SettingsConstants.AUDIO_VOICE).apply();
        }
        this.audioOutput = AudioOutput.values()[this.mSharedPrefs.getInt(SettingsConstants.AUDIO_OUTPUT_PREFERENCE, SettingsConstants.AUDIO_OUTPUT_PREFERENCE_DEFAULT)];
        boolean voiceAvailable = false;
        if (VERSION.SDK_INT >= 21) {
            voiceAvailable = true;
            try {
                TextToSpeech textToSpeech = this.mAudioRouter.getTextToSpeech();
                this.mInitialVoice = textToSpeech.getVoice();
                if (this.mInitialVoice == null) {
                    sLogger.e("TTS getVoice() returned null, trying to select default voice");
                    this.mInitialVoice = textToSpeech.getDefaultVoice();
                    if (this.mInitialVoice != null) {
                        sLogger.d("Default voice : " + this.mInitialVoice);
                        textToSpeech.setVoice(this.mInitialVoice);
                        this.mSelectedVoice = this.mInitialVoice.getName();
                    } else {
                        sLogger.d("Default voice is also null");
                    }
                }
                Set<Voice> voicesSet = textToSpeech.getVoices();
                if (voicesSet == null) {
                    sLogger.d("List of voices available is null. Not giving an option to select the voice");
                    voiceAvailable = false;
                } else if (StringUtils.isEmptyAfterTrim(this.mSelectedVoice)) {
                    sLogger.d("User selected voice is empty");
                    if (this.mInitialVoice != null) {
                        this.mSelectedVoice = this.mInitialVoice.getName();
                    }
                } else {
                    sLogger.d("User selected voice " + this.mSelectedVoice);
                    if (this.mInitialVoice == null || !this.mSelectedVoice.equals(this.mInitialVoice.getName())) {
                        boolean found = false;
                        for (Voice voice : voicesSet) {
                            if (this.mSelectedVoice.equals(voice.getName())) {
                                textToSpeech.setVoice(voice);
                                found = true;
                                break;
                            }
                        }
                        if (!found) {
                            sLogger.d("Could not find the voice that user selected in the list of voices");
                            if (this.mInitialVoice != null) {
                                sLogger.d("Falling back to the voice currently configured");
                                this.mSelectedVoice = this.mInitialVoice.getName();
                            } else {
                                sLogger.d("No voice currently configured on the TTS");
                                this.mSelectedVoice = null;
                            }
                        }
                    } else {
                        sLogger.d("User selected voice is same as TTS voice currently configured");
                    }
                }
            } catch (Throwable e2) {
                sLogger.e("Exception while getting the voice ", e2);
                voiceAvailable = false;
            }
        }
        sLogger.d("Initializing the Audio preferences");
        sLogger.d("Welcome Message: " + this.mWelcomeMessage);
        sLogger.d("TTS Volume : " + this.mTtsLevel);
        sLogger.d("TBT : " + this.mTbtInstructionsIsOn);
        sLogger.d("Speed warning : " + this.mSpeedWarningsIsOn);
        sLogger.d("Camera warning : " + this.mCameraWarningsIsOn);
        sLogger.d("Output Preference : " + this.audioOutput.name());
        sLogger.d("Saved Voice : " + this.mSelectedVoice);
        this.mMainDescription.setText(StringUtils.fromHtml((int) R.string.settings_audio_title));
        if (this.mTurnByTurnNavigation != null) {
            this.mTurnByTurnNavigation.setChecked(this.mTbtInstructionsIsOn);
            this.mTurnByTurnNavigation.setOnCheckedChangeListener(this);
        }
        boolean ttsIsLow = this.mTtsLevel <= 0.3f;
        boolean ttsIsLoud = this.mTtsLevel >= 1.0f;
        MultipleChoiceLayout multipleChoiceLayout = this.speechLevelSettings;
        int i = ttsIsLow ? 0 : ttsIsLoud ? 2 : 1;
        multipleChoiceLayout.setSelectedIndex(i);
        this.speechLevelSettings.setChoiceListener(new ChoiceListener() {
            public void onChoiceSelected(String text, int index) {
                switch (index) {
                    case 0:
                        AudioSettingsActivity.this.setTtsLevel(0.3f);
                        break;
                    case 1:
                        AudioSettingsActivity.this.setTtsLevel(0.6f);
                        break;
                    case 2:
                        AudioSettingsActivity.this.setTtsLevel(1.0f);
                        break;
                }
                AudioSettingsActivity.this.playTestAudio();
            }
        });
        this.mWelcomeMessageSwitch.setChecked(this.mWelcomeMessage);
        this.mWelcomeMessageSwitch.setOnCheckedChangeListener(this);
        this.mSppedLimitWarnings.setChecked(this.mSpeedWarningsIsOn);
        this.mSppedLimitWarnings.setOnCheckedChangeListener(this);
        this.mCameraWarnings.setChecked(this.mCameraWarningsIsOn);
        this.mCameraWarnings.setOnCheckedChangeListener(this);
        if (VERSION.SDK_INT < 21 || !voiceAvailable) {
            this.mVoicePreference.setVisibility(GONE);
        } else {
            this.mTxtVoiceName.setText(getDisplayName(this.mAudioRouter.getTextToSpeech().getVoice()));
        }
        if (this.audioOutput == AudioOutput.BEST_AVAILABLE) {
            this.mSpeechDelayPreference.setVisibility(VISIBLE);
        } else {
            this.mSpeechDelayPreference.setVisibility(GONE);
        }
        setAudioOutputAndUpdateUI(this.audioOutput);
    }

    protected void onResume() {
        super.onResume();
        Tracker.tagScreen(Settings.AUDIO);
        this.tvSpeechLevel.setText(Integer.toString(this.mSharedPrefs.getInt(SettingsConstants.AUDIO_HFP_DELAY_LEVEL, 1) + 1));
    }

    private void sendPreferencesToDisplay() {
        if (SettingsUtils.sendNavSettingsToTheHud(SettingsUtils.buildNavigationPreferences(SettingsUtils.incrementSerialNumber("nav_serial_number"), this.mSharedPrefs.getBoolean(SettingsConstants.NAVIGATION_ROUTE_CALCULATION, false), this.mSharedPrefs.getBoolean(SettingsConstants.NAVIGATION_AUTO_RECALC, false), this.mSharedPrefs.getBoolean(SettingsConstants.NAVIGATION_HIGHWAYS, true), this.mSharedPrefs.getBoolean(SettingsConstants.NAVIGATION_TOLL_ROADS, true), this.mSharedPrefs.getBoolean(SettingsConstants.NAVIGATION_FERRIES, true), this.mSharedPrefs.getBoolean(SettingsConstants.NAVIGATION_TUNNELS, true), this.mSharedPrefs.getBoolean(SettingsConstants.NAVIGATION_UNPAVED_ROADS, true), this.mSharedPrefs.getBoolean(SettingsConstants.NAVIGATION_AUTO_TRAINS, true), this.mSppedLimitWarnings.isChecked(), this.mCameraWarnings.isChecked(), this.mTurnByTurnNavigation.isChecked()))) {
            Tracker.tagEvent(Event.Settings.AUDIO_SETTINGS_CHANGED);
        } else {
            BaseActivity.showShortToast(R.string.settings_need_to_be_connected_to_hud, new Object[0]);
        }
    }

    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            case R.id.turn_by_turn_instructions /*2131755903*/:
                this.mTbtInstructionsIsOn = isChecked;
                break;
            case R.id.welcome_message /*2131755904*/:
                this.mWelcomeMessage = isChecked;
                break;
            case R.id.speed_warnings /*2131755905*/:
                this.mSpeedWarningsIsOn = isChecked;
                break;
            case R.id.camera_warnings /*2131755906*/:
                this.mCameraWarningsIsOn = isChecked;
                break;
        }
        this.somethingChanged = true;
    }

    protected void saveChanges() {
        this.mSharedPrefs.edit().putFloat(SettingsConstants.AUDIO_TTS_VOLUME, this.mTtsLevel).putBoolean(SettingsConstants.AUDIO_PLAY_WELCOME_MESSAGE, this.mWelcomeMessage).putBoolean(SettingsConstants.AUDIO_TURN_BY_TURN_INSTRUCTIONS, this.mTbtInstructionsIsOn).putBoolean(SettingsConstants.AUDIO_SPEED_WARNINGS, this.mSpeedWarningsIsOn).putBoolean(SettingsConstants.AUDIO_CAMERA_WARNINGS, this.mCameraWarningsIsOn).putString(SettingsConstants.AUDIO_VOICE, this.mSelectedVoice).putInt(SettingsConstants.AUDIO_OUTPUT_PREFERENCE, this.audioOutput.ordinal()).apply();
        sendPreferencesToDisplay();
    }

    @OnClick({R.id.smart_bluetooth_container, R.id.bluetooth, R.id.speaker, R.id.btn_test_audio, R.id.preference_voice, R.id.preference_speech_delay})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_test_audio /*R.id.btn_test_audio*/:
                playTestAudio();
                return;
            case R.id.smart_bluetooth_container /*R.id.smart_bluetooth_container*/:
                setAudioOutputAndUpdateUI(AudioOutput.BEST_AVAILABLE);
                this.mSpeechDelayPreference.setVisibility(VISIBLE);
                playTestAudio();
                return;
            case R.id.bluetooth /*R.id.bluetooth*/:
                setAudioOutputAndUpdateUI(AudioOutput.BLUETOOTH_MEDIA);
                this.mSpeechDelayPreference.setVisibility(GONE);
                playTestAudio();
                return;
            case R.id.speaker /*R.id.speaker*/:
                setAudioOutputAndUpdateUI(AudioOutput.PHONE_SPEAKER);
                this.mSpeechDelayPreference.setVisibility(GONE);
                playTestAudio();
                return;
            case R.id.preference_voice /*R.id.preference_voice*/:
                showSimpleDialog(1, null, null);
                return;
            case R.id.preference_speech_delay /*R.id.preference_speech_delay*/:
                if (TTSAudioRouter.isHFPSpeakerConnected()) {
                    HFPAudioDelaySettingsActivity.start(this);
                    return;
                } else {
                    AudioDialogActivity.showHFPNotConnected(this);
                    return;
                }
            default:
                return;
        }
    }

    private void playTestAudio() {
        if (this.mAudioRouter.isTTSAvailable() && this.audioTests != null && this.audioTests.length > 0) {
            this.mAudioRouter.processTTSRequest(this.audioTests[this.audioTestIndex], null, false);
            this.audioTestIndex++;
            if (this.audioTestIndex >= this.audioTests.length) {
                this.audioTestIndex = 0;
            }
        }
        AudioDialogActivity.startAudioStatusActivity(this);
    }

    private void setAudioOutputAndUpdateUI(AudioOutput audioOutput) {
        switch (audioOutput) {
            case BEST_AVAILABLE:
                this.mSmartBluetooth.setChecked(true);
                this.mBluetooth.setChecked(false);
                this.mSpeaker.setChecked(false);
                break;
            case BLUETOOTH_MEDIA:
                this.mBluetooth.setChecked(true);
                this.mSmartBluetooth.setChecked(false);
                this.mSpeaker.setChecked(false);
                break;
            case PHONE_SPEAKER:
                this.mSpeaker.setChecked(true);
                this.mBluetooth.setChecked(false);
                this.mSmartBluetooth.setChecked(false);
                break;
        }
        this.audioOutput = audioOutput;
        this.somethingChanged = true;
        this.mAudioRouter.setAudioOutput(audioOutput, true);
    }

    public void setTtsLevel(float ttsLevel) {
        this.mTtsLevel = ttsLevel;
        this.mAudioRouter.setTtsVolume(this.mTtsLevel);
        this.somethingChanged = true;
    }

    public Dialog createDialog(int id, Bundle arguments) {
        switch (id) {
            case 1:
                if (VERSION.SDK_INT >= 21) {
                    Set<Voice> voices = this.mAudioRouter.getTextToSpeech().getVoices();
                    if (voices != null && voices.size() > 0) {
                        Object[] voicesArray = voices.toArray();
                        ArrayList<VoiceLabel> voiceLabelsList = new ArrayList();
                        for (Voice voiceObj : voicesArray) {
                            VoiceLabel voiceLabel = new VoiceLabel();
                            voiceLabel.voice = voiceObj;
                            voiceLabel.label = getDisplayName(voiceObj);
                            voiceLabelsList.add(voiceLabel);
                        }
                        final VoiceLabel[] resultantArray = new VoiceLabel[voiceLabelsList.size()];
                        voiceLabelsList.toArray(resultantArray);
                        CharSequence[] labels = new CharSequence[resultantArray.length];
                        int length = resultantArray.length;
                        int i = 0;
                        int i2 = 0;
                        while (i < length) {
                            int i3 = i2 + 1;
                            labels[i2] = resultantArray[i].label;
                            i++;
                            i2 = i3;
                        }
                        Builder builder = new Builder(this);
                        builder.setItems(labels, new OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                if (VERSION.SDK_INT >= 21) {
                                    AudioSettingsActivity.this.somethingChanged = true;
                                    VoiceLabel voiceLabel = resultantArray[which];
                                    if (voiceLabel != null) {
                                        AudioSettingsActivity.sLogger.d("Selected voice :" + voiceLabel.label + " " + voiceLabel.voice.getName());
                                        AudioSettingsActivity.this.mAudioRouter.getTextToSpeech().setVoice(voiceLabel.voice);
                                        AudioSettingsActivity.this.mTxtVoiceName.setText(AudioSettingsActivity.this.mAudioRouter.getTextToSpeech().getVoice() != null ? voiceLabel.label : "");
                                        AudioSettingsActivity.this.mSelectedVoice = voiceLabel.voice.getName();
                                        return;
                                    }
                                    AudioSettingsActivity.sLogger.e("Selected voice does not exists , total :" + resultantArray.length + ", Index :" + which);
                                }
                            }
                        });
                        Dialog dialog = builder.create();
                        dialog.setTitle(getString(R.string.select_voice));
                        return dialog;
                    }
                }
                break;
        }
        return super.createDialog(id, arguments);
    }

    public void onDescriptionClick(View view) {
        startActivity(new Intent(getApplicationContext(), AudioDialogActivity.class));
    }

    private static String getDisplayName(Object voiceObj) {
        if (VERSION.SDK_INT < 21) {
            return null;
        }
        Voice voice = (Voice) voiceObj;
        if (voice == null) {
            return "";
        }
        String displayName = voice.getLocale().getDisplayName();
        String uniqueName = voice.getName();
        int index = uniqueName.indexOf("#");
        if (index <= 0 || index >= uniqueName.length() - 2) {
            return displayName + DDL.OPENING_BRACE + uniqueName + ")";
        }
        return displayName + " " + uniqueName.substring(index + 1, uniqueName.length());
    }

    protected void discardChanges() {
        super.discardChanges();
        if (VERSION.SDK_INT >= 21) {
            this.mAudioRouter.getTextToSpeech().setVoice(this.mInitialVoice);
        }
        this.mAudioRouter.resetToUserPreference();
    }
}
