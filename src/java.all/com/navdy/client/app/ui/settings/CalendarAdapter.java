package com.navdy.client.app.ui.settings;

import android.content.Context;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import com.alelec.navdyclient.R;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.models.Calendar;
import com.navdy.client.app.framework.models.Calendar.CalendarListItemType;
import com.navdy.client.app.ui.homescreen.CalendarUtils;
import java.util.ArrayList;

class CalendarAdapter extends Adapter {
    private Context appContext = NavdyApplication.getAppContext();
    private final ArrayList<Calendar> calendars = CalendarUtils.listCalendars(this.appContext);

    CalendarAdapter() {
    }

    public Calendar getItem(int position) {
        return this.calendars != null ? (Calendar) this.calendars.get(position) : null;
    }

    public long getItemId(int position) {
        Calendar calendar = getItem(position);
        return calendar != null ? calendar.id : 0;
    }

    public int getItemViewType(int position) {
        Calendar calendar = getItem(position);
        return calendar != null ? calendar.type.getValue() : 0;
    }

    public int getItemCount() {
        return this.calendars != null ? this.calendars.size() : 0;
    }

    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater vi = LayoutInflater.from(this.appContext);
        switch (CalendarListItemType.fromValue(viewType)) {
            case GLOBAL_SWITCH:
                return new CalendarGlobalSwitchViewHolder(vi.inflate(R.layout.settings_switch_row, parent, false), new OnCheckedChangeListener() {
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        CalendarAdapter.this.notifyDataSetChanged();
                    }
                });
            case TITLE:
                return new CalendarTitleViewHolder(vi.inflate(R.layout.calendar_title_list_row, parent, false));
            case CALENDAR:
                return new CalendarViewHolder(vi.inflate(R.layout.calendar_list_row, parent, false));
            default:
                return null;
        }
    }

    public void onBindViewHolder(ViewHolder holder, int position) {
        Calendar calendar = getItem(position);
        if (calendar != null) {
            switch (calendar.type) {
                case TITLE:
                    if (holder instanceof CalendarTitleViewHolder) {
                        ((CalendarTitleViewHolder) holder).setName(calendar.displayName);
                        return;
                    }
                    return;
                case CALENDAR:
                    if (holder instanceof CalendarViewHolder) {
                        ((CalendarViewHolder) holder).setCalendar(calendar);
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }
}
