package com.navdy.client.app.ui;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Build.VERSION;
import androidx.core.content.FileProvider;
import com.alelec.navdyclient.R;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.ui.base.BaseActivity;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.util.IOUtils;
import java.io.File;

public class PhotoUtils {
    public static final int COMPRESS_PHOTO_QUALITY = 85;
    public static final int PICK_PHOTO_FROM_GALLERY = 2;
    public static final int TAKE_PHOTO = 1;
    public static final String TEMP_PHOTO_FILE = "temp_photo.jpg";

    public static void takePhoto(final BaseActivity activity, final Logger logger, String filename) {
        if (IOUtils.isExternalStorageReadable()) {
            if (StringUtils.isEmptyAfterTrim(filename)) {
                filename = IOUtils.getTempFilename();
            }
            final String finalName = filename;
            activity.requestCameraPermission(new Runnable() {
                public void run() {
                    activity.requestStoragePermission(new Runnable() {
                        public void run() {
                            Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                            File file = IOUtils.getTempFile(logger, finalName);
                            if (file != null) {
                                try {
                                    intent.putExtra("output", getUri(file));
                                    intent.putExtra("return-data", true);
                                    logger.d("filename: " + finalName);
                                    try {
                                        activity.startActivityForResult(intent, 1);
                                        return;
                                    } catch (ActivityNotFoundException e) {
                                        BaseActivity.showLongToast(R.string.no_camera_found, new Object[0]);
                                        return;
                                    }
                                } catch (Throwable t) {
                                    logger.e("Unable to get a temp file", t);
                                    BaseActivity.showLongToast(R.string.storage_permission_not_granted, new Object[0]);
                                    return;
                                }
                            }
                            logger.e("PhotoUtils:: temporary file is null");
                        }

                        public Uri getUri(File file) {
                            if (VERSION.SDK_INT >= 23) {
                                return FileProvider.getUriForFile(NavdyApplication.getAppContext(), "com.navdy.client.provider", file);
                            }
                            return Uri.fromFile(file);
                        }
                    }, new Runnable() {
                        public void run() {
                            BaseActivity.showLongToast(R.string.storage_permission_not_granted, new Object[0]);
                        }
                    });
                }
            }, new Runnable() {
                public void run() {
                    BaseActivity.showLongToast(R.string.camera_permission_not_granted, new Object[0]);
                }
            });
            return;
        }
        BaseActivity.showLongToast(R.string.external_storage_not_mounted, new Object[0]);
    }

    public static void getPhotoFromGallery(final BaseActivity activity) {
        if (activity != null) {
            activity.requestStoragePermission(new Runnable() {
                public void run() {
                    Intent getIntent = new Intent("android.intent.action.GET_CONTENT");
                    getIntent.setType("image/*");
                    activity.startActivityForResult(Intent.createChooser(getIntent, "Attach an image"), 2);
                }
            }, new Runnable() {
                public void run() {
                    BaseActivity.showLongToast(R.string.storage_permission_not_granted, new Object[0]);
                }
            });
        }
    }
}
