package com.navdy.client.app.ui.base;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.location.Location;
import android.os.Bundle;
import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.LatLngBounds.Builder;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.alelec.navdyclient.R;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.location.NavdyLocationManager;
import com.navdy.client.app.framework.location.NavdyLocationManager.OnNavdyLocationChangedListener;
import com.navdy.client.app.framework.map.MapUtils;
import com.navdy.client.app.framework.util.BusProvider;
import com.navdy.client.app.framework.util.GmsUtils;
import com.navdy.client.app.framework.util.SystemUtils;
import com.navdy.client.app.ui.settings.GeneralSettingsActivity.LimitCellDataEvent;
import com.navdy.client.app.ui.settings.SettingsUtils;
import com.navdy.service.library.events.location.Coordinate;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Subscribe;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicBoolean;

public class BaseGoogleMapFragment extends MapFragment {
    private static final boolean AUTO_CENTER_DEFAULT = false;
    private static final boolean CAR_LOCATION_ENABLED_DEFAULT = true;
    private static final boolean CENTER_MAP_ON_LAST_KNOWN_LOCATION_ENABLED_DEFAULT = true;
    private static final float CENTER_MAP_ZOOM = 16.0f;
    public static final float KEEP_ZOOM = -1.0f;
    private static final boolean MAP_TOOLBAR_ENABLED_DEFAULT = false;
    private static final double MIN_DISTANCE_CENTER_MAP_BOTH = 50.0d;
    private static final boolean MY_LOCATION_BUTTON_ENABLED_DEFAULT = false;
    private static final boolean MY_LOCATION_ENABLED_DEFAULT = true;
    private static final boolean SCROLL_GESTURES_ENABLED_DEFAULT = true;
    private static final boolean TRAFFIC_ENABLED_DEFAULT = true;
    public static final float ZOOM_DEFAULT = 14.0f;
    private static final Logger logger = new Logger(BaseGoogleMapFragment.class);
    private final Queue<OnGoogleMapFragmentAttached> attachCompleteListeners = new LinkedList();
    private boolean autoCenter;
    private final BusProvider bus = BusProvider.getInstance();
    private boolean carLocationEnabled;
    private Marker carMarker;
    private boolean centerMapOnLastKnownLocation;
    AtomicBoolean isAttached = new AtomicBoolean(false);
    private int mapPaddingBottom;
    private int mapPaddingLeft;
    private int mapPaddingRight;
    private int mapPaddingTop;
    private boolean mapToolbarEnabled;
    private boolean myLocationButtonEnabled;
    private boolean myLocationEnabled;
    private final OnNavdyLocationChangedListener navdyLocationListener = new OnNavdyLocationChangedListener() {
        public void onPhoneLocationChanged(@NonNull Coordinate phoneLocation) {
            BaseGoogleMapFragment.this.getMapAsync(new OnMapReadyCallback() {
                public void onMapReady(GoogleMap googleMap) {
                    try {
                        if (BaseGoogleMapFragment.this.autoCenter) {
                            BaseGoogleMapFragment.this.centerOnLastKnownLocation(true);
                        }
                    } catch (Throwable e) {
                        BaseGoogleMapFragment.logger.e(e);
                    }
                }
            });
        }

        public void onCarLocationChanged(@NonNull Coordinate carLocation) {
            BaseGoogleMapFragment.this.updateCarMarker(carLocation);
        }
    };
    private final NavdyLocationManager navdyLocationManager = NavdyLocationManager.getInstance();
    private final Queue<OnGoogleMapFragmentReady> readyCompleteListeners = new LinkedList();
    private boolean scrollGesturesEnabled;
    private boolean trafficEnabled;

    private interface OnGoogleMapFragmentReady {
        void onReady();
    }

    private interface OnGoogleMapFragmentAttached {
        void onAttached();
    }

    public interface OnShowMapListener {
        void onShow();
    }

    static {
        MapsInitializer.initialize(NavdyApplication.getAppContext());
    }

    public void show(@Nullable final OnShowMapListener onShowMapListener) {
        logger.v("show");
        whenReady(new OnGoogleMapFragmentReady() {
            public void onReady() {
                BaseGoogleMapFragment.this.getMapAsync(new OnMapReadyCallback() {
                    public void onMapReady(GoogleMap googleMap) {
                        if (BaseGoogleMapFragment.this.centerMapOnLastKnownLocation) {
                            BaseGoogleMapFragment.this.centerOnLastKnownLocation(false);
                            BaseGoogleMapFragment.this.fadeIn();
                            if (onShowMapListener != null) {
                                onShowMapListener.onShow();
                                return;
                            }
                            return;
                        }
                        BaseGoogleMapFragment.logger.v("do not centerOnLastKnownLocation");
                        BaseGoogleMapFragment.this.fadeIn();
                        if (onShowMapListener != null) {
                            onShowMapListener.onShow();
                        }
                    }
                });
            }
        });
    }

    @MainThread
    public void centerOnLastKnownLocation(boolean animate) {
        SystemUtils.ensureOnMainThread();
        centerMap(animate, this.navdyLocationManager.getPhoneCoordinates(), this.navdyLocationManager.getCarCoordinates());
    }

    @MainThread
    public void moveMap(Location location, float zoom, boolean animate) {
        SystemUtils.ensureOnMainThread();
        moveMap(new LatLng(location.getLatitude(), location.getLongitude()), zoom, animate);
    }

    @MainThread
    public void moveMap(LatLng latLng, float zoom, boolean animate) {
        SystemUtils.ensureOnMainThread();
        moveMapInternal(buildCameraUpdate(latLng, zoom), animate);
    }

    @MainThread
    public void zoomTo(final LatLngBounds latLngBounds, final boolean animate) {
        SystemUtils.ensureOnMainThread();
        getMapAsync(new OnMapReadyCallback() {
            public void onMapReady(GoogleMap googleMap) {
                BaseGoogleMapFragment.logger.v("zoomTo, creating new CameraUpdate, map ready? " + (googleMap != null));
                final View container = BaseGoogleMapFragment.this.getView();
                if (container == null || !container.getViewTreeObserver().isAlive()) {
                    BaseGoogleMapFragment.logger.e("zoomTo, view is null or its viewtreeobserver not alive");
                } else {
                    container.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
                        public void onGlobalLayout() {
                            int height = container.getHeight();
                            int width = container.getWidth();
                            if (height == 0 || width == 0) {
                                BaseGoogleMapFragment.logger.e("zoomTo, googleMap has no dimensions");
                            } else {
                                int padding = 0;
                                int additionalTopDownPadding = (int) (0.15d * ((double) height));
                                boolean additionalTopDownPaddingFitsSides = additionalTopDownPadding < ((width - BaseGoogleMapFragment.this.mapPaddingRight) - BaseGoogleMapFragment.this.mapPaddingLeft) * 2;
                                if ((additionalTopDownPadding < ((height - BaseGoogleMapFragment.this.mapPaddingTop) - BaseGoogleMapFragment.this.mapPaddingBottom) * 2) && additionalTopDownPaddingFitsSides) {
                                    padding = additionalTopDownPadding;
                                } else {
                                    int additionalSidePadding = (int) (0.1d * ((double) width));
                                    boolean additionalSidePaddingFitsSides = additionalSidePadding < ((width - BaseGoogleMapFragment.this.mapPaddingRight) - BaseGoogleMapFragment.this.mapPaddingLeft) * 2;
                                    if ((additionalSidePadding < ((height - BaseGoogleMapFragment.this.mapPaddingTop) - BaseGoogleMapFragment.this.mapPaddingBottom) * 2) && additionalSidePaddingFitsSides) {
                                        padding = additionalSidePadding;
                                    }
                                }
                                BaseGoogleMapFragment.this.moveMapInternal(CameraUpdateFactory.newLatLngBounds(latLngBounds, padding), animate);
                            }
                            container.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        }
                    });
                }
            }
        });
    }

    public void hide() {
        whenAttached(new OnGoogleMapFragmentAttached() {
            public void onAttached() {
                if (BaseGoogleMapFragment.this.isVisible()) {
                    BaseGoogleMapFragment.this.getFragmentManager().beginTransaction().hide(BaseGoogleMapFragment.this).commitAllowingStateLoss();
                    BaseGoogleMapFragment.this.getFragmentManager().executePendingTransactions();
                    return;
                }
                BaseGoogleMapFragment.logger.v("hide, already hidden, no-op");
            }
        });
    }

    @Subscribe
    public void onLimitCellDataEvent(final LimitCellDataEvent event) {
        getMapAsync(new OnMapReadyCallback() {
            public void onMapReady(GoogleMap googleMap) {
                googleMap.setTrafficEnabled(!event.hasLimitedCellData);
            }
        });
    }

    public void onAttach(Context context) {
        super.onAttach(context);
        this.bus.register(this);
        this.isAttached.set(true);
        while (!this.attachCompleteListeners.isEmpty()) {
            ((OnGoogleMapFragmentAttached) this.attachCompleteListeners.poll()).onAttached();
        }
    }

    public void onInflate(Activity activity, AttributeSet attrs, Bundle savedInstanceState) {
        logger.v("onInflate");
        if (GmsUtils.finishIfGmsIsNotUpToDate(activity)) {
            logger.e("GMS is out of date or missing, preventing inflation");
            return;
        }
        super.onInflate(activity, attrs, savedInstanceState);
        TypedArray viewStyles = activity.obtainStyledAttributes(attrs, R.styleable.GoogleMapFragment);
        this.myLocationEnabled = viewStyles.getBoolean(4, true);
        this.myLocationButtonEnabled = viewStyles.getBoolean(5, false);
        this.carLocationEnabled = viewStyles.getBoolean(6, true);
        this.scrollGesturesEnabled = viewStyles.getBoolean(7, true);
        this.centerMapOnLastKnownLocation = viewStyles.getBoolean(8, true);
        this.trafficEnabled = viewStyles.getBoolean(9, true);
        this.mapToolbarEnabled = viewStyles.getBoolean(10, false);
        this.autoCenter = viewStyles.getBoolean(11, false);
        this.mapPaddingTop = viewStyles.getDimensionPixelSize(0, 0);
        this.mapPaddingBottom = viewStyles.getDimensionPixelSize(1, 0);
        this.mapPaddingLeft = viewStyles.getDimensionPixelSize(2, 0);
        this.mapPaddingRight = viewStyles.getDimensionPixelSize(3, 0);
        viewStyles.recycle();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        logger.v("onCreateView");
        getMapAsync(new OnMapReadyCallback() {
            public void onMapReady(final GoogleMap googleMap) {
                try {
                    MapsInitializer.initialize(BaseGoogleMapFragment.this.getContext());
                    UiSettings uiSettings = googleMap.getUiSettings();
                    uiSettings.setScrollGesturesEnabled(BaseGoogleMapFragment.this.scrollGesturesEnabled);
                    uiSettings.setMyLocationButtonEnabled(BaseGoogleMapFragment.this.myLocationButtonEnabled);
                    uiSettings.setMapToolbarEnabled(BaseGoogleMapFragment.this.mapToolbarEnabled);
                    boolean shouldEnableTraffic = BaseGoogleMapFragment.this.trafficEnabled && !SettingsUtils.isLimitingCellularData();
                    googleMap.setTrafficEnabled(shouldEnableTraffic);
                    if (BaseGoogleMapFragment.this.myLocationEnabled) {
                        BaseActivity.requestLocationPermission(new Runnable() {
                            public void run() {
                                googleMap.setMyLocationEnabled(true);
                            }
                        }, null, BaseGoogleMapFragment.this.getActivity());
                    }
                } catch (Throwable e) {
                    BaseGoogleMapFragment.logger.e(e);
                }
            }
        });
        try {
            View view = super.onCreateView(inflater, container, savedInstanceState);
            if (view == null || !view.getViewTreeObserver().isAlive()) {
                logger.e("container view is null or its viewtreeobserver is not alive");
                return view;
            }
            final View finalView = view;
            view.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
                public void onGlobalLayout() {
                    boolean paddingFitsSides;
                    boolean paddingFitsTopDown;
                    int width = finalView.getWidth();
                    int height = finalView.getHeight();
                    if (((double) width) > ((double) (BaseGoogleMapFragment.this.mapPaddingLeft + BaseGoogleMapFragment.this.mapPaddingRight)) * 1.5d) {
                        paddingFitsSides = true;
                    } else {
                        paddingFitsSides = false;
                    }
                    if (((double) height) > ((double) (BaseGoogleMapFragment.this.mapPaddingTop + BaseGoogleMapFragment.this.mapPaddingBottom)) * 1.5d) {
                        paddingFitsTopDown = true;
                    } else {
                        paddingFitsTopDown = false;
                    }
                    if (paddingFitsSides && !paddingFitsTopDown) {
                        BaseGoogleMapFragment.this.mapPaddingTop = 0;
                        BaseGoogleMapFragment.this.mapPaddingBottom = 0;
                    } else if (paddingFitsTopDown && !paddingFitsSides) {
                        BaseGoogleMapFragment.this.mapPaddingRight = 0;
                        BaseGoogleMapFragment.this.mapPaddingLeft = 0;
                    }
                    BaseGoogleMapFragment.this.getMapAsync(new OnMapReadyCallback() {
                        public void onMapReady(GoogleMap googleMap) {
                            googleMap.setPadding(BaseGoogleMapFragment.this.mapPaddingLeft, BaseGoogleMapFragment.this.mapPaddingTop, BaseGoogleMapFragment.this.mapPaddingRight, BaseGoogleMapFragment.this.mapPaddingBottom);
                        }
                    });
                    finalView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                }
            });
            return view;
        } catch (Throwable t) {
            logger.e("could not inflate Google Map Fragment", t);
            return inflater.inflate(R.layout.error_loading_google_maps, container);
        }
    }

    public void onResume() {
        super.onResume();
        while (!this.readyCompleteListeners.isEmpty()) {
            ((OnGoogleMapFragmentReady) this.readyCompleteListeners.poll()).onReady();
        }
        if (this.centerMapOnLastKnownLocation) {
            centerOnLastKnownLocation(false);
        }
        if (this.carLocationEnabled && this.navdyLocationManager.getCarCoordinates() != null) {
            updateCarMarker(this.navdyLocationManager.getCarCoordinates());
        }
        if (this.myLocationEnabled || this.carLocationEnabled) {
            this.navdyLocationManager.addListener(this.navdyLocationListener);
        }
    }

    public void onPause() {
        if (this.myLocationEnabled || this.carLocationEnabled) {
            this.navdyLocationManager.removeListener(this.navdyLocationListener);
        }
        super.onPause();
    }

    public void onDetach() {
        if (this.isAttached.get()) {
            this.bus.unregister(this);
            this.isAttached.set(false);
        }
        super.onDetach();
    }

    public Context getContext() {
        return NavdyApplication.getAppContext();
    }

    @MainThread
    private void whenAttached(OnGoogleMapFragmentAttached attachListener) {
        if (attachListener == null) {
            logger.w("tried to add a null attachListener");
            return;
        }
        SystemUtils.ensureOnMainThread();
        if (isAdded()) {
            attachListener.onAttached();
        } else if (isRemoving()) {
            logger.v("whenAttached, isRemoving, no-op");
        } else {
            this.attachCompleteListeners.add(attachListener);
        }
    }

    @MainThread
    private void whenReady(OnGoogleMapFragmentReady readyListener) {
        if (readyListener == null) {
            logger.w("tried to add a null readyListener");
            return;
        }
        SystemUtils.ensureOnMainThread();
        if (isResumed()) {
            readyListener.onReady();
        } else if (isRemoving()) {
            logger.v("whenAttached, isRemoving, no-op");
        } else {
            this.readyCompleteListeners.add(readyListener);
        }
    }

    private void fadeIn() {
        whenAttached(new OnGoogleMapFragmentAttached() {
            public void onAttached() {
                BaseGoogleMapFragment.this.getFragmentManager().beginTransaction().setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out).show(BaseGoogleMapFragment.this).commitAllowingStateLoss();
            }
        });
    }

    private void centerMap(boolean animate, @Nullable Coordinate phoneLocation, @Nullable Coordinate carLocation) {
        if (MapUtils.isValidSetOfCoordinates(phoneLocation) && MapUtils.isValidSetOfCoordinates(carLocation)) {
            if (MapUtils.distanceBetween(phoneLocation, carLocation) > MIN_DISTANCE_CENTER_MAP_BOTH) {
                Builder builder = new Builder();
                builder.include(new LatLng(phoneLocation.latitude.doubleValue(), phoneLocation.longitude.doubleValue()));
                builder.include(new LatLng(carLocation.latitude.doubleValue(), carLocation.longitude.doubleValue()));
                zoomTo(builder.build(), animate);
                return;
            }
            moveMap(new LatLng(phoneLocation.latitude.doubleValue(), phoneLocation.longitude.doubleValue()), 16.0f, animate);
        } else if (MapUtils.isValidSetOfCoordinates(phoneLocation)) {
            moveMap(new LatLng(phoneLocation.latitude.doubleValue(), phoneLocation.longitude.doubleValue()), 16.0f, animate);
        } else if (MapUtils.isValidSetOfCoordinates(carLocation)) {
            moveMap(new LatLng(carLocation.latitude.doubleValue(), carLocation.longitude.doubleValue()), 16.0f, animate);
        }
    }

    private void moveMapInternal(@NonNull final CameraUpdate cameraUpdate, final boolean animate) {
        getMapAsync(new OnMapReadyCallback() {
            public void onMapReady(GoogleMap googleMap) {
                try {
                    if (animate) {
                        googleMap.animateCamera(cameraUpdate);
                    } else {
                        googleMap.moveCamera(cameraUpdate);
                    }
                } catch (Exception e) {
                    BaseGoogleMapFragment.logger.e("moveMapInternal: Unable to move the map", e);
                }
            }
        });
    }

    private LatLng getTargetSafely(GoogleMap googleMap) {
        try {
            return googleMap.getCameraPosition().target;
        } catch (RuntimeException e) {
            logger.e("Unable to get camera target because: " + e.getCause());
            return null;
        }
    }

    private LatLngBounds getLatLongBoundsSafely(GoogleMap googleMap) {
        try {
            return googleMap.getProjection().getVisibleRegion().latLngBounds;
        } catch (RuntimeException e) {
            logger.e("Unable to get lat long bounds because: " + e.getCause());
            return null;
        }
    }

    private CameraUpdate buildCameraUpdate(@NonNull LatLng latLng, float zoom) {
        CameraPosition.Builder builder = new CameraPosition.Builder().target(latLng);
        if (zoom != -1.0f) {
            builder.zoom(zoom);
        }
        return CameraUpdateFactory.newCameraPosition(builder.build());
    }

    private void updateCarMarker(@NonNull final Coordinate carLocation) {
        if (this.carLocationEnabled) {
            getMapAsync(new OnMapReadyCallback() {
                public void onMapReady(GoogleMap googleMap) {
                    LatLng carLatLng = new LatLng(carLocation.latitude.doubleValue(), carLocation.longitude.doubleValue());
                    if (BaseGoogleMapFragment.this.carMarker != null) {
                        BaseGoogleMapFragment.this.carMarker.remove();
                    }
                    BaseGoogleMapFragment.this.carMarker = googleMap.addMarker(new MarkerOptions().position(carLatLng).title(NavdyApplication.getAppContext().getString(R.string.car_location)).icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_pin_car)));
                    if (BaseGoogleMapFragment.this.autoCenter) {
                        BaseGoogleMapFragment.this.centerOnLastKnownLocation(true);
                    }
                }
            });
        }
    }
}
