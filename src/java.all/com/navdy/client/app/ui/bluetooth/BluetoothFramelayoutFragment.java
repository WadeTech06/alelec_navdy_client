package com.navdy.client.app.ui.bluetooth;

import android.app.Activity;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import androidx.appcompat.app.AlertDialog.Builder;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.TextView;
import com.alelec.navdyclient.R;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.AppInstance;
import com.navdy.client.app.framework.DeviceConnection.DeviceConnectedEvent;
import com.navdy.client.app.framework.DeviceConnection.DeviceConnectionFailedEvent;
import com.navdy.client.app.framework.LocalyticsManager;
import com.navdy.client.app.tracking.TrackerConstants.Attributes;
import com.navdy.client.app.tracking.TrackerConstants.Event;
import com.navdy.client.app.ui.base.BaseActivity;
import com.navdy.client.app.ui.base.BaseDialogFragment;
import com.navdy.client.debug.devicepicker.Device;
import com.navdy.service.library.device.RemoteDeviceRegistry;
import com.navdy.service.library.device.RemoteDeviceRegistry.DeviceListUpdatedListener;
import com.navdy.service.library.device.connection.ConnectionInfo;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Subscribe;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

public class BluetoothFramelayoutFragment extends BaseDialogFragment implements OnClickListener {
    public static final String EXTRA_KILL_ACTIVITY_ON_CANCEL = "1";
    public static final String EXTRA_USER_WAS_IN_FIRST_LAUNCH_EXPERIENCE = "USER_WAS_IN_FIRST_LAUNCH";
    private static final int TIMEOUT = 15000;
    public Context context;
    private int currentLayout;
    private String deviceName = "";
    private LayoutInflater inflater;
    private boolean killActivityOnCancel;
    protected AppInstance mAppInstance;
    protected BluetoothAdapter mBluetoothAdapter;
    protected DeviceListUpdatedListener mDeviceListUpdateListener;
    protected ArrayList<Device> mDevices;
    private RecyclerView mRecyclerView;
    protected RemoteDeviceRegistry mRemoteDeviceRegistry;
    private boolean pairing = false;
    private int position;
    private BluetoothDialogRecyclerAdapter recyclerAdapter;
    private FrameLayout rootView;
    private SwipeRefreshLayout swipeContainer;
    private boolean userWasInFirstLaunch = false;

    private enum BluetoothState {
        NO_BLUETOOTH,
        DISABLED,
        ENABLED
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        this.context = NavdyApplication.getAppContext();
        setupBluetooth(this.context);
        Intent intent = getActivity().getIntent();
        if (intent != null) {
            this.killActivityOnCancel = intent.getBooleanExtra(EXTRA_KILL_ACTIVITY_ON_CANCEL, false);
            this.userWasInFirstLaunch = intent.getBooleanExtra(EXTRA_USER_WAS_IN_FIRST_LAUNCH_EXPERIENCE, false);
        }
    }

    public void onDismiss(DialogInterface dialog) {
        this.logger.v("onDismiss");
        if (getFragmentManager() != null) {
            if (isAlive() && isInForeground()) {
                getFragmentManager().beginTransaction().remove(this).commitAllowingStateLoss();
            }
            if (this.killActivityOnCancel) {
                Activity activity = getActivity();
                if (!BaseActivity.isEnding(activity)) {
                    activity.finish();
                }
            }
        }
    }

    public synchronized Dialog onCreateDialog(Bundle savedInstanceState) {
        Builder builder;
        this.logger.v("onCreateDialog");
        builder = new Builder(getActivity());
        this.inflater = getActivity().getLayoutInflater();
        this.rootView = (FrameLayout) this.inflater.inflate(R.layout.fl_fragment_bluetooth_dialog_framelayout, null);
        builder.setView(this.rootView);
        selectDialogBasedOnState();
        return builder.create();
    }

    public synchronized void setupBluetooth(Context context) {
        this.logger.v("setupBluetooth called");
        this.mDevices = new ArrayList();
        this.logger.v("mDevices: " + System.identityHashCode(this.mDevices));
        this.mRemoteDeviceRegistry = RemoteDeviceRegistry.getInstance(context);
        this.mAppInstance = AppInstance.getInstance();
        addConnectionInfo(this.mRemoteDeviceRegistry.getKnownConnectionInfo());
        this.mDeviceListUpdateListener = new DeviceListUpdatedListener() {
            public void onDeviceListChanged(Set<ConnectionInfo> deviceList) {
                BluetoothFramelayoutFragment.this.addConnectionInfo(deviceList);
                BluetoothFramelayoutFragment.this.logger.v("DeviceListChanged. Here is the current deviceList: " + BluetoothFramelayoutFragment.this.mDevices);
                if (!(BluetoothFramelayoutFragment.this.currentLayout == R.layout.fl_fragment_bluetooth_dialog_pairing || BluetoothFramelayoutFragment.this.currentLayout == R.layout.fl_fragment_bluetooth_dialog_searching_found)) {
                    BluetoothFramelayoutFragment.this.showDialog(R.layout.fl_fragment_bluetooth_dialog_searching_found);
                }
                if (BluetoothFramelayoutFragment.this.mRecyclerView != null) {
                    BluetoothFramelayoutFragment.this.updateRecyclerView(BluetoothFramelayoutFragment.this.mDevices);
                }
            }
        };
    }

    public synchronized void showDialog(int res) {
        if (this.rootView != null) {
            this.logger.v("showDialog called on res: " + getResources().getResourceEntryName(res));
            if (this.currentLayout != res) {
                if (this.currentLayout != 0) {
                    this.logger.v("savedLayout: " + this.currentLayout);
                    this.rootView.removeAllViews();
                }
                this.currentLayout = res;
                LayoutParams layoutParams = new LayoutParams(-2, -2);
                if (this.inflater == null && getActivity() != null) {
                    this.inflater = getActivity().getLayoutInflater();
                }
                if (this.inflater != null) {
                    this.rootView.addView(this.inflater.inflate(res, this.rootView, false), layoutParams);
                }
                setUpCancelButton();
                switch (res) {
                    case R.layout.fl_fragment_bluetooth_dialog_connect_failed /*2130903130*/:
                        ((Button) this.rootView.findViewById(R.id.retry_pairing_button)).setOnClickListener(new OnClickListener() {
                            public void onClick(View view) {
                                BluetoothFramelayoutFragment.this.retryPairing(view);
                            }
                        });
                        break;
                    case R.layout.fl_fragment_bluetooth_dialog_not_compatible /*2130903133*/:
                        ((Button) this.rootView.findViewById(R.id.go_to_bluetooth_settings_button)).setOnClickListener(new OnClickListener() {
                            public void onClick(View view) {
                                BluetoothFramelayoutFragment.this.goToSettings();
                            }
                        });
                        break;
                    case R.layout.fl_fragment_bluetooth_dialog_pairing /*2130903134*/:
                        ((TextView) this.rootView.findViewById(R.id.device_name)).setText(this.deviceName);
                        break;
                    case R.layout.fl_fragment_bluetooth_dialog_searching_found /*2130903136*/:
                        setupRecyclerView();
                        break;
                    case R.layout.fl_fragment_bluetooth_dialog_searching_not_found /*2130903138*/:
                        this.swipeContainer = (SwipeRefreshLayout) this.rootView.findViewById(R.id.swipe_container);
                        this.swipeContainer.setColorSchemeResources(R.color.blue, R.color.green_success, R.color.yellow, R.color.purple_warm);
                        this.swipeContainer.setOnRefreshListener(new OnRefreshListener() {
                            public void onRefresh() {
                                BluetoothFramelayoutFragment.this.stopRefresh();
                                BluetoothFramelayoutFragment.this.rootView.removeAllViews();
                                BluetoothFramelayoutFragment.this.selectDialogBasedOnState();
                            }
                        });
                        break;
                }
            }
        }
    }

    public synchronized void setUpCancelButton() {
        if (this.rootView != null) {
            Button cancelButton = (Button) this.rootView.findViewById(R.id.cancel_button);
            if (cancelButton != null) {
                if (this.killActivityOnCancel) {
                    cancelButton.setOnClickListener(new OnClickListener() {
                        public void onClick(View view) {
                            BluetoothFramelayoutFragment.this.getActivity().finish();
                        }
                    });
                } else {
                    cancelButton.setOnClickListener(new OnClickListener() {
                        public void onClick(View view) {
                            BluetoothFramelayoutFragment.this.getFragmentManager().popBackStack();
                        }
                    });
                }
            }
        }
    }

    public synchronized void setDeviceName(String prettyName) {
        this.deviceName = prettyName;
    }

    public synchronized void selectDialogBasedOnState() {
        switch (getBluetoothState()) {
            case NO_BLUETOOTH:
            case DISABLED:
                showDialog(R.layout.fl_fragment_bluetooth_dialog_not_compatible);
                break;
            case ENABLED:
                if (!this.pairing) {
                    showDialog(R.layout.fl_fragment_bluetooth_dialog_searching);
                    if (this.mRemoteDeviceRegistry != null) {
                        this.mDevices.clear();
                        this.logger.v("selectDialogBasedOnState(): mRemoteDeviceRegistry.startScanning()");
                        this.mRemoteDeviceRegistry.stopScanning();
                        this.mRemoteDeviceRegistry.startScanning();
                    }
                    if (this.mRecyclerView != null) {
                        this.logger.v("RecyclerView was not null");
                        updateRecyclerView(this.mDevices);
                    }
                    this.handler.postDelayed(new Runnable() {
                        public void run() {
                            if (BluetoothFramelayoutFragment.this.isAlive()) {
                                BluetoothFramelayoutFragment.this.logger.v("entered postDelayed");
                                BluetoothFramelayoutFragment.this.logger.v("mDevices size: " + BluetoothFramelayoutFragment.this.mDevices.size());
                                if (BluetoothFramelayoutFragment.this.mDevices.isEmpty()) {
                                    BluetoothFramelayoutFragment.this.logger.v("mDevices was empty");
                                    BluetoothFramelayoutFragment.this.showDialog(R.layout.fl_fragment_bluetooth_dialog_searching_not_found);
                                    return;
                                }
                                if (!(BluetoothFramelayoutFragment.this.currentLayout == R.layout.fl_fragment_bluetooth_dialog_searching_found || BluetoothFramelayoutFragment.this.currentLayout == R.layout.fl_fragment_bluetooth_dialog_pairing)) {
                                    BluetoothFramelayoutFragment.this.showDialog(R.layout.fl_fragment_bluetooth_dialog_searching_found);
                                }
                                BluetoothFramelayoutFragment.this.updateRecyclerView(BluetoothFramelayoutFragment.this.mDevices);
                            }
                        }
                    }, 15000);
                    break;
                }
                showDialog(R.layout.fl_fragment_bluetooth_dialog_pairing);
                break;
        }
    }

    public synchronized BluetoothState getBluetoothState() {
        BluetoothState bluetoothState;
        if (this.mBluetoothAdapter == null) {
            bluetoothState = BluetoothState.NO_BLUETOOTH;
        } else if (this.mBluetoothAdapter.isEnabled()) {
            bluetoothState = BluetoothState.ENABLED;
        } else {
            bluetoothState = BluetoothState.DISABLED;
        }
        return bluetoothState;
    }

    public synchronized void stopRefresh() {
        this.logger.v("swipeContainer: stopRefresh");
        this.swipeContainer.setRefreshing(false);
    }

    public synchronized void onResume() {
        super.onResume();
        this.logger.v("added listener to mRemoteDeviceRegistry");
        this.mRemoteDeviceRegistry.addListener(this.mDeviceListUpdateListener);
        selectDialogBasedOnState();
    }

    public synchronized void onPause() {
        if (this.mRemoteDeviceRegistry != null) {
            this.mRemoteDeviceRegistry.stopScanning();
            this.logger.v("removing mRemoteDeviceRegistry listener");
            this.mRemoteDeviceRegistry.removeListener(this.mDeviceListUpdateListener);
        }
        super.onPause();
    }

    public synchronized void setupRecyclerView() {
        this.logger.v("setupRecyclerView()");
        if (this.rootView != null) {
            this.mRecyclerView = (RecyclerView) this.rootView.findViewById(R.id.bluetooth_recycler_view);
        }
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(1);
        if (this.mRecyclerView != null) {
            this.mRecyclerView.setLayoutManager(layoutManager);
        }
        this.recyclerAdapter = new BluetoothDialogRecyclerAdapter();
        this.recyclerAdapter.setClickListener(this);
        if (this.mRecyclerView != null) {
            this.mRecyclerView.setAdapter(this.recyclerAdapter);
        }
        if (this.rootView != null) {
            this.swipeContainer = (SwipeRefreshLayout) this.rootView.findViewById(R.id.swipe_container);
            this.swipeContainer.setColorSchemeResources(R.color.blue, R.color.green_success, R.color.yellow, R.color.purple_warm);
            this.swipeContainer.setOnRefreshListener(new OnRefreshListener() {
                public void onRefresh() {
                    BluetoothFramelayoutFragment.this.logger.v("swipeContainer: refresh called");
                    BluetoothFramelayoutFragment.this.selectDialogBasedOnState();
                }
            });
        }
    }

    public synchronized void updateRecyclerView(ArrayList<Device> devices) {
        this.logger.v("updateRecyclerView");
        Iterator it = devices.iterator();
        while (it.hasNext()) {
            Device device = (Device) it.next();
            if (!this.recyclerAdapter.contains(device.getPrettyName())) {
                this.recyclerAdapter.addDevice(device);
                this.recyclerAdapter.notifyDataSetChanged();
            }
        }
    }

    protected synchronized void addConnectionInfo(ConnectionInfo connectionInfo, boolean updateIfFound) {
        Device newDevice = new Device(connectionInfo);
        int index = this.mDevices.indexOf(newDevice);
        if (index != -1) {
            ((Device) this.mDevices.get(index)).add(connectionInfo, updateIfFound);
        } else {
            this.mDevices.add(newDevice);
            this.logger.v("Added a device" + newDevice.getPrettyName());
        }
    }

    protected synchronized void addConnectionInfo(Set<ConnectionInfo> connectionInfoSet) {
        this.logger.v("mDevices: add " + System.identityHashCode(this.mDevices));
        for (ConnectionInfo connectionInfo : connectionInfoSet) {
            addConnectionInfo(connectionInfo, true);
        }
    }

    public synchronized void selectOption(int position) {
        if (this.mDevices == null || position >= this.mDevices.size() || position < 0) {
            String str;
            Logger logger = this.logger;
            StringBuilder append = new StringBuilder().append("Can't selectOption position is ").append(position).append(" and mDevice ");
            if (this.mDevices != null) {
                str = "only has " + this.mDevices.size() + " entries";
            } else {
                str = "is null";
            }
            logger.e(append.append(str).toString());
        } else {
            this.mRemoteDeviceRegistry.stopScanning();
            this.logger.v("mRemoteDeviceRegistry.stopScanning");
            this.logger.v("mDevices: selectOption " + System.identityHashCode(this.mDevices));
            this.logger.v("mDevices size during selectOption: " + this.mDevices.size());
            Device pickedDevice = (Device) this.mDevices.get(position);
            setDeviceName(pickedDevice.getPrettyName());
            this.pairing = true;
            showDialog(R.layout.fl_fragment_bluetooth_dialog_pairing);
            this.mRemoteDeviceRegistry.setDefaultConnectionInfo(pickedDevice.getPreferredConnectionInfo());
            this.mAppInstance.initializeDevice();
        }
    }

    @Subscribe
    public void appInstanceDeviceConnectionEvent(DeviceConnectedEvent deviceConnectedEvent) {
        this.logger.v("appInstanceDeviceConnectionEvent");
        pairingComplete(true);
        finishBluetoothPairActivity();
    }

    @Subscribe
    public void appInstanceDeviceConnectionFailedEvent(DeviceConnectionFailedEvent deviceConnectionFailedEvent) {
        this.logger.e("appInstanceDeviceConnectionFailedEvent: " + deviceConnectionFailedEvent);
        pairingComplete(false);
        showDialog(R.layout.fl_fragment_bluetooth_dialog_connect_failed);
    }

    private void pairingComplete(boolean connected) {
        this.pairing = false;
        HashMap<String, String> pairPhoneToDisplayValues = new HashMap();
        pairPhoneToDisplayValues.put(Attributes.SUCCESS_ATTRIBUTE, Boolean.toString(connected));
        pairPhoneToDisplayValues.put("First_Launch", Boolean.toString(this.userWasInFirstLaunch));
        LocalyticsManager.tagEvent(Event.PAIR_PHONE_TO_DISPLAY, pairPhoneToDisplayValues);
    }

    public synchronized void onClick(View v) {
        int position = this.mRecyclerView.getChildAdapterPosition(v);
        this.logger.v("onItemClick called. position is " + position);
        this.position = position;
        selectOption(position);
    }

    public synchronized void finishBluetoothPairActivity() {
        this.logger.v("finishBluetoothPairActivity");
        Activity currentActivity = getActivity();
        if (currentActivity != null) {
            this.logger.v("currentActivity.finish(): " + currentActivity);
            currentActivity.finish();
        }
    }

    public synchronized void goToSettings() {
        this.logger.v("go to bluetooth settings");
        Intent intentOpenBluetoothSettings = new Intent();
        intentOpenBluetoothSettings.setAction("android.settings.BLUETOOTH_SETTINGS");
        startActivity(intentOpenBluetoothSettings);
    }

    public synchronized void retryPairing(View view) {
        selectOption(this.position);
    }
}
