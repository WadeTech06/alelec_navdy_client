package com.navdy.client.app.ui.firstlaunch;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.EditText;
import android.widget.Spinner;
import com.alelec.navdyclient.R;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.LocalyticsManager;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.tracking.Tracker;
import com.navdy.client.app.tracking.TrackerConstants.Attributes;
import com.navdy.client.app.tracking.TrackerConstants.Event;
import com.navdy.client.app.tracking.TrackerConstants.Screen.FirstLaunch;
import com.navdy.client.app.ui.base.BaseActivity;
import com.navdy.client.app.ui.base.BaseToolbarActivity;
import com.navdy.client.app.ui.base.BaseToolbarActivity.ToolbarBuilder;
import com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences;
import com.navdy.client.app.ui.settings.SettingsUtils;
import com.navdy.service.library.task.TaskManager;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import org.droidparts.adapter.widget.StringSpinnerAdapter;

public class EditCarInfoActivity extends BaseToolbarActivity {
    static final String APP_SETUP = "app_setup";
    static final String EXTRA_NEXT_STEP = "extra_next_step";
    static final String INSTALL = "installation_flow";
    private static final int OTHER_DIALOG_ID = 1;
    private static final String STATE_SELECTOR_MODE = "state_selector_mode";
    private boolean ignoreTheNextMakeSelection = false;
    private boolean ignoreTheNextModelSelection = false;
    private boolean ignoreTheNextYearSelection = false;
    private boolean isInSelectorMode = true;
    private Spinner make;
    final OnItemSelectedListener makeSelectionListener = new OnItemSelectedListener() {
        public void onItemSelected(AdapterView<?> adapterView, View view, int selectedMakePosition, long id) {
            handleSelection(selectedMakePosition);
        }

        public void onNothingSelected(AdapterView<?> adapterView) {
            handleSelection(-1);
        }

        private void handleSelection(int selectedMakePosition) {
            if (EditCarInfoActivity.this.ignoreTheNextMakeSelection) {
                EditCarInfoActivity.this.ignoreTheNextMakeSelection = false;
                return;
            }
            EditCarInfoActivity.this.logger.d("onItemSelected selectedMakePosition: " + selectedMakePosition);
            if (selectedMakePosition >= EditCarInfoActivity.this.make.getCount() - 1) {
                EditCarInfoActivity.this.showDialogAboutOther();
                return;
            }
            EditCarInfoActivity.this.makeString = (String) EditCarInfoActivity.this.make.getSelectedItem();
            EditCarInfoActivity.this.updateSpinnersWithSelection();
        }
    };
    private String makeString;
    private EditText makeTv;
    private Spinner model;
    final OnItemSelectedListener modelSelectionListener = new OnItemSelectedListener() {
        public void onItemSelected(AdapterView<?> adapterView, View view, int selectedModelPosition, long id) {
            handleSelection(selectedModelPosition);
        }

        public void onNothingSelected(AdapterView<?> adapterView) {
            handleSelection(-1);
        }

        private void handleSelection(int selectedModelPosition) {
            if (EditCarInfoActivity.this.ignoreTheNextModelSelection) {
                EditCarInfoActivity.this.ignoreTheNextModelSelection = false;
                return;
            }
            EditCarInfoActivity.this.logger.d("onItemSelected selectedModelPosition: " + selectedModelPosition);
            if (selectedModelPosition >= EditCarInfoActivity.this.model.getCount() - 1) {
                EditCarInfoActivity.this.showDialogAboutOther();
                return;
            }
            EditCarInfoActivity.this.modelString = (String) EditCarInfoActivity.this.model.getSelectedItem();
            EditCarInfoActivity.this.updateSpinnersWithSelection();
        }
    };
    private String modelString;
    private EditText modelTv;
    private Spinner year;
    private HashMap<String, HashMap<String, HashMap<String, ObdLocation>>> yearMap = new HashMap();
    final OnItemSelectedListener yearSelectionListener = new OnItemSelectedListener() {
        public void onItemSelected(AdapterView<?> adapterView, View view, int selectedYearPosition, long id) {
            handleSelection(selectedYearPosition);
        }

        public void onNothingSelected(AdapterView<?> adapterView) {
            handleSelection(-1);
        }

        private void handleSelection(int selectedYearPosition) {
            if (EditCarInfoActivity.this.ignoreTheNextYearSelection) {
                EditCarInfoActivity.this.ignoreTheNextYearSelection = false;
                return;
            }
            EditCarInfoActivity.this.logger.d("onItemSelected selectedYearPosition: " + selectedYearPosition);
            if (selectedYearPosition >= EditCarInfoActivity.this.year.getCount() - 1) {
                EditCarInfoActivity.this.showDialogAboutOther();
                return;
            }
            EditCarInfoActivity.this.yearString = (String) EditCarInfoActivity.this.year.getSelectedItem();
            EditCarInfoActivity.this.updateSpinnersWithSelection();
        }
    };
    private String yearString;
    private EditText yearTv;

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        boolean z = true;
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.fle_vehicle_car_md_info);
        new ToolbarBuilder().title((int) R.string.select_your_car).build();
        if (!CarMdUtils.hasCachedCarList()) {
            showProgressDialog();
        }
        if (savedInstanceState != null) {
            String yr = savedInstanceState.getString(ProfilePreferences.CAR_YEAR, this.yearString);
            if (!StringUtils.isEmptyAfterTrim(yr)) {
                this.yearString = yr;
            }
            String ma = savedInstanceState.getString(ProfilePreferences.CAR_MAKE, this.makeString);
            if (!StringUtils.isEmptyAfterTrim(ma)) {
                this.makeString = ma;
            }
            String mo = savedInstanceState.getString(ProfilePreferences.CAR_MODEL, this.modelString);
            if (!StringUtils.isEmptyAfterTrim(mo)) {
                this.modelString = mo;
            }
            if (savedInstanceState.getInt(STATE_SELECTOR_MODE, 1) != 1) {
                z = false;
            }
            this.isInSelectorMode = z;
        }
        new AsyncTask<Void, Void, Void>() {
            protected void onPreExecute() {
                EditCarInfoActivity.this.loadImage(R.id.illustration, R.drawable.image_car_info);
            }

            protected Void doInBackground(Void... params) {
                EditCarInfoActivity.this.yearMap = CarMdUtils.buildCarList();
                return null;
            }

            protected void onPostExecute(Void aVoid) {
                EditCarInfoActivity.this.hideProgressDialog();
                if (EditCarInfoActivity.this.isInSelectorMode) {
                    EditCarInfoActivity.this.initCarSelectorScreen();
                } else {
                    EditCarInfoActivity.this.showManualEntry(null);
                }
            }
        }.execute(new Void[0]);
    }

    public void onSaveInstanceState(Bundle outState) {
        int i;
        super.onSaveInstanceState(outState);
        if (this.isInSelectorMode) {
            if (this.year == null || this.make == null || this.model == null) {
                this.logger.e("Missing layout element.");
                return;
            }
            int yearInt = this.year.getSelectedItemPosition();
            int makeInt = this.make.getSelectedItemPosition();
            int modelInt = this.model.getSelectedItemPosition();
            if (yearInt != 0) {
                this.yearString = this.year.getSelectedItem().toString();
            }
            if (makeInt != 0) {
                this.makeString = this.make.getSelectedItem().toString();
            }
            if (modelInt != 0) {
                this.modelString = this.model.getSelectedItem().toString();
            }
        } else if (this.yearTv == null || this.makeTv == null || this.modelTv == null) {
            this.logger.e("Missing layout element.");
            return;
        } else {
            this.yearString = this.yearTv.getText().toString();
            this.makeString = this.makeTv.getText().toString();
            this.modelString = this.modelTv.getText().toString();
        }
        outState.putString(ProfilePreferences.CAR_YEAR, this.yearString);
        outState.putString(ProfilePreferences.CAR_MAKE, this.makeString);
        outState.putString(ProfilePreferences.CAR_MODEL, this.modelString);
        String str = STATE_SELECTOR_MODE;
        if (this.isInSelectorMode) {
            i = 1;
        } else {
            i = 0;
        }
        outState.putInt(str, i);
    }

    protected void onResume() {
        super.onResume();
        Tracker.tagScreen(FirstLaunch.EDIT_CAR_INFO);
    }

    public void showManualEntry(View view) {
        setContentView((int) R.layout.fle_vehicle_car_info);
        loadImage(R.id.illustration, R.drawable.image_car_info);
        initManualEntryScreen();
    }

    public void showCarSelector(View view) {
        setContentView((int) R.layout.fle_vehicle_car_md_info);
        loadImage(R.id.illustration, R.drawable.image_car_info);
        initCarSelectorScreen();
    }

    private void initCarSelectorScreen() {
        this.year = (Spinner) findViewById(R.id.pick_a_year);
        this.make = (Spinner) findViewById(R.id.pick_a_make);
        this.model = (Spinner) findViewById(R.id.pick_a_model);
        if (this.year == null || this.make == null || this.model == null) {
            this.logger.e("Missing layout element.");
            return;
        }
        if (StringUtils.isEmptyAfterTrim(this.yearString) && StringUtils.isEmptyAfterTrim(this.makeString) && StringUtils.isEmptyAfterTrim(this.modelString)) {
            SharedPreferences customerPrefs = SettingsUtils.getCustomerPreferences();
            this.yearString = customerPrefs.getString(ProfilePreferences.CAR_YEAR, "");
            this.makeString = customerPrefs.getString(ProfilePreferences.CAR_MAKE, "");
            this.modelString = customerPrefs.getString(ProfilePreferences.CAR_MODEL, "");
        }
        updateSpinnersWithSelection();
        this.year.setOnItemSelectedListener(this.yearSelectionListener);
        this.make.setOnItemSelectedListener(this.makeSelectionListener);
        this.model.setOnItemSelectedListener(this.modelSelectionListener);
        this.isInSelectorMode = true;
    }

    private void updateSpinnersWithSelection() {
        HashMap<String, HashMap<String, ObdLocation>> makeMap;
        this.ignoreTheNextYearSelection = true;
        this.ignoreTheNextMakeSelection = true;
        this.ignoreTheNextModelSelection = true;
        Context context = NavdyApplication.getAppContext();
        ArrayList<String> yearList = new ArrayList();
        yearList.add(context.getString(R.string.settings_profile_year_label));
        ArrayList<String> arrayList = new ArrayList(this.yearMap.keySet());
        Collections.sort(arrayList, Collections.reverseOrder());
        yearList.addAll(arrayList);
        yearList.add(context.getString(R.string.other));
        this.year.setAdapter(new StringSpinnerAdapter(this.year, (List) yearList));
        this.logger.d("setting year list to: " + yearList);
        int yearSelection = yearList.indexOf(this.yearString);
        if (yearSelection >= 0) {
            this.year.setSelection(yearSelection);
        }
        List makeList = new ArrayList();
        makeList.add(context.getString(R.string.settings_profile_make_label));
        if (!StringUtils.isEmptyAfterTrim(this.yearString)) {
            makeMap = (HashMap) this.yearMap.get(this.yearString);
            if (makeMap != null) {
                ArrayList<String> makesSorted = new ArrayList(makeMap.keySet());
                Collections.sort(makesSorted);
                makeList.addAll(makesSorted);
            }
        }
        makeList.add(context.getString(R.string.other));
        this.make.setAdapter(new StringSpinnerAdapter(this.make, makeList));
        this.logger.d("setting make list to: " + makeList);
        int makeSelection = makeList.indexOf(this.makeString);
        if (makeSelection >= 0) {
            this.make.setSelection(makeSelection);
        }
        List modelList = new ArrayList();
        modelList.add(context.getString(R.string.settings_profile_model_label));
        if (!(StringUtils.isEmptyAfterTrim(this.yearString) || StringUtils.isEmptyAfterTrim(this.makeString))) {
            makeMap = (HashMap) this.yearMap.get(this.yearString);
            if (makeMap != null) {
                HashMap<String, ObdLocation> modelMap = (HashMap) makeMap.get(this.makeString);
                if (modelMap != null) {
                    ArrayList<String> modelsSorted = new ArrayList(modelMap.keySet());
                    Collections.sort(modelsSorted);
                    modelList.addAll(modelsSorted);
                }
            }
        }
        modelList.add(context.getString(R.string.other));
        this.model.setAdapter(new StringSpinnerAdapter(this.model, modelList));
        this.logger.d("setting model list to: " + modelList);
        int modelSelection = modelList.indexOf(this.modelString);
        if (modelSelection >= 0) {
            this.model.setSelection(modelSelection);
        }
    }

    public void onSetCarMdInfoClick(View view) {
        if (this.year == null || this.make == null || this.model == null) {
            this.logger.e("Missing layout element.");
            return;
        }
        SharedPreferences customerPrefs = SettingsUtils.getCustomerPreferences();
        int yearInt = this.year.getSelectedItemPosition();
        int makeInt = this.make.getSelectedItemPosition();
        int modelInt = this.model.getSelectedItemPosition();
        String yearString = this.year.getSelectedItem().toString();
        String makeString = this.make.getSelectedItem().toString();
        String modelString = this.model.getSelectedItem().toString();
        if (yearInt == 0 || yearInt >= this.year.getCount() - 1 || makeInt == 0 || makeInt >= this.make.getCount() - 1 || modelInt == 0 || modelInt >= this.model.getCount() - 1 || StringUtils.isEmptyAfterTrim(yearString) || StringUtils.isEmptyAfterTrim(makeString) || StringUtils.isEmptyAfterTrim(modelString)) {
            BaseActivity.showLongToast(R.string.please_enter_car_info, new Object[0]);
        } else {
            saveCarInfoAndDownloadObdInfo(customerPrefs, yearString, makeString, modelString, false);
        }
    }

    public void saveCarInfoAndDownloadObdInfo(SharedPreferences customerPrefs, final String yearString, final String makeString, final String modelString, boolean comesFromManualEntry) {
        showProgressDialog();
        boolean isInFle = StringUtils.equalsOrBothEmptyAfterTrim(getIntent().getStringExtra(EXTRA_NEXT_STEP), INSTALL);
        Tracker.resetPhotoAndLocation(customerPrefs);
        saveCarInfo(customerPrefs, yearString, makeString, modelString, comesFromManualEntry, isInFle);
        saveObdInfo(customerPrefs, CarMdUtils.getObdLocation(yearString, makeString, modelString, this.yearMap));
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                if (SettingsUtils.isUsingDefaultObdScanSetting()) {
                    SettingsUtils.setDefaultObdSettingDependingOnBlacklistAsync();
                }
            }
        }, 1);
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                CarMdUtils.downloadObdCrimePhoto(yearString, makeString, modelString, new Runnable() {
                    public void run() {
                        EditCarInfoActivity.this.goToNextStep();
                    }
                });
            }
        }, 3);
    }

    public void onBackClick(View view) {
        finish();
    }

    private void goToNextStep() {
        final String nextStep = getIntent().getStringExtra(EXTRA_NEXT_STEP);
        runOnUiThread(new Runnable() {
            public void run() {
                EditCarInfoActivity.this.hideProgressDialog();
                if (StringUtils.equalsOrBothEmptyAfterTrim(nextStep, EditCarInfoActivity.INSTALL)) {
                    EditCarInfoActivity.this.startActivity(new Intent(EditCarInfoActivity.this.getApplicationContext(), CheckMountActivity.class));
                } else if (StringUtils.equalsOrBothEmptyAfterTrim(nextStep, EditCarInfoActivity.APP_SETUP)) {
                    AppSetupActivity.goToAppSetup(EditCarInfoActivity.this);
                } else {
                    Intent intent = new Intent(EditCarInfoActivity.this.getApplicationContext(), CarInfoActivity.class);
                    intent.putExtra(EditCarInfoActivity.EXTRA_NEXT_STEP, nextStep);
                    EditCarInfoActivity.this.startActivity(intent);
                    EditCarInfoActivity.this.finish();
                }
            }
        });
    }

    private void showDialogAboutOther() {
        showSimpleDialog(1, getString(R.string.manual_entry), getString(R.string.manual_entry_dialog_description));
        showManualEntry(null);
    }

    private void initManualEntryScreen() {
        this.yearTv = (EditText) findViewById(R.id.enter_year);
        this.makeTv = (EditText) findViewById(R.id.enter_make);
        this.modelTv = (EditText) findViewById(R.id.enter_model);
        if (this.yearTv == null || this.makeTv == null || this.modelTv == null) {
            this.logger.e("Missing layout element.");
            return;
        }
        if (StringUtils.isEmptyAfterTrim(this.yearString) && StringUtils.isEmptyAfterTrim(this.makeString) && StringUtils.isEmptyAfterTrim(this.modelString)) {
            SharedPreferences customerPrefs = SettingsUtils.getCustomerPreferences();
            this.yearString = customerPrefs.getString(ProfilePreferences.CAR_YEAR, "");
            this.makeString = customerPrefs.getString(ProfilePreferences.CAR_MAKE, "");
            this.modelString = customerPrefs.getString(ProfilePreferences.CAR_MODEL, "");
        }
        this.yearTv.setText(this.yearString);
        this.makeTv.setText(this.makeString);
        this.modelTv.setText(this.modelString);
        this.isInSelectorMode = false;
    }

    public void onSetCarInfoClick(View view) {
        EditText year = (EditText) findViewById(R.id.enter_year);
        EditText make = (EditText) findViewById(R.id.enter_make);
        EditText model = (EditText) findViewById(R.id.enter_model);
        if (year == null || make == null || model == null) {
            this.logger.e("Missing layout element.");
            return;
        }
        String yearString = year.getText().toString();
        String makeString = make.getText().toString();
        String modelString = model.getText().toString();
        if (fieldsAreValid(yearString, makeString, modelString)) {
            saveCarInfoAndDownloadObdInfo(SettingsUtils.getCustomerPreferences(), yearString, makeString, modelString, true);
            return;
        }
        BaseActivity.showLongToast(R.string.please_enter_car_info, new Object[0]);
    }

    public boolean fieldsAreValid(String yearString, String makeString, String modelString) {
        return (StringUtils.isEmptyAfterTrim(yearString) || !yearString.matches("[12][0-9]{3}") || StringUtils.isEmptyAfterTrim(makeString) || StringUtils.isEmptyAfterTrim(modelString)) ? false : true;
    }

    public static void saveCarInfo(@NonNull SharedPreferences customerPrefs, @NonNull String yearString, @NonNull String makeString, @NonNull String modelString, boolean comesFromManualEntry, boolean isInFle) {
        String oldYearString = customerPrefs.getString(ProfilePreferences.CAR_YEAR, "");
        String oldMakeString = customerPrefs.getString(ProfilePreferences.CAR_MAKE, "");
        String oldModelString = customerPrefs.getString(ProfilePreferences.CAR_MODEL, "");
        if (!(StringUtils.equalsOrBothEmptyAfterTrim(oldYearString, yearString) && StringUtils.equalsOrBothEmptyAfterTrim(oldMakeString, makeString) && StringUtils.equalsOrBothEmptyAfterTrim(oldModelString, modelString))) {
            sLogger.v("Resetting car md data.");
            Tracker.resetPhotoAndLocation(customerPrefs);
            HashMap<String, String> attributes = new HashMap(3);
            attributes.put(Attributes.CAR_YEAR, yearString);
            attributes.put(Attributes.CAR_MAKE, makeString);
            attributes.put(Attributes.CAR_MODEL, modelString);
            attributes.put(Attributes.DURING_FLE, isInFle ? "True" : "False");
            Tracker.tagEvent(Event.CAR_INFO_CHANGED, attributes);
        }
        long serial = customerPrefs.getLong(ProfilePreferences.SERIAL_NUM, ProfilePreferences.SERIAL_NUM_DEFAULT.longValue());
        yearString = yearString.trim();
        makeString = makeString.trim();
        modelString = modelString.trim();
        customerPrefs.edit().putString(ProfilePreferences.CAR_YEAR, yearString).putString(ProfilePreferences.CAR_MAKE, makeString).putString(ProfilePreferences.CAR_MODEL, modelString).putBoolean(ProfilePreferences.CAR_MANUAL_ENTRY, comesFromManualEntry).putLong(ProfilePreferences.SERIAL_NUM, 1 + serial).apply();
        LocalyticsManager.setProfileAttribute(Attributes.CAR_YEAR, yearString);
        LocalyticsManager.setProfileAttribute(Attributes.CAR_MAKE, makeString);
        LocalyticsManager.setProfileAttribute(Attributes.CAR_MODEL, modelString);
        LocalyticsManager.setProfileAttribute(Attributes.CAR_MANUAL_ENTRY, Boolean.toString(comesFromManualEntry));
    }

    public static void saveObdInfo(@NonNull SharedPreferences customerPrefs, @Nullable ObdLocation obdLocation) {
        if (obdLocation == null) {
            Tracker.resetPhotoAndLocation(customerPrefs);
        } else {
            customerPrefs.edit().putString(ProfilePreferences.CAR_OBD_LOCATION_NOTE, obdLocation.note).putString(ProfilePreferences.CAR_OBD_LOCATION_ACCESS_NOTE, obdLocation.accessNote).putInt(ProfilePreferences.CAR_OBD_LOCATION_NUM, obdLocation.location).apply();
        }
    }
}
