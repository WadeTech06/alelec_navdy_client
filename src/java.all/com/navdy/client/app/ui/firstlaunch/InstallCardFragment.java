package com.navdy.client.app.ui.firstlaunch;

import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.alelec.navdyclient.R;
import com.navdy.client.app.framework.models.MountInfo.MountType;
import com.navdy.client.app.framework.util.ImageCache;
import com.navdy.client.app.framework.util.ImageUtils;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.tracking.TrackerConstants.Screen.FirstLaunch.Install;
import com.navdy.client.app.ui.base.BaseSupportFragment;
import com.navdy.client.app.ui.settings.SettingsConstants;
import com.navdy.client.app.ui.settings.SettingsUtils;

public class InstallCardFragment extends BaseSupportFragment {
    public static final String LAYOUT_ID = "layoutId";
    private int layoutId;
    public MountType mountType;
    protected View rootView;

    public void setMountType(MountType type) {
        this.mountType = type;
    }

    public void setLayoutId(int layoutId) {
        this.layoutId = layoutId;
    }

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        boolean showNewShortMountAssets = false;
        if (savedInstanceState != null) {
            this.layoutId = savedInstanceState.getInt(LAYOUT_ID);
        }
        ImageCache imageCache = null;
        InstallActivity activity = (InstallActivity) getActivity();
        if (activity != null) {
            imageCache = activity.imageCache;
        }
        this.rootView = inflater.inflate(this.layoutId, container, false);
        ImageView imageView = (ImageView) this.rootView.findViewById(R.id.illustration);
        if (imageView != null) {
            String box = SettingsUtils.getSharedPreferences().getString(SettingsConstants.BOX, "Old_Box");
            if (!StringUtils.equalsOrBothEmptyAfterTrim(box, "Old_Box")) {
                showNewShortMountAssets = true;
            }
            switch (this.layoutId) {
                case R.layout.fle_install_dial /*2130903150*/:
                    ImageUtils.loadImage(imageView, R.drawable.image_fle_installation_dial, imageCache);
                    break;
                case R.layout.fle_install_overview /*2130903156*/:
                    int overview_asset = R.drawable.asset_install_2016_short_pov;
                    if (this.mountType != MountType.SHORT) {
                        overview_asset = R.drawable.asset_install_medium_pov;
                    } else if (showNewShortMountAssets) {
                        overview_asset = R.drawable.asset_install_2017_short_pov;
                    }
                    ImageUtils.loadImage(imageView, overview_asset, imageCache);
                    if (StringUtils.equalsOrBothEmptyAfterTrim(box, "New_Box_Plus_Mounts")) {
                        TextView checkText2 = (TextView) this.rootView.findViewById(R.id.check_text_2);
                        if (checkText2 != null) {
                            checkText2.setText(R.string.whats_in_the_mount_kit_box);
                            break;
                        }
                    }
                    break;
                case R.layout.fle_install_secure_mount /*2130903157*/:
                    int secureAsset = R.drawable.installation_image_secure_2016;
                    if (showNewShortMountAssets) {
                        secureAsset = R.drawable.installation_image_secure_2017;
                    }
                    ImageUtils.loadImage(imageView, secureAsset, imageCache);
                    break;
                case R.layout.fle_install_tidying_up /*2130903159*/:
                    ImageUtils.loadImage(imageView, R.drawable.image_fle_installation_tidying, imageCache);
                    break;
                case R.layout.fle_install_turn_on /*2130903160*/:
                    ImageUtils.loadImage(imageView, R.drawable.image_fle_installation_power, imageCache);
                    break;
            }
        }
        return this.rootView;
    }

    public void onSaveInstanceState(Bundle outState) {
        outState.putInt(LAYOUT_ID, this.layoutId);
        super.onSaveInstanceState(outState);
    }

    public String getScreen() {
        switch (this.layoutId) {
            case R.layout.fle_install_dial /*2130903150*/:
                return Install.DIAL;
            case R.layout.fle_install_lens_check /*2130903152*/:
                return Install.LENS_POSITION;
            case R.layout.fle_install_locate_obd /*2130903153*/:
                return Install.PLUG_OBD;
            case R.layout.fle_install_mounts /*2130903155*/:
                return Install.SHORT_MOUNT;
            case R.layout.fle_install_overview /*2130903156*/:
                return Install.OVERVIEW;
            case R.layout.fle_install_secure_mount /*2130903157*/:
                return Install.SECURE;
            case R.layout.fle_install_tidying_up /*2130903159*/:
                return Install.TIDY_UP;
            case R.layout.fle_install_turn_on /*2130903160*/:
                return Install.POWER_ON;
            default:
                return null;
        }
    }
}
