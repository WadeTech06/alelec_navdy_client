package com.navdy.client.app.ui.firstlaunch;

import android.content.Context;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.alelec.navdyclient.R;
import com.navdy.client.app.framework.util.ImageCache;
import com.navdy.client.app.ui.ImageResourcePagerAdaper;

public class MarketingFlowImagePagerAdaper extends ImageResourcePagerAdaper {
    public MarketingFlowImagePagerAdaper(Context context, int[] images, ImageCache imageCache) {
        super(context, images, imageCache);
    }

    public int getCount() {
        return this.imageResourceIds.length + 1;
    }

    public Object instantiateItem(ViewGroup container, int position) {
        ImageView imageView = (ImageView) this.mLayoutInflater.inflate(R.layout.fle_marketing_flow_bg_image, container, false);
        if (imageView == null) {
            return null;
        }
        if (position >= this.imageResourceIds.length) {
            imageView.setBackgroundColor(0);
        } else {
            imageView.setImageResource(this.imageResourceIds[position]);
        }
        container.addView(imageView);
        return imageView;
    }
}
