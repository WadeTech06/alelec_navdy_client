package com.navdy.client.app.ui.firstlaunch;

import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.alelec.navdyclient.R;
import com.navdy.client.app.framework.util.ImageCache;
import com.navdy.client.app.framework.util.ImageUtils;

public class InstallLocateObdFragment extends InstallCardFragment {
    public InstallLocateObdFragment() {
        setLayoutId(R.layout.fle_install_locate_obd);
    }

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ImageCache imageCache = null;
        InstallActivity activity = (InstallActivity) getActivity();
        if (activity != null) {
            imageCache = activity.imageCache;
        }
        this.rootView = inflater.inflate(R.layout.fle_install_locate_obd, container, false);
        ImageUtils.loadImage((ImageView) this.rootView.findViewById(R.id.illustration), R.drawable.image_fle_installation_locating, imageCache);
        return this.rootView;
    }
}
