package com.navdy.client.app.framework.navigation;

import androidx.annotation.NonNull;
import com.google.android.gms.maps.model.LatLng;
import com.navdy.client.app.framework.DeviceConnection;
import com.navdy.client.app.framework.map.MapUtils;
import com.navdy.client.app.framework.models.Destination;
import com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteInfo;
import com.navdy.client.debug.navigation.HUDNavigationManager;
import com.navdy.client.debug.navigation.NavigationManager;
import com.navdy.service.library.events.destination.Destination.FavoriteType;
import com.navdy.service.library.events.location.Coordinate;
import com.navdy.service.library.events.navigation.GetNavigationSessionState;
import com.navdy.service.library.events.navigation.NavigationRouteCancelRequest;
import com.navdy.service.library.events.navigation.NavigationSessionRequest;
import com.navdy.service.library.events.navigation.NavigationSessionState;
import com.navdy.service.library.log.Logger;

class NavigationHelper {
    private static final Logger logger = new Logger(NavigationHelper.class);
    private final NavigationManager hudNavigationManager = new HUDNavigationManager();

    NavigationHelper() {
    }

    synchronized void navigateToDestination(@NonNull Destination destination, String requestId, boolean initiatedOnHud) {
        logger.v("navigateToDestination: " + destination.rawAddressNotForDisplay);
        boolean geocodeStreetAddressOnHud = !destination.hasOneValidSetOfCoordinates();
        if (geocodeStreetAddressOnHud) {
            logger.i("No valid coordinates found. Requesting for HUD to geocode the street address for Navigation: " + destination.rawAddressNotForDisplay);
        }
        searchRoutesOnHud(requestId, destination.getNameForDisplay(), destination.rawAddressNotForDisplay, new LatLng(destination.navigationLat, destination.navigationLong), String.valueOf(destination.id), destination.getFavoriteTypeForProto(), MapUtils.buildNewCoordinate(destination.displayLat, destination.displayLong), geocodeStreetAddressOnHud, initiatedOnHud, destination.toProtobufDestinationObject());
    }

    void cancelRouteCalculation(@NonNull String cancelHandle) {
        if (DeviceConnection.postEvent(new NavigationRouteCancelRequest(cancelHandle))) {
            logger.v("cancelRouteCalculation sent successfully.");
        } else {
            logger.w("cancelRouteCalculation failed to send.");
        }
    }

    void cancelRoute(@NonNull NavdyRouteInfo route) {
        sendStateChangeRequest(NavigationSessionState.NAV_SESSION_STOPPED, route);
    }

    void getNavigationSessionState() {
        if (DeviceConnection.postEvent(new GetNavigationSessionState())) {
            logger.v("getNavigationSessionState sent successfully");
        } else {
            logger.w("getNavigationSessionState failed to send");
        }
    }

    private void searchRoutesOnHud(String requestId, String name, String address, LatLng navigationCoordinate, String destinationId, FavoriteType favoriteType, Coordinate display, boolean geocodeAddressOnHud, boolean initiatedOnHud, com.navdy.service.library.events.destination.Destination destination) {
        Coordinate coordinate = new Coordinate(Double.valueOf(navigationCoordinate.latitude), Double.valueOf(navigationCoordinate.longitude), Float.valueOf(0.0f), Double.valueOf(0.0d), Float.valueOf(0.0f), Float.valueOf(0.0f), Long.valueOf(0), "Google");
        logger.v("=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=");
        logger.v("Requesting new route for " + destination);
        logger.v("sending route to HUD:" + name + " address: " + address + " destinationId: " + destinationId + " favoriteType:" + favoriteType + " coordinate:" + coordinate + " display: " + display + " streetAddress:" + address + " useStreetAddress: " + geocodeAddressOnHud + " initiatedOnHud: " + initiatedOnHud);
        logger.v("=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=");
        this.hudNavigationManager.startRouteRequest(coordinate, name, null, address, destinationId, favoriteType, display, requestId, geocodeAddressOnHud, initiatedOnHud, destination);
    }

    private void sendStateChangeRequest(NavigationSessionState newState, NavdyRouteInfo route) {
        sendStateChangeRequest(newState, route, 0);
    }

    private void sendStateChangeRequest(NavigationSessionState newState, NavdyRouteInfo route, int simulationSpeed) {
        logger.d("Attempting to change state: " + newState);
        if (newState == null) {
            logger.e("sendStateChangeRequest failed because new NavigationSessionState is null");
        } else if (!DeviceConnection.isConnected()) {
            logger.v("Device is no longer valid. Creating new Device Connection");
        } else if (route == null) {
            logger.w("Didn't get any route as chosen");
        } else {
            if (DeviceConnection.postEvent(new NavigationSessionRequest(newState, route.getDestination().name, route.routeId, Integer.valueOf(simulationSpeed), Boolean.valueOf(false)))) {
                logger.v("State change request sent successfully.");
            } else {
                logger.w("State change request failed to send.");
            }
        }
    }
}
