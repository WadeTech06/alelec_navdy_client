package com.navdy.client.app.framework.navigation;

import android.content.Intent;
import android.content.SharedPreferences.Editor;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings.Secure;
import android.provider.Settings.SettingNotFoundException;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.common.GeoPolyline;
import com.here.android.mpa.common.OnEngineInitListener;
import com.here.android.mpa.routing.Route;
import com.here.android.mpa.routing.Route.TrafficPenaltyMode;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.AppInstance;
import com.navdy.client.app.framework.DeviceConnection;
import com.navdy.client.app.framework.DeviceConnection.DeviceConnectedEvent;
import com.navdy.client.app.framework.DeviceConnection.DeviceDisconnectedEvent;
import com.navdy.client.app.framework.location.NavdyLocationManager;
import com.navdy.client.app.framework.map.HereMapsManager;
import com.navdy.client.app.framework.map.MapUtils;
import com.navdy.client.app.framework.map.NavCoordsAddressProcessor;
import com.navdy.client.app.framework.map.NavCoordsAddressProcessor.OnCompleteCallback;
import com.navdy.client.app.framework.models.Destination;
import com.navdy.client.app.framework.navigation.HereRouteManager.Listener;
import com.navdy.client.app.framework.navigation.HereRouteManager.RouteHandle;
import com.navdy.client.app.framework.servicehandler.DisplayNavigationServiceHandler.ArrivedTripEvent;
import com.navdy.client.app.framework.servicehandler.DisplayNavigationServiceHandler.DisplayInitiatedRequestEvent;
import com.navdy.client.app.framework.servicehandler.DisplayNavigationServiceHandler.HUDReadyEvent;
import com.navdy.client.app.framework.servicehandler.DisplayNavigationServiceHandler.RerouteActiveTripEvent;
import com.navdy.client.app.framework.servicehandler.DisplayNavigationServiceHandler.RouteCalculationProgress;
import com.navdy.client.app.framework.servicehandler.DisplayNavigationServiceHandler.RoutesCalculatedEvent;
import com.navdy.client.app.framework.servicehandler.DisplayNavigationServiceHandler.RoutesCalculationErrorEvent;
import com.navdy.client.app.framework.servicehandler.DisplayNavigationServiceHandler.StartActiveTripEvent;
import com.navdy.client.app.framework.servicehandler.DisplayNavigationServiceHandler.StopActiveTripEvent;
import com.navdy.client.app.framework.util.BusProvider;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.framework.util.SuggestionManager;
import com.navdy.client.app.ui.homescreen.CalendarUtils;
import com.navdy.client.app.ui.settings.GeneralSettingsActivity.LimitCellDataEvent;
import com.navdy.client.app.ui.settings.LocationDialogActivity;
import com.navdy.client.app.ui.settings.SettingsUtils;
import com.navdy.service.library.events.TripUpdate;
import com.navdy.service.library.events.location.LatLong;
import com.navdy.service.library.events.navigation.NavigationRouteResult;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Subscribe;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;
import java.util.UUID;

public final class NavdyRouteHandler {
    private static final String CACHED_ARRIVED_DESTINATION = "cached_arrived_destination";
    public static final int INVALID_DISTANCE = -1;
    public static final int INVALID_DURATION = -1;
    private static final long PENDING_ROUTE_REFRESH = 9000000;
    private static final String SAVED_DESTINATION = "saved_destination";
    private static final String SHARED_PREF_SAVED_ROUTE = "saved_route";
    private static final boolean VERBOSE = true;
    private static final NavdyRouteHandler instance = new NavdyRouteHandler();
    private static final Logger logger = new Logger(NavdyRouteHandler.class);
    private final AppInstance appInstance = AppInstance.getInstance();
    private Error currentError;
    private NavdyRouteInfo currentRoute;
    private State currentState;
    private final Handler handler = new Handler(Looper.getMainLooper());
    private Destination lastDestinationArrived = getLastDestinationArrived();
    private final List<WeakReference<NavdyRouteListener>> listeners = new ArrayList();
    private final NavigationHelper navigationHelper = new NavigationHelper();
    private RouteHandle pendingRouteHandle;
    private final Runnable refreshPendingRoute = new Runnable() {
        public void run() {
            if (NavdyRouteHandler.this.currentState == State.PENDING_ROUTE && !SettingsUtils.isLimitingCellularData()) {
                NavdyRouteHandler.this.calculatePendingRoute(NavdyRouteHandler.this.currentRoute.destination);
            }
        }
    };
    private boolean requestedHighAccuracy;

    private interface CallListener {
        void call(NavdyRouteListener navdyRouteListener);
    }

    public enum Error {
        NONE,
        NO_ROUTES
    }

    public static class NavdyRouteInfo {
        public static final double HEAVY_TRAFFIC_THRESHOLD = 1.25d;
        @Nullable
        private String cancelHandle;
        @NonNull
        private final Destination destination;
        private int distanceToDestination;
        public final boolean isTrafficHeavy;
        @Nullable
        private GeoPolyline progress;
        @Nullable
        final String requestId;
        @Nullable
        private final GeoPolyline route;
        @Nullable
        final String routeId;
        private int timeToDestination;
        @Nullable
        public final String via;

//        /* synthetic */ NavdyRouteInfo(NavdyRouteInfo x0, AnonymousClass1 x1) {
//            this(x0);
//        }

        NavdyRouteInfo(@NonNull Destination destination, @Nullable String via, @Nullable GeoPolyline route, @Nullable String requestId, @Nullable String routeId, int duration, int durationWithTraffic, int distanceToDestination) {
            boolean z = false;
            this.via = via;
            this.requestId = requestId;
            this.routeId = routeId;
            this.destination = destination;
            this.route = route;
            this.distanceToDestination = distanceToDestination;
            if (durationWithTraffic != -1) {
                this.timeToDestination = durationWithTraffic;
                if (((double) durationWithTraffic) / ((double) duration) >= 1.25d) {
                    z = true;
                }
                this.isTrafficHeavy = z;
                return;
            }
            this.timeToDestination = duration;
            this.isTrafficHeavy = false;
        }

        private NavdyRouteInfo(NavdyRouteInfo navdyRouteInfo) {
            this.via = navdyRouteInfo.via;
            this.requestId = navdyRouteInfo.requestId;
            this.routeId = navdyRouteInfo.routeId;
            this.isTrafficHeavy = navdyRouteInfo.isTrafficHeavy;
            this.destination = navdyRouteInfo.destination;
            this.route = navdyRouteInfo.route;
            this.progress = navdyRouteInfo.progress;
            this.cancelHandle = navdyRouteInfo.cancelHandle;
            this.timeToDestination = navdyRouteInfo.timeToDestination;
            this.distanceToDestination = navdyRouteInfo.distanceToDestination;
        }

        @NonNull
        public Destination getDestination() {
            return this.destination;
        }

        @Nullable
        public GeoPolyline getRoute() {
            return this.route;
        }

        @Nullable
        public GeoPolyline getProgress() {
            return this.progress;
        }

        public void setHandle(String cancelHandle) {
            this.cancelHandle = cancelHandle;
        }

        public int getTimeToDestination() {
            return this.timeToDestination;
        }

        public int getDistanceToDestination() {
            return this.distanceToDestination;
        }

        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            NavdyRouteInfo that = (NavdyRouteInfo) o;
            if (this.isTrafficHeavy == that.isTrafficHeavy && this.timeToDestination == that.timeToDestination && this.distanceToDestination == that.distanceToDestination && this.via != null) {
                return this.via.equals(that.via);
            }
            if (that.via == null && this.requestId != null) {
                return this.requestId.equals(that.requestId);
            }
            if (that.requestId == null && this.routeId != null) {
                return this.routeId.equals(that.routeId);
            }
            if (that.routeId == null && this.destination.equals(that.destination) && this.route != null) {
                return this.route.equals(that.route);
            }
            if (that.route == null && this.progress != null) {
                return this.progress.equals(that.progress);
            }
            if (that.progress == null && this.cancelHandle != null) {
                return this.cancelHandle.equals(that.cancelHandle);
            }
            if (that.cancelHandle != null) {
                return false;
            }
            return true;
        }

        public int hashCode() {
            int result;
            int hashCode;
            int i = 0;
            if (this.via != null) {
                result = this.via.hashCode();
            } else {
                result = 0;
            }
            int i2 = result * 31;
            if (this.requestId != null) {
                hashCode = this.requestId.hashCode();
            } else {
                hashCode = 0;
            }
            i2 = (i2 + hashCode) * 31;
            if (this.routeId != null) {
                hashCode = this.routeId.hashCode();
            } else {
                hashCode = 0;
            }
            i2 = (i2 + hashCode) * 31;
            if (this.isTrafficHeavy) {
                hashCode = 1;
            } else {
                hashCode = 0;
            }
            i2 = (((i2 + hashCode) * 31) + this.destination.hashCode()) * 31;
            if (this.route != null) {
                hashCode = this.route.hashCode();
            } else {
                hashCode = 0;
            }
            i2 = (i2 + hashCode) * 31;
            if (this.progress != null) {
                hashCode = this.progress.hashCode();
            } else {
                hashCode = 0;
            }
            hashCode = (i2 + hashCode) * 31;
            if (this.cancelHandle != null) {
                i = this.cancelHandle.hashCode();
            }
            return ((((hashCode + i) * 31) + this.timeToDestination) * 31) + this.distanceToDestination;
        }
    }

    public interface NavdyRouteListener {
        void onPendingRouteCalculated(@NonNull Error error, @NonNull NavdyRouteInfo navdyRouteInfo);

        void onPendingRouteCalculating(@NonNull Destination destination);

        void onReroute();

        void onRouteArrived(@NonNull Destination destination);

        void onRouteCalculated(@NonNull Error error, @NonNull NavdyRouteInfo navdyRouteInfo);

        void onRouteCalculating(@NonNull Destination destination);

        void onRouteStarted(@NonNull NavdyRouteInfo navdyRouteInfo);

        void onStopRoute();

        void onTripProgress(@NonNull NavdyRouteInfo navdyRouteInfo);
    }

    public enum State {
        INACTIVE,
        CALCULATING_PENDING_ROUTE,
        PENDING_ROUTE,
        CALCULATING_ROUTES,
        ROUTE_CALCULATED,
        EN_ROUTE
    }

    public static NavdyRouteHandler getInstance() {
        return instance;
    }

    @Subscribe
    public void onTripUpdate(TripUpdate tripUpdate) {
        if (this.currentState == State.EN_ROUTE) {
            logger.v("Received trip update: " + tripUpdate);
            if (HereMapsManager.getInstance().isInitialized()) {
                LatLong currentLatLng = tripUpdate.current_position;
                if (currentLatLng != null) {
                    int tta = -1;
                    int distance = -1;
                    if (tripUpdate.estimated_time_remaining != null) {
                        tta = tripUpdate.estimated_time_remaining.intValue();
                    }
                    if (tripUpdate.distance_to_destination != null) {
                        distance = tripUpdate.distance_to_destination.intValue();
                    }
                    updateCurrentProgress(new GeoCoordinate(currentLatLng.latitude.doubleValue(), currentLatLng.longitude.doubleValue()), tta, distance);
                    return;
                }
                return;
            }
            logger.e("Here Map Engine not initialized yet.");
        }
    }

    private NavdyRouteHandler() {
        logger.v("init, setting state to INACTIVE");
        this.currentState = State.INACTIVE;
        this.currentError = Error.NONE;
        BusProvider.getInstance().register(this);
        Destination pendingDestination = getPendingTrip();
        if (pendingDestination != null) {
            requestNewRoute(pendingDestination);
        }
    }

    @Nullable
    public Destination getCurrentDestination() {
        if (this.currentRoute != null) {
            return this.currentRoute.destination;
        }
        return null;
    }

    public boolean isInOneOfThePendingTripStates() {
        return this.currentState == State.CALCULATING_PENDING_ROUTE || this.currentState == State.PENDING_ROUTE;
    }

    public boolean isInOneOfTheActiveTripStates() {
        return this.currentState == State.CALCULATING_ROUTES || this.currentState == State.ROUTE_CALCULATED || this.currentState == State.EN_ROUTE;
    }

    public void addListener(@NonNull NavdyRouteListener navdyRouteListener) {
        this.listeners.add(new WeakReference(navdyRouteListener));
        switch (this.currentState) {
            case INACTIVE:
                if (this.lastDestinationArrived != null) {
                    navdyRouteListener.onRouteArrived(this.lastDestinationArrived);
                }
                navdyRouteListener.onStopRoute();
                return;
            case CALCULATING_PENDING_ROUTE:
                navdyRouteListener.onPendingRouteCalculating(this.currentRoute.destination);
                return;
            case PENDING_ROUTE:
                navdyRouteListener.onPendingRouteCalculated(this.currentError, this.currentRoute);
                return;
            case CALCULATING_ROUTES:
                navdyRouteListener.onRouteCalculating(this.currentRoute.destination);
                return;
            case ROUTE_CALCULATED:
                navdyRouteListener.onRouteCalculated(this.currentError, this.currentRoute);
                return;
            case EN_ROUTE:
                navdyRouteListener.onRouteStarted(this.currentRoute);
                if (this.currentRoute.progress != null && this.currentRoute.progress.length() > 1.0d) {
                    navdyRouteListener.onTripProgress(this.currentRoute);
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void removeListener(NavdyRouteListener navdyRouteListener) {
        ListIterator<WeakReference<NavdyRouteListener>> iterator = this.listeners.listIterator();
        while (iterator.hasNext()) {
            NavdyRouteListener listener = (NavdyRouteListener) ((WeakReference) iterator.next()).get();
            if (listener == null || listener.equals(navdyRouteListener)) {
                iterator.remove();
            }
        }
    }

    public State getCurrentState() {
        return this.currentState;
    }

    public void requestNewRoute(Destination destination) {
        requestNewRoute(destination, false);
    }

    public void requestNewRoute(final Destination destination, final boolean initiatedOnHud) {
        if (destination == null) {
            logger.w("attempted to call requestNewRoute with no destination, no-op");
            return;
        }
        if (!(initiatedOnHud || this.requestedHighAccuracy)) {
            try {
                if (Secure.getInt(NavdyApplication.getAppContext().getContentResolver(), "location_mode") != 3) {
                    this.requestedHighAccuracy = true;
                    Intent intent = new Intent(NavdyApplication.getAppContext(), LocationDialogActivity.class);
                    intent.addFlags(268435456);
                    intent.putExtra(LocationDialogActivity.PENDING_DESTINATION, destination);
                    NavdyApplication.getAppContext().startActivity(intent);
                    return;
                }
            } catch (SettingNotFoundException e) {
                logger.e("location setting not found: " + e);
            }
        }
        this.handler.removeCallbacks(this.refreshPendingRoute);
        destination.refreshPlaceIdDataAndUpdateListsAsync(new Runnable() {
            public void run() {
                HereMapsManager.getInstance().addOnInitializedListener(new OnEngineInitListener() {
                    public void onEngineInitializationCompleted(com.here.android.mpa.common.OnEngineInitListener.Error error) {
                        NavdyRouteHandler.this.currentError = Error.NONE;
                        if (DeviceConnection.isConnected() && NavdyRouteHandler.this.appInstance.isHudMapEngineReady()) {
                            NavdyRouteHandler.this.removeLastDestinationArrived();
                            NavdyRouteHandler.this.removePendingTrip();
                            NavdyRouteHandler.logger.v("requestNewRoute, setting state to CALCULATING_ROUTES");
                            NavdyRouteHandler.this.currentState = State.CALCULATING_ROUTES;
                            final String newRequestId = UUID.randomUUID().toString();
                            NavdyRouteHandler.this.currentRoute = new NavdyRouteInfo(destination, null, null, newRequestId, null, -1, -1, -1);
                            NavCoordsAddressProcessor.processDestination(destination, new OnCompleteCallback() {
                                public void onSuccess(Destination destination) {
                                    prepareNavigation(destination, newRequestId, initiatedOnHud);
                                }

                                public void onFailure(Destination destination, com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Error error) {
                                    if (error != com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Error.NONE) {
                                        NavdyRouteHandler.logger.w("error while calling processDestination before navigating to it");
                                    }
                                    prepareNavigation(destination, newRequestId, initiatedOnHud);
                                }

                                private void prepareNavigation(Destination destination, String newRequestId, boolean initiatedOnHud) {
                                    if (NavdyRouteHandler.this.currentState != State.CALCULATING_ROUTES) {
                                        NavdyRouteHandler.logger.i("processDestination complete, but state is not CALCULATING_ROUTES anymore, no-op");
                                    } else if (Destination.equals(destination, NavdyRouteHandler.this.currentRoute.destination)) {
                                        NavdyRouteHandler.this.navigationHelper.navigateToDestination(destination, newRequestId, initiatedOnHud);
                                    } else {
                                        NavdyRouteHandler.logger.i("processDestination complete, but current destinations are not the same, no-op");
                                    }
                                }
                            });
                            NavdyRouteHandler.this.callListeners(new CallListener() {
                                public void call(NavdyRouteListener listener) {
                                    listener.onRouteCalculating(destination);
                                }
                            });
                            return;
                        }
                        NavdyRouteHandler.this.setPendingTrip(destination);
                        NavdyRouteHandler.logger.v("requestNewRoute, setting state to CALCULATING_PENDING_ROUTE");
                        NavdyRouteHandler.this.currentState = State.CALCULATING_PENDING_ROUTE;
                        NavdyRouteHandler.this.currentRoute = new NavdyRouteInfo(destination, null, null, null, null, -1, -1, -1);
                        NavdyRouteHandler.this.callListeners(new CallListener() {
                            public void call(NavdyRouteListener listener) {
                                listener.onPendingRouteCalculating(NavdyRouteHandler.this.currentRoute.destination);
                            }
                        });
                        NavdyRouteHandler.this.calculatePendingRoute(destination);
                    }
                });
            }
        });
    }

    public void retryRoute() {
        if (this.currentRoute == null) {
            logger.w("retryRoute but currentRoute is null, no-op");
        } else {
            requestNewRoute(this.currentRoute.destination);
        }
    }

    public void stopRouting() {
        if (this.currentState == State.INACTIVE) {
            logger.w("called stopRouting when state is INACTIVE, no-op");
            return;
        }
        switch (this.currentState) {
            case CALCULATING_PENDING_ROUTE:
                if (this.pendingRouteHandle != null) {
                    this.pendingRouteHandle.cancel();
                }
                this.pendingRouteHandle = null;
                break;
            case PENDING_ROUTE:
                this.handler.removeCallbacks(this.refreshPendingRoute);
                break;
            case CALCULATING_ROUTES:
                if (this.currentRoute.cancelHandle == null) {
                    logger.w("stopRouting, cancel handle can't be null");
                } else {
                    this.navigationHelper.cancelRouteCalculation(this.currentRoute.cancelHandle);
                }
                this.navigationHelper.cancelRoute(this.currentRoute);
                break;
            case ROUTE_CALCULATED:
            case EN_ROUTE:
                this.navigationHelper.cancelRoute(this.currentRoute);
                break;
        }
        removePendingTrip();
        setStoppedState();
        callListeners(new CallListener() {
            public void call(NavdyRouteListener listener) {
                listener.onStopRoute();
            }
        });
    }

    @Subscribe
    public void onHUDConnected(DeviceConnectedEvent event) {
        logger.v("HUD is connected");
        if (this.currentState == State.INACTIVE) {
            this.navigationHelper.getNavigationSessionState();
        } else if (this.currentState == State.PENDING_ROUTE) {
            this.handler.removeCallbacks(this.refreshPendingRoute);
        }
    }

    @Subscribe
    public void onHUDDisconnected(DeviceDisconnectedEvent event) {
        logger.v("HUD is disconnected");
        if (this.currentState != State.INACTIVE) {
            stopRouting();
        }
    }

    @Subscribe
    public void onDisplayInitiatedRequestEvent(DisplayInitiatedRequestEvent event) {
        if (this.currentState == State.CALCULATING_ROUTES || this.currentState == State.CALCULATING_PENDING_ROUTE || this.currentState == State.PENDING_ROUTE) {
            logger.i("onDisplayInitiatedRequestEvent but we have a pending route on the phone");
            return;
        }
        event.destination.updateLastRoutedDateInDb();
        event.destination.refreshPlaceIdDataAndUpdateListsAsync();
        removeLastDestinationArrived();
        removePendingTrip();
        logger.v("onDisplayInitiatedRequestEvent, setting state to CALCULATING_ROUTES");
        this.currentState = State.CALCULATING_ROUTES;
        this.currentError = Error.NONE;
        this.currentRoute = new NavdyRouteInfo(event.destination, null, null, event.requestId, null, -1, -1, -1);
        callListeners(new CallListener() {
            public void call(NavdyRouteListener listener) {
                listener.onRouteCalculating(NavdyRouteHandler.this.currentRoute.destination);
            }
        });
    }

    @Subscribe
    public void onRouteSubmitProgress(RouteCalculationProgress event) {
        if (this.currentState != State.CALCULATING_ROUTES) {
            logger.w("onRouteSubmitProgress, state is not CALCULATING_ROUTES, no-op");
        } else if (this.currentRoute.requestId != null && !this.currentRoute.requestId.equals(event.requestId)) {
            logger.w("onRouteSubmitProgress, requestIds do not match, no-op");
        } else if (this.currentRoute.cancelHandle == null) {
            NavdyRouteInfo updatedRoute = new NavdyRouteInfo(this.currentRoute, null);
            updatedRoute.setHandle(event.handle);
            this.currentRoute = updatedRoute;
        }
    }

    @Subscribe
    public void onRoutesCalculatedEvent(RoutesCalculatedEvent event) {
        if (this.currentState != State.CALCULATING_ROUTES) {
            logger.w("onRoutesCalculatedEvent, state is not CALCULATING_ROUTES, no-op");
        } else if (this.currentRoute.requestId == null || this.currentRoute.requestId.equals(event.requestId)) {
            Destination currentDestination = this.currentRoute.destination;
            String cancelHandle = this.currentRoute.cancelHandle;
            logger.v("onRoutesCalculatedEvent, setting state to ROUTE_CALCULATED");
            this.currentState = State.ROUTE_CALCULATED;
            this.currentError = Error.NONE;
            this.currentRoute = null;
            for (NavigationRouteResult navigationRouteResult : event.routes) {
                NavdyRouteInfo route = new NavdyRouteInfo(currentDestination, navigationRouteResult.via, new GeoPolyline(getRouteGeo(navigationRouteResult.routeLatLongs)), event.requestId, navigationRouteResult.routeId, navigationRouteResult.duration.intValue(), navigationRouteResult.duration_traffic.intValue(), navigationRouteResult.length.intValue());
                route.setHandle(cancelHandle);
                if (this.currentRoute == null) {
                    this.currentRoute = route;
                }
            }
            callListeners(new CallListener() {
                public void call(NavdyRouteListener listener) {
                    listener.onRouteCalculated(NavdyRouteHandler.this.currentError, NavdyRouteHandler.this.currentRoute);
                }
            });
        } else {
            logger.w("onRoutesCalculatedEvent, requestIds are not the same, no-op");
        }
    }

    @Subscribe
    public void onRoutesCalculationErrorEvent(RoutesCalculationErrorEvent event) {
        if (this.currentState != State.CALCULATING_ROUTES) {
            logger.w("onRoutesCalculationErrorEvent, state is not CALCULATING_ROUTES, no-op");
            return;
        }
        logger.v("onRoutesCalculationErrorEvent, setting state to CALCULATION_FAILED");
        this.currentState = State.ROUTE_CALCULATED;
        this.currentError = Error.NO_ROUTES;
        callListeners(new CallListener() {
            public void call(NavdyRouteListener listener) {
                listener.onRouteCalculated(NavdyRouteHandler.this.currentError, NavdyRouteHandler.this.currentRoute);
            }
        });
    }

    @Subscribe
    public void onStartActiveTripEvent(StartActiveTripEvent event) {
        if (isInOneOfThePendingTripStates()) {
            logger.w("onStartActiveTripEvent, state is not an active route state, no-op");
        } else if (this.currentRoute == null || !StringUtils.equalsOrBothEmptyAfterTrim(event.routeId, this.currentRoute.routeId)) {
            logger.i("onStartActiveTripEvent, routeIds in current and new are not the same");
            if (event.destination == null || event.route == null) {
                logger.w("trying to recover from insufficient data from the HUD");
                this.navigationHelper.getNavigationSessionState();
                return;
            }
            this.currentRoute = new NavdyRouteInfo(event.destination, event.route.via, new GeoPolyline(getRouteGeo(event.route.routeLatLongs)), null, event.route.routeId, event.route.duration.intValue(), event.route.duration_traffic.intValue(), event.route.length.intValue());
            setEnRouteState();
            callListeners(new CallListener() {
                public void call(NavdyRouteListener listener) {
                    listener.onRouteStarted(NavdyRouteHandler.this.currentRoute);
                }
            });
        } else {
            setEnRouteState();
            callListeners(new CallListener() {
                public void call(NavdyRouteListener listener) {
                    listener.onRouteStarted(NavdyRouteHandler.this.currentRoute);
                }
            });
        }
    }

    @Subscribe
    public void onRerouteActiveTripEvent(RerouteActiveTripEvent event) {
        if (this.currentState != State.EN_ROUTE) {
            logger.w("onRerouteActiveTripEvent, state is not EN_ROUTE, no-op");
        }
        this.navigationHelper.getNavigationSessionState();
        callListeners(new CallListener() {
            public void call(NavdyRouteListener listener) {
                listener.onReroute();
            }
        });
    }

    @Subscribe
    public void onArrivedTripEvent(ArrivedTripEvent event) {
        if (this.currentState != State.EN_ROUTE) {
            logger.w("onArrivedTripEvent, state must be EN_ROUTE, no-op");
            return;
        }
        CalendarUtils.forceCalendarRefresh();
        setLastDestinationArrived(this.lastDestinationArrived);
        callListeners(new CallListener() {
            public void call(NavdyRouteListener listener) {
                listener.onRouteArrived(NavdyRouteHandler.this.lastDestinationArrived);
            }
        });
    }

    @Subscribe
    public void onStopActiveTripEvent(StopActiveTripEvent event) {
        switch (this.currentState) {
            case INACTIVE:
                logger.v("onStopActiveTripEvent, state is already INACTIVE, no-op");
                return;
            case CALCULATING_PENDING_ROUTE:
            case PENDING_ROUTE:
                logger.w("onStopActiveTripEvent, state can't be " + this.currentState.name());
                return;
            default:
                boolean sameRequestIds;
                logger.v("onStopActiveTripEvent.requestId: " + event.requestId);
                logger.v("currentRoute.requestId: " + this.currentRoute.requestId);
                logger.v("onStopActiveTripEvent.routeId: " + event.routeId);
                logger.v("currentRoute.routeId: " + this.currentRoute.routeId);
                logger.v("onStopActiveTripEvent.handle: " + event.handle);
                logger.v("currentRoute.handle: " + this.currentRoute.cancelHandle);
                if (event.requestId == null || !StringUtils.equalsOrBothEmptyAfterTrim(event.requestId, this.currentRoute.requestId)) {
                    sameRequestIds = false;
                } else {
                    sameRequestIds = true;
                }
                boolean sameRouteIds;
                if (event.routeId == null || !StringUtils.equalsOrBothEmptyAfterTrim(event.routeId, this.currentRoute.routeId)) {
                    sameRouteIds = false;
                } else {
                    sameRouteIds = true;
                }
                boolean sameHandles;
                if (event.handle == null || !StringUtils.equalsOrBothEmptyAfterTrim(event.handle, this.currentRoute.cancelHandle)) {
                    sameHandles = false;
                } else {
                    sameHandles = true;
                }
                if (sameRequestIds || sameRouteIds || sameHandles) {
                    setStoppedState();
                    callListeners(new CallListener() {
                        public void call(NavdyRouteListener listener) {
                            listener.onStopRoute();
                        }
                    });
                    this.navigationHelper.getNavigationSessionState();
                    return;
                }
                logger.w("canceling different stuff, no-op");
                return;
        }
    }

    @Subscribe
    public void onHudReadyEvent(HUDReadyEvent event) {
        logger.v("HUD is ready");
        if (this.currentState == State.CALCULATING_PENDING_ROUTE || this.currentState == State.PENDING_ROUTE) {
            logger.i("sending pending route to " + this.currentRoute.destination);
            requestNewRoute(this.currentRoute.destination);
        }
    }

    @Subscribe
    public void onLimitCellDataEvent(LimitCellDataEvent event) {
        if (this.currentState != State.PENDING_ROUTE) {
            return;
        }
        if (event.hasLimitedCellData) {
            this.handler.removeCallbacks(this.refreshPendingRoute);
        } else {
            calculatePendingRoute(this.currentRoute.destination);
        }
    }

    private void calculatePendingRoute(final Destination destination) {
        double latitude;
        double longitude;
        if (MapUtils.isValidSetOfCoordinates(destination.navigationLat, destination.navigationLong)) {
            latitude = destination.navigationLat;
            longitude = destination.navigationLong;
        } else if (MapUtils.isValidSetOfCoordinates(destination.displayLat, destination.displayLong)) {
            latitude = destination.displayLat;
            longitude = destination.displayLong;
        } else {
            this.currentState = State.PENDING_ROUTE;
            this.currentError = Error.NO_ROUTES;
            callListeners(new CallListener() {
                public void call(NavdyRouteListener listener) {
                    listener.onPendingRouteCalculated(NavdyRouteHandler.this.currentError, NavdyRouteHandler.this.currentRoute);
                }
            });
            return;
        }
        HereRouteManager.getInstance().calculateRoute(latitude, longitude, new Listener() {
            public void onPreCalculation(@NonNull RouteHandle routeHandle) {
                NavdyRouteHandler.this.pendingRouteHandle = routeHandle;
            }

            public void onRouteCalculated(@NonNull com.navdy.client.app.framework.navigation.HereRouteManager.Error error, Route route) {
                if (NavdyRouteHandler.this.currentState == State.CALCULATING_PENDING_ROUTE) {
                    NavdyRouteHandler.this.currentState = State.PENDING_ROUTE;
                    NavdyRouteHandler.this.pendingRouteHandle = null;
                    if (error != com.navdy.client.app.framework.navigation.HereRouteManager.Error.NONE) {
                        NavdyRouteHandler.logger.e("failed to calculate pending trip");
                        NavdyRouteHandler.logger.v("requestNewRoute pending route has error " + error.name() + ", setting state to CALCULATION_FAILED");
                        NavdyRouteHandler.this.currentError = Error.NO_ROUTES;
                    } else {
                        NavdyRouteHandler.logger.v("requestNewRoute pending route success, setting state to PENDING_TRIP");
                        NavdyRouteHandler.this.currentError = Error.NONE;
                        NavdyRouteHandler.this.currentRoute = new NavdyRouteInfo(destination, null, new GeoPolyline(route.getRouteGeometry()), null, null, route.getTta(TrafficPenaltyMode.DISABLED, Route.WHOLE_ROUTE).getDuration(), route.getTta(TrafficPenaltyMode.OPTIMAL, Route.WHOLE_ROUTE).getDuration(), route.getLength());
                        NavdyRouteHandler.this.handler.postDelayed(NavdyRouteHandler.this.refreshPendingRoute, NavdyRouteHandler.PENDING_ROUTE_REFRESH);
                    }
                    if (!NavdyRouteHandler.this.appInstance.isHudMapEngineReady()) {
                        NavdyRouteHandler.this.callListeners(new CallListener() {
                            public void call(NavdyRouteListener listener) {
                                listener.onPendingRouteCalculated(NavdyRouteHandler.this.currentError, NavdyRouteHandler.this.currentRoute);
                            }
                        });
                    }
                }
            }
        });
    }

    private void updateCurrentProgress(GeoCoordinate coords, int tta, int distanceToDestination) {
        logger.v("updateCurrentProgressPolyline: " + coords);
        logger.v("updateCurrentProgressTTA: " + tta);
        logger.v("updateCurrentProgressDistance: " + distanceToDestination);
        if (this.currentRoute == null || this.currentRoute.route == null) {
            logger.w("trying to make progress without a route, no-op");
            return;
        }
        NavdyRouteInfo updatedRoute = new NavdyRouteInfo(this.currentRoute, null);
        updatedRoute.timeToDestination = tta;
        updatedRoute.distanceToDestination = distanceToDestination;
        int indexOnRoute = 0;
        if (updatedRoute.route != null) {
            indexOnRoute = updatedRoute.route.getNearestIndex(coords);
        }
        if (indexOnRoute > 1) {
            updatedRoute.progress = new GeoPolyline();
            for (int i = 0; i <= indexOnRoute; i++) {
                updatedRoute.progress.add(updatedRoute.route.getPoint(i));
            }
        }
        this.currentRoute = updatedRoute;
        callListeners(new CallListener() {
            public void call(NavdyRouteListener listener) {
                listener.onTripProgress(NavdyRouteHandler.this.currentRoute);
            }
        });
    }

    private List<GeoCoordinate> getRouteGeo(List<Float> routeLatLongs) {
        List<GeoCoordinate> routeGeo = new ArrayList();
        for (int i = 0; i < routeLatLongs.size() - 1; i += 2) {
            routeGeo.add(new GeoCoordinate((double) ((Float) routeLatLongs.get(i)).floatValue(), (double) ((Float) routeLatLongs.get(i + 1)).floatValue()));
        }
        return routeGeo;
    }

    private void setEnRouteState() {
        logger.v("setEnRouteState, setting state to EN_ROUTE");
        this.currentState = State.EN_ROUTE;
        this.currentError = Error.NONE;
        Destination currentDestination = getCurrentDestination();
        if (currentDestination != null) {
            currentDestination.lastRoutedDate = new Date().getTime();
            currentDestination.updateLastRoutedDateInDbAsync();
        }
    }

    private void setStoppedState() {
        logger.v("setStoppedState, setting state to INACTIVE");
        this.currentState = State.INACTIVE;
        this.currentError = Error.NONE;
        this.currentRoute = null;
        SuggestionManager.forceSuggestionFullRefresh();
        SuggestionManager.rebuildSuggestionListAndSendToHudAsync();
        NavdyLocationManager.forceLastKnownCountryCodeUpdate();
    }

    private void callListeners(CallListener callListener) {
        for (WeakReference<NavdyRouteListener> listenerWeakReference : this.listeners) {
            NavdyRouteListener listener = (NavdyRouteListener) listenerWeakReference.get();
            if (listener != null) {
                callListener.call(listener);
            }
        }
    }

    private Destination getPendingTrip() {
        return getCachedDestination(SAVED_DESTINATION);
    }

    private void setPendingTrip(Destination destination) {
        setCachedDestination(SAVED_DESTINATION, destination);
    }

    private void removePendingTrip() {
        removeCachedDestination(SAVED_DESTINATION);
    }

    private Destination getLastDestinationArrived() {
        return getCachedDestination(CACHED_ARRIVED_DESTINATION);
    }

    private void setLastDestinationArrived(Destination destination) {
        this.lastDestinationArrived = this.currentRoute.destination;
        setCachedDestination(CACHED_ARRIVED_DESTINATION, destination);
    }

    private void removeLastDestinationArrived() {
        this.lastDestinationArrived = null;
        removeCachedDestination(CACHED_ARRIVED_DESTINATION);
    }

    private Destination getCachedDestination(String cacheKey) {
        String destinationString = NavdyApplication.getAppContext().getSharedPreferences(SHARED_PREF_SAVED_ROUTE, 0).getString(cacheKey, null);
        logger.d("Cached destination is: " + destinationString);
        if (StringUtils.isEmptyAfterTrim(destinationString)) {
            return null;
        }
        return (Destination) new Gson().fromJson(destinationString, Destination.class);
    }

    private void setCachedDestination(String cacheKey, Destination destination) {
        Throwable e;
        if (destination == null) {
            logger.e("Unable to save pending trip. Provided destination is null !");
            return;
        }
        Editor editor = NavdyApplication.getAppContext().getSharedPreferences(SHARED_PREF_SAVED_ROUTE, 0).edit();
        try {
            editor.putString(cacheKey, new Gson().toJson((Object) destination));
            editor.apply();
            logger.d("Saving this as pending trip: " + destination);
            return;
        } catch (IllegalStateException e2) {
            e = e2;
        } catch (JsonSyntaxException e3) {
            e = e3;
        }
        logger.e(e);
    }

    private void removeCachedDestination(String cacheKey) {
        Throwable e;
        Editor editor = NavdyApplication.getAppContext().getSharedPreferences(SHARED_PREF_SAVED_ROUTE, 0).edit();
        try {
            editor.remove(cacheKey);
            editor.apply();
            logger.d("Removed the pending trip.");
            return;
        } catch (IllegalStateException e2) {
            e = e2;
        } catch (JsonSyntaxException e3) {
            e = e3;
        }
        logger.e(e);
    }
}
