package com.navdy.client.app.framework;

import android.os.Handler;
import android.os.Looper;
import com.navdy.client.app.framework.util.BusProvider;
import com.navdy.service.library.device.RemoteDevice;
import com.navdy.service.library.device.RemoteDevice.Listener;
import com.navdy.service.library.device.connection.Connection.ConnectionFailureCause;
import com.navdy.service.library.device.connection.Connection.DisconnectCause;
import com.navdy.service.library.device.connection.Connection.Status;
import com.navdy.service.library.device.connection.ConnectionInfo;
import com.navdy.service.library.device.connection.ConnectionType;
import com.navdy.service.library.events.DeviceInfo;
import com.navdy.service.library.events.NavdyEvent;
import com.navdy.service.library.events.NavdyEventUtil;
import com.navdy.service.library.log.Logger;
import com.squareup.wire.Message;

public class DeviceConnection implements Listener, AppInstance.Listener {
    private static final DeviceConnection instance = new DeviceConnection();
    private Handler handler = new Handler(Looper.getMainLooper());
    private Logger logger = new Logger(DeviceConnection.class);
    private RemoteDevice remoteDevice;

    private static class DeviceRelatedEvent {
        public RemoteDevice device;

        DeviceRelatedEvent(RemoteDevice device) {
            this.device = device;
        }
    }

    public static class DeviceConnectedEvent extends DeviceRelatedEvent {
        public DeviceConnectedEvent(RemoteDevice device) {
            super(device);
        }
    }

    public static class DeviceConnectingEvent extends DeviceRelatedEvent {
        DeviceConnectingEvent(RemoteDevice device) {
            super(device);
        }
    }

    public static class DeviceConnectionFailedEvent extends DeviceRelatedEvent {
        ConnectionFailureCause cause;

        DeviceConnectionFailedEvent(RemoteDevice device, ConnectionFailureCause cause) {
            super(device);
            this.cause = cause;
        }

        public String toString() {
            return "DeviceConnectionFailedEvent: cause=[" + this.cause + "] device=[" + this.device + "]";
        }
    }

    public static class DeviceDisconnectedEvent extends DeviceRelatedEvent {
        DisconnectCause cause;

        public DeviceDisconnectedEvent(RemoteDevice device, DisconnectCause cause) {
            super(device);
            this.cause = cause;
        }

        public String toString() {
            return "DeviceConnectionFailedEvent: cause=[" + this.cause + "] device=[" + this.device + "]";
        }
    }

    public static class DeviceInfoEvent {
        public DeviceInfo deviceInfo;

        DeviceInfoEvent(DeviceInfo deviceInfo) {
            this.deviceInfo = deviceInfo;
        }

        public String toString() {
            return "DeviceInfoEvent: info=[" + this.deviceInfo + "]";
        }
    }

    private DeviceConnection() {
        AppInstance appInstance = AppInstance.getInstance();
        appInstance.addListener(this);
        onDeviceChanged(appInstance.getRemoteDevice());
    }

    public static synchronized DeviceConnection getInstance() {
        DeviceConnection deviceConnection;
        synchronized (DeviceConnection.class) {
            deviceConnection = instance;
        }
        return deviceConnection;
    }

    public synchronized ConnectionInfo getConnectionInfo() {
        ConnectionInfo activeConnectionInfo;
        if (this.remoteDevice != null) {
            activeConnectionInfo = this.remoteDevice.getActiveConnectionInfo();
        } else {
            activeConnectionInfo = null;
        }
        return activeConnectionInfo;
    }

    public synchronized Status getConnectionStatus() {
        Status connectionStatus;
        if (this.remoteDevice != null) {
            connectionStatus = this.remoteDevice.getConnectionStatus();
        } else {
            connectionStatus = Status.DISCONNECTED;
        }
        return connectionStatus;
    }

    public ConnectionType getConnectionType() {
        ConnectionInfo info = getConnectionInfo();
        if (info != null) {
            return info.getType();
        }
        return ConnectionType.BT_PROTOBUF;
    }

    public static boolean isConnected() {
        boolean z;
        synchronized (instance) {
            z = instance.remoteDevice != null && instance.remoteDevice.isConnected();
        }
        return z;
    }

    public static boolean postEvent(Message message) {
        boolean z;
        synchronized (instance) {
            z = instance.remoteDevice != null && instance.remoteDevice.postEvent(message);
        }
        return z;
    }

    public static boolean postEvent(NavdyEvent event) {
        boolean z;
        synchronized (instance) {
            z = instance.remoteDevice != null && instance.remoteDevice.postEvent(event);
        }
        return z;
    }

    private synchronized void close() {
        if (this.remoteDevice != null) {
            this.remoteDevice.removeListener(this);
            this.remoteDevice = null;
        }
    }

    public synchronized void onDeviceChanged(RemoteDevice newDevice) {
        if (this.remoteDevice != null) {
            this.remoteDevice.removeListener(this);
        }
        this.remoteDevice = newDevice;
        if (this.remoteDevice != null) {
            this.remoteDevice.addListener(this);
        }
    }

    public void onDeviceConnecting(final RemoteDevice device) {
        this.logger.d("Connection status changed: CONNECTING | " + device);
        this.handler.post(new Runnable() {
            public void run() {
                BusProvider.getInstance().post(new DeviceConnectingEvent(device));
            }
        });
    }

    public void onDeviceConnected(final RemoteDevice device) {
        this.logger.d("Connection status changed: CONNECTED | " + device);
        this.handler.post(new Runnable() {
            public void run() {
                AppInstance.getInstance().onDeviceConnectedFirstResponder(device);
            }
        });
    }

    public void onDeviceConnectFailure(final RemoteDevice device, final ConnectionFailureCause cause) {
        this.logger.d("Connection status changed: CONNECT_FAIL | " + device);
        this.handler.post(new Runnable() {
            public void run() {
                BusProvider.getInstance().post(new DeviceConnectionFailedEvent(device, cause));
            }
        });
    }

    public void onDeviceDisconnected(final RemoteDevice device, final DisconnectCause cause) {
        this.logger.d("Connection status changed: DISCONNECTED | " + device);
        this.handler.post(new Runnable() {
            public void run() {
                AppInstance.getInstance().onDeviceDisconnectedFirstResponder(device, cause);
            }
        });
    }

    public void onNavdyEventReceived(RemoteDevice device, byte[] event) {
    }

    public void onNavdyEventReceived(RemoteDevice device, NavdyEvent event) {
        final Message message = NavdyEventUtil.messageFromEvent(event);
        if (message != null) {
            this.handler.post(new Runnable() {
                public void run() {
                    BusProvider.getInstance().post(message);
                }
            });
        }
    }
}
