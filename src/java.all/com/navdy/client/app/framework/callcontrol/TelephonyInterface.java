package com.navdy.client.app.framework.callcontrol;

public interface TelephonyInterface {
    void acceptRingingCall();

    void endCall();

    void rejectRingingCall();

    void toggleMute();
}
