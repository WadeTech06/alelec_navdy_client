package com.navdy.client.app.framework.util;

import android.content.Context;
import androidx.annotation.NonNull;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.AppInstance;
import com.navdy.client.app.framework.models.Suggestion;
import com.navdy.client.app.framework.servicehandler.DestinationsServiceHandler;
import com.navdy.client.app.framework.servicehandler.DestinationsServiceHandler.DestinationType;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import java.util.List;

public class VersioningUtils {
    private static final String SHARED_PREF_FAVORITES = "favorites";
    private static final String SHARED_PREF_SUGGESTIONS = "suggestions";
    private static final String VERSION = "version";
    private static final Logger sLogger = new Logger(VersioningUtils.class);

    public static long getCurrentVersion(Context context, DestinationType destinationType) {
        return context.getSharedPreferences(getSharedPrefFor(destinationType), 0).getLong(VERSION, 0);
    }

    private static void setCurrentVersion(Context context, DestinationType destinationType, Long version) {
        context.getSharedPreferences(getSharedPrefFor(destinationType), 0).edit().putLong(VERSION, version.longValue()).apply();
    }

    private static String getSharedPrefFor(DestinationType destinationType) {
        switch (destinationType) {
            case FAVORITES:
                return SHARED_PREF_FAVORITES;
            default:
                return SHARED_PREF_SUGGESTIONS;
        }
    }

    public static void increaseVersionAndSendToDisplay(DestinationType destinationType) {
        increaseVersion(NavdyApplication.getAppContext(), destinationType);
        if (AppInstance.getInstance().isDeviceConnected()) {
            DestinationsServiceHandler.sendDestinationsToDisplay(destinationType);
        } else {
            sLogger.v("HUD is not connected so ignoring the request to increaseVersionAndSendToDisplay (destinationType = " + destinationType + ")");
        }
    }

    static void increaseVersionAndSendTheseSuggestionsToDisplayAsync(final List<Suggestion> suggestions) {
        final Long serialNumber = increaseVersion(NavdyApplication.getAppContext(), DestinationType.SUGGESTIONS);
        if (AppInstance.getInstance().isDeviceConnected()) {
            TaskManager.getInstance().execute(new Runnable() {
                public void run() {
                    DestinationsServiceHandler.sendSuggestionsToDisplay(serialNumber.longValue(), suggestions);
                }
            }, 1);
        } else {
            sLogger.v("HUD is not connected so ignoring the request to increaseVersionAndSendTheseSuggestionsToDisplayAsync");
        }
    }

    @NonNull
    private static Long increaseVersion(Context context, DestinationType destinationType) {
        Long newVersion = Long.valueOf(getCurrentVersion(context, destinationType) + 1);
        setCurrentVersion(context, destinationType, newVersion);
        return newVersion;
    }
}
