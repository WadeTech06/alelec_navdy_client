package com.navdy.client.app.framework.util;

import androidx.annotation.NonNull;
import android.util.SparseLongArray;
import com.navdy.service.library.log.Logger;

public class ThrottlingLogger {
    private SparseLongArray lastLogTimes = new SparseLongArray();
    private final Logger logger;
    private final long wait;

    public ThrottlingLogger(@NonNull Logger logger, long wait) {
        this.logger = logger;
        this.wait = wait;
    }

    public void v(int tag, String msg) {
        if (checkTime(tag)) {
            this.logger.v(msg);
        }
    }

    public void v(int tag, String msg, Throwable tr) {
        if (checkTime(tag)) {
            this.logger.v(msg, tr);
        }
    }

    public void d(int tag, String msg) {
        if (checkTime(tag)) {
            this.logger.d(msg);
        }
    }

    public void d(int tag, String msg, Throwable tr) {
        if (checkTime(tag)) {
            this.logger.d(msg, tr);
        }
    }

    public void i(int tag, String msg) {
        if (checkTime(tag)) {
            this.logger.i(msg);
        }
    }

    public void i(int tag, String msg, Throwable tr) {
        if (checkTime(tag)) {
            this.logger.i(msg, tr);
        }
    }

    public void w(int tag, String msg) {
        if (checkTime(tag)) {
            this.logger.w(msg);
        }
    }

    public void w(int tag, String msg, Throwable tr) {
        if (checkTime(tag)) {
            this.logger.w(msg, tr);
        }
    }

    public void e(int tag, String msg) {
        if (checkTime(tag)) {
            this.logger.e(msg);
        }
    }

    public void e(int tag, String msg, Throwable tr) {
        if (checkTime(tag)) {
            this.logger.e(msg, tr);
        }
    }

    private synchronized boolean checkTime(int tag) {
        boolean isReady;
        long currentTime = System.currentTimeMillis();
        isReady = currentTime - this.lastLogTimes.get(tag, 0) > this.wait;
        if (isReady) {
            this.lastLogTimes.put(tag, currentTime);
        }
        return isReady;
    }
}
