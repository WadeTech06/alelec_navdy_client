package com.navdy.client.app.framework.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings.Global;
import androidx.annotation.CheckResult;
import androidx.annotation.MainThread;
import androidx.annotation.WorkerThread;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.ui.base.BaseActivity;
import com.navdy.service.library.log.Logger;
import java.util.Calendar;

public final class SystemUtils {
    private static final Logger sLogger = new Logger("SystemUtils");

    public static class CallOnMainThreadException extends RuntimeException {
    }

    private SystemUtils() {
    }

    public static void dismissKeyboard(Activity activity) {
        sLogger.v("dismissKeyboard() called");
        InputMethodManager inputManager = (InputMethodManager) NavdyApplication.getAppContext().getSystemService("input_method");
        if (inputManager.isAcceptingText()) {
            sLogger.v("keyboard is accepting text");
            try {
                View currentFocus = activity.getCurrentFocus();
                if (currentFocus != null) {
                    inputManager.hideSoftInputFromWindow(currentFocus.getWindowToken(), 0);
                }
            } catch (Exception e) {
                sLogger.v("SystemUtils failed hide keyboard");
            }
        }
    }

    public static void closeCursor(Cursor cursor) {
        if (cursor != null) {
            try {
                cursor.close();
            } catch (Throwable throwable) {
                sLogger.e(throwable);
            }
        }
    }

    @MainThread
    public static void ensureOnMainThread() {
        if (!isOnUiThread()) {
            throw new CallOnMainThreadException();
        }
    }

    @WorkerThread
    public static void ensureNotOnMainThread() {
        if (isOnUiThread()) {
            throw new CallOnMainThreadException();
        }
    }

    public static boolean isOnUiThread() {
        return Looper.myLooper() == Looper.getMainLooper();
    }

    @CheckResult
    public static long convertToNumber(String s) {
        return Long.parseLong(s.replaceAll("[^0-9\\._]+", ""));
    }

    public static void sleep(int millis) {
        try {
            Thread.sleep((long) millis);
        } catch (Throwable th) {
            sLogger.e("Unable to sleep!");
        }
    }

    public boolean canUseMaterialAnimations() {
        return VERSION.SDK_INT >= 21;
    }

    public static void goToSystemSettingsAppInfoForOurApp(Activity activity) {
        if (BaseActivity.isEnding(activity)) {
            sLogger.e("Can't call goToSystemSettingsAppInfoForOurApp while activity is ending.");
            return;
        }
        Intent intent = new Intent();
        intent.setAction("android.settings.APPLICATION_DETAILS_SETTINGS");
        intent.addCategory("android.intent.category.DEFAULT");
        intent.setData(Uri.parse("package:" + activity.getPackageName()));
        activity.startActivity(intent);
    }

    public static boolean isAirplaneModeOn(Context context) {
        return Global.getInt(context.getContentResolver(), "airplane_mode_on", 0) != 0;
    }

    public static boolean isPackageInstalled(String packageName) {
        try {
            NavdyApplication.getAppContext().getPackageManager().getPackageInfo(packageName, 1);
            return true;
        } catch (NameNotFoundException e) {
            return false;
        }
    }

    public static void logIntentExtras(Intent intent, Logger logger) {
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            for (String key : bundle.keySet()) {
                logger.d("extra: [" + key + "=" + bundle.get(key) + "]");
            }
        }
    }

    public static int getTimeZoneAndDaylightSavingOffset(Long timestamp) {
        return Calendar.getInstance().getTimeZone().getOffset(timestamp.longValue());
    }

    public static void runOnUiThread(final Runnable runnable) {
        if (isOnUiThread()) {
            runnable.run();
        } else {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    runnable.run();
                }
            });
        }
    }
}
