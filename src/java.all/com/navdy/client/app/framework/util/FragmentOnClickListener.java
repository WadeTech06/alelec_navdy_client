package com.navdy.client.app.framework.util;

import android.view.View;

public interface FragmentOnClickListener {
    void myClickMethod(View view);
}
