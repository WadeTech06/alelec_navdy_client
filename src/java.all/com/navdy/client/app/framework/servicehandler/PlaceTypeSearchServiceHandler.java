package com.navdy.client.app.framework.servicehandler;

import android.os.Handler;
import android.os.Looper;
import androidx.annotation.Nullable;
import com.navdy.client.app.framework.DeviceConnection;
import com.navdy.client.app.framework.map.NavCoordsAddressProcessor;
import com.navdy.client.app.framework.map.NavCoordsAddressProcessor.OnCompleteCallback;
import com.navdy.client.app.framework.models.GoogleTextSearchDestinationResult;
import com.navdy.client.app.framework.search.GooglePlacesSearch;
import com.navdy.client.app.framework.search.GooglePlacesSearch.Error;
import com.navdy.client.app.framework.search.GooglePlacesSearch.GoogleSearchListener;
import com.navdy.client.app.framework.search.GooglePlacesSearch.Query;
import com.navdy.client.app.framework.search.GooglePlacesSearch.ServiceTypes;
import com.navdy.client.app.framework.search.NavdySearch;
import com.navdy.client.app.framework.util.BusProvider;
import com.navdy.service.library.events.RequestStatus;
import com.navdy.service.library.events.destination.Destination;
import com.navdy.service.library.events.destination.Destination.Builder;
import com.navdy.service.library.events.destination.Destination.FavoriteType;
import com.navdy.service.library.events.location.LatLong;
import com.navdy.service.library.events.places.DestinationSelectedRequest;
import com.navdy.service.library.events.places.DestinationSelectedResponse;
import com.navdy.service.library.events.places.PlaceType;
import com.navdy.service.library.events.places.PlaceTypeSearchRequest;
import com.navdy.service.library.events.places.PlaceTypeSearchResponse;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Subscribe;
import com.squareup.wire.Message;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

public class PlaceTypeSearchServiceHandler {
    private static final String GOOGLE_PLACES_ID_HEADER = "google-places-id:";
    private static final Logger logger = new Logger(PlaceTypeSearchServiceHandler.class);

    private static class PlaceTypeSearchListener implements GoogleSearchListener {
        private static final int MAX_RESULTS = 7;
        private static final int MIN_ACCEPTABLE_RESULTS = 5;
        private static final int RETRY_RADIUS = 16093;
        private final Handler handler;
        private boolean hasRetried;
        private final String requestId;
        private final ServiceTypes serviceType;

//        /* synthetic */ PlaceTypeSearchListener(String x0, ServiceTypes x1, AnonymousClass1 x2) {
//            this(x0, x1);
//        }

        private PlaceTypeSearchListener(String requestId, ServiceTypes serviceType) {
            this.handler = new Handler(Looper.getMainLooper());
            this.requestId = requestId;
            this.serviceType = serviceType;
            this.hasRetried = false;
        }

        public void onGoogleSearchResult(List<GoogleTextSearchDestinationResult> destinations, Query queryType, Error error) {
            if (error == Error.SERVICE_ERROR) {
                postFailure();
            } else if ((destinations == null || destinations.size() < 5) && !this.hasRetried) {
                retryWithBiggerRadius();
            } else if (destinations == null || destinations.size() == 0) {
                postFailure();
            } else {
                List<Destination> outboundList = new ArrayList();
                Iterator<GoogleTextSearchDestinationResult> destinationsIterator = destinations.iterator();
                while (destinationsIterator.hasNext() && outboundList.size() < 7) {
                    GoogleTextSearchDestinationResult destination = (GoogleTextSearchDestinationResult) destinationsIterator.next();
                    String subtitle = (destination.streetNumber == null || destination.streetName == null) ? destination.address : destination.streetNumber + " " + destination.streetName;
                    outboundList.add(new Builder().navigation_position(new LatLong(Double.valueOf(0.0d), Double.valueOf(0.0d))).display_position(new LatLong(Double.valueOf(Double.parseDouble(destination.lat)), Double.valueOf(Double.parseDouble(destination.lng)))).destination_title(destination.name).destination_subtitle(subtitle).favorite_type(FavoriteType.FAVORITE_NONE).full_address(destination.address).identifier(UUID.randomUUID().toString()).place_type(getPlaceTypeForGoogleServiceType(this.serviceType)).place_id("google-places-id:" + destination.place_id).build());
                }
                Message placeTypeSearchResponse = new PlaceTypeSearchResponse.Builder().request_id(this.requestId).request_status(RequestStatus.REQUEST_SUCCESS).destinations(outboundList).build();
                PlaceTypeSearchServiceHandler.printPlaceTypeSearchResponse(placeTypeSearchResponse);
                DeviceConnection.postEvent(placeTypeSearchResponse);
            }
        }

        void runSearch() {
            NavdySearch.runServiceSearch(new GooglePlacesSearch(this, this.handler), this.serviceType);
        }

        private void postFailure() {
            DeviceConnection.postEvent(new PlaceTypeSearchResponse.Builder().destinations(new ArrayList()).request_id(this.requestId).request_status(RequestStatus.REQUEST_NOT_AVAILABLE).build());
        }

        private void retryWithBiggerRadius() {
            PlaceTypeSearchServiceHandler.logger.v("retrying with a 10 mile radius since results count is less than 5");
            this.hasRetried = true;
            new GooglePlacesSearch(this, this.handler).runServiceSearchWebApi(this.serviceType, RETRY_RADIUS);
        }

        private PlaceType getPlaceTypeForGoogleServiceType(ServiceTypes serviceType) {
            switch (serviceType) {
                case GAS:
                    return PlaceType.PLACE_TYPE_GAS;
                case PARKING:
                    return PlaceType.PLACE_TYPE_PARKING;
                case FOOD:
                    return PlaceType.PLACE_TYPE_RESTAURANT;
                case COFFEE:
                    return PlaceType.PLACE_TYPE_COFFEE;
                case ATM:
                    return PlaceType.PLACE_TYPE_ATM;
                case HOSPITAL:
                    return PlaceType.PLACE_TYPE_HOSPITAL;
                case STORE:
                    return PlaceType.PLACE_TYPE_STORE;
                default:
                    return PlaceType.PLACE_TYPE_UNKNOWN;
            }
        }
    }

    public PlaceTypeSearchServiceHandler() {
        BusProvider.getInstance().register(this);
    }

    private static void printPlaceTypeSearchResponse(PlaceTypeSearchResponse placeTypeSearchResponse) {
        if (placeTypeSearchResponse == null) {
            logger.e("printPlaceTypeSearchResponse, placeTypeSearchResponse is null!");
        } else {
            logger.d("placeTypeSearchResponse: " + placeTypeSearchResponse);
        }
    }

    private static void printDestinationSelectedResponse(DestinationSelectedResponse destinationSelectedResponse) {
        if (destinationSelectedResponse == null) {
            logger.e("printDestinationSelectedResponse, destinationSelectedResponse is null!");
        } else {
            logger.d("destinationSelectedResponse: " + destinationSelectedResponse);
        }
    }

    @Subscribe
    public void onPlaceTypeSearch(@Nullable PlaceTypeSearchRequest placeTypeSearchRequest) {
        if (placeTypeSearchRequest == null) {
            logger.w("event is null on handlePlaceTypeSearch");
            return;
        }
        PlaceType placeType = placeTypeSearchRequest.place_type;
        logger.i("received new PlaceTypeSearchRequest for " + placeType);
        ServiceTypes serviceType = getGoogleServiceTypeForPlaceType(placeType);
        if (serviceType != null) {
            performSearch(placeTypeSearchRequest.request_id, serviceType);
            return;
        }
        logger.e("invalid PlaceType, returning REQUEST_SERVICE_ERROR to HUD");
        DeviceConnection.postEvent(new PlaceTypeSearchResponse.Builder().request_id(placeTypeSearchRequest.request_id).destinations(new ArrayList()).request_status(RequestStatus.REQUEST_SERVICE_ERROR).build());
    }

    @Subscribe
    public void onDestinationSelectedRequest(@Nullable final DestinationSelectedRequest destinationSelectedRequest) {
        if (destinationSelectedRequest == null || destinationSelectedRequest.destination == null) {
            logger.w("event or its destination is null on handleDestinationSelectedRequest");
            return;
        }
        logger.v("received new DestinationSelectedRequest event for destination " + destinationSelectedRequest);
        NavCoordsAddressProcessor.processDestination(new com.navdy.client.app.framework.models.Destination(destinationSelectedRequest.destination), new OnCompleteCallback() {
            public void onSuccess(com.navdy.client.app.framework.models.Destination destination) {
                reply(RequestStatus.REQUEST_SUCCESS, destination);
            }

            public void onFailure(com.navdy.client.app.framework.models.Destination destination, NavCoordsAddressProcessor.Error error) {
                reply(RequestStatus.REQUEST_SERVICE_ERROR, destination);
            }

            private void reply(RequestStatus status, com.navdy.client.app.framework.models.Destination destination) {
                Message destinationSelectedResponse = new DestinationSelectedResponse.Builder().request_id(destinationSelectedRequest.request_id).request_status(status).destination(PlaceTypeSearchServiceHandler.this.EnsurePhoneNumbersAreNotLost(destinationSelectedRequest.destination, destination.toProtobufDestinationObject())).build();
                PlaceTypeSearchServiceHandler.printDestinationSelectedResponse(destinationSelectedResponse);
                DeviceConnection.postEvent(destinationSelectedResponse);
            }
        });
    }

    private Destination EnsurePhoneNumbersAreNotLost(Destination input, Destination output) {
        Builder builder = new Builder(output);
        if (output.phoneNumbers == null || output.phoneNumbers.size() <= 0) {
            builder.phoneNumbers(input.phoneNumbers);
        }
        if (output.contacts == null || output.contacts.size() <= 0) {
            builder.contacts(input.contacts);
        }
        return builder.build();
    }

    private void performSearch(String requestId, ServiceTypes serviceType) {
        new PlaceTypeSearchListener(requestId, serviceType, null).runSearch();
    }

    private ServiceTypes getGoogleServiceTypeForPlaceType(PlaceType placeType) {
        switch (placeType) {
            case PLACE_TYPE_GAS:
                return ServiceTypes.GAS;
            case PLACE_TYPE_PARKING:
                return ServiceTypes.PARKING;
            case PLACE_TYPE_RESTAURANT:
                return ServiceTypes.FOOD;
            case PLACE_TYPE_COFFEE:
                return ServiceTypes.COFFEE;
            case PLACE_TYPE_ATM:
                return ServiceTypes.ATM;
            case PLACE_TYPE_HOSPITAL:
                return ServiceTypes.HOSPITAL;
            case PLACE_TYPE_STORE:
                return ServiceTypes.STORE;
            default:
                return null;
        }
    }
}
