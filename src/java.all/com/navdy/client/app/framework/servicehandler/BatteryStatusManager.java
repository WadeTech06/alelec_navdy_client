package com.navdy.client.app.framework.servicehandler;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.tracking.Tracker;
import com.navdy.client.app.tracking.TrackerConstants.Event;
import com.navdy.service.library.device.RemoteDevice;
import com.navdy.service.library.events.callcontrol.PhoneBatteryStatus;
import com.navdy.service.library.events.callcontrol.PhoneBatteryStatus.BatteryStatus;
import com.navdy.service.library.log.Logger;
import com.squareup.wire.Message;

public class BatteryStatusManager {
    private static final int LEVEL_EXTREMELY_LOW = 10;
    private static final int LEVEL_LOW = 20;
    private static final Logger sLogger = new Logger(BatteryStatusManager.class);
    private boolean lastCharging;
    private int lastLevel = -1;
    private BatteryStatus lastStatus;
    private BroadcastReceiver receiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            int status = intent.getIntExtra("status", -1);
            boolean isCharging = status == 2 || status == 5;
            int level = intent.getIntExtra("level", -1);
            int scale = intent.getIntExtra("scale", -1);
            if (level == -1 || scale == -1) {
                BatteryStatusManager.sLogger.v("[battery] invalid info");
                return;
            }
            PhoneBatteryStatus phoneBatteryStatus = BatteryStatusManager.this.getPhoneBatteryStatus((level * 100) / scale, isCharging);
            if (phoneBatteryStatus != null) {
                BatteryStatusManager.this.sendEvent(phoneBatteryStatus);
            }
        }
    };
    private boolean registered;
    private RemoteDevice remoteDevice;

    public BatteryStatusManager(RemoteDevice remoteDevice) {
        this.remoteDevice = remoteDevice;
    }

    public synchronized void register() {
        if (!this.registered) {
            NavdyApplication.getAppContext().registerReceiver(this.receiver, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
            this.registered = true;
        }
        this.lastLevel = -1;
        this.lastCharging = false;
        this.lastStatus = null;
    }

    public synchronized void unregister() {
        if (this.registered) {
            NavdyApplication.getAppContext().unregisterReceiver(this.receiver);
            this.registered = false;
        }
        this.lastLevel = -1;
        this.lastCharging = false;
        this.lastStatus = null;
    }

    private void sendEvent(PhoneBatteryStatus status) {
        try {
            this.remoteDevice.postEvent((Message) status);
            sLogger.v("[battery] status sent");
        } catch (Throwable t) {
            sLogger.e("[battery]", t);
        }
    }

    private PhoneBatteryStatus getPhoneBatteryStatus(int newLevel, boolean charging) {
        BatteryStatus status;
        if (newLevel > 20) {
            status = BatteryStatus.BATTERY_OK;
        } else if (newLevel > 10 && newLevel <= 20) {
            status = BatteryStatus.BATTERY_LOW;
            Tracker.tagEvent(Event.BATTERY_LOW);
        } else if (newLevel <= 0 || newLevel > 20) {
            return null;
        } else {
            status = BatteryStatus.BATTERY_EXTREMELY_LOW;
        }
        if (this.lastLevel != -1 && this.lastStatus == status && this.lastCharging == charging) {
            return null;
        }
        this.lastCharging = charging;
        this.lastLevel = newLevel;
        this.lastStatus = status;
        sLogger.v("[battery] status[" + this.lastStatus + "] level [" + this.lastLevel + "] charging[" + charging + "]");
        return new PhoneBatteryStatus(this.lastStatus, Integer.valueOf(this.lastLevel), Boolean.valueOf(this.lastCharging));
    }
}
