package com.navdy.client.app.framework.servicehandler;

import com.navdy.client.app.framework.DeviceConnection;
import com.navdy.client.app.framework.DeviceConnection.DeviceConnectedEvent;
import com.navdy.client.app.framework.models.ContactModel;
import com.navdy.client.app.framework.models.PhoneNumberModel;
import com.navdy.client.app.framework.util.BusProvider;
import com.navdy.client.app.framework.util.ContactsManager;
import com.navdy.client.app.framework.util.ContactsManager.Attributes;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.ui.base.BaseActivity;
import com.navdy.service.library.events.RequestStatus;
import com.navdy.service.library.events.contacts.Contact;
import com.navdy.service.library.events.contacts.ContactRequest;
import com.navdy.service.library.events.contacts.ContactResponse;
import com.navdy.service.library.events.contacts.FavoriteContactsRequest;
import com.navdy.service.library.events.contacts.FavoriteContactsRequest.Builder;
import com.navdy.service.library.events.contacts.FavoriteContactsResponse;
import com.navdy.service.library.events.contacts.PhoneNumberType;
import com.navdy.service.library.task.TaskManager;
import com.squareup.otto.Subscribe;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.List;

public class ContactServiceHandler {
    public static final int DEFAULT_MAX_CONTACT = 30;
    private static ContactServiceHandler singleton;

    public static synchronized ContactServiceHandler getInstance() {
        ContactServiceHandler contactServiceHandler;
        synchronized (ContactServiceHandler.class) {
            if (singleton == null) {
                singleton = new ContactServiceHandler();
            }
            contactServiceHandler = singleton;
        }
        return contactServiceHandler;
    }

    private ContactServiceHandler() {
        BusProvider.getInstance().register(this);
    }

    @Subscribe
    public void onDeviceConnectedEvent(DeviceConnectedEvent event) {
        forceSendFavoriteContactsToHud();
    }

    public void forceSendFavoriteContactsToHud() {
        onFavoriteContactsRequest(new Builder().maxContacts(Integer.valueOf(30)).build());
    }

    @Subscribe
    public void onFavoriteContactsRequest(final FavoriteContactsRequest request) {
        final FavoriteContactsResponse.Builder responseBuilder = new FavoriteContactsResponse.Builder();
        if (BaseActivity.weHaveContactsPermission()) {
            TaskManager.getInstance().execute(new Runnable() {
                public void run() {
                    Iterator<ContactModel> contactIterator = ContactServiceHandler.this.sanitizeContacts(ContactsManager.getInstance().getFavoriteContactsWithPhone()).iterator();
                    List<Contact> outboundList = new ArrayList();
                    while (contactIterator.hasNext() && outboundList.size() < request.maxContacts.intValue()) {
                        ContactModel contact = (ContactModel) contactIterator.next();
                        if (contact.favorite && contact.phoneNumbers != null) {
                            for (PhoneNumberModel phoneNumber : contact.phoneNumbers) {
                                if (!StringUtils.isEmptyAfterTrim(phoneNumber.number)) {
                                    outboundList.add(new Contact(contact.name, phoneNumber.number, ContactServiceHandler.this.getNumberType(phoneNumber.type), null));
                                }
                            }
                        }
                    }
                    responseBuilder.status(RequestStatus.REQUEST_SUCCESS).contacts(outboundList);
                    DeviceConnection.postEvent(responseBuilder.build());
                }
            }, 1);
            return;
        }
        responseBuilder.status(RequestStatus.REQUEST_NOT_AVAILABLE);
        DeviceConnection.postEvent(responseBuilder.build());
    }

    private List<ContactModel> sanitizeContacts(List<ContactModel> list) {
        List<ContactModel> sanitizedList = new ArrayList();
        for (ContactModel contact : list) {
            contact.deduplicatePhoneNumbers();
            sanitizedList.add(contact);
        }
        return sanitizedList;
    }

    @Subscribe
    public void onContactRequest(final ContactRequest request) {
        final ContactResponse.Builder responseBuilder = new ContactResponse.Builder();
        if (StringUtils.isEmptyAfterTrim(request.identifier)) {
            responseBuilder.identifier(request.identifier).status(RequestStatus.REQUEST_INVALID_REQUEST);
            DeviceConnection.postEvent(responseBuilder.build());
        } else if (BaseActivity.weHaveContactsPermission()) {
            TaskManager.getInstance().execute(new Runnable() {
                public void run() {
                    List<ContactModel> contactList = ContactsManager.getInstance().getContactsWithoutLoadingPhotos(request.identifier, EnumSet.of(Attributes.PHONE));
                    List<Contact> list = new ArrayList(4);
                    if (contactList.size() > 0) {
                        ContactModel c = (ContactModel) contactList.get(0);
                        if (c.phoneNumbers != null) {
                            for (PhoneNumberModel phoneNumber : c.phoneNumbers) {
                                if (!StringUtils.isEmptyAfterTrim(phoneNumber.number)) {
                                    list.add(new Contact(c.name, phoneNumber.number, ContactServiceHandler.this.getNumberType(phoneNumber.type), null));
                                }
                            }
                        }
                    }
                    responseBuilder.identifier(request.identifier).status(RequestStatus.REQUEST_SUCCESS).contacts(list);
                    DeviceConnection.postEvent(responseBuilder.build());
                }
            }, 1);
        } else {
            responseBuilder.identifier(request.identifier).status(RequestStatus.REQUEST_NOT_AVAILABLE);
            DeviceConnection.postEvent(responseBuilder.build());
        }
    }

    private PhoneNumberType getNumberType(int type) {
        switch (type) {
            case 1:
                return PhoneNumberType.PHONE_NUMBER_HOME;
            case 2:
                return PhoneNumberType.PHONE_NUMBER_MOBILE;
            case 3:
                return PhoneNumberType.PHONE_NUMBER_WORK;
            default:
                return PhoneNumberType.PHONE_NUMBER_OTHER;
        }
    }
}
