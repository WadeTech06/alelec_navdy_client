package com.navdy.client.app.framework.servicehandler;

import android.content.Context;
import android.os.Handler;
import android.provider.CalendarContract.Events;
import com.navdy.client.app.framework.util.CalendarObserver;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;

public class CalendarHandler {
    private static final Logger sLogger = new Logger(CalendarHandler.class);
    private CalendarObserver calendarObserver = new CalendarObserver(new Handler());
    private Context context;

    public CalendarHandler(Context context) {
        this.context = context;
        registerObserver();
    }

    public void registerObserver() {
        sLogger.d("registerObserver");
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                CalendarHandler.this.context.getContentResolver().registerContentObserver(Events.CONTENT_URI, false, CalendarHandler.this.calendarObserver);
            }
        }, 1);
    }

    public void unregisterObserver() {
        sLogger.d("unregisterObserver");
        this.context.getContentResolver().unregisterContentObserver(this.calendarObserver);
    }

    public CalendarObserver getCalendarObserver() {
        return this.calendarObserver;
    }
}
