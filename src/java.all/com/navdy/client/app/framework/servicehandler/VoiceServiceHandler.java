package com.navdy.client.app.framework.servicehandler;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ResolveInfo;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.speech.RecognitionListener;
import android.speech.SpeechRecognizer;
import androidx.annotation.ArrayRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import com.alelec.navdyclient.R;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.DeviceConnection;
import com.navdy.client.app.framework.LocalyticsManager;
import com.navdy.client.app.framework.map.NavCoordsAddressProcessor;
import com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Error;
import com.navdy.client.app.framework.map.NavCoordsAddressProcessor.OnCompleteCallback;
import com.navdy.client.app.framework.models.Destination;
import com.navdy.client.app.framework.navigation.NavdyRouteHandler;
import com.navdy.client.app.framework.search.NavdySearch;
import com.navdy.client.app.framework.search.NavdySearch.SearchCallback;
import com.navdy.client.app.framework.search.SearchResults;
import com.navdy.client.app.framework.util.BusProvider;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.framework.util.TTSAudioRouter;
import com.navdy.client.app.framework.util.TTSAudioRouter.VoiceSearchSetupListener;
import com.navdy.client.app.framework.util.VoiceCommand;
import com.navdy.client.app.framework.util.VoiceCommandUtils;
import com.navdy.client.app.providers.NavdyContentProvider;
import com.navdy.client.app.tracking.Tracker;
import com.navdy.client.app.tracking.TrackerConstants.Attributes.HudVoiceSearchAttributes;
import com.navdy.client.app.tracking.TrackerConstants.Event;
import com.navdy.client.app.ui.settings.AudioDialogActivity;
import com.navdy.client.app.ui.settings.SettingsConstants;
import com.navdy.client.app.ui.settings.SettingsUtils;
import com.navdy.service.library.events.audio.VoiceAssistRequest;
import com.navdy.service.library.events.audio.VoiceAssistResponse;
import com.navdy.service.library.events.audio.VoiceAssistResponse.VoiceAssistState;
import com.navdy.service.library.events.audio.VoiceSearchRequest;
import com.navdy.service.library.events.audio.VoiceSearchResponse.Builder;
import com.navdy.service.library.events.audio.VoiceSearchResponse.VoiceSearchError;
import com.navdy.service.library.events.audio.VoiceSearchResponse.VoiceSearchState;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.service.library.events.ui.ShowScreen;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.util.IOUtils;
import com.squareup.otto.Subscribe;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.inject.Inject;

public class VoiceServiceHandler {
    private static final String GOOGLE_APP_PACKAGE = "com.google.android.googlequicksearchbox";
    private static final int VOICE_SEARCH_MAX_RESULTS = 10;
    private static VoiceServiceHandler instance = null;
    public static final Logger sLogger = new Logger(VoiceServiceHandler.class);
    private boolean alreadyRetried = false;
    private Handler handler = new Handler(Looper.getMainLooper());
    private int hfpSoundId = -1;
    private SoundPool hfpSoundPool = new SoundPool(1, 0, 0);
    private boolean isReadyForSpeech = false;
    private boolean listeningOverBt;
    @Inject
    TTSAudioRouter mAudioRouter;
    private RecognitionListener recognitionListener = new RecognitionListener() {
        private static final long MAX_LISTENING_TIME = 30000;
        private static final long MIN_LISTENING_TIME = 2000;
        private long beginTimestamp = 0;
        private Handler handler = new Handler();

        public void onReadyForSpeech(Bundle params) {
            VoiceServiceHandler.this.isReadyForSpeech = true;
            VoiceServiceHandler.sLogger.d("onReadyForSpeech true");
            DeviceConnection.postEvent(new Builder().state(VoiceSearchState.VOICE_SEARCH_LISTENING).listeningOverBluetooth(Boolean.valueOf(VoiceServiceHandler.this.listeningOverBt)).build());
            startTimeoutTimer();
        }

        void startTimeoutTimer() {
            this.handler.postDelayed(new Runnable() {
                public void run() {
                    AnonymousClass2.this.onError(6);
                }
            }, 30000);
        }

        public void onBeginningOfSpeech() {
            this.beginTimestamp = new Date().getTime();
            this.handler.removeCallbacksAndMessages(null);
            startTimeoutTimer();
            DeviceConnection.postEvent(new Builder().state(VoiceSearchState.VOICE_SEARCH_LISTENING).volumeLevel(Integer.valueOf(50)).listeningOverBluetooth(Boolean.valueOf(VoiceServiceHandler.this.listeningOverBt)).build());
            VoiceServiceHandler.sLogger.d("onBeginningOfSpeech");
        }

        public void onRmsChanged(float rmsdB) {
        }

        public void onBufferReceived(byte[] buffer) {
        }

        public void onEndOfSpeech() {
            VoiceServiceHandler.sLogger.d("onEndOfSpeech");
            this.handler.removeCallbacksAndMessages(null);
            startTimeoutTimer();
            DeviceConnection.postEvent(new Builder().state(VoiceSearchState.VOICE_SEARCH_SEARCHING).listeningOverBluetooth(Boolean.valueOf(VoiceServiceHandler.this.listeningOverBt)).build());
        }

        public void onError(int error) {
            this.handler.removeCallbacksAndMessages(null);
            if (VoiceServiceHandler.this.userCanceledVoiceSearch.get()) {
                VoiceServiceHandler.sLogger.v("Voice search was canceled on the HUD so ignoring this error: " + error);
            } else if (VoiceServiceHandler.this.isReadyForSpeech || error != 7) {
                Builder responseBuilder = new Builder();
                responseBuilder.state(VoiceSearchState.VOICE_SEARCH_ERROR);
                long timeListened = new Date().getTime() - this.beginTimestamp;
                if (error == 7) {
                    if (timeListened <= 0 || timeListened >= MIN_LISTENING_TIME) {
                        VoiceServiceHandler.sLogger.d(timeListened + "ms. elapsed since we started listening.");
                    } else {
                        HashMap<String, String> attributes = new HashMap();
                        attributes.put(HudVoiceSearchAttributes.NOISE_RETRY, Boolean.toString(VoiceServiceHandler.this.alreadyRetried));
                        LocalyticsManager.tagEvent(Event.HUD_VOICE_SEARCH_AMBIENT_NOISE_FAILURE, attributes);
                        if (!(VoiceServiceHandler.this.alreadyRetried || VoiceServiceHandler.this.sr == null)) {
                            VoiceServiceHandler.sLogger.d("Only " + timeListened + "ms. elapsed since we started listening and we haven't retried yet so starting the mic again.");
                            VoiceServiceHandler.this.alreadyRetried = true;
                            DeviceConnection.postEvent(new Builder().state(VoiceSearchState.VOICE_SEARCH_LISTENING).listeningOverBluetooth(Boolean.valueOf(VoiceServiceHandler.this.listeningOverBt)).build());
                            VoiceServiceHandler.this.sr.startListening(VoiceServiceHandler.this.recognizerIntent);
                            return;
                        }
                    }
                }
                VoiceServiceHandler.this.onStopListening();
                Context context = NavdyApplication.getAppContext();
                String message;
                switch (error) {
                    case 1:
                    case 2:
                    case 4:
                    case 5:
                        String errorType;
                        switch (error) {
                            case 1:
                                errorType = "ERROR_NETWORK_TIMEOUT";
                                break;
                            case 4:
                                errorType = "ERROR_SERVER";
                                break;
                            case 5:
                                errorType = "ERROR_CLIENT";
                                break;
                            default:
                                errorType = "ERROR_NETWORK";
                                break;
                        }
                        VoiceServiceHandler.sLogger.e("onError: " + errorType);
                        message = context.getString(R.string.network_issues);
                        responseBuilder.error(VoiceSearchError.OFFLINE);
                        VoiceServiceHandler.this.mAudioRouter.processTTSRequest(message, null, false);
                        break;
                    case 3:
                        VoiceServiceHandler.sLogger.e("onError: ERROR_AUDIO");
                        message = context.getString(R.string.audio_recording_error);
                        responseBuilder.error(VoiceSearchError.FAILED_TO_START);
                        VoiceServiceHandler.this.mAudioRouter.processTTSRequest(message, null, false);
                        break;
                    case 6:
                    case 7:
                        VoiceServiceHandler.sLogger.e("onError: " + (error == 6 ? "SpeechRecognizer.ERROR_SPEECH_TIMEOUT" : "ERROR_NO_MATCH"));
                        if (error == 7 && timeListened > 0 && timeListened < MIN_LISTENING_TIME) {
                            VoiceServiceHandler.sLogger.d("Only " + timeListened + "ms. elapsed since we started listening and we've already retried so giving up due to environment noise.");
                            responseBuilder.error(VoiceSearchError.AMBIENT_NOISE);
                            VoiceServiceHandler.this.mAudioRouter.processTTSRequest(context.getString(R.string.environment_too_loud), null, false);
                            break;
                        }
                        responseBuilder.error(VoiceSearchError.NO_WORDS_RECOGNIZED);
                        VoiceServiceHandler.this.mAudioRouter.processTTSRequest(context.getString(R.string.no_voice_match), null, false);
                        break;
                        break;
                    case 8:
                        VoiceServiceHandler.sLogger.e("onError: ERROR_RECOGNIZER_BUSY");
                        responseBuilder.error(VoiceSearchError.FAILED_TO_START);
                        VoiceServiceHandler.this.mAudioRouter.processTTSRequest(context.getString(R.string.mic_busy), null, false);
                        break;
                    case 9:
                        VoiceServiceHandler.sLogger.e("onError: ERROR_INSUFFICIENT_PERMISSIONS");
                        responseBuilder.error(VoiceSearchError.NEED_PERMISSION);
                        VoiceServiceHandler.this.mAudioRouter.processTTSRequest(context.getString(R.string.need_mic_permissions), null, false);
                        break;
                    default:
                        VoiceServiceHandler.sLogger.e("onError " + error);
                        responseBuilder.error(VoiceSearchError.FAILED_TO_START);
                        break;
                }
                responseBuilder.listeningOverBluetooth(Boolean.valueOf(VoiceServiceHandler.this.listeningOverBt));
                DeviceConnection.postEvent(responseBuilder.build());
                VoiceServiceHandler.this.onVoiceSearchComplete();
            } else {
                VoiceServiceHandler.sLogger.w("Ignoring an error that occurred before the engine was ready for speech. Error: " + error);
            }
        }

        public void onPartialResults(Bundle partialResults) {
        }

        public void onResults(Bundle results) {
            this.handler.removeCallbacksAndMessages(null);
            if (VoiceServiceHandler.this.userCanceledVoiceSearch.get()) {
                VoiceServiceHandler.sLogger.v("Voice search was canceled on the HUD so ignoring the results.");
                return;
            }
            VoiceServiceHandler.this.onStopListening();
            String query = null;
            float confidenceLevel = -1.0f;
            if (results != null) {
                float[] confidence = results.getFloatArray("confidence_scores");
                ArrayList data = results.getStringArrayList("results_recognition");
                int i = 0;
                while (data != null && i < data.size()) {
                    if (data.get(i) != null) {
                        query = (String) data.get(i);
                        if (confidence != null && i < confidence.length) {
                            confidenceLevel = confidence[i];
                        }
                    } else {
                        i++;
                    }
                }
            }
            VoiceServiceHandler.sLogger.d("onResult: \"" + query + "\" confidence level: " + confidenceLevel);
            if (confidenceLevel <= 0.0f || confidenceLevel >= 0.5f) {
                DeviceConnection.postEvent(new Builder().state(VoiceSearchState.VOICE_SEARCH_SEARCHING).recognizedWords(query).listeningOverBluetooth(Boolean.valueOf(VoiceServiceHandler.this.listeningOverBt)).confidenceLevel(Integer.valueOf((int) (100.0f * confidenceLevel))).build());
                VoiceServiceHandler.this.handleVoiceCommand(query);
                if (query != null) {
                    VoiceServiceHandler.this.logAnonymousAnalytics(query);
                    return;
                }
                return;
            }
            VoiceServiceHandler.this.handleError(R.string.not_sure_i_understand);
        }

        public void onEvent(int eventType, Bundle params) {
            VoiceServiceHandler.sLogger.d("onEvent " + eventType);
        }
    };
    private final Intent recognizerIntent;
    private final SharedPreferences sharedPreferences = SettingsUtils.getSharedPreferences();
    private SpeechRecognizer sr;
    private AtomicBoolean userCanceledVoiceSearch = new AtomicBoolean(false);

    public static synchronized VoiceServiceHandler getInstance() {
        VoiceServiceHandler voiceServiceHandler;
        synchronized (VoiceServiceHandler.class) {
            if (instance == null) {
                instance = new VoiceServiceHandler();
            }
            voiceServiceHandler = instance;
        }
        return voiceServiceHandler;
    }

    private VoiceServiceHandler() {
        try {
            this.hfpSoundId = this.hfpSoundPool.load(NavdyApplication.getAppContext(), R.raw.voice_search, 1);
        } catch (RuntimeException re) {
            sLogger.e("RuntimeException loading the sound ", re);
        }
        Injector.inject(NavdyApplication.getAppContext(), this);
        this.recognizerIntent = new Intent("android.speech.action.RECOGNIZE_SPEECH");
        this.recognizerIntent.putExtra("android.speech.extra.LANGUAGE_MODEL", "free_form");
        this.recognizerIntent.putExtra("calling_package", getClass().getPackage().getName());
        this.recognizerIntent.putExtra("android.speech.extra.PARTIAL_RESULTS", false);
        this.recognizerIntent.putExtra("android.speech.extra.PROMPT", true);
        this.recognizerIntent.putExtra("android.speech.extra.MAX_RESULTS", 1);
        this.recognizerIntent.putExtra("android.speech.extras.SPEECH_INPUT_MINIMUM_LENGTH_MILLIS", 7000);
        this.recognizerIntent.putExtra("android.speech.extras.SPEECH_INPUT_COMPLETE_SILENCE_LENGTH_MILLIS", AudioDialogActivity.DELAY_MILLIS);
        this.recognizerIntent.putExtra("android.speech.extras.SPEECH_INPUT_POSSIBLY_COMPLETE_SILENCE_LENGTH_MILLIS", AudioDialogActivity.DELAY_MILLIS);
        BusProvider.getInstance().register(this);
    }

    @Subscribe
    public void onVoiceAssistRequest(VoiceAssistRequest voiceAssistRequest) {
        handleGoogleNowRequest(this.mAudioRouter);
    }

    @Subscribe
    public void onVoiceSearchRequest(VoiceSearchRequest voiceSearchRequest) {
        if (voiceSearchRequest == null || !Boolean.TRUE.equals(voiceSearchRequest.end)) {
            this.userCanceledVoiceSearch.set(false);
            handleVoiceSearchRequest();
            return;
        }
        sLogger.v("Receiving request to cancel Voice search from the HUD");
        this.userCanceledVoiceSearch.set(true);
        onStopListening();
        onVoiceSearchComplete();
    }

    private void handleGoogleNowRequest(@NonNull TTSAudioRouter audioRouter) {
        sLogger.d("Handle Google now request");
        try {
            audioRouter.cancelTtsAndClearQueue();
            Intent intent = new Intent("android.speech.action.VOICE_SEARCH_HANDS_FREE");
            intent.addFlags(268435456);
            intent.setPackage(GOOGLE_APP_PACKAGE);
            Context context = NavdyApplication.getAppContext();
            List<ResolveInfo> resolveInfoList = context.getPackageManager().queryIntentActivities(intent, 0);
            if (resolveInfoList == null || resolveInfoList.size() == 0) {
                sLogger.e("Google now is not installed");
                DeviceConnection.postEvent(new VoiceAssistResponse(VoiceAssistState.VOICE_ASSIST_NOT_AVAILABLE));
                return;
            }
            context.startActivity(intent);
            DeviceConnection.postEvent(new VoiceAssistResponse(VoiceAssistState.VOICE_ASSIST_LISTENING));
        } catch (Throwable th) {
            sLogger.e("Unable to tell the HUD that we couldn't start Google Now", th);
        }
    }

    private void handleVoiceSearchRequest() {
        this.mAudioRouter.cancelTtsAndClearQueue();
        if (!this.sharedPreferences.getBoolean(SettingsConstants.USE_EXPERIMENTAL_HUD_VOICE_ENABLED, false) || this.sharedPreferences.getBoolean(SettingsConstants.HUD_VOICE_USED_BEFORE, false)) {
            DeviceConnection.postEvent(new Builder().state(VoiceSearchState.VOICE_SEARCH_STARTING).build());
            this.alreadyRetried = false;
            this.mAudioRouter.setVoiceSearchSetupListener(new VoiceSearchSetupListener() {
                public void onAudioInputReady(boolean usingBluetooth, boolean hfpConnectionFailed) {
                    VoiceServiceHandler.this.listeningOverBt = usingBluetooth;
                    if (VoiceServiceHandler.this.listeningOverBt && VoiceServiceHandler.this.hfpSoundId != -1) {
                        VoiceServiceHandler.this.hfpSoundPool.play(VoiceServiceHandler.this.hfpSoundId, 1.0f, 1.0f, 1, 0, 1.0f);
                    }
                    VoiceServiceHandler.this.handler.post(new Runnable() {
                        public void run() {
                            VoiceServiceHandler.this.sr = SpeechRecognizer.createSpeechRecognizer(NavdyApplication.getAppContext());
                            VoiceServiceHandler.this.sr.setRecognitionListener(VoiceServiceHandler.this.recognitionListener);
                            try {
                                VoiceServiceHandler.this.sr.startListening(VoiceServiceHandler.this.recognizerIntent);
                            } catch (SecurityException e) {
                                VoiceServiceHandler.sLogger.e("Unable to start the listening intent due to SecurityException.", e);
                                VoiceServiceHandler.this.recognitionListener.onError(9);
                            }
                        }
                    });
                }
            });
            this.mAudioRouter.startVoiceSearchSession();
            Tracker.tagEvent(Event.NAVIGATE_USING_HUD_VOICE_SEARCH);
            return;
        }
        this.sharedPreferences.edit().putBoolean(SettingsConstants.HUD_VOICE_USED_BEFORE, true).apply();
        Context context = NavdyApplication.getAppContext();
        String helloThere = context.getString(R.string.hello_there);
        this.mAudioRouter.processTTSRequest(helloThere + context.getString(R.string.i_can_do_this), null, false);
        Builder responseBuilder = new Builder();
        responseBuilder.state(VoiceSearchState.VOICE_SEARCH_ERROR);
        responseBuilder.error(VoiceSearchError.NOT_AVAILABLE);
        DeviceConnection.postEvent(responseBuilder.build());
        onVoiceSearchComplete();
    }

    private void onStopListening() {
        this.isReadyForSpeech = false;
        if (this.listeningOverBt && this.hfpSoundId != -1) {
            this.hfpSoundPool.play(this.hfpSoundId, 1.0f, 1.0f, 1, 0, 1.0f);
        }
        if (this.sr != null) {
            try {
                this.sr.stopListening();
                this.sr.cancel();
                this.sr.destroy();
            } catch (Exception e) {
                sLogger.e("Something went wrong while trying to close the speech recognizer.", e);
            }
            this.sr = null;
        }
    }

    private void onVoiceSearchComplete() {
        this.mAudioRouter.stopVoiceSearchSession();
    }

    private void handleVoiceCommand(String voiceCommand) {
        sLogger.d("Handling the command: \"" + voiceCommand + "\"");
        if (StringUtils.isEmptyAfterTrim(voiceCommand)) {
            handleError(R.string.no_voice_match);
            return;
        }
        Context context = NavdyApplication.getAppContext();
        String navdy = context.getString(R.string.navdy) + " ";
        if (voiceCommand.toLowerCase().startsWith(navdy)) {
            voiceCommand = voiceCommand.substring(navdy.length() - 1);
        }
        navdy = " " + context.getString(R.string.navdy);
        if (voiceCommand.toLowerCase().endsWith(navdy)) {
            voiceCommand = voiceCommand.substring(0, voiceCommand.length() - navdy.length());
        }
        if (handleSpecialCommands(voiceCommand, this.mAudioRouter)) {
            Builder responseBuilder = new Builder();
            responseBuilder.state(VoiceSearchState.VOICE_SEARCH_SUCCESS);
            responseBuilder.recognizedWords(voiceCommand);
            responseBuilder.prefix(voiceCommand);
            DeviceConnection.postEvent(responseBuilder.build());
            return;
        }
        String prefix;
        String query;
        VoiceCommand vc = VoiceCommandUtils.getMatchingVoiceCommand(R.raw.navigation_prefixes, voiceCommand);
        if (vc == null || StringUtils.isEmptyAfterTrim(vc.query)) {
            prefix = null;
            query = voiceCommand;
        } else {
            prefix = vc.prefix;
            query = vc.query;
        }
        sLogger.d("Cleaned up voice query is: " + query);
        if (!handleHomeAndWork(query)) {
            new NavdySearch(new SearchCallback() {
                public void onSearchStarted() {
                    Builder responseBuilder = new Builder();
                    responseBuilder.state(VoiceSearchState.VOICE_SEARCH_SEARCHING).recognizedWords(query);
                    DeviceConnection.postEvent(responseBuilder.build());
                }

                public void onSearchCompleted(SearchResults searchResults) {
                    if (SearchResults.isEmpty(searchResults)) {
                        VoiceServiceHandler.this.sayNoResultFound(searchResults.getQuery());
                        return;
                    }
                    VoiceServiceHandler.this.sendDestinationsToHud(searchResults.getDestinationsList(), searchResults.getQuery(), prefix);
                }
            }).runSearch(query);
        }
    }

    private void logAnonymousAnalytics(@NonNull String query) {
        String encodedQuery;
        String build = "p";
        try {
            encodedQuery = URLEncoder.encode(query, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            encodedQuery = query;
        }
        InputStream inputStream = null;
        try {
            inputStream = ((HttpURLConnection) new URL("https://s3-us-west-2.amazonaws.com/navdy-analytics/search/" + build + "/android/voice_search.txt?q=" + encodedQuery).openConnection()).getInputStream();
        } catch (Throwable e2) {
            sLogger.v("logAnonymousAnalytics: throwable e: " + e2);
        } finally {
            IOUtils.closeStream(inputStream);
        }
    }

    private void handleError(@StringRes int errorPrompt) {
        this.mAudioRouter.processTTSRequest(NavdyApplication.getAppContext().getString(errorPrompt), null, false);
        DeviceConnection.postEvent(new Builder().state(VoiceSearchState.VOICE_SEARCH_ERROR).error(VoiceSearchError.NO_WORDS_RECOGNIZED).listeningOverBluetooth(Boolean.valueOf(this.listeningOverBt)).build());
    }

    private boolean handleHomeAndWork(String query) {
        Context context = NavdyApplication.getAppContext();
        if (VoiceCommandUtils.isHome(query)) {
            Destination homeDestination = NavdyContentProvider.getHome();
            if (homeDestination != null) {
                sendDestinationToHud(homeDestination, query, query);
                return true;
            }
            sendNoResultsFoundResponse(query, context.getString(R.string.i_don_t_know_your_home));
            return true;
        } else if (!VoiceCommandUtils.isWork(query)) {
            return false;
        } else {
            Destination workDestination = NavdyContentProvider.getWork();
            if (workDestination != null) {
                sendDestinationToHud(workDestination, query, query);
                return true;
            }
            sendNoResultsFoundResponse(query, context.getString(R.string.i_don_t_know_your_work));
            return true;
        }
    }

    private boolean handleSpecialCommands(@NonNull String queryParam, @NonNull TTSAudioRouter audioRouter) {
        if (VoiceCommandUtils.isOneOfTheseVoiceCommands(R.raw.help_strings, queryParam)) {
            audioRouter.processTTSRequest(NavdyApplication.getAppContext().getString(R.string.i_can_do_this), null, false);
            return true;
        } else if (!this.sharedPreferences.getBoolean(SettingsConstants.USE_EXPERIMENTAL_HUD_VOICE_ENABLED, false)) {
            return false;
        } else {
            if (handleThisSimpleCommand(queryParam, audioRouter, R.array.i_love_you, R.string.i_love_you_too) || handleThisSimpleCommand(queryParam, audioRouter, R.array.this_is_broken, R.string.sorry_to_hear_that) || handleThisSimpleCommand(queryParam, audioRouter, R.array.who_are_you, R.string.i_am_navdy)) {
                return true;
            }
            if (handleThisCommand(queryParam, audioRouter, R.array.end_trip, R.string.ok_ending_trip, new Runnable() {
                public void run() {
                    NavdyRouteHandler.getInstance().stopRouting();
                }
            })) {
                return true;
            }
            Screen[] screens = new Screen[]{Screen.SCREEN_DASHBOARD, Screen.SCREEN_HYBRID_MAP, Screen.SCREEN_MENU, Screen.SCREEN_BACK, Screen.SCREEN_HOME, Screen.SCREEN_FAVORITE_PLACES, Screen.SCREEN_RECOMMENDED_PLACES, Screen.SCREEN_FAVORITE_CONTACTS, Screen.SCREEN_RECENT_CALLS, Screen.SCREEN_MUSIC, Screen.SCREEN_MEDIA_BROWSER, Screen.SCREEN_OPTIONS, Screen.SCREEN_SETTINGS, Screen.SCREEN_AUTO_BRIGHTNESS, Screen.SCREEN_DIAL_PAIRING, Screen.SCREEN_GESTURE_LEARNING, Screen.SCREEN_SYSTEM_INFO};
            int[] screenCommands = new int[]{R.array.show_dash, R.array.show_map, R.array.show_menu, R.array.go_back, R.array.exit, R.array.show_favorite_places, R.array.show_places, R.array.show_favorite_contacts, R.array.show_recent_calls, R.array.show_music, R.array.show_media_browser, R.array.show_options, R.array.show_settings, R.array.show_brightness, R.array.show_dial_pairing, R.array.show_gesture_learning, R.array.show_system_info};
            int i = 0;
            while (i < screenCommands.length && i < screens.length) {
                final Screen screen = screens[i];
                if (handleThisCommand(queryParam, audioRouter, screenCommands[i], R.string.here_you_go, new Runnable() {
                    public void run() {
                        DeviceConnection.postEvent(new ShowScreen.Builder().screen(screen).build());
                    }
                })) {
                    return true;
                }
                i++;
            }
            return false;
        }
    }

    private boolean handleThisSimpleCommand(String queryParam, TTSAudioRouter audioRouter, @ArrayRes int array, @StringRes int answer) {
        return handleThisCommand(queryParam, audioRouter, array, answer, null);
    }

    private boolean handleThisCommand(String queryParam, TTSAudioRouter audioRouter, @ArrayRes int array, @StringRes int answer, @Nullable Runnable runnable) {
        Context context = NavdyApplication.getAppContext();
        for (String s : context.getResources().getStringArray(array)) {
            if (queryParam.equalsIgnoreCase(s)) {
                audioRouter.processTTSRequest(context.getString(answer), null, false);
                if (runnable != null) {
                    runnable.run();
                }
                return true;
            }
        }
        return false;
    }

    private void sayNoResultFound(@Nullable String query) {
        String title;
        Context context = NavdyApplication.getAppContext();
        if (StringUtils.isEmptyAfterTrim(query)) {
            title = context.getString(R.string.no_results_found);
        } else {
            title = context.getString(R.string.no_results_found_for, new Object[]{query});
        }
        sendNoResultsFoundResponse(query, title);
    }

    private void sendNoResultsFoundResponse(@Nullable String query, String title) {
        this.mAudioRouter.processTTSRequest(title, null, false);
        DeviceConnection.postEvent(new Builder().state(VoiceSearchState.VOICE_SEARCH_ERROR).error(VoiceSearchError.NO_RESULTS_FOUND).listeningOverBluetooth(Boolean.valueOf(this.listeningOverBt)).recognizedWords(query).build());
        onVoiceSearchComplete();
    }

    private void sendDestinationToHud(@Nullable Destination destination, @Nullable String query, @Nullable String prefix) {
        ArrayList<Destination> destinations = new ArrayList(1);
        if (destination != null) {
            destinations.add(destination);
        }
        sendDestinationsToHud(destinations, query, prefix);
    }

    private void sendDestinationsToHud(@Nullable final ArrayList<Destination> destinations, @Nullable final String query, @Nullable final String prefix) {
        int size;
        if (destinations != null) {
            size = destinations.size();
        } else {
            size = 0;
        }
        sLogger.d("Processing " + size + " destination" + (size > 1 ? "s" : "") + " to be sent to the HUD: " + destinations);
        if (destinations == null || destinations.isEmpty()) {
            sayNoResultFound(query);
        } else if (size == 1) {
            NavCoordsAddressProcessor.processDestination((Destination) destinations.get(0), new OnCompleteCallback() {
                public void onSuccess(Destination destination) {
                    replaceAndSend(destination);
                }

                public void onFailure(Destination destination, Error error) {
                    replaceAndSend(destination);
                }

                private void replaceAndSend(Destination destination) {
                    destinations.set(0, destination);
                    VoiceServiceHandler.this.sendToHud(destinations, query, prefix);
                }
            });
        } else {
            sendToHud(destinations, query, prefix);
        }
    }

    private void sendToHud(ArrayList<Destination> destinations, @Nullable String query, @Nullable String prefix) {
        if (destinations == null || destinations.isEmpty()) {
            sayNoResultFound(query);
            return;
        }
        int size;
        boolean hudIsCapableOfSearchResultList = this.sharedPreferences.getBoolean(SettingsConstants.HUD_SEARCH_RESULT_LIST_CAPABLE, false);
        if (destinations.size() > 1 && hudIsCapableOfSearchResultList) {
            this.mAudioRouter.processTTSRequest(NavdyApplication.getAppContext().getString(R.string.ok_heres_what_i_found_for, new Object[]{query}), null, false);
        }
        List<com.navdy.service.library.events.destination.Destination> protoDestinations = Destination.convertDestinationsToProto(destinations);
        if (protoDestinations != null) {
            size = protoDestinations.size();
        } else {
            size = 0;
        }
        sLogger.d("Sending " + size + " protobuf destination" + (size > 1 ? "s" : "") + " to the HUD: " + protoDestinations);
        if (size > 10) {
            protoDestinations = protoDestinations.subList(0, 10);
            size = protoDestinations.size();
        }
        Builder responseBuilder = new Builder();
        responseBuilder.state(VoiceSearchState.VOICE_SEARCH_SUCCESS);
        responseBuilder.recognizedWords(query);
        responseBuilder.prefix(prefix);
        if (hudIsCapableOfSearchResultList) {
            responseBuilder.results(protoDestinations);
        }
        DeviceConnection.postEvent(responseBuilder.build());
        if (!hudIsCapableOfSearchResultList) {
            sLogger.w("The HUD we are connected to can not handle search result lists so routing to the 1st result.");
            NavdyRouteHandler.getInstance().requestNewRoute((Destination) destinations.get(0), true);
        }
        onVoiceSearchComplete();
    }
}
