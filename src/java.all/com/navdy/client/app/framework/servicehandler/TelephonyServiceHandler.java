package com.navdy.client.app.framework.servicehandler;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build;
import androidx.core.content.ContextCompat;
import android.telephony.PhoneStateListener;
import android.telephony.ServiceState;
import android.telephony.TelephonyManager;
import com.here.odnp.config.OdnpConfigStatic;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.DeviceConnection;
import com.navdy.client.app.framework.DeviceConnection.DeviceDisconnectedEvent;
import com.navdy.client.app.framework.callcontrol.TelephonyInterface;
import com.navdy.client.app.framework.callcontrol.TelephonyInterfaceFactory;
import com.navdy.client.app.framework.receiver.PhoneCallReceiver;
import com.navdy.client.app.framework.util.BusProvider;
import com.navdy.service.library.events.RequestStatus;
import com.navdy.service.library.events.callcontrol.PhoneEvent;
import com.navdy.service.library.events.callcontrol.PhoneStatus;
import com.navdy.service.library.events.callcontrol.PhoneStatusRequest;
import com.navdy.service.library.events.callcontrol.PhoneStatusResponse;
import com.navdy.service.library.events.callcontrol.TelephonyRequest;
import com.navdy.service.library.events.callcontrol.TelephonyResponse;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Subscribe;
import java.util.Timer;
import java.util.TimerTask;

public class TelephonyServiceHandler {
    private static final int TURN_SPEAKER_ON_DELAY = 3000;
    private static final Logger sLogger = new Logger(TelephonyServiceHandler.class);
    private int currentServiceState = 1;
    private Context mContext;
    private TelephonyInterface mTelephony;
    private PhoneStateListener phoneStateListener = new PhoneStateListener() {
        public void onServiceStateChanged(ServiceState serviceState) {
            TelephonyServiceHandler.this.currentServiceState = serviceState.getState();
            TelephonyServiceHandler.sLogger.v("service state changed:" + TelephonyServiceHandler.this.currentServiceState);
        }
    };

    public TelephonyServiceHandler(Context context) {
        this.mContext = context;
        this.mTelephony = TelephonyInterfaceFactory.getTelephonyInterface(context);
        sLogger.v("Telephony model[" + Build.MODEL + "] manufacturer[" + Build.MANUFACTURER + "] device[" + Build.DEVICE + "]");
        sLogger.v("Telephony controller is :" + this.mTelephony.getClass().getName());
        ((TelephonyManager) context.getSystemService("phone")).listen(this.phoneStateListener, 1);
        sLogger.v("registeted phone state listener");
        BusProvider.getInstance().register(this);
    }

    @Subscribe
    public void onDeviceDisconnectedEvent(DeviceDisconnectedEvent event) {
        ((TelephonyManager) this.mContext.getSystemService("phone")).listen(this.phoneStateListener, 0);
    }

    @Subscribe
    public void onTelephonyRequest(TelephonyRequest telephonyRequest) {
        switch (telephonyRequest.action) {
            case CALL_ACCEPT:
                this.mTelephony.acceptRingingCall();
                turnOnSpeakerPhoneIfNeeded();
                break;
            case CALL_REJECT:
                this.mTelephony.rejectRingingCall();
                break;
            case CALL_END:
                this.mTelephony.endCall();
                break;
            case CALL_DIAL:
                if (this.currentServiceState == 0) {
                    String uri = "tel:" + telephonyRequest.number.trim();
                    Intent intent = new Intent("android.intent.action.CALL");
                    intent.setFlags(402653184);
                    intent.setData(Uri.parse(uri));
                    if (ContextCompat.checkSelfPermission(NavdyApplication.getAppContext(), "android.permission.CALL_PHONE") != 0) {
                        sLogger.e("Missing CALL_PHONE permission !");
                        break;
                    }
                    this.mContext.startActivity(intent);
                    turnOnSpeakerPhoneIfNeeded();
                    break;
                }
                DeviceConnection.postEvent(new PhoneEvent(PhoneStatus.PHONE_DISCONNECTING, telephonyRequest.number, null, null, null));
                DeviceConnection.postEvent(new TelephonyResponse(telephonyRequest.action, RequestStatus.REQUEST_SERVICE_ERROR, null));
                return;
            case CALL_MUTE:
            case CALL_UNMUTE:
                this.mTelephony.toggleMute();
                break;
            default:
                sLogger.e("Unknown TelephonyServiceHandler request action: " + telephonyRequest.action);
                break;
        }
        DeviceConnection.postEvent(new TelephonyResponse(telephonyRequest.action, RequestStatus.REQUEST_SUCCESS, null));
    }

    @Subscribe
    public void onPhoneStatusRequest(PhoneStatusRequest phoneStatusRequest) {
        DeviceConnection.postEvent(new PhoneStatusResponse(RequestStatus.REQUEST_SUCCESS, PhoneCallReceiver.getCurrentStatus()));
    }

    private void turnOnSpeakerPhoneIfNeeded() {
        if (!isHFPSpeakerConnected()) {
            new Timer().schedule(new TimerTask() {
                public void run() {
                    TelephonyServiceHandler.sLogger.d("HFP is not connected, turning on the speaker phone");
                    AudioManager mAudioManager = (AudioManager) TelephonyServiceHandler.this.mContext.getSystemService("audio");
                    mAudioManager.setMode(2);
                    mAudioManager.setSpeakerphoneOn(true);
                }
            }, OdnpConfigStatic.CELL_NO_CHANGE_LIMITER_TIME);
        }
    }

    private boolean isHFPSpeakerConnected() {
        return BluetoothAdapter.getDefaultAdapter().getProfileConnectionState(1) == 2;
    }
}
