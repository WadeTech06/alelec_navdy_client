package com.navdy.client.app.framework.servicehandler;

import com.navdy.client.app.framework.util.TTSAudioRouter;
import dagger.MembersInjector;
import dagger.internal.Binding;
import dagger.internal.Linker;
import java.util.Set;

public final class VoiceServiceHandler$$InjectAdapter extends Binding<VoiceServiceHandler> implements MembersInjector<VoiceServiceHandler> {
    private Binding<TTSAudioRouter> mAudioRouter;

    public VoiceServiceHandler$$InjectAdapter() {
        super(null, "members/com.navdy.client.app.framework.servicehandler.VoiceServiceHandler", false, VoiceServiceHandler.class);
    }

    public void attach(Linker linker) {
        this.mAudioRouter = linker.requestBinding("com.navdy.client.app.framework.util.TTSAudioRouter", VoiceServiceHandler.class, getClass().getClassLoader());
    }

    public void getDependencies(Set<Binding<?>> set, Set<Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.mAudioRouter);
    }

    public void injectMembers(VoiceServiceHandler object) {
        object.mAudioRouter = (TTSAudioRouter) this.mAudioRouter.get();
    }
}
