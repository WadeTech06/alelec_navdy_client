package com.navdy.client.app.framework.service;

import android.app.Notification;
import android.app.Notification.Action;
import android.content.SharedPreferences;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.core.app.RemoteInput;
import androidx.core.app.RemoteInput.Builder;
import android.text.TextUtils;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.AppInstance;
import com.navdy.client.app.framework.glances.CalendarNotificationHandler;
import com.navdy.client.app.framework.glances.EmailNotificationHandler;
import com.navdy.client.app.framework.glances.GenericNotificationHandler;
import com.navdy.client.app.framework.glances.GlanceConstants;
import com.navdy.client.app.framework.glances.GlanceConstants.Group;
import com.navdy.client.app.framework.glances.MessagingNotificationHandler;
import com.navdy.client.app.framework.glances.SocialNotificationHandler;
import com.navdy.client.app.framework.models.Destination;
import com.navdy.client.app.framework.servicehandler.MusicServiceHandler;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.ui.settings.SettingsConstants;
import com.navdy.client.app.ui.settings.SettingsUtils;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;

public class NavdyCustomNotificationListenerService extends NotificationListenerService {
    private static final int INDENT_INCREMENT = 4;
    private static final long NOTIFICATION_WAIT_FOR_MORE_DELAY = 1000;
    private static final String SPACES = " ";
    public static final Handler notifListenerServiceHandler = new Handler();
    public static final Logger sLogger = new Logger("NavdyNotifHandler");
    public static final Pattern splitTickerTextOnColon = Pattern.compile("(.*): (.*)");
    private List<String> activeMusicApps = new ArrayList();
    private HashMap<String, Runnable> incomingNotifications = new HashMap();
    private HashMap<String, Boolean> knownGroups = new HashMap();
    private HashMap<String, Boolean> knownNotifications = new HashMap();
    private String lastOngoing = "";
    private HashMap<String, Runnable> pendingGroupNotifications = new HashMap();
    private final SharedPreferences sharedPrefs = SettingsUtils.getSharedPreferences();

    public NavdyCustomNotificationListenerService() {
        GlanceConstants.addPackageToGroup(SettingsUtils.getDialerPackage(NavdyApplication.getAppContext().getPackageManager()), Group.IGNORE_GROUP);
    }

    public void onCreate() {
        sLogger.d("onCreate");
        super.onCreate();
    }

    public void onDestroy() {
        sLogger.d("onDestroy");
        super.onDestroy();
    }

    public void onNotificationPosted(final StatusBarNotification sbn) {
        if (sbn != null) {
            TaskManager.getInstance().execute(new Runnable() {
                public void run() {
                    NavdyCustomNotificationListenerService.this.handleStatusBarNotification(sbn);
                }
            }, 7);
        }
    }

    @NonNull
    private String getHash(@NonNull StatusBarNotification sbn) {
        return sbn.getId() + Destination.IDENTIFIER_CONTACT_ID_SEPARATOR + sbn.getPackageName() + Destination.IDENTIFIER_CONTACT_ID_SEPARATOR + sbn.getTag() + Destination.IDENTIFIER_CONTACT_ID_SEPARATOR + getText(sbn);
    }

    @Nullable
    private String getText(@Nullable StatusBarNotification sbn) {
        if (sbn == null) {
            return null;
        }
        Notification notif = sbn.getNotification();
        if (notif == null) {
            return null;
        }
        Bundle extras = NotificationCompat.getExtras(notif);
        if (extras == null) {
            return null;
        }
        String text = getLastMessageInCarConversation(extras);
        if (!StringUtils.isEmptyAfterTrim(text)) {
            return text;
        }
        text = getStringSafely(extras, "android.text");
        if (!StringUtils.isEmptyAfterTrim(text)) {
            return text;
        }
        text = getStringSafely(extras, "android.bigText");
        if (!StringUtils.isEmptyAfterTrim(text)) {
            return text;
        }
        text = getStringSafely(extras, "android.title");
        if (StringUtils.isEmptyAfterTrim(text)) {
            return notif.tickerText != null ? notif.tickerText.toString() : null;
        } else {
            return text;
        }
    }

    @Nullable
    public static String getLastMessageInCarConversation(@NonNull Bundle extras) {
        Bundle carExt = extras.getBundle(GlanceConstants.CAR_EXT);
        if (carExt == null) {
            return null;
        }
        Bundle convo = carExt.getBundle(GlanceConstants.CAR_EXT_CONVERSATION);
        if (convo == null) {
            return null;
        }
        Parcelable[] messages = convo.getParcelableArray(GlanceConstants.CAR_EXT_CONVERSATION_MESSAGES);
        if (messages == null || messages.length <= 0) {
            return null;
        }
        Parcelable lastMessage = messages[messages.length - 1];
        if (lastMessage == null || !(lastMessage instanceof Bundle)) {
            return null;
        }
        String text = ((Bundle) lastMessage).getString(GlanceConstants.CAR_EXT_CONVERSATION_MESSAGES_TEXT);
        if (StringUtils.isEmptyAfterTrim(text)) {
            return null;
        }
        return text;
    }

    @Nullable
    public static String getFirstParticipantInCarConversation(@NonNull Bundle extras) {
        Bundle carExt = extras.getBundle(GlanceConstants.CAR_EXT);
        if (carExt != null) {
            Bundle convo = carExt.getBundle(GlanceConstants.CAR_EXT_CONVERSATION);
            if (convo != null) {
                String[] pariticipants = convo.getStringArray(GlanceConstants.CAR_EXT_CONVERSATION_PARTICIPANTS);
                if (pariticipants != null && pariticipants.length > 0) {
                    return pariticipants[0];
                }
            }
        }
        return null;
    }

    @Nullable
    public static String getLastTextLine(@NonNull Bundle extras) {
        Object textLines = extras.get("android.textLines");
        if (textLines != null && (textLines instanceof CharSequence[])) {
            CharSequence[] lines = (CharSequence[]) textLines;
            CharSequence lastLine = lines[lines.length - 1];
            if (!StringUtils.isEmptyAfterTrim(lastLine)) {
                return lastLine.toString();
            }
        }
        return null;
    }

    @Nullable
    public static String getStringSafely(@NonNull Bundle extras, @NonNull String key) {
        String o = extras.get(key);
        if (o instanceof String) {
            return o;
        }
        if (o instanceof CharSequence) {
            return o.toString();
        }
        return null;
    }

    private void handleStatusBarNotification(@NonNull StatusBarNotification sbn) {
        try {
            final String packageName = sbn.getPackageName();
            final Notification notif = sbn.getNotification();
            Bundle extras = NotificationCompat.getExtras(notif);
            if (isMusicNotification(sbn)) {
                sLogger.i("Music notification posted: " + packageName);
                addActiveMusicApp(packageName);
            } else if (!this.sharedPrefs.getBoolean(SettingsConstants.GLANCES, false)) {
            } else {
                if (!sbn.isOngoing()) {
                    String ticker = null;
                    if (notif.tickerText != null) {
                        ticker = notif.tickerText.toString();
                    }
                    if (GlanceConstants.isPackageInGroup(packageName, Group.IGNORE_GROUP)) {
                        sLogger.d("handleStatusBarNotification: ignored [" + packageName + "]");
                    } else if (!isConnectedToHud()) {
                        sLogger.v("handleStatusBarNotification: hud not connected [" + packageName + "]");
                    } else if (extras == null) {
                        sLogger.v("handleStatusBarNotification: no extras [" + packageName + "]");
                    } else {
                        String notificationHashKey = getHash(sbn);
                        if (Boolean.TRUE.equals(this.knownNotifications.get(notificationHashKey))) {
                            sLogger.v("handleStatusBarNotification: already seen this notification: [" + notificationHashKey + "] so won't send to HUD to avoid duplicates");
                            return;
                        }
                        this.knownNotifications.put(notificationHashKey, Boolean.valueOf(true));
                        final String finalTicker = ticker;
                        final StatusBarNotification statusBarNotification = sbn;
                        Runnable runnable = new Runnable() {
                            public void run() {
                                TaskManager.getInstance().execute(new Runnable() {
                                    public void run() {
                                        String group = NotificationCompat.getGroup(notif);
                                        boolean z = !StringUtils.isEmptyAfterTrim(group) && Boolean.TRUE.equals(NavdyCustomNotificationListenerService.this.knownGroups.get(group));
                                        Boolean isKnown = Boolean.valueOf(z);
                                        boolean isGroupSummary = NotificationCompat.isGroupSummary(notif);
                                        NavdyCustomNotificationListenerService.sLogger.d("NotifListenerService: handleStatusBarNotification: pkg [" + packageName + "]" + " id [" + statusBarNotification.getId() + "]" + " tag [" + statusBarNotification.getTag() + "]" + " ticker[" + finalTicker + "]" + " group[" + group + "]" + " isKnownGroup[" + isKnown + "]" + " isGroupSummary[" + isGroupSummary + "]");
                                        if (!StringUtils.isEmptyAfterTrim(group)) {
                                            NavdyCustomNotificationListenerService.this.knownGroups.put(group, Boolean.valueOf(true));
                                        }
                                        if (!isGroupSummary && !StringUtils.equalsOrBothEmptyAfterTrim(packageName, GlanceConstants.WHATS_APP)) {
                                            NavdyCustomNotificationListenerService.sLogger.i("Remove Pending Handling For Group: " + group);
                                            NavdyCustomNotificationListenerService.this.removePendingHandlingForGroup(group);
                                            NavdyCustomNotificationListenerService.this.categorizeAndHandleNotification(statusBarNotification, packageName, finalTicker);
                                        } else if (!Boolean.TRUE.equals(isKnown) || StringUtils.equalsOrBothEmptyAfterTrim(packageName, GlanceConstants.WHATS_APP)) {
                                            NavdyCustomNotificationListenerService.sLogger.i("Remove Pending Handling For Group: " + group);
                                            NavdyCustomNotificationListenerService.this.removePendingHandlingForGroup(group);
                                            Runnable r = new Runnable() {
                                                public void run() {
                                                    TaskManager.getInstance().execute(new Runnable() {
                                                        public void run() {
                                                            NavdyCustomNotificationListenerService.this.categorizeAndHandleNotification(statusBarNotification, packageName, finalTicker);
                                                        }
                                                    }, 7);
                                                }
                                            };
                                            NavdyCustomNotificationListenerService.sLogger.i("Adding Pending Handling For Group: " + group);
                                            NavdyCustomNotificationListenerService.this.pendingGroupNotifications.put(group, r);
                                            NavdyCustomNotificationListenerService.notifListenerServiceHandler.postDelayed(r, 1000);
                                        } else {
                                            NavdyCustomNotificationListenerService.sLogger.i("Ignoring summary notifications to prevent duplicates.");
                                        }
                                    }
                                }, 7);
                            }
                        };
                        cancelPendingHandlerIfAnyFor(sbn);
                        sLogger.d("onNotificationPosted:   Adding a pending runnable for " + notificationHashKey);
                        this.incomingNotifications.put(notificationHashKey, runnable);
                        notifListenerServiceHandler.postDelayed(runnable, 1000);
                    }
                } else if (!TextUtils.equals(this.lastOngoing, packageName)) {
                    sLogger.v("handleStatusBarNotification: ongoing notif skip:" + packageName);
                    this.lastOngoing = packageName;
                }
            }
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }

    private void categorizeAndHandleNotification(StatusBarNotification sbn, String packageName, String ticker) {
        sLogger.d("categorizeAndHandleNotification: [" + packageName + "] ticker[" + ticker + "]");
        if (GlanceConstants.isPackageInGroup(packageName, Group.MESSAGING_GROUP)) {
            MessagingNotificationHandler.handleMessageNotification(sbn);
        } else if (GlanceConstants.isPackageInGroup(packageName, Group.EMAIL_GROUP)) {
            EmailNotificationHandler.handleEmailNotification(sbn);
        } else if (GlanceConstants.isPackageInGroup(packageName, Group.CALENDAR_GROUP) || GlanceConstants.ANDROID_CALENDAR.equals(packageName)) {
            if (GlanceConstants.GOOGLE_CALENDAR.equals(packageName) && ticker == null) {
                sLogger.v("null ticker");
            } else {
                CalendarNotificationHandler.handleCalendarNotification(sbn);
            }
        } else if (GlanceConstants.isPackageInGroup(packageName, Group.SOCIAL_GROUP)) {
            SocialNotificationHandler.handleSocialNotification(sbn);
        } else {
            GenericNotificationHandler.handleGenericNotification(sbn);
            sLogger.v("NotifListenerService: generic notification [" + packageName + "]");
        }
    }

    public void onNotificationRemoved(final StatusBarNotification sbn) {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                if (sbn != null) {
                    NavdyCustomNotificationListenerService.sLogger.d("onNotificationRemoved: pkg [" + sbn.getPackageName() + "]" + " tag [" + sbn.getTag() + "]" + " id [" + sbn.getId() + "]" + " time [" + sbn.getPostTime() + "]");
                    if (NavdyCustomNotificationListenerService.this.isMusicNotification(sbn)) {
                        NavdyCustomNotificationListenerService.this.removeActiveMusicApp(sbn.getPackageName());
                    }
                    NavdyCustomNotificationListenerService.this.cancelPendingHandlerIfAnyFor(sbn);
                    if (NotificationCompat.isGroupSummary(sbn.getNotification())) {
                        String group = NotificationCompat.getGroup(sbn.getNotification());
                        NavdyCustomNotificationListenerService.this.knownGroups.remove(group);
                        NavdyCustomNotificationListenerService.this.removePendingHandlingForGroup(group);
                    }
                    NavdyCustomNotificationListenerService.this.knownNotifications.remove(NavdyCustomNotificationListenerService.this.getHash(sbn));
                }
            }
        }, 7);
    }

    public void cancelPendingHandlerIfAnyFor(StatusBarNotification sbn) {
        String notificationHashKey = getHash(sbn);
        Runnable previousRunnable = (Runnable) this.incomingNotifications.get(notificationHashKey);
        if (previousRunnable != null) {
            sLogger.d("onNotificationPosted: Removing a pending runnable for " + notificationHashKey);
            notifListenerServiceHandler.removeCallbacks(previousRunnable);
        }
    }

    public void removePendingHandlingForGroup(String group) {
        Runnable pendingRunnable = (Runnable) this.pendingGroupNotifications.get(group);
        if (pendingRunnable != null) {
            notifListenerServiceHandler.removeCallbacks(pendingRunnable);
        }
    }

    private boolean isMusicNotification(StatusBarNotification sbn) {
        String packageName = sbn.getPackageName();
        if (GlanceConstants.SPOTIFY.equals(packageName)) {
            return false;
        }
        if (VERSION.SDK_INT >= 21) {
            String category = NotificationCompat.getCategory(sbn.getNotification());
            if (category != null && category.equals("transport")) {
                return true;
            }
        }
        if (sbn.isOngoing() && GlanceConstants.isPackageInGroup(packageName, Group.MUSIC_GROUP)) {
            return true;
        }
        return false;
    }

    public void addActiveMusicApp(String packageName) {
        if (!this.activeMusicApps.contains(packageName)) {
            this.activeMusicApps.add(packageName);
            MusicServiceHandler.getInstance().setLastMusicApp(packageName);
        }
    }

    public void removeActiveMusicApp(String packageName) {
        if (this.activeMusicApps.contains(packageName)) {
            this.activeMusicApps.remove(packageName);
        }
    }

    private static boolean isConnectedToHud() {
        return AppInstance.getInstance().isDeviceConnected();
    }

    private void print(StatusBarNotification sbn) {
        if (sbn != null) {
            Notification notif = sbn.getNotification();
            sLogger.d("################ STATUS BAR NOTIFICATION ###############");
            sLogger.d("Time: " + sbn.getPostTime());
            sLogger.d("ID: " + sbn.getId());
            sLogger.d("Tag: " + sbn.getTag());
            sLogger.d("Package: " + sbn.getPackageName());
            sLogger.d("Ongoing: " + sbn.isOngoing());
            sLogger.d("Clearable: " + sbn.isClearable());
            logNotification(notif, 2);
        }
    }

    private void logNotification(Notification notif, int indentation) {
        sLogger.d(String.format("%" + indentation + "s################ NOTIFICATION", new Object[]{SPACES}));
        sLogger.d(String.format("%" + indentation + "sPriority: %s", new Object[]{SPACES, Integer.valueOf(notif.priority)}));
        sLogger.d(String.format("%" + indentation + "sFlags: %s", new Object[]{SPACES, Integer.valueOf(notif.flags)}));
        sLogger.d(String.format("%" + indentation + "sTicker text: %s", new Object[]{SPACES, notif.tickerText}));
        if (VERSION.SDK_INT >= 21) {
            sLogger.d(String.format("%" + indentation + "sPublic Version: %s", new Object[]{SPACES, notif.publicVersion}));
        }
        String category = NotificationCompat.getCategory(notif);
        String group = NotificationCompat.getGroup(notif);
        String sortKey = NotificationCompat.getSortKey(notif);
        boolean isGroupSummary = NotificationCompat.isGroupSummary(notif);
        sLogger.d(String.format("%" + indentation + "sCategory: %s", new Object[]{SPACES, category}));
        sLogger.d(String.format("%" + indentation + "sGroup: %s", new Object[]{SPACES, group}));
        sLogger.d(String.format("%" + indentation + "sSort Key: %s", new Object[]{SPACES, sortKey}));
        sLogger.d(String.format("%" + indentation + "sIs Group Summary: %s", new Object[]{SPACES, Boolean.valueOf(isGroupSummary)}));
        Bundle extras = NotificationCompat.getExtras(notif);
        sLogger.d(String.format("%" + indentation + "s####### BUNDLE", new Object[]{SPACES}));
        logBundle(indentation + 4, extras);
        sLogger.d(String.format("%" + indentation + "s####### ACTIONS", new Object[]{SPACES}));
        int actionCount = NotificationCompat.getActionCount(notif);
        sLogger.d(String.format("%" + indentation + "sAction Count: %d", new Object[]{SPACES, Integer.valueOf(actionCount)}));
        for (int i = 0; i < actionCount; i++) {
            logAction(indentation, NotificationCompat.getAction(notif, i));
        }
    }

    public void logBundle(int indentation, Bundle extras) {
        if (extras != null && !extras.isEmpty()) {
            for (String key : extras.keySet()) {
                logUnknownObject(indentation, key, extras.get(key));
            }
        }
    }

    public void logUnknownObject(int indentation, String key, Object value) {
        int i;
        if (value == null) {
            sLogger.d(String.format("%" + indentation + "s- %s: null", new Object[]{SPACES, key}));
        } else if (value instanceof Bundle) {
            sLogger.d(String.format("%" + indentation + "s####### %s: \"%s\" (%s)", new Object[]{SPACES, key, value.toString(), value.getClass().getName()}));
            logBundle(indentation + 4, (Bundle) value);
        } else if (value instanceof RemoteInput) {
            logRemoteInput(indentation, (RemoteInput) value);
        } else if (value instanceof Action) {
            logAction(indentation, (Action) value);
        } else if (value instanceof Notification) {
            logNotification((Notification) value, indentation);
        } else if (value instanceof Object[]) {
            sLogger.d(String.format("%" + indentation + "s- %s: \"%s\" (%s)", new Object[]{SPACES, key, value.toString(), value.getClass().getName()}));
            Object[] array = (Object[]) value;
            for (i = 0; i < array.length; i++) {
                logUnknownObject(indentation + 4, key + "[" + i + "]", array[i]);
            }
        } else if (value instanceof List) {
            sLogger.d(String.format("%" + indentation + "s- %s: \"%s\" (%s)", new Object[]{SPACES, key, value.toString(), value.getClass().getName()}));
            List array2 = (List) value;
            for (i = 0; i < array2.size(); i++) {
                logUnknownObject(indentation + 4, key + "[" + i + "]", array2.get(i));
            }
        } else {
            sLogger.d(String.format("%" + indentation + "s- %s: \"%s\" (%s)", new Object[]{SPACES, key, value.toString(), value.getClass().getName()}));
        }
    }

    public void logRemoteInput(int indentation, @NonNull android.app.RemoteInput remoteInput) {
        if (VERSION.SDK_INT >= 20) {
            Builder rib = new Builder(remoteInput.getResultKey());
            rib.setLabel(remoteInput.getLabel());
            rib.setChoices(remoteInput.getChoices());
            rib.setAllowFreeFormInput(remoteInput.getAllowFreeFormInput());
            rib.addExtras(remoteInput.getExtras());
            logRemoteInput(indentation, rib.build());
        }
    }

    public void logRemoteInput(int indentation, @NonNull RemoteInput input) {
        sLogger.d(String.format("%" + indentation + "sInput: %s", new Object[]{SPACES, input.getLabel()}));
        sLogger.d(String.format("%" + indentation + "sAllowFreeFormInput: %s", new Object[]{SPACES, Boolean.valueOf(input.getAllowFreeFormInput())}));
        CharSequence[] choices = input.getChoices();
        if (choices != null) {
            for (CharSequence choice : choices) {
                sLogger.d(String.format("%" + indentation + "s  + Choice: %s", new Object[]{SPACES, choice}));
            }
        }
    }

    public void logAction(int indentation, @NonNull Action action) {
        logAction(indentation, new NotificationCompat.Action.Builder(action.icon, action.title, action.actionIntent).build());
    }

    public void logAction(int indentation, @NonNull NotificationCompat.Action action) {
        int i = 0;
        sLogger.d(String.format("%" + indentation + "s-- %s:", new Object[]{SPACES, action.title}));
        sLogger.d(String.format("%" + (indentation + 4) + "sAction Intent: %s", new Object[]{SPACES, action.actionIntent}));
        if (VERSION.SDK_INT >= 24) {
            sLogger.d(String.format("%" + (indentation + 4) + "sAllow generated replies: %b", new Object[]{SPACES, Boolean.valueOf(action.getAllowGeneratedReplies())}));
        }
        if (VERSION.SDK_INT >= 20) {
            logBundle(indentation + 4, action.getExtras());
            RemoteInput[] remoteInputs = action.getRemoteInputs();
            if (remoteInputs != null) {
                int length = remoteInputs.length;
                while (i < length) {
                    logRemoteInput(indentation + 4, remoteInputs[i]);
                    i++;
                }
            }
        }
    }
}
