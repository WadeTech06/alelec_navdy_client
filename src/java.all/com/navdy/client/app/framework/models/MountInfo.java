package com.navdy.client.app.framework.models;

import com.navdy.client.app.tracking.TrackerConstants.Attributes.InstallAttributes;

public class MountInfo {
    public boolean mediumSupported = true;
    public MountType recommendedMount = MountType.SHORT;
    public boolean shortSupported = true;
    public boolean tallSupported = true;

    public enum MountType {
        SHORT(InstallAttributes.SHORT_MOUNT),
        MEDIUM(InstallAttributes.MEDIUM_MOUNT),
        TALL(InstallAttributes.TALL_MOUNT);
        
        String type;

        private MountType(String type) {
            this.type = type;
        }

        public String getValue() {
            return this.type;
        }

        public static MountType getMountTypeForValue(String value) {
            Object obj = -1;
            switch (value.hashCode()) {
                case -1362194858:
                    if (value.equals(InstallAttributes.SHORT_MOUNT)) {
                        obj = 1;
                        break;
                    }
                    break;
                case -236362033:
                    if (value.equals(InstallAttributes.MEDIUM_MOUNT)) {
                        obj = 2;
                        break;
                    }
                    break;
                case 548719111:
                    if (value.equals(InstallAttributes.TALL_MOUNT)) {
                        obj = 3;
                        break;
                    }
                    break;
            }
            switch (obj) {
                case 2:
                    return MEDIUM;
                case 3:
                    return TALL;
                default:
                    return SHORT;
            }
        }

        public static MountType getMountTypeForName(String name) {
            Object obj = -1;
            switch (name.hashCode()) {
                case -2024701067:
                    if (name.equals("MEDIUM")) {
                        obj = 2;
                        break;
                    }
                    break;
                case 2567341:
                    if (name.equals("TALL")) {
                        obj = 3;
                        break;
                    }
                    break;
                case 79011047:
                    if (name.equals("SMALL")) {
                        obj = 1;
                        break;
                    }
                    break;
            }
            switch (obj) {
                case 2:
                    return MEDIUM;
                case 3:
                    return TALL;
                default:
                    return SHORT;
            }
        }
    }

    public boolean noneOfTheMountsWillWork() {
        return (this.shortSupported || this.mediumSupported || this.tallSupported) ? false : true;
    }
}
