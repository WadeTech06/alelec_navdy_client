package com.navdy.client.app.framework.glances;

import android.app.Notification;
import android.os.Bundle;
import android.service.notification.StatusBarNotification;
import androidx.core.app.NotificationCompat;
import com.navdy.client.app.framework.service.NavdyCustomNotificationListenerService;
import com.navdy.client.app.framework.util.CrashlyticsAppender;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.ui.glances.GlanceUtils;
import com.navdy.service.library.events.glances.CalendarConstants;
import com.navdy.service.library.events.glances.GlanceEvent.Builder;
import com.navdy.service.library.events.glances.GlanceEvent.GlanceType;
import com.navdy.service.library.events.glances.KeyValue;
import com.navdy.service.library.log.Logger;
import java.util.ArrayList;
import java.util.List;

public class CalendarNotificationHandler {
    private static Logger sLogger = NavdyCustomNotificationListenerService.sLogger;

    public static void handleCalendarNotification(StatusBarNotification sbn) {
        String packageName = sbn.getPackageName();
        if (GlanceUtils.isThisGlanceEnabledAsWellAsGlobal(packageName)) {
            Object obj = -1;
            switch (packageName.hashCode()) {
                case -456066902:
                    if (packageName.equals(GlanceConstants.ANDROID_CALENDAR)) {
                        obj = null;
                        break;
                    }
                    break;
                case 578428293:
                    if (packageName.equals(GlanceConstants.GOOGLE_CALENDAR)) {
                        obj = 1;
                        break;
                    }
                    break;
            }
            switch (obj) {
                case null:
                case 1:
                    handleGoogleCalendarNotification(sbn);
                    return;
                default:
                    sLogger.w("calendar notification not handled [" + packageName + "]");
                    return;
            }
        }
    }

    private static void handleGoogleCalendarNotification(StatusBarNotification sbn) {
        String packageName = sbn.getPackageName();
        Notification notification = sbn.getNotification();
        Bundle extras = NotificationCompat.getExtras(notification);
        String title = extras.getString("android.title");
        String bigtext = extras.getString("android.bigText");
        String text = extras.getString("android.text");
        String when = null;
        String location = null;
        if (!StringUtils.isEmptyAfterTrim(bigtext)) {
            text = bigtext;
        }
        if (text != null) {
            int i = text.indexOf("\n");
            if (i != -1) {
                when = text.substring(0, i);
                location = text.substring(i + 1);
            } else if (StringUtils.isEmptyAfterTrim(bigtext)) {
                title = title + CrashlyticsAppender.SEPARATOR + text;
            } else {
                int i2 = text.indexOf(",");
                if (i2 != -1) {
                    when = text.substring(0, i2);
                    location = text.substring(i2 + 1);
                } else {
                    when = text;
                }
            }
        }
        boolean hasLocationAction = false;
        if (GlancesHelper.getActions(notification).contains(GlanceConstants.ACTION_MAP)) {
            hasLocationAction = true;
        } else {
            location = null;
        }
        String id = GlancesHelper.getId();
        if (title != null) {
            title = title.trim();
        }
        if (when != null) {
            when = when.trim();
        }
        if (location != null) {
            location = location.trim();
        }
        sLogger.v("[navdyinfo-gcalendar] title[" + title + "] when[" + when + "] location[" + location + "] hasLocationAction[" + hasLocationAction + "] uuid[" + id + "]");
        List<KeyValue> data = new ArrayList();
        data.add(new KeyValue(CalendarConstants.CALENDAR_TITLE.name(), title));
        data.add(new KeyValue(CalendarConstants.CALENDAR_TIME_STR.name(), when));
        if (hasLocationAction) {
            data.add(new KeyValue(CalendarConstants.CALENDAR_LOCATION.name(), location));
        }
        GlancesHelper.sendEvent(new Builder().glanceType(GlanceType.GLANCE_TYPE_CALENDAR).provider(packageName).id(id).postTime(Long.valueOf(System.currentTimeMillis())).glanceData(data).build());
    }
}
