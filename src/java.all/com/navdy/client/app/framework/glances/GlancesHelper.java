package com.navdy.client.app.framework.glances;

import android.app.Notification;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationCompat.Action;
import com.navdy.client.app.framework.AppInstance;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.service.library.device.RemoteDevice;
import com.navdy.service.library.events.glances.GlanceEvent;
import com.navdy.service.library.events.glances.GlanceEvent.GlanceActions;
import com.navdy.service.library.events.glances.KeyValue;
import com.navdy.service.library.log.Logger;
import com.squareup.wire.Message;
import java.util.HashSet;
import java.util.UUID;

public class GlancesHelper {
    private static final float HUD_VERSION_GLANCE_CHANGE = 0.2f;
    private static Logger sLogger = new Logger(GlancesHelper.class);

    static HashSet<String> getActions(Notification notif) {
        HashSet<String> actions = new HashSet();
        int actionCount = NotificationCompat.getActionCount(notif);
        for (int actionIndex = 0; actionIndex < actionCount; actionIndex++) {
            Action action = NotificationCompat.getAction(notif, actionIndex);
            if (!StringUtils.isEmptyAfterTrim(action.title)) {
                actions.add(action.title.toString());
            }
        }
        return actions;
    }

    public static void sendEvent(GlanceEvent message) {
        printGlance(message);
        RemoteDevice remoteDevice = AppInstance.getInstance().getRemoteDevice();
        if (remoteDevice != null) {
            remoteDevice.postEvent((Message) message);
        }
    }

    public static String getId() {
        return UUID.randomUUID().toString();
    }

    private static void printGlance(GlanceEvent event) {
        sLogger.v("[glance-event] id[" + event.id + "]" + " app[" + event.provider + "]" + " type[" + event.glanceType + "]" + " postTime[" + event.postTime + "]");
        if (event.glanceData == null || event.glanceData.size() == 0) {
            sLogger.v("[glance-event] no data");
        } else {
            for (KeyValue keyValue : event.glanceData) {
                sLogger.v("[glance-event] data key[" + keyValue.key + "] val[" + keyValue.value + "]");
            }
        }
        if (event.actions == null || event.actions.size() == 0) {
            sLogger.v("[glance-event] no actions");
            return;
        }
        for (GlanceActions action : event.actions) {
            sLogger.v("[glance-event] action[" + action + "]");
        }
    }

    public static boolean isHudVersionCompatible(String version) {
        try {
            if (Float.parseFloat(version) >= HUD_VERSION_GLANCE_CHANGE) {
                return true;
            }
            return false;
        } catch (Throwable t) {
            sLogger.e(t);
            return false;
        }
    }
}
