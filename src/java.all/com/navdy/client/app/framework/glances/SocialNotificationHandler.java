package com.navdy.client.app.framework.glances;

import android.app.Notification;
import android.os.Bundle;
import android.service.notification.StatusBarNotification;
import androidx.core.app.NotificationCompat;
import com.navdy.client.app.framework.service.NavdyCustomNotificationListenerService;
import com.navdy.client.app.ui.glances.GlanceUtils;
import com.navdy.service.library.events.glances.GlanceEvent.Builder;
import com.navdy.service.library.events.glances.GlanceEvent.GlanceType;
import com.navdy.service.library.events.glances.KeyValue;
import com.navdy.service.library.events.glances.SocialConstants;
import com.navdy.service.library.log.Logger;
import java.util.ArrayList;
import java.util.List;

public class SocialNotificationHandler {
    private static Logger sLogger = NavdyCustomNotificationListenerService.sLogger;

    public static void handleSocialNotification(StatusBarNotification sbn) {
        String packageName = sbn.getPackageName();
        if (GlanceUtils.isThisGlanceEnabledAsWellAsGlobal(packageName)) {
            Object obj = -1;
            switch (packageName.hashCode()) {
                case 10619783:
                    if (packageName.equals(GlanceConstants.TWITTER)) {
                        obj = 1;
                        break;
                    }
                    break;
                case 714499313:
                    if (packageName.equals(GlanceConstants.FACEBOOK)) {
                        obj = null;
                        break;
                    }
                    break;
            }
            switch (obj) {
                case null:
                    handleFacebookNotification(sbn);
                    return;
                case 1:
                    handleTwitterNotification(sbn);
                    return;
                default:
                    sLogger.w("calendar notification not handled [" + packageName + "]");
                    return;
            }
        }
    }

    private static void handleFacebookNotification(StatusBarNotification sbn) {
        String packageName = sbn.getPackageName();
        Notification notification = sbn.getNotification();
        String event = NotificationCompat.getExtras(notification).getString("android.text");
        sLogger.v("[navdyinfo-facebook] event[" + event + "]");
        String id = GlancesHelper.getId();
        List<KeyValue> data = new ArrayList();
        data.add(new KeyValue(SocialConstants.SOCIAL_MESSAGE.name(), event));
        GlancesHelper.sendEvent(new Builder().glanceType(GlanceType.GLANCE_TYPE_SOCIAL).provider(packageName).id(id).postTime(Long.valueOf(notification.when)).glanceData(data).build());
    }

    private static void handleTwitterNotification(StatusBarNotification sbn) {
        String packageName = sbn.getPackageName();
        Notification notification = sbn.getNotification();
        Bundle extras = NotificationCompat.getExtras(notification);
        String sender = extras.getString("android.title");
        String message = extras.getString("android.text");
        String to = null;
        if (message != null && message.startsWith(GlanceConstants.EMAIL_AT)) {
            int index = message.indexOf(" ");
            if (index != -1) {
                to = message.substring(0, index);
                message = message.substring(index + 1);
            }
        }
        sLogger.v("[navdyinfo-twitter] sender[" + sender + "] to[" + to + "] message[" + message + "]");
        String id = GlancesHelper.getId();
        List<KeyValue> data = new ArrayList();
        data.add(new KeyValue(SocialConstants.SOCIAL_FROM.name(), sender));
        data.add(new KeyValue(SocialConstants.SOCIAL_TO.name(), to));
        data.add(new KeyValue(SocialConstants.SOCIAL_MESSAGE.name(), message));
        GlancesHelper.sendEvent(new Builder().glanceType(GlanceType.GLANCE_TYPE_SOCIAL).provider(packageName).id(id).postTime(Long.valueOf(notification.when)).glanceData(data).build());
    }
}
