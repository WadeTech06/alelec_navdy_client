package com.navdy.client.app.framework.map;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import java.util.ArrayList;
import java.util.List;

public class MapsForWorkUtils {
    public static LatLng m4bToGms(LatLng latLng) {
        if (latLng == null) {
            return null;
        }
        return new LatLng(latLng.latitude, latLng.longitude);
    }

    public static LatLng gmsToM4b(LatLng latLng) {
        if (latLng == null) {
            return null;
        }
        return new LatLng(latLng.latitude, latLng.longitude);
    }

    public static LatLngBounds m4bToGms(LatLngBounds latLngBounds) {
        if (latLngBounds == null) {
            return null;
        }
        return new LatLngBounds(new LatLng(latLngBounds.southwest.latitude, latLngBounds.southwest.longitude), new LatLng(latLngBounds.northeast.latitude, latLngBounds.northeast.longitude));
    }

    public static LatLngBounds gmsToM4b(LatLngBounds latLngBounds) {
        if (latLngBounds == null) {
            return null;
        }
        return new LatLngBounds(new LatLng(latLngBounds.southwest.latitude, latLngBounds.southwest.longitude), new LatLng(latLngBounds.northeast.latitude, latLngBounds.northeast.longitude));
    }

    public static List<LatLng> gmsToM4b(List<LatLng> points) {
        if (points == null) {
            return null;
        }
        List<LatLng> m4bPoints = new ArrayList(points.size());
        for (LatLng point : points) {
            m4bPoints.add(gmsToM4b(point));
        }
        return m4bPoints;
    }

    public static List<LatLng> m4bToGms(List<LatLng> points) {
        if (points == null) {
            return null;
        }
        List<LatLng> m4bPoints = new ArrayList(points.size());
        for (LatLng point : points) {
            m4bPoints.add(m4bToGms(point));
        }
        return m4bPoints;
    }
}
