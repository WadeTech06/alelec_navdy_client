package com.navdy.client.app.framework.map;

import android.content.Context;
import android.os.SystemClock;
import androidx.annotation.WorkerThread;
import com.alelec.navdyclient.R;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.PathManager;
import com.navdy.client.app.framework.util.PackagedResource;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.framework.util.SystemUtils;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.util.IOUtils;
import java.io.File;
import java.util.List;

public class HereMapsConfigurator {
    private static final HereMapsConfigurator sInstance = new HereMapsConfigurator();
    private static final Logger sLogger = new Logger(HereMapsConfigurator.class);
    private volatile boolean isAlreadyConfigured;

    public static HereMapsConfigurator getInstance() {
        return sInstance;
    }

    @WorkerThread
    public synchronized void updateMapsConfig() {
        if (this.isAlreadyConfigured) {
            sLogger.i("MWConfig is already configured");
        } else if (PathManager.getInstance().hasDiskCacheDir()) {
            SystemUtils.ensureNotOnMainThread();
            Context context = NavdyApplication.getAppContext();
            List<String> hereMapsConfigFolders = PathManager.getInstance().getHereMapsConfigDirs();
            String latestHereMapConfigFolder = PathManager.getInstance().getLatestHereMapsConfigPath();
            PackagedResource mwConfigLatest = new PackagedResource("mwconfig_client", latestHereMapConfigFolder);
            try {
                sLogger.i("MWConfig: starting");
                long l1 = SystemClock.elapsedRealtime();
                mwConfigLatest.updateFromResources(context, R.raw.mwconfig_client, R.raw.mwconfig_client_checksum);
                removeOldHereMapsFolders(hereMapsConfigFolders, latestHereMapConfigFolder);
                sLogger.i("MWConfig: time took " + (SystemClock.elapsedRealtime() - l1));
                this.isAlreadyConfigured = true;
            } catch (Throwable throwable) {
                sLogger.e("MWConfig error", throwable);
            }
        } else {
            sLogger.i("diskcache does not exist");
        }
        return;
    }

    private void removeOldHereMapsFolders(List<String> hereMapsConfigFolders, String latestHereMapConfigFolder) {
        for (String hereMapsConfigFolder : hereMapsConfigFolders) {
            if (!StringUtils.equalsOrBothEmptyAfterTrim(hereMapsConfigFolder, latestHereMapConfigFolder)) {
                IOUtils.deleteDirectory(NavdyApplication.getAppContext(), new File(hereMapsConfigFolder));
            }
        }
    }
}
