package com.navdy.client.app.framework.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import com.navdy.client.app.framework.AppInstance;
import com.navdy.client.debug.util.Contacts;
import com.navdy.service.library.device.RemoteDevice;
import com.navdy.service.library.events.Ext_NavdyEvent;
import com.navdy.service.library.events.NavdyEvent;
import com.navdy.service.library.events.NavdyEvent.MessageType;
import com.navdy.service.library.events.callcontrol.PhoneEvent;
import com.navdy.service.library.events.callcontrol.PhoneEvent.Builder;
import com.navdy.service.library.events.callcontrol.PhoneStatus;
import com.navdy.service.library.log.Logger;

public class PhoneCallReceiver extends BroadcastReceiver {
    private static PhoneEvent currentStatus;
    private static final Logger sLogger = new Logger(PhoneCallReceiver.class);

    public void onReceive(Context context, Intent intent) {
        if (intent != null) {
            try {
                String action = intent.getAction();
                if (action.equals("android.intent.action.NEW_OUTGOING_CALL")) {
                    String phoneNumber = getResultData();
                    if (phoneNumber == null) {
                        phoneNumber = intent.getStringExtra("android.intent.extra.PHONE_NUMBER");
                    }
                    sLogger.d("New outgoing call " + phoneNumber);
                    sendEvent(context, buildPhoneEvent(context, phoneNumber, PhoneStatus.PHONE_DIALING));
                } else if (action.equals("android.intent.action.PHONE_STATE")) {
                    Bundle bundle = intent.getExtras();
                    if (bundle != null) {
                        String stateStr = bundle.getString("state");
                        String number = bundle.getString("incoming_number");
                        PhoneStatus status = PhoneStatus.PHONE_IDLE;
                        sLogger.d("Phone state change: " + stateStr + " - " + number);
                        if (TelephonyManager.EXTRA_STATE_IDLE.equals(stateStr)) {
                            status = PhoneStatus.PHONE_IDLE;
                        } else if (TelephonyManager.EXTRA_STATE_OFFHOOK.equals(stateStr)) {
                            status = PhoneStatus.PHONE_OFFHOOK;
                        } else if (TelephonyManager.EXTRA_STATE_RINGING.equals(stateStr)) {
                            status = PhoneStatus.PHONE_RINGING;
                        }
                        currentStatus = buildPhoneEvent(context, number, status);
                        sendEvent(context, currentStatus);
                        return;
                    }
                    sLogger.i("no extras for phone event");
                }
            } catch (Throwable t) {
                sLogger.e(t);
            }
        }
    }

    private PhoneEvent buildPhoneEvent(Context context, String number, PhoneStatus status) {
        String name = "";
        if (number != null) {
            name = Contacts.lookupNameFromPhoneNumber(context, number);
        }
        return new Builder().number(number).status(status).contact_name(name).build();
    }

    private void sendEvent(Context context, PhoneEvent message) {
        NavdyEvent event = new NavdyEvent.Builder().type(MessageType.PhoneEvent).setExtension(Ext_NavdyEvent.phoneEvent, (Object) message).build();
        RemoteDevice device = AppInstance.getInstance().getRemoteDevice();
        if (device != null) {
            device.postEvent(event);
        }
    }

    public static PhoneEvent getCurrentStatus() {
        return currentStatus;
    }
}
