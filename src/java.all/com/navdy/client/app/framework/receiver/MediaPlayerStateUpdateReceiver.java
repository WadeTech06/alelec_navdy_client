package com.navdy.client.app.framework.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.navdy.client.app.framework.glances.GlanceConstants;
import com.navdy.client.app.framework.servicehandler.MusicServiceHandler;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.service.library.log.Logger;

public class MediaPlayerStateUpdateReceiver extends BroadcastReceiver {
    private static final String INTENT_PARAM_PLAYING = "playing";
    public static final Logger sLogger = new Logger(MediaPlayerStateUpdateReceiver.class);

    public void onReceive(Context context, Intent intent) {
        sLogger.d("onReceive, intent: " + intent);
        String action = intent.getAction();
        if (!StringUtils.isEmptyAfterTrim(action)) {
            String packageName = action.substring(0, action.lastIndexOf(46));
            if (GlanceConstants.SPOTIFY.equals(packageName) && intent.getBooleanExtra(INTENT_PARAM_PLAYING, false)) {
                MusicServiceHandler.getInstance().setLastMusicApp(packageName);
            }
        }
    }
}
