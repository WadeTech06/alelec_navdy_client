package com.navdy.client.app.providers;

import android.database.Cursor;
import androidx.annotation.Nullable;
import com.navdy.client.app.framework.util.StringUtils;

public class DbUtils {
    private static boolean areValidParams(Cursor cursor, String columnName) {
        return (cursor == null || StringUtils.isEmptyAfterTrim(columnName)) ? false : true;
    }

    @Nullable
    public static String getString(@Nullable Cursor cursor, @Nullable String columnName) {
        if (areValidParams(cursor, columnName)) {
            return cursor.getString(cursor.getColumnIndex(columnName));
        }
        return null;
    }

    public static int getInt(@Nullable Cursor cursor, @Nullable String columnName) {
        if (areValidParams(cursor, columnName)) {
            return cursor.getInt(cursor.getColumnIndex(columnName));
        }
        return 0;
    }

    public static long getLong(@Nullable Cursor cursor, @Nullable String columnName) {
        if (areValidParams(cursor, columnName)) {
            return cursor.getLong(cursor.getColumnIndex(columnName));
        }
        return 0;
    }

    public static boolean getBoolean(@Nullable Cursor cursor, @Nullable String columnName) {
        if (areValidParams(cursor, columnName) && cursor.getInt(cursor.getColumnIndex(columnName)) > 0) {
            return true;
        }
        return false;
    }
}
