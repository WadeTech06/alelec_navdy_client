package com.navdy.client.debug;

import android.app.ListFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import butterknife.ButterKnife;
import com.alelec.navdyclient.R;
import com.navdy.client.app.framework.AppInstance;
import com.navdy.service.library.device.RemoteDevice;
import com.navdy.service.library.events.notification.ShowCustomNotification;
import com.navdy.service.library.log.Logger;
import com.squareup.wire.Message;
import java.util.ArrayList;
import java.util.Arrays;

public class CustomNotificationFragment extends ListFragment {
    private static final Logger sLogger = new Logger(CustomNotificationFragment.class);
    AppInstance mAppInstance = AppInstance.getInstance();
    protected ArrayList<ListItem> mListItems;

    enum ListItem {
        INCOMING_PHONE_CALL_323("Incoming phone 323-222-1111"),
        END_PHONE_CALL_323("End phone 323-222-1111"),
        PHONE_BATTERY_LOW("Phone battery Low"),
        PHONE_BATTERY_EXTREMELY_LOW("Phone battery Extremely Low"),
        PHONE_BATTERY_OK("Phone battery ok"),
        TEXT_NOTIFICAION_WITH_REPLY_408("Text message 408-111-1111"),
        TEXT_NOTIFICAION_WITH_REPLY_999("Text message 999-999-0999"),
        TEXT_NOTIFICAION_WITH_NO_REPLY_510("Text message 510-454-4444"),
        DIAL_CONNECTED("Dial Connected"),
        DIAL_DISCONNECTED("Dial Disconnected"),
        DIAL_FORGOTTEN_SINGLE("Dial Forgotten Single"),
        DIAL_FORGOTTEN_MULTIPLE("Dial Forgotten Multiple"),
        DIAL_BATTERY_LOW("Dial battery Low"),
        DIAL_BATTERY_EXTREMELY_LOW("Dial battery Extremely Low"),
        DIAL_BATTERY_VERY_LOW("Dial battery Very Low"),
        DIAL_BATTERY_OK("Dial battery OK"),
        MUSIC("Music notification"),
        VOICE_ASSIST("Voice assistance"),
        BRIGHTNESS("Brightness"),
        TRAFFIC_REROUTE("Traffic reroute"),
        CLEAR_TRAFFIC_REROUTE("Clear Traffic reroute"),
        TRAFFIC_JAM("Traffic Jam"),
        CLEAR_TRAFFIC_JAM("Clear Traffic Jam"),
        TRAFFIC_INCIDENT("Traffic Incident"),
        CLEAR_TRAFFIC_INCIDENT("Clear Traffic Incident"),
        TRAFFIC_DELAY("Traffic Delay"),
        CLEAR_TRAFFIC_DELAY("Clear Traffic Delay"),
        PHONE_DISCONNECTED("Phone disconnected"),
        PHONE_CONNECTED("Phone connected"),
        PHONE_APP_DISCONNECTED("App disconnected"),
        CLEAR_CURRENT_TOAST("Clear current toast"),
        CLEAR_ALL_TOAST("Clear all toast"),
        CLEAR_ALL_TOAST_AND_CURRENT("Clear all toast + current"),
        GOOGLE_CALENDAR_1("Google Calendar"),
        APPLE_CALENDAR_1("Apple Calendar"),
        LOW_FUEL_LEVEL("Low fuel Level Glance"),
        LOW_FUEL_LEVEL_NOCHECK("Low fuel Level Glance - no preference check"),
        LOW_FUEL_LEVEL_CLEAR("Clear Low Fuel Level Glance"),
        TEST_OBD_LOW_FUEL("Test OBD Low Fuel Level"),
        FIND_GAS_STATION("Find Gas Stations in vicinity"),
        FIND_ROUTE_TO_CLOSEST_GAS_STATION("Find Route to nearest Gas Station"),
        CLEAR_TEST_OBD_LOW_FUEL("Clear Test OBD Low Fuel Level"),
        FUEL_ADDED_TEST("Test for fuel added after going to a gas station"),
        GENERIC_1("Generic 1"),
        GENERIC_2("Generic 2");
        
        private final String mText;

        private ListItem(String item) {
            this.mText = item;
        }

        public String toString() {
            return this.mText;
        }
    }

    public void onCreate(Bundle savedState) {
        super.onCreate(savedState);
        this.mListItems = new ArrayList(Arrays.asList(ListItem.values()));
        setListAdapter(new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_activated_1, android.R.id.text1, ListItem.values()));
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_test_screen, container, false);
        ButterKnife.bind((Object) this, rootView);
        return rootView;
    }

    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        l.setItemChecked(position, false);
        switch ((ListItem) this.mListItems.get(position)) {
            case INCOMING_PHONE_CALL_323:
                sendEvent(new ShowCustomNotification("INCOMING_PHONE_CALL_323"));
                return;
            case END_PHONE_CALL_323:
                sendEvent(new ShowCustomNotification("END_PHONE_CALL_323"));
                return;
            case PHONE_BATTERY_LOW:
                sendEvent(new ShowCustomNotification("PHONE_BATTERY_LOW"));
                return;
            case PHONE_BATTERY_EXTREMELY_LOW:
                sendEvent(new ShowCustomNotification("PHONE_BATTERY_EXTREMELY_LOW"));
                return;
            case PHONE_BATTERY_OK:
                sendEvent(new ShowCustomNotification("PHONE_BATTERY_OK"));
                return;
            case TEXT_NOTIFICAION_WITH_REPLY_408:
                sendEvent(new ShowCustomNotification("TEXT_NOTIFICAION_WITH_REPLY_408"));
                return;
            case TEXT_NOTIFICAION_WITH_REPLY_999:
                sendEvent(new ShowCustomNotification("TEXT_NOTIFICAION_WITH_REPLY_999"));
                return;
            case TEXT_NOTIFICAION_WITH_NO_REPLY_510:
                sendEvent(new ShowCustomNotification("TEXT_NOTIFICAION_WITH_NO_REPLY_510"));
                return;
            case DIAL_CONNECTED:
                sendEvent(new ShowCustomNotification("DIAL_CONNECTED"));
                return;
            case DIAL_DISCONNECTED:
                sendEvent(new ShowCustomNotification("DIAL_DISCONNECTED"));
                return;
            case DIAL_FORGOTTEN_SINGLE:
                sendEvent(new ShowCustomNotification("DIAL_FORGOTTEN_SINGLE"));
                return;
            case DIAL_FORGOTTEN_MULTIPLE:
                sendEvent(new ShowCustomNotification("DIAL_FORGOTTEN_MULTIPLE"));
                return;
            case DIAL_BATTERY_LOW:
                sendEvent(new ShowCustomNotification("DIAL_BATTERY_LOW"));
                return;
            case DIAL_BATTERY_EXTREMELY_LOW:
                sendEvent(new ShowCustomNotification("DIAL_BATTERY_EXTREMELY_LOW"));
                return;
            case DIAL_BATTERY_VERY_LOW:
                sendEvent(new ShowCustomNotification("DIAL_BATTERY_VERY_LOW"));
                return;
            case DIAL_BATTERY_OK:
                sendEvent(new ShowCustomNotification("DIAL_BATTERY_OK"));
                return;
            case MUSIC:
                sendEvent(new ShowCustomNotification("MUSIC"));
                return;
            case VOICE_ASSIST:
                sendEvent(new ShowCustomNotification("VOICE_ASSIST"));
                return;
            case BRIGHTNESS:
                sendEvent(new ShowCustomNotification("BRIGHTNESS"));
                return;
            case TRAFFIC_REROUTE:
                sendEvent(new ShowCustomNotification("TRAFFIC_REROUTE"));
                return;
            case CLEAR_TRAFFIC_REROUTE:
                sendEvent(new ShowCustomNotification("CLEAR_TRAFFIC_REROUTE"));
                return;
            case TRAFFIC_JAM:
                sendEvent(new ShowCustomNotification("TRAFFIC_JAM"));
                return;
            case CLEAR_TRAFFIC_JAM:
                sendEvent(new ShowCustomNotification("CLEAR_TRAFFIC_JAM"));
                return;
            case TRAFFIC_INCIDENT:
                sendEvent(new ShowCustomNotification("TRAFFIC_INCIDENT"));
                return;
            case CLEAR_TRAFFIC_INCIDENT:
                sendEvent(new ShowCustomNotification("CLEAR_TRAFFIC_INCIDENT"));
                return;
            case TRAFFIC_DELAY:
                sendEvent(new ShowCustomNotification("TRAFFIC_DELAY"));
                return;
            case CLEAR_TRAFFIC_DELAY:
                sendEvent(new ShowCustomNotification("CLEAR_TRAFFIC_DELAY"));
                return;
            case PHONE_CONNECTED:
                sendEvent(new ShowCustomNotification("PHONE_CONNECTED"));
                return;
            case PHONE_DISCONNECTED:
                sendEvent(new ShowCustomNotification("PHONE_DISCONNECTED"));
                return;
            case PHONE_APP_DISCONNECTED:
                sendEvent(new ShowCustomNotification("PHONE_APP_DISCONNECTED"));
                return;
            case CLEAR_ALL_TOAST:
                sendEvent(new ShowCustomNotification("CLEAR_ALL_TOAST"));
                return;
            case CLEAR_ALL_TOAST_AND_CURRENT:
                sendEvent(new ShowCustomNotification("CLEAR_ALL_TOAST_AND_CURRENT"));
                return;
            case CLEAR_CURRENT_TOAST:
                sendEvent(new ShowCustomNotification("CLEAR_CURRENT_TOAST"));
                return;
            case GOOGLE_CALENDAR_1:
                sendEvent(new ShowCustomNotification("GOOGLE_CALENDAR_1"));
                return;
            case APPLE_CALENDAR_1:
                sendEvent(new ShowCustomNotification("APPLE_CALENDAR_1"));
                return;
            case LOW_FUEL_LEVEL:
                sendEvent(new ShowCustomNotification("LOW_FUEL_LEVEL"));
                return;
            case LOW_FUEL_LEVEL_NOCHECK:
                sendEvent(new ShowCustomNotification("LOW_FUEL_LEVEL_NOCHECK"));
                return;
            case LOW_FUEL_LEVEL_CLEAR:
                sendEvent(new ShowCustomNotification("LOW_FUEL_LEVEL_CLEAR"));
                return;
            case TEST_OBD_LOW_FUEL:
                sendEvent(new ShowCustomNotification("OBD_LOW_FUEL_LEVEL"));
                return;
            case CLEAR_TEST_OBD_LOW_FUEL:
                sendEvent(new ShowCustomNotification("CLEAR_OBD_LOW_FUEL_LEVEL"));
                return;
            case FUEL_ADDED_TEST:
                sendEvent(new ShowCustomNotification("FUEL_ADDED_TEST"));
                return;
            case FIND_GAS_STATION:
                sendEvent(new ShowCustomNotification("FIND_GAS_STATION"));
                return;
            case FIND_ROUTE_TO_CLOSEST_GAS_STATION:
                sendEvent(new ShowCustomNotification("FIND_ROUTE_TO_CLOSEST_GAS_STATION"));
                return;
            case GENERIC_1:
                sendEvent(new ShowCustomNotification("GENERIC_1"));
                return;
            case GENERIC_2:
                sendEvent(new ShowCustomNotification("GENERIC_2"));
                return;
            default:
                sLogger.d("unhandled: " + id);
                return;
        }
    }

    private void sendEvent(Message event) {
        RemoteDevice remoteDevice = this.mAppInstance.getRemoteDevice();
        if (remoteDevice != null) {
            remoteDevice.postEvent(event);
        }
    }
}
