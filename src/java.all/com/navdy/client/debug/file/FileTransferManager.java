package com.navdy.client.debug.file;

import android.content.Context;
import com.navdy.service.library.events.file.FileTransferError;
import com.navdy.service.library.events.file.FileTransferResponse;
import com.navdy.service.library.events.file.FileTransferStatus;
import com.navdy.service.library.events.file.FileType;
import com.navdy.service.library.file.TransferDataSource;
import com.navdy.service.library.log.Logger;
import java.io.File;

public abstract class FileTransferManager {
    protected static final Logger sLogger = new Logger(FileTransferManager.class);
    protected final Context mContext;
    protected final TransferDataSource mDataSource;
    protected final String mDestinationFileName;
    protected final String mDestinationFolder;
    protected final FileTransferListener mFileTransferListener;
    protected final FileType mFileType;

    public interface FileTransferListener {
        void onError(FileTransferError fileTransferError, String str);

        void onFileTransferResponse(FileTransferResponse fileTransferResponse);

        void onFileTransferStatus(FileTransferStatus fileTransferStatus);
    }

    public abstract void cancelFileUpload();

    public abstract boolean pullFile();

    public abstract boolean sendFile();

    public abstract boolean sendFile(long j);

    public FileTransferManager(Context context, FileType fileType, String destinationFolder, FileTransferListener listener) {
        this.mContext = context;
        this.mFileType = fileType;
        this.mFileTransferListener = listener;
        this.mDataSource = null;
        this.mDestinationFileName = null;
        this.mDestinationFolder = destinationFolder;
    }

    public FileTransferManager(Context context, TransferDataSource source, FileType fileType, String destinationFileName, FileTransferListener fileTransferListener) {
        this.mContext = context;
        this.mFileTransferListener = fileTransferListener;
        File file = source.getFile();
        if (!(file == null || file.exists())) {
            postOnError(null, "File does not exist");
        }
        this.mDataSource = source;
        this.mFileType = fileType;
        this.mDestinationFileName = destinationFileName;
        this.mDestinationFolder = null;
    }

    protected void postOnFileTransferResponse(FileTransferResponse response) {
        try {
            this.mFileTransferListener.onFileTransferResponse(response);
        } catch (Throwable t) {
            sLogger.e("Bad listener : onFileTransferResponse ", t);
        }
    }

    protected void postOnFileTransferStatus(FileTransferStatus status) {
        try {
            this.mFileTransferListener.onFileTransferStatus(status);
        } catch (Throwable t) {
            sLogger.e("Bad listener : onFileTransferStatus ", t);
        }
    }

    protected void postOnError(FileTransferError errorCode, String error) {
        try {
            this.mFileTransferListener.onError(errorCode, error);
        } catch (Throwable t) {
            sLogger.e("Bad listener : onError ", t);
        }
    }
}
