package com.navdy.client.debug.file;

import android.content.Context;
import com.navdy.client.app.framework.DeviceConnection;
import com.navdy.client.app.framework.DeviceConnection.DeviceDisconnectedEvent;
import com.navdy.client.app.framework.util.BusProvider;
import com.navdy.client.debug.file.FileTransferManager.FileTransferListener;
import com.navdy.client.debug.util.S3Constants;
import com.navdy.service.library.events.file.FileTransferData;
import com.navdy.service.library.events.file.FileTransferError;
import com.navdy.service.library.events.file.FileTransferRequest.Builder;
import com.navdy.service.library.events.file.FileTransferResponse;
import com.navdy.service.library.events.file.FileTransferStatus;
import com.navdy.service.library.events.file.FileType;
import com.navdy.service.library.file.FileTransferSessionManager;
import com.navdy.service.library.file.TransferDataSource;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.util.IOUtils;
import com.squareup.otto.Subscribe;
import com.squareup.wire.Message;
import com.squareup.wire.Wire;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ConcurrentModificationException;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class RemoteFileTransferManager extends FileTransferManager {
    private static final long CHUNK_TIMEOUT = 30000;
    private static final boolean FLOW_CONTROL_ENABLED = true;
    private static final long INITIAL_REQUEST_TIMEOUT = 10000;
    private static final Logger sLogger = new Logger("HUDFileTransferManager");
    private long mBytesTransferredSoFar = 0;
    private int mChunkId = -1;
    private int mChunkSize = 0;
    private volatile boolean mFileUploadCancelled = false;
    private FileOutputStream mFileWriter;
    private boolean mIsDone = false;
    private final Object mRemoteDeviceLock = new Object();
    private long mRequestTimeout;
    private ExecutorService mSerialExecutor;
    private boolean mServerSupportsFlowControl = false;
    private long mStartOffset = 0;
    private int mTransferId = -1;
    private Timer timeoutTimer;

    public RemoteFileTransferManager(Context context, FileType fileType, String destinationFolder, long requestTimeOut, FileTransferListener fileTransferListener) {
        super(context, fileType, destinationFolder, fileTransferListener);
        this.mRequestTimeout = requestTimeOut;
        this.mSerialExecutor = Executors.newSingleThreadExecutor();
        BusProvider.getInstance().register(this);
    }

    public RemoteFileTransferManager(Context context, File fileToSend, FileType fileType, String destinationFileName, FileTransferListener fileTransferListener) {
        super(context, TransferDataSource.fileSource(fileToSend), fileType, destinationFileName, fileTransferListener);
        BusProvider.getInstance().register(this);
    }

    public RemoteFileTransferManager(Context context, FileType fileType, long dataLength, FileTransferListener fileTransferListener) {
        super(context, TransferDataSource.testSource(dataLength), fileType, null, fileTransferListener);
        BusProvider.getInstance().register(this);
    }

    public boolean sendFile() {
        return sendFile(0);
    }

    public boolean sendFile(long offset) {
        sLogger.i(String.format("Sending file %s (%sB) to the HUD", new Object[]{this.mDataSource.getName(), Long.valueOf(this.mDataSource.length())}));
        long fileLength = this.mDataSource.length();
        if (offset >= fileLength) {
            offset = fileLength;
        }
        this.mStartOffset = offset;
        boolean result = sendFileTransferRequest(offset, false);
        setTimer(INITIAL_REQUEST_TIMEOUT, "Timeout while waiting for initial file transfer response");
        return result;
    }

    public boolean pullFile() {
        Message fileTransferRequest = new Builder().fileType(this.mFileType).supportsAcks(Boolean.valueOf(true)).build();
        this.mChunkId = -1;
        this.mBytesTransferredSoFar = 0;
        setTimer(this.mRequestTimeout, "Display did not respond in time");
        if (DeviceConnection.isConnected()) {
            return DeviceConnection.postEvent(fileTransferRequest);
        }
        postOnError(FileTransferError.FILE_TRANSFER_IO_ERROR, "Phone is not connected to the Navdy display");
        return false;
    }

    public void cancelFileUpload() {
        sLogger.d("File upload cancel called");
        synchronized (this.mRemoteDeviceLock) {
            if (!this.mFileUploadCancelled) {
                this.mFileUploadCancelled = true;
                cancelTimeoutTimer();
                done();
            }
        }
    }

    private boolean sendFileTransferRequest(long offset, boolean override) {
        return postEvent(new Builder().fileType(this.mFileType).destinationFileName(this.mDestinationFileName).offset(Long.valueOf(offset)).fileDataChecksum(this.mDataSource.checkSum(offset)).fileSize(Long.valueOf(this.mDataSource.length())).override(Boolean.valueOf(override)).build());
    }

    private boolean postEvent(Message message) {
        synchronized (this.mRemoteDeviceLock) {
            if (this.mFileUploadCancelled) {
                sLogger.d("Not sending the event to the HUD as the file upload is cancelled");
                return false;
            }
            boolean postEvent = DeviceConnection.postEvent(message);
            return postEvent;
        }
    }

    private void done() {
        if (!this.mIsDone) {
            BusProvider.getInstance().unregister(this);
        }
        this.mIsDone = true;
    }

    private void doneWithError(FileTransferError errorCode, String error) {
        done();
        postOnError(errorCode, error);
    }

    private void setTimer(long timeout, final String timeoutMessage) {
        try {
            buildTimerInstance();
        } catch (ConcurrentModificationException e) {
            doneWithError(null, e.getMessage());
        }
        this.timeoutTimer.schedule(new TimerTask() {
            public void run() {
                RemoteFileTransferManager.this.doneWithError(null, timeoutMessage);
            }
        }, timeout);
    }

    private synchronized void buildTimerInstance() throws ConcurrentModificationException {
        if (this.timeoutTimer != null) {
            throw new ConcurrentModificationException("Multiple instances of timeout timer are not allowed");
        }
        this.timeoutTimer = new Timer();
    }

    private synchronized void cancelTimeoutTimer() {
        if (this.timeoutTimer != null) {
            this.timeoutTimer.cancel();
            this.timeoutTimer.purge();
            this.timeoutTimer = null;
        } else {
            sLogger.w("Timeout timer for file transfer was null while trying to cancel it");
        }
    }

    @Subscribe
    public void onFileTransferResponse(FileTransferResponse response) {
        if (!this.mFileUploadCancelled && response.fileType == this.mFileType) {
            this.mServerSupportsFlowControl = ((Boolean) Wire.get(response.supportsAcks, FileTransferResponse.DEFAULT_SUPPORTSACKS)).booleanValue();
            if (this.mDestinationFileName != null) {
                if (response.destinationFileName != null && response.destinationFileName.equals(this.mDestinationFileName)) {
                    sLogger.d("Response received " + response.fileType + " " + response.destinationFileName);
                } else {
                    return;
                }
            }
            cancelTimeoutTimer();
            postOnFileTransferResponse(response);
            if (!response.success.booleanValue()) {
                postOnError(response.error, "File transfer response error");
            } else if (FileTransferSessionManager.isPullRequest(response.fileType)) {
                this.mTransferId = response.transferId.intValue();
                String destinationFileName = response.destinationFileName;
                this.mChunkSize = response.maxChunkSize.intValue();
                File destinationFile = new File(this.mDestinationFolder, destinationFileName);
                if (destinationFile.exists() && !destinationFile.delete()) {
                    sLogger.e("Unable to delete destination file: " + this.mDestinationFolder + S3Constants.S3_FILE_DELIMITER + destinationFileName);
                }
                try {
                    if (destinationFile.createNewFile()) {
                        this.mFileWriter = new FileOutputStream(destinationFile);
                        setTimer(30000, "Failed to receive chunks in time");
                        return;
                    }
                    postOnError(FileTransferError.FILE_TRANSFER_PERMISSION_DENIED, "Cannot create the file");
                } catch (Exception e) {
                    postOnError(FileTransferError.FILE_TRANSFER_IO_ERROR, "Exception while creating the file");
                }
            } else {
                this.mTransferId = response.transferId.intValue();
                long negotiatedOffset = response.offset.longValue();
                this.mChunkSize = response.maxChunkSize.intValue();
                if (negotiatedOffset != this.mStartOffset) {
                    boolean override = false;
                    if (negotiatedOffset == 0) {
                        this.mStartOffset = 0;
                    } else {
                        if (negotiatedOffset < 0 || negotiatedOffset > this.mDataSource.length()) {
                            override = true;
                        } else if (!this.mDataSource.checkSum(negotiatedOffset).equals(response.checksum)) {
                            override = true;
                        }
                        if (override) {
                            negotiatedOffset = 0;
                        }
                        this.mStartOffset = negotiatedOffset;
                    }
                    if (override) {
                        sLogger.d("Overriding the negotiated offset as the file may be corrupted");
                        sendFileTransferRequest(this.mStartOffset, true);
                        return;
                    }
                }
                this.mBytesTransferredSoFar = this.mStartOffset;
                if (this.mStartOffset <= this.mDataSource.length()) {
                    sendNextChunk();
                }
            }
        }
    }

    @Subscribe
    public void onFileTransferStatus(FileTransferStatus fileTransferStatus) {
        if (!this.mFileUploadCancelled && fileTransferStatus.transferId != null && fileTransferStatus.transferId.equals(Integer.valueOf(this.mTransferId))) {
            sLogger.d("File transfer status received ");
            cancelTimeoutTimer();
            postOnFileTransferStatus(fileTransferStatus);
            if (fileTransferStatus.success.booleanValue() && fileTransferStatus.chunkIndex.intValue() == this.mChunkId) {
                this.mBytesTransferredSoFar = fileTransferStatus.totalBytesTransferred.longValue();
                if (fileTransferStatus.transferComplete.booleanValue()) {
                    done();
                    return;
                }
                if (this.mDataSource != null) {
                    File file = this.mDataSource.getFile();
                    if (TransferDataSource.TEST_DATA_NAME.equals(this.mDataSource.getName()) || (file != null && file.exists() && file.length() > 0)) {
                        sendNextChunk();
                        return;
                    }
                }
                postOnError(fileTransferStatus.error, "File transfer status error: File empty or does not exist anymore.");
                return;
            }
            postOnError(fileTransferStatus.error, "File transfer status error: Invalid chunk ID");
        }
    }

    protected void postOnError(FileTransferError errorCode, String error) {
        super.postOnError(errorCode, error);
        done();
    }

    @Subscribe
    public void onFileTransferData(final FileTransferData data) {
        if (!this.mFileUploadCancelled && data.transferId != null && data.transferId.equals(Integer.valueOf(this.mTransferId))) {
            boolean illegalChunk;
            sLogger.d("Transfer data received");
            cancelTimeoutTimer();
            if (data.chunkIndex.intValue() != this.mChunkId + 1) {
                illegalChunk = true;
            } else {
                illegalChunk = false;
            }
            if (illegalChunk) {
                sLogger.e("Illegal chuck received from HUD Expected :" + this.mChunkId + 1 + ", Received :" + data.chunkIndex);
            }
            this.mBytesTransferredSoFar = (!illegalChunk ? (long) data.dataBytes.size() : 0) + this.mBytesTransferredSoFar;
            this.mChunkId = data.chunkIndex.intValue();
            if (this.mServerSupportsFlowControl) {
                FileTransferStatus.Builder builder = new FileTransferStatus.Builder();
                builder.transferId(data.transferId);
                if (illegalChunk) {
                    builder.success(Boolean.valueOf(false)).transferComplete(Boolean.valueOf(false));
                    postEvent(builder.build());
                    return;
                }
                builder.chunkIndex(data.chunkIndex).success(Boolean.valueOf(true)).totalBytesTransferred(Long.valueOf(this.mBytesTransferredSoFar));
                builder.transferComplete(data.lastChunk);
                postEvent(builder.build());
            }
            if (!illegalChunk) {
                if (!data.lastChunk.booleanValue()) {
                    setTimer(30000, "Error receiving next chunk");
                }
                this.mSerialExecutor.submit(new Runnable() {
                    public void run() {
                        try {
                            RemoteFileTransferManager.this.mFileWriter.write(data.dataBytes.toByteArray());
                            RemoteFileTransferManager.this.mFileWriter.flush();
                            if (data.lastChunk.booleanValue()) {
                                IOUtils.closeStream(RemoteFileTransferManager.this.mFileWriter);
                                FileTransferStatus.Builder builder = new FileTransferStatus.Builder();
                                builder.success(Boolean.valueOf(true)).transferComplete(Boolean.valueOf(true)).transferId(Integer.valueOf(RemoteFileTransferManager.this.mTransferId));
                                RemoteFileTransferManager.this.postOnFileTransferStatus(builder.build());
                                RemoteFileTransferManager.this.done();
                            }
                            IOUtils.fileSync(RemoteFileTransferManager.this.mFileWriter);
                        } catch (Exception e) {
                            RemoteFileTransferManager.sLogger.e("Exception while writing the data to the file");
                            IOUtils.closeStream(RemoteFileTransferManager.this.mFileWriter);
                        } finally {
                            IOUtils.fileSync(RemoteFileTransferManager.this.mFileWriter);
                        }
                    }
                });
            }
        }
    }

    private void sendNextChunk() {
        try {
            int i = this.mTransferId;
            TransferDataSource transferDataSource = this.mDataSource;
            int i2 = this.mChunkId + 1;
            this.mChunkId = i2;
            if (postEvent(FileTransferSessionManager.prepareFileTransferData(i, transferDataSource, i2, this.mChunkSize, this.mStartOffset, this.mBytesTransferredSoFar))) {
                setTimer(30000, "Timeout while waiting for response on sent chunk");
            } else {
                sLogger.e("sendNextChunk: cannot send the chunk");
            }
        } catch (Throwable throwable) {
            sLogger.e("Error sending the chunk", throwable);
            postOnError(null, "Error sending chunk");
        }
    }

    @Subscribe
    public void onDeviceDisconnectedEvent(DeviceDisconnectedEvent event) {
        doneWithError(null, "Disconnected from the file receiver during the transfer process");
    }
}
