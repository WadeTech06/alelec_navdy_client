package com.navdy.client.debug;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import androidx.annotation.Nullable;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import com.alelec.navdyclient.R;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.framework.util.SystemUtils;
import com.navdy.client.debug.navdebug.NavCoordTestSuite;

public class NavCoordTestSuiteActivity extends Activity {
    private EditText input;
    private TextView logOutput;
    private NavCoordTestSuite navCoordTestSuite;

    public static void startNavCoordTestSuiteActivity(Context context, String query) {
        Intent intent = new Intent(context, NavCoordTestSuiteActivity.class);
        intent.setAction(query);
        context.startActivity(intent);
    }

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.debug_nav_coord_test_suite);
        this.input = (EditText) findViewById(R.id.address_input);
        String query = getIntent().getAction();
        if (!StringUtils.isEmptyAfterTrim(query)) {
            this.input.setText(query);
            this.input.setSelection(query.length());
        }
        this.navCoordTestSuite = NavCoordTestSuite.getInstance();
    }

    protected void onResume() {
        Handler handler = new Handler(Looper.getMainLooper());
        this.logOutput = (TextView) findViewById(R.id.log_output);
        this.navCoordTestSuite.onResume(handler, this.logOutput);
        super.onResume();
    }

    protected void onPause() {
        this.navCoordTestSuite.onPause();
        super.onPause();
    }

    public void onRunTestSuiteClick(View view) {
        SystemUtils.dismissKeyboard(this);
        this.navCoordTestSuite.run();
    }

    public void onRunSingleTestClick(View view) {
        SystemUtils.dismissKeyboard(this);
        this.navCoordTestSuite.run(this.input.getText().toString());
    }
}
