package com.navdy.service.library.events.contacts;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.ProtoField;

public final class PhoneNumber extends Message {
    public static final String DEFAULT_CUSTOMTYPE = "";
    public static final String DEFAULT_NUMBER = "";
    public static final PhoneNumberType DEFAULT_NUMBERTYPE = PhoneNumberType.PHONE_NUMBER_HOME;
    private static final long serialVersionUID = 0;
    @ProtoField(tag = 3, type = Datatype.STRING)
    public final String customType;
    @ProtoField(tag = 1, type = Datatype.STRING)
    public final String number;
    @ProtoField(tag = 2, type = Datatype.ENUM)
    public final PhoneNumberType numberType;

    public static final class Builder extends com.squareup.wire.Message.Builder<PhoneNumber> {
        public String customType;
        public String number;
        public PhoneNumberType numberType;

        public Builder() {}

        public Builder(PhoneNumber message) {
            super(message);
            if (message != null) {
                this.number = message.number;
                this.numberType = message.numberType;
                this.customType = message.customType;
            }
        }

        public Builder number(String number) {
            this.number = number;
            return this;
        }

        public Builder numberType(PhoneNumberType numberType) {
            this.numberType = numberType;
            return this;
        }

        public Builder customType(String customType) {
            this.customType = customType;
            return this;
        }

        public PhoneNumber build() {
            return new PhoneNumber(this);
        }
    }

    public PhoneNumber(String number, PhoneNumberType numberType, String customType) {
        this.number = number;
        this.numberType = numberType;
        this.customType = customType;
    }

    private PhoneNumber(Builder builder) {
        this(builder.number, builder.numberType, builder.customType);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof PhoneNumber)) {
            return false;
        }
        PhoneNumber o = (PhoneNumber) other;
        if (equals((Object) this.number, (Object) o.number) && equals((Object) this.numberType, (Object) o.numberType) && equals((Object) this.customType, (Object) o.customType)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode;
        int hashCode2 = (this.number != null ? this.number.hashCode() : 0) * 37;
        if (this.numberType != null) {
            hashCode = this.numberType.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (hashCode2 + hashCode) * 37;
        if (this.customType != null) {
            i = this.customType.hashCode();
        }
        result = hashCode + i;
        this.hashCode = result;
        return result;
    }
}
