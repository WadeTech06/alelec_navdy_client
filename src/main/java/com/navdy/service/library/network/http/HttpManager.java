package com.navdy.service.library.network.http;

import javax.inject.Inject;
import javax.inject.Singleton;

import okhttp3.OkHttpClient;
import okhttp3.OkHttpClient.Builder;

@Singleton
public class HttpManager implements IHttpManager {
    protected OkHttpClient mHttpClient;

    @Inject
    public HttpManager() {
        initHttpClient();
    }

    private void initHttpClient() {
        this.mHttpClient = new Builder().retryOnConnectionFailure(false).build();
    }

    public OkHttpClient getClient() {
        return this.mHttpClient;
    }

    public Builder getClientCopy() {
        return this.mHttpClient.newBuilder();
    }
}
