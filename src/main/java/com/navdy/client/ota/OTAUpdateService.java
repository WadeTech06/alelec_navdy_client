package com.navdy.client.ota;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.Notification.Builder;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Binder;
import android.os.IBinder;
import android.os.StatFs;
import com.alelec.navdyclient.R;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.AppInstance;
import com.navdy.client.app.framework.PathManager;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.ui.settings.OtaSettingsActivity;
import com.navdy.client.app.ui.settings.SettingsConstants;
import com.navdy.client.app.ui.settings.SettingsUtils;
import com.navdy.client.debug.util.S3Constants;
import com.navdy.client.debug.util.S3Constants.BuildSource;
import com.navdy.client.debug.util.S3Constants.BuildType;
import com.navdy.client.ota.OTAUpdateListener.CheckOTAUpdateResult;
import com.navdy.client.ota.OTAUpdateListener.DownloadUpdateStatus;
import com.navdy.client.ota.OTAUpdateListener.UploadToHUDStatus;
import com.navdy.client.ota.OTAUpdateUIClient.Error;
import com.navdy.client.ota.impl.OTAUpdateManagerImpl;
import com.navdy.client.ota.model.UpdateInfo;
import com.navdy.service.library.device.RemoteDevice;
import com.navdy.service.library.events.DeviceInfo;
import com.navdy.service.library.events.file.FileTransferError;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.util.IOUtils;
import com.navdy.service.library.util.SystemUtils;
import java.io.File;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.concurrent.atomic.AtomicBoolean;

public class OTAUpdateService extends Service implements OTAUpdateListener {
    private static final long BUFFER_SPACE_FOR_FILE_DOWNLOAD = 52428800;
    public static final String BUILD_SOURCE = "BUILD_SOURCE";
    public static final String BUILD_TYPE = "BUILD_TYPE";
    public static final int CHECK_UPDATE_ACTIVITY_REQ = 256;
    public static final int CHECK_UPDATE_SERVICE_REQ = 128;
    public static final String DOWNLOAD_ID = "DOWNLOAD_ID";
    public static final String EXTRA_OTA_UPDATE_UI = "OTA_UPDATE_UI";
    public static final String EXTRA_PERIODIC_CHECK = "PERIODIC_CHECK";
    public static final String EXTRA_WIFI_TRIGGER = "WIFI_TRIGGER";
    public static final String FORCE_FULL_UPDATE = "FORCE_FULL_UPDATE";
    private static final int INCREMENTAL_INDEX = 2;
    public static final String LAST_CONNECTED_DEVICE_ID = "DEVICE_ID";
    public static final String LAST_UPDATE_CHECK = "LAST_UPDATE_CHECK";
    private static final int MAJOR_MINOR_INCREMENTAL_LENGTH = 3;
    public static final String PARTIAL_UPLOAD_SIZE = "PARTIAL_UPLOAD_SIZE";
    public static final String SW_VERSION = "SW_VERSION";
    public static final String SW_VERSION_NAME = "SW_VERSION_NAME";
    private static final Logger sLogger = new Logger(OTAUpdateService.class);
    private AtomicBoolean mCheckingForUpdate;
    private Config mConfig;
    private AtomicBoolean mIsDownloadingUpdate = new AtomicBoolean(false);
    private AtomicBoolean mIsStarted = new AtomicBoolean(false);
    private AtomicBoolean mIsUploadingUpdateToHUD = new AtomicBoolean(false);
    private long mLastKnownUploadSize = 0;
    private volatile boolean mNetworkDownloadIsApproved = false;
    private OTAUpdateServiceInterface mServiceInterface;
    private volatile State mState = State.UP_TO_DATE;
    private WeakReference<OTAUpdateUIClient> mUIClientRef;
    private volatile UpdateInfo mUpdateInfo;
    private OTAUpdateManager mUpdateManager;
    private volatile boolean mUserApproved = true;

    class OTAUpdateServiceInterfaceImpl extends Binder implements OTAUpdateServiceInterface {
        OTAUpdateServiceInterfaceImpl() {
        }

        public boolean isCheckingForUpdate() {
            return OTAUpdateService.this.mCheckingForUpdate.get();
        }

        public boolean checkForUpdate() {
            return OTAUpdateService.this.checkForUpdate();
        }

        public void cancelDownload() {
            OTAUpdateService.sLogger.d("Download canceled by user");
            if (OTAUpdateService.this.mIsDownloadingUpdate.compareAndSet(true, false)) {
                OTAUpdateService.bResetDownloadId();
                OTAUpdateService.this.mUpdateManager.abortDownload();
                OTAUpdateService.this.downloadStopped(false);
            }
        }

        public void cancelUpload() {
            OTAUpdateService.sLogger.d("Upload cancelled by the user");
            if (OTAUpdateService.this.mIsUploadingUpdateToHUD.compareAndSet(true, false)) {
                OTAUpdateService.this.mUpdateManager.abortUpload();
                OTAUpdateService.this.uploadStopped(false);
            }
        }

        public void toggleAutoDownload(boolean enableAutoDownload) {
            OTAUpdateService.sLogger.d("toggleAutoDownload");
            OTAUpdateService.this.mUserApproved = enableAutoDownload;
        }

        public void resetUpdate() {
            OTAUpdateService.this.setState(State.UP_TO_DATE);
            OTAUpdateService.this.discardOldUpdateData();
        }

        public State getOTAUpdateState() {
            return OTAUpdateService.this.mState;
        }

        public UpdateInfo getUpdateInfo() {
            return OTAUpdateService.this.mUpdateInfo;
        }

        public void downloadOTAUpdate() {
            OTAUpdateService.sLogger.d("User initiated the download");
            OTAUpdateService.this.mUserApproved = true;
            checkForUpdate();
        }

        public void registerUIClient(OTAUpdateUIClient client) {
            OTAUpdateService.this.mUIClientRef = new WeakReference(client);
        }

        public void unregisterUIClient() {
            OTAUpdateService.this.mUIClientRef = new WeakReference(null);
        }

        public String getHUDBuildVersionText() {
            String lastConnectedDeviceId = OTAUpdateService.getDeviceId();
            if (lastConnectedDeviceId == null) {
                return "";
            }
            String version = OTAUpdateService.getSWVersionText(lastConnectedDeviceId);
            if (version == null) {
                return "";
            }
            return version;
        }

        public long lastKnownUploadSize() {
            return OTAUpdateService.this.mLastKnownUploadSize;
        }

        public void setNetworkDownloadApproval(boolean isApproved) {
            OTAUpdateService.this.mNetworkDownloadIsApproved = isApproved;
        }
    }

    public enum State {
        UPDATE_AVAILABLE,
        UP_TO_DATE,
        DOWNLOADING_UPDATE,
        READY_TO_UPLOAD,
        UPLOADING,
        READY_TO_INSTALL
    }

    public void onCreate() {
        super.onCreate();
        this.mUpdateManager = new OTAUpdateManagerImpl(this, this);
        this.mServiceInterface = new OTAUpdateServiceInterfaceImpl();
        this.mCheckingForUpdate = new AtomicBoolean(false);
        this.mConfig = new Config(getResources());
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        this.mIsStarted.set(true);
        if (intent != null && (intent.hasExtra(EXTRA_PERIODIC_CHECK) || intent.hasExtra(EXTRA_WIFI_TRIGGER))) {
            sLogger.d("Setting mUserApproved to true due to periodic check or wi-fi reachability");
            this.mUserApproved = true;
        }
        if (getDeviceId() == null) {
            stopSelf();
            return super.onStartCommand(intent, flags, startId);
        } else if (this.mIsDownloadingUpdate.get() || this.mIsUploadingUpdateToHUD.get()) {
            sLogger.d("onStartCommand: not processing the command as the service is in transient state Downloading :" + this.mIsDownloadingUpdate.get() + ", Uploading :" + this.mIsUploadingUpdateToHUD.get());
            return super.onStartCommand(intent, flags, startId);
        } else {
            this.mState = getSavedState();
            sLogger.d("onStart: Need to check for update again");
            checkForUpdate();
            return super.onStartCommand(intent, flags, startId);
        }
    }

    public IBinder onBind(Intent intent) {
        if (!this.mIsStarted.get()) {
            startService(getApplicationContext());
        }
        return this.mServiceInterface;
    }

    private boolean checkForUpdate() {
        if (this.mIsDownloadingUpdate.get() || !this.mCheckingForUpdate.compareAndSet(false, true)) {
            return false;
        }
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                boolean forceFullUpdate = false;
                SharedPreferences preferences = OTAUpdateService.getSharedPreferences();
                String lastConnectedDeviceId = preferences.getString(OTAUpdateService.LAST_CONNECTED_DEVICE_ID, null);
                if (lastConnectedDeviceId == null || lastConnectedDeviceId.equals("")) {
                    OTAUpdateService.this.mCheckingForUpdate.set(false);
                    OTAUpdateService.this.setState(State.UP_TO_DATE);
                    OTAUpdateService.this.removeNotification();
                    return;
                }
                int currentSwVersion = OTAUpdateService.getSWVersion(lastConnectedDeviceId);
                if (OTAUpdateService.needsFullUpdate(lastConnectedDeviceId) || preferences.getBoolean("FORCE_FULL_UPDATE", false)) {
                    forceFullUpdate = true;
                }
                OTAUpdateService.sLogger.d("Device needs full update? :" + forceFullUpdate);
                String stateString = preferences.getString(SettingsConstants.OTA_STATUS, SettingsConstants.OTA_STATUS_DEFAULT);
                if (!(stateString == null || State.valueOf(stateString) == State.UP_TO_DATE)) {
                    UpdateInfo updateInfo = OTAUpdateService.bReadUpdateInfo();
                    if (updateInfo.incremental && updateInfo.fromVersion != currentSwVersion) {
                        OTAUpdateService.this.setState(State.UP_TO_DATE);
                    }
                }
                OTAUpdateService.this.scheduleNextCheckForUpdate();
                OTAUpdateService.this.mUpdateManager.checkForUpdate(currentSwVersion, forceFullUpdate);
            }
        }, 2);
        return true;
    }

    public void onCheckForUpdateFinished(CheckOTAUpdateResult result, UpdateInfo info) {
        if (this.mCheckingForUpdate.compareAndSet(true, false)) {
            OTAUpdateUIClient client = client();
            if (result != CheckOTAUpdateResult.NO_CONNECTIVITY && result != CheckOTAUpdateResult.SERVER_ERROR) {
                bSaveLastCheckTime();
                if (result == CheckOTAUpdateResult.AVAILABLE) {
                    UpdateInfo updateInfo = bReadUpdateInfo();
                    if (updateInfo == null || !updateInfo.equals(info) || this.mState == State.UP_TO_DATE) {
                        bPersistUpdateInfo(info);
                        this.mUpdateInfo = info;
                        setState(State.UPDATE_AVAILABLE);
                        notifyUpdateAvailable();
                        discardOldUpdateData();
                    } else if (client != null) {
                        client.onStateChanged(this.mState, this.mUpdateInfo);
                    }
                } else if (result == CheckOTAUpdateResult.UPTODATE) {
                    bPersistUpdateInfo(info);
                    this.mUpdateInfo = info;
                    if (client != null) {
                        client.onStateChanged(this.mState, this.mUpdateInfo);
                    }
                }
                performUpdate();
            } else if (client != null) {
                client.onErrorCheckingForUpdate(result == CheckOTAUpdateResult.NO_CONNECTIVITY ? Error.NO_CONNECTIVITY : Error.SERVER_ERROR);
            }
        }
    }

    public void onDownloadProgress(DownloadUpdateStatus status, int downloadId, long completedSoFar, long total) {
        if (!this.mIsDownloadingUpdate.get()) {
            return;
        }
        if (status == DownloadUpdateStatus.STARTED) {
            bSetDownloadId(downloadId);
        } else if (status == DownloadUpdateStatus.PAUSED) {
            downloadStopped(false);
        } else {
            OTAUpdateUIClient client = client();
            int percentage = total > 0 ? (int) ((((float) completedSoFar) / ((float) total)) * 100.0f) : 0;
            if (client != null) {
                try {
                    client.onDownloadProgress(status, completedSoFar, (byte) percentage);
                } catch (Throwable t) {
                    sLogger.d("Error " + t.getMessage());
                }
            }
            if (percentage > 0 && status == DownloadUpdateStatus.DOWNLOADING) {
                publishDownloadProgress(percentage);
            }
            if (status == DownloadUpdateStatus.DOWNLOAD_FAILED) {
                sLogger.d("Download failed");
                bResetDownloadId();
                downloadStopped(false);
            } else if (status == DownloadUpdateStatus.COMPLETED) {
                sLogger.d("Download complete");
                this.mLastKnownUploadSize = 0;
                saveUploadProgress(0);
                bResetDownloadId();
                downloadStopped(true);
                checkForUpdate();
            } else if (status != DownloadUpdateStatus.DOWNLOADING) {
                downloadStopped(false);
            }
        }
    }

    public void onUploadProgress(UploadToHUDStatus status, long completedSoFar, long total, FileTransferError error) {
        int percentage;
        OTAUpdateUIClient client = client();
        if (total > 0) {
            percentage = (int) ((((float) completedSoFar) / ((float) total)) * 100.0f);
        } else {
            percentage = 0;
        }
        if (client != null) {
            client.onUploadProgress(status, completedSoFar, (byte) percentage);
        }
        if (percentage > 0 && status == UploadToHUDStatus.UPLOADING) {
            this.mLastKnownUploadSize = completedSoFar;
            saveUploadProgress(completedSoFar);
            publishUploadProgress(percentage);
        }
        switch (status) {
            case COMPLETED:
                uploadStopped(true);
                return;
            case UPLOAD_FAILED:
                uploadStopped(false);
                return;
            case UPLOADING:
                return;
            default:
                uploadStopped(false);
                return;
        }
    }

    private void saveUploadProgress(long partialUploadSize) {
        getSharedPreferences().edit().putLong(PARTIAL_UPLOAD_SIZE, partialUploadSize).apply();
    }

    private void checkPrecondition() {
        this.mUpdateInfo = bReadUpdateInfo();
        if (this.mUpdateInfo != null) {
            String lastConnectedDeviceId = getDeviceId();
            if (lastConnectedDeviceId != null) {
                int deviceSwVersion = getSWVersion(lastConnectedDeviceId);
                if (deviceSwVersion >= this.mUpdateInfo.version) {
                    sLogger.d("Device is already up to date. " + deviceSwVersion);
                    setState(State.UP_TO_DATE);
                    File file = getUpdateFile();
                    if (file != null && file.exists()) {
                        IOUtils.deleteFile(this, file.getAbsolutePath());
                    }
                }
            }
        }
    }

    private void performUpdate() {
        checkPrecondition();
        switch (this.mState) {
            case UP_TO_DATE:
                sLogger.d("The current sw is up to date");
                clearUpdateFiles();
                return;
            case UPDATE_AVAILABLE:
                sLogger.d("There is an update available, that needs to be downloaded");
                this.mUpdateInfo = bReadUpdateInfo();
                notifyUpdateAvailable();
                if (this.mUserApproved && canBeDownloadedInBackground()) {
                    sLogger.d("The update can be downloaded in the background, so start the download");
                    startDownload(this.mUserApproved);
                    return;
                }
                return;
            case READY_TO_UPLOAD:
                sLogger.d("The update is downloaded and ready to be uploaded to the HUD");
                this.mUpdateInfo = bReadUpdateInfo();
                tryUploading();
                return;
            case READY_TO_INSTALL:
                sLogger.d("The update has been downloaded to the device and will be installed when the device restarts");
                return;
            case DOWNLOADING_UPDATE:
                sLogger.d("The update is downloading...");
                return;
            case UPLOADING:
                sLogger.d("The update is uploading...");
                return;
            default:
                sLogger.e("Unknown OTA state: " + this.mState);
                return;
        }
    }

    private void scheduleNextCheckForUpdate() {
        sLogger.d("Scheduling for next check for update");
        Intent intent = new Intent(this, OTAUpdateService.class);
        intent.putExtra(EXTRA_PERIODIC_CHECK, true);
        ((AlarmManager) getSystemService("alarm")).set(1, System.currentTimeMillis() + this.mConfig.mUpdateInterval, PendingIntent.getService(this, 128, intent, 268435456));
    }

    private static String getDeviceId() {
        return getSharedPreferences().getString(LAST_CONNECTED_DEVICE_ID, null);
    }

    private static String getSWVersionText(String deviceId) {
        SharedPreferences preferences = getSharedPreferences();
        if (StringUtils.isEmptyAfterTrim(preferences.getString(deviceId + "_" + SW_VERSION_NAME, ""))) {
            return String.valueOf(getSWVersion(deviceId));
        }
        return preferences.getString(deviceId + "_" + SW_VERSION_NAME, "").split("-")[0];
    }

    private static int getSWVersion(String deviceId) {
        int i = -1;
        String swVersionString = getSharedPreferences().getString(deviceId + "_" + SW_VERSION, null);
        if (StringUtils.isEmptyAfterTrim(swVersionString)) {
            return i;
        }
        try {
            return Integer.parseInt(swVersionString);
        } catch (NumberFormatException e) {
            sLogger.e("error parsing software version, not a number: " + swVersionString);
            return i;
        }
    }

    public static String getLastDeviceVersionText() {
        return getSWVersionText(getDeviceId());
    }

    private static boolean needsFullUpdate(String deviceId) {
        return getSharedPreferences().getBoolean(deviceId + "_" + "FORCE_FULL_UPDATE", false);
    }

    public static BuildType getBuildType(String deviceId) {
        String buildTypeName = getSharedPreferences().getString(deviceId + "_" + BUILD_TYPE, SettingsConstants.OTA_BUILD_TYPE_DEFAULT);
        BuildType buildType = BuildType.user;
        try {
            return BuildType.valueOf(buildTypeName);
        } catch (IllegalArgumentException e) {
            sLogger.e("Cannot parse the build type " + buildTypeName, e);
            return buildType;
        }
    }

    private boolean canBeDownloadedInBackground() {
        Context c = NavdyApplication.getAppContext();
        return (this.mNetworkDownloadIsApproved && SystemUtils.isConnectedToNetwork(c)) || SystemUtils.isConnectedToWifi(c);
    }

    private void downloadStopped(boolean completed) {
        stopForeground(true);
        notifyUpdateAvailable();
        if (completed) {
            setState(State.READY_TO_UPLOAD);
        } else {
            setState(State.UPDATE_AVAILABLE);
        }
        this.mIsDownloadingUpdate.set(false);
    }

    private void uploadStopped(boolean completed) {
        stopForeground(true);
        removeNotification();
        if (completed) {
            setState(State.READY_TO_INSTALL);
        } else {
            setState(State.READY_TO_UPLOAD);
        }
        this.mIsUploadingUpdateToHUD.set(false);
    }

    private void clearUpdateFiles() {
        File outputFile = getUpdateFile();
        sLogger.d("Clearing the update file as the device is up to date " + outputFile.getAbsolutePath() + " , Size (Bytes) :" + outputFile.length());
        IOUtils.deleteFile(this, outputFile.getAbsolutePath());
    }

    private void startDownload(boolean userInitiated) {
        if (this.mIsDownloadingUpdate.compareAndSet(false, true)) {
            OTAUpdateUIClient client;
            setState(State.DOWNLOADING_UPDATE);
            int downloadId = bReadLastDownloadId();
            File outputFile = getUpdateFile();
            long partiallyDownloadedFileSize = 0;
            if (outputFile.exists()) {
                if (downloadId == -1) {
                    IOUtils.deleteFile(this, outputFile.getAbsolutePath());
                } else {
                    partiallyDownloadedFileSize = outputFile.length();
                }
            }
            if (!ensureSpaceAvailableForDownload(this.mUpdateInfo.size, partiallyDownloadedFileSize)) {
                client = client();
                if (client != null) {
                    client.onDownloadProgress(DownloadUpdateStatus.NOT_ENOUGH_SPACE, 0, (byte) 0);
                    downloadStopped(false);
                }
            }
            if (downloadId == -1) {
                try {
                    if (!outputFile.createNewFile()) {
                        sLogger.e("Unable to create the output file: " + outputFile);
                    }
                } catch (IOException e) {
                    client = client();
                    if (client != null) {
                        client.onDownloadProgress(DownloadUpdateStatus.DOWNLOAD_FAILED, -1, (byte) 0);
                        downloadStopped(false);
                    }
                    return;
                }
            }
            startForeground(1, getDownloadingNotification());
            this.mUpdateManager.download(this.mUpdateInfo, outputFile, downloadId);
        }
    }

    private File getUpdateFile() {
        return new File(PathManager.getInstance().getOTAUpdateFilePath());
    }

    private boolean ensureSpaceAvailableForDownload(long sizeOfTheUpdate, long partialDownloadSize) {
        String otaUpdateFolder = PathManager.getInstance().getOtaUpdateFolderPath();
        StatFs statFs = new StatFs(otaUpdateFolder);
        long freeSpace = ((long) statFs.getFreeBlocks()) * ((long) statFs.getBlockSize());
        long spaceRequired = (sizeOfTheUpdate - partialDownloadSize) + BUFFER_SPACE_FOR_FILE_DOWNLOAD;
        if (sLogger.isLoggable(3) || freeSpace <= spaceRequired) {
            sLogger.i("In " + otaUpdateFolder + "; Space required: " + spaceRequired + "; Space free: " + freeSpace + " enough space:" + Boolean.toString(freeSpace > spaceRequired));
        }
        return freeSpace > spaceRequired;
    }

    private boolean isUpdateFileValid() {
        File updateFile = new File(PathManager.getInstance().getOTAUpdateFilePath());
        if (!updateFile.exists()) {
            return false;
        }
        if (updateFile.length() == this.mUpdateInfo.size) {
            return true;
        }
        sLogger.d("Downloaded update file is invalid");
        return false;
    }

    private void tryUploading() {
        sLogger.d("Trying to transfer the update to HUD");
        if (this.mIsUploadingUpdateToHUD.compareAndSet(false, true)) {
            RemoteDevice mRemoteDevice = AppInstance.getInstance().getRemoteDevice();
            if (mRemoteDevice == null || !mRemoteDevice.isConnected()) {
                uploadStopped(false);
                return;
            }
            sLogger.d("Device is connected, initiating transfer of update ");
            if (isUpdateFileValid()) {
                File updateFile = getUpdateFile();
                long partialUploadedSize = getSharedPreferences().getLong(PARTIAL_UPLOAD_SIZE, 0);
                this.mLastKnownUploadSize = partialUploadedSize;
                setState(State.UPLOADING);
                startForeground(1, getUploadingNotification());
                if (this.mUpdateInfo.incremental) {
                    this.mUpdateManager.uploadToHUD(updateFile, partialUploadedSize, PathManager.getInstance().getOtaUpdateFileNameOnHUD(this.mUpdateInfo.fromVersion, this.mUpdateInfo.version));
                    return;
                } else {
                    this.mUpdateManager.uploadToHUD(updateFile, partialUploadedSize, PathManager.getInstance().getOtaUpdateFileNameOnHUD(this.mUpdateInfo.version));
                    return;
                }
            }
            uploadStopped(false);
            setState(State.UPDATE_AVAILABLE);
        }
    }

    private void discardOldUpdateData() {
        bResetDownloadId();
        File file = new File(PathManager.getInstance().getOtaUpdateFolderPath());
        if (file.exists() && !file.isFile()) {
            File[] children = file.listFiles();
            if (children != null) {
                for (File child : children) {
                    IOUtils.deleteFile(this, child.getAbsolutePath());
                }
            }
        }
    }

    public static void startService(Context context) {
        context.startService(getServiceIntent(context));
    }

    public static Intent getServiceIntent(Context context) {
        return new Intent(context, OTAUpdateService.class);
    }

    private void setState(State state) {
        this.mState = state;
        if (!(state == null || state == State.DOWNLOADING_UPDATE || state == State.UPLOADING)) {
            SharedPreferences preferences = getSharedPreferences();
            if (preferences != null) {
                preferences.edit().putString(SettingsConstants.OTA_STATUS, state.name()).commit();
            }
        }
        OTAUpdateUIClient client = client();
        if (client != null) {
            client.onStateChanged(state, this.mUpdateInfo);
        }
    }

    private State getSavedState() {
        String stateString = getSharedPreferences().getString(SettingsConstants.OTA_STATUS, null);
        State state = State.UP_TO_DATE;
        if (stateString != null) {
            return State.valueOf(stateString);
        }
        return state;
    }

    public static void bResetDownloadId() {
        SharedPreferences preferences = getSharedPreferences();
        if (preferences != null) {
            preferences.edit().remove(DOWNLOAD_ID).commit();
        }
    }

    public static int bReadLastDownloadId() {
        SharedPreferences preferences = getSharedPreferences();
        if (preferences != null) {
            return preferences.getInt(DOWNLOAD_ID, -1);
        }
        return -1;
    }

    public static void bSetDownloadId(int id) {
        SharedPreferences preferences = getSharedPreferences();
        if (preferences != null) {
            preferences.edit().putInt(DOWNLOAD_ID, id).commit();
        }
    }

    @SuppressLint({"CommitPrefEdits"})
    public static void bPersistDeviceInfo(DeviceInfo deviceInfo) {
        SharedPreferences preferences = getSharedPreferences();
        if (preferences != null) {
            preferences.edit().putString(LAST_CONNECTED_DEVICE_ID, deviceInfo.deviceId).putString(deviceInfo.deviceId + "_" + SW_VERSION, deviceInfo.systemVersion).putString(deviceInfo.deviceId + "_" + SW_VERSION_NAME, deviceInfo.clientVersion).putString(deviceInfo.deviceId + "_" + BUILD_TYPE, deviceInfo.buildType).putBoolean(deviceInfo.deviceId + "_" + "FORCE_FULL_UPDATE", Boolean.TRUE.equals(deviceInfo.forceFullUpdate)).commit();
        }
    }

    @SuppressLint({"CommitPrefEdits"})
    public static void bPersistUpdateInfo(UpdateInfo updateInfo) {
        SharedPreferences preferences = getSharedPreferences();
        if (preferences != null) {
            preferences.edit().putString(SettingsConstants.OTA_STATUS, State.UPDATE_AVAILABLE.name()).putString(SettingsConstants.OTA_VERSION_NAME, updateInfo.versionName).putInt(SettingsConstants.OTA_VERSION, updateInfo.version).putString(SettingsConstants.OTA_URL, updateInfo.url).putString(SettingsConstants.OTA_DESCRIPTION, updateInfo.description).putLong(SettingsConstants.OTA_SIZE, updateInfo.size).putBoolean(SettingsConstants.OTA_IS_INCREMENTAL, updateInfo.incremental).putInt(SettingsConstants.OTA_FROM_VERSION, updateInfo.fromVersion).putString(SettingsConstants.OTA_META_DATA, updateInfo.metaData).commit();
        }
    }

    public static UpdateInfo bReadUpdateInfo() {
        SharedPreferences preferences = getSharedPreferences();
        UpdateInfo updateInfo = new UpdateInfo();
        updateInfo.version = preferences.getInt(SettingsConstants.OTA_VERSION, -1);
        updateInfo.versionName = preferences.getString(SettingsConstants.OTA_VERSION_NAME, "");
        updateInfo.url = preferences.getString(SettingsConstants.OTA_URL, "");
        updateInfo.description = preferences.getString(SettingsConstants.OTA_DESCRIPTION, "");
        updateInfo.size = preferences.getLong(SettingsConstants.OTA_SIZE, 0);
        updateInfo.incremental = preferences.getBoolean(SettingsConstants.OTA_IS_INCREMENTAL, false);
        updateInfo.fromVersion = preferences.getInt(SettingsConstants.OTA_FROM_VERSION, 0);
        updateInfo.metaData = preferences.getString(SettingsConstants.OTA_META_DATA, null);
        return updateInfo;
    }

    public static void bSaveLastCheckTime() {
        SharedPreferences preferences = getSharedPreferences();
        preferences.edit().putLong(preferences.getString(LAST_CONNECTED_DEVICE_ID, null) + "_" + LAST_UPDATE_CHECK, System.currentTimeMillis()).commit();
    }

    public static SharedPreferences getSharedPreferences() {
        return SettingsUtils.getSharedPreferences();
    }

    private OTAUpdateUIClient client() {
        if (this.mUIClientRef != null) {
            return (OTAUpdateUIClient) this.mUIClientRef.get();
        }
        return null;
    }

    public static boolean isLaunchedByOtaUpdateService(Intent intent) {
        return intent != null && intent.hasExtra(EXTRA_OTA_UPDATE_UI) && intent.getBooleanExtra(EXTRA_OTA_UPDATE_UI, false);
    }

    private PendingIntent getUIPendingIntent() {
        Context appContext = getApplicationContext();
        return PendingIntent.getActivity(appContext, 256, new Intent(appContext, OtaSettingsActivity.class), 1073741824);
    }

    public void notifyUpdateAvailable() {
        Builder builder = new Builder(this);
        builder.setContentIntent(getUIPendingIntent());
        builder.setContentTitle(getString(R.string.navdy_software_update));
        builder.setSmallIcon(R.drawable.icon_status_bar);
        builder.setContentText(getString(R.string.update_available));
        builder.setAutoCancel(true);
        ((NotificationManager) getSystemService("notification")).notify(1, builder.build());
    }

    public void removeNotification() {
        ((NotificationManager) getSystemService("notification")).cancel(1);
    }

    public Notification getDownloadingNotification() {
        Builder builder = new Builder(this);
        builder.setContentIntent(getUIPendingIntent());
        builder.setContentTitle(getString(R.string.navdy_software_update));
        builder.setSmallIcon(R.drawable.icon_status_bar);
        builder.setContentText(getString(R.string.downloading_update));
        builder.setProgress(0, 0, true);
        return builder.build();
    }

    public void publishDownloadProgress(int percentage) {
        Builder builder = new Builder(this);
        builder.setContentIntent(getUIPendingIntent());
        builder.setContentTitle(getString(R.string.navdy_software_update));
        builder.setSmallIcon(R.drawable.icon_status_bar);
        builder.setContentText(getString(R.string.downloading_update));
        builder.setAutoCancel(true);
        builder.setProgress(100, percentage, false);
        ((NotificationManager) getSystemService("notification")).notify(1, builder.build());
    }

    public void publishUploadProgress(int percentage) {
        Builder builder = new Builder(this);
        builder.setContentIntent(getUIPendingIntent());
        builder.setContentTitle(getString(R.string.navdy_software_update));
        builder.setSmallIcon(R.drawable.icon_status_bar);
        builder.setContentText(getString(R.string.transferring_update));
        builder.setProgress(100, percentage, false);
        builder.setAutoCancel(true);
        ((NotificationManager) getSystemService("notification")).notify(1, builder.build());
    }

    public Notification getUploadingNotification() {
        Builder builder = new Builder(this);
        builder.setContentIntent(getUIPendingIntent());
        builder.setContentTitle(getString(R.string.navdy_software_update));
        builder.setSmallIcon(R.drawable.icon_status_bar);
        builder.setContentText(getString(R.string.transferring_update));
        builder.setProgress(0, 0, true);
        return builder.build();
    }

    public static BuildSource getBuildSource() {
        BuildType buildType = getBuildType();
        BuildSource[] sourcesForBuildType = S3Constants.getSourcesForBuildType(buildType);
        String preference = getUserPreferredSourceForTheDevice();
        BuildSource source = S3Constants.getDefaultSourceForBuildType(buildType);
        try {
            source = BuildSource.valueOf(preference);
        } catch (IllegalArgumentException iae) {
            sLogger.e("Error parsing the saved user preference for OTA source " + preference, iae);
        }
        sLogger.d("User preference for the source " + preference + ", Source :" + source.name());
        boolean isSourceInAllowedSourcesForBuildType = false;
        for (BuildSource sourceForBuildType : sourcesForBuildType) {
            if (sourceForBuildType == source) {
                isSourceInAllowedSourcesForBuildType = true;
                break;
            }
        }
        if (isSourceInAllowedSourcesForBuildType) {
            sLogger.d("User preferred source is compatible with the build type");
            return source;
        }
        sLogger.d("User preferred source is not compatible with build type, setting to default");
        BuildSource defaultSourceForBuildType = S3Constants.getDefaultSourceForBuildType(buildType);
        persistUserPreferredBuildSource(source);
        return defaultSourceForBuildType;
    }

    public static BuildSource[] getBuildSources() {
        return S3Constants.getSourcesForBuildType(getBuildType());
    }

    public static BuildType getBuildType() {
        return getBuildType(getDeviceId());
    }

    public static String getUserPreferredSourceForTheDevice() {
        return getBuildSource(getDeviceId());
    }

    public static String getBuildSource(String deviceId) {
        return getSharedPreferences().getString(deviceId + "_" + BUILD_SOURCE, S3Constants.getDefaultSourceForBuildType(getBuildType(deviceId)).name());
    }

    public static void persistUserPreferredBuildSource(BuildSource source) {
        getSharedPreferences().edit().putString(getDeviceId() + "_" + BUILD_SOURCE, source.name()).commit();
    }
}
