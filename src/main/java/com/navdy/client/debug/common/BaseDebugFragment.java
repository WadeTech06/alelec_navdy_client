package com.navdy.client.debug.common;

import android.app.Activity;
import android.app.Fragment;
import com.navdy.service.library.log.Logger;

public class BaseDebugFragment extends Fragment {
    protected BaseDebugActivity baseActivity;
    protected Logger logger = new Logger(getClass());

    public void onAttach(Activity activity) {
        this.baseActivity = (BaseDebugActivity) activity;
        super.onAttach(activity);
    }

    public void onDetach() {
        this.baseActivity = null;
        super.onDetach();
    }

    public boolean isAlive() {
        return this.baseActivity != null && !this.baseActivity.isActivityDestroyed();
    }
}
