package com.navdy.client.debug;

import android.app.ActionBar;
import android.app.Activity;
import android.app.FragmentTransaction;
import android.app.ListFragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;
import butterknife.ButterKnife;
import butterknife.BindView;
import butterknife.OnClick;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.alelec.navdyclient.R;
import com.navdy.client.app.framework.DeviceConnection;
import com.navdy.client.app.framework.search.NavdySearch;
import com.navdy.client.app.framework.util.BusProvider;
import com.navdy.client.app.framework.util.CrashlyticsAppender;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.debug.models.RecentSearches;
import com.navdy.client.debug.navigation.HUDNavigationManager;
import com.navdy.client.debug.navigation.NavigationManager;
import com.navdy.service.library.events.RequestStatus;
import com.navdy.service.library.events.location.Coordinate;
import com.navdy.service.library.events.navigation.NavigationRouteResponse;
import com.navdy.service.library.events.navigation.NavigationRouteResult;
import com.navdy.service.library.events.places.PlacesSearchRequest.Builder;
import com.navdy.service.library.events.places.PlacesSearchResponse;
import com.navdy.service.library.events.places.PlacesSearchResult;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Subscribe;
import java.util.ArrayList;
import java.util.List;
import org.droidparts.contract.SQL.DDL;
import org.droidparts.widget.ClearableEditText;
import org.droidparts.widget.ClearableEditText.Listener;

public class RemoteAddressPickerFragment extends ListFragment {
    private static final double MI_TO_METERS = 1690.34d;
    public static final String PREFS_FILE_USER_HISTORY = "UserHistory";
    public static final String PREFS_KEY_RECENT_SEARCHES = "RecentSearches";
    private static final int SEARCH_RADIUS_METERS = 169034;
    protected static int fragmentTitle = R.string.title_address_search;
    public static final Logger sLogger = new Logger(RemoteAddressPickerFragment.class);
    private Activity activity;
    @BindView(R.id.nav_picker_button_search)
    Button mButtonSearch;
    protected ArrayList<String> mDestinationLabels;
    @BindView(R.id.nav_picker_edittext_destination)
    ClearableEditText mEditTextDestinationQuery;
    protected NavigationManager mNavigationManager;
    protected Gson mObjectMapper;
    protected RecentSearches mRecentSearches;
    protected ArrayList<PlacesSearchResult> mSearchResults;
    protected SharedPreferences mSharedPrefs;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Context context = getActivity().getApplicationContext();
        this.mObjectMapper = new Gson();
        this.mSharedPrefs = context.getSharedPreferences(PREFS_FILE_USER_HISTORY, 0);
        this.mRecentSearches = loadRecentSearches();
        this.mDestinationLabels = new ArrayList<>();
        setListAdapter(new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, android.R.id.text1, this.mDestinationLabels));
        addDestinations(this.mRecentSearches.getResults());
        this.mNavigationManager = new HUDNavigationManager();
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = activity;
    }

    public void onDetach() {
        super.onDetach();
        this.activity = null;
    }

    private boolean isAttached() {
        return this.activity != null && !this.activity.isFinishing();
    }

    protected void addDestinations(List<PlacesSearchResult> searchResults) {
        this.mDestinationLabels.clear();
        for (PlacesSearchResult result : searchResults) {
            this.mDestinationLabels.add(result.label);
        }
        ((ArrayAdapter) getListAdapter()).notifyDataSetChanged();
    }

    protected RecentSearches loadRecentSearches() {
        try {
            String json = this.mSharedPrefs.getString(PREFS_KEY_RECENT_SEARCHES, "");
            if (json.length() > 0) {
                return (RecentSearches) this.mObjectMapper.fromJson(json, RecentSearches.class);
            }
        } catch (JsonParseException e) {
            sLogger.e("Unable to read recent searches", e);
        }
        return new RecentSearches();
    }

    protected void saveRecentSearches() {
        this.mSharedPrefs.edit().putString(PREFS_KEY_RECENT_SEARCHES, this.mObjectMapper.toJson(this.mRecentSearches)).apply();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_nav_address_picker, container, false);
        ButterKnife.bind((Object) this, rootView);
        this.mEditTextDestinationQuery.setOnEditorActionListener(new OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId != 3) {
                    return false;
                }
                RemoteAddressPickerFragment.this.onSearchClicked();
                return true;
            }
        });
        this.mEditTextDestinationQuery.setListener(new Listener() {
            public void didClearText() {
                RemoteAddressPickerFragment.this.showKeyboard();
            }
        });
        return rootView;
    }

    public void onResume() {
        super.onResume();
        refreshUI();
        BusProvider.getInstance().register(this);
    }

    public void onPause() {
        BusProvider.getInstance().unregister(this);
        super.onPause();
    }

    public void onDestroy() {
        saveRecentSearches();
        super.onDestroy();
    }

    public void refreshUI() {
        if (this.mDestinationLabels.size() == 0) {
            showKeyboard();
        }
    }

    public void onListItemClick(ListView l, View v, int position, long id) {
        PlacesSearchResult destination;
        super.onListItemClick(l, v, position, id);
        sLogger.d("locationClick: " + getListView().getItemAtPosition(position));
        if (this.mSearchResults != null) {
            destination = this.mSearchResults.get(position);
        } else {
            destination = this.mRecentSearches.getResults().get(position);
        }
        Coordinate navigationPosition = destination.navigationPosition;
        if (navigationPosition != null) {
            showError("NavPosition: " + navigationPosition.latitude + DDL.SEPARATOR + navigationPosition.longitude);
        } else if (destination.destinationLocation != null) {
            navigationPosition = destination.destinationLocation;
            showError("DestPosition: " + navigationPosition.latitude + DDL.SEPARATOR + navigationPosition.longitude);
        } else {
            showError("Unable to find coordinate.");
            return;
        }
        this.mRecentSearches.add(destination);
        hideKeyboard();
        processSelectedLocation(navigationPosition, (String) this.mDestinationLabels.get(position), null);
    }

    protected void processSelectedLocation(Coordinate location, String locationLabel, String streetAddress) {
        if (this.mNavigationManager != null) {
            this.mNavigationManager.startRouteRequest(location, locationLabel, streetAddress, null);
        }
    }

    @OnClick({R.id.nav_picker_button_search})
    public void onSearchClicked() {
        String queryText = this.mEditTextDestinationQuery.getText().toString();
        if (!StringUtils.isEmptyAfterTrim(queryText)) {
            boolean postSuccess;
            addDestinations(new ArrayList<PlacesSearchResult>(0));
            if (DeviceConnection.isConnected()) {
                postSuccess = DeviceConnection.postEvent(new Builder().searchQuery(queryText).searchArea(NavdySearch.SEARCH_AREA_VALUE).maxResults(30).build());
                if (!postSuccess) {
                    showError("Unable to post search");
                }
            } else {
                showError("Device not connected.");
            }
        }
    }

    @Subscribe
    public void onPlacesSearchResponse(PlacesSearchResponse response) {
        if (response.status != RequestStatus.REQUEST_SUCCESS) {
            String errorString = "Error: " + response.status + " " + response.statusDetail;
            sLogger.e(errorString);
            showError(errorString);
            return;
        }
        List<PlacesSearchResult> results = response.results;
        if (results == null || results.size() == 0) {
            showError("No results");
            sLogger.d("no results");
            this.mEditTextDestinationQuery.requestFocus();
            return;
        }
        sLogger.d("returned results: " + results.size());
        hideKeyboard();
        addDestinations(results);
        this.mSearchResults = new ArrayList<>(results);
    }

    protected void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.hideSoftInputFromWindow(this.mEditTextDestinationQuery.getWindowToken(), 0);
        }
    }

    protected void showKeyboard() {
        ((InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE)).showSoftInput(this.mEditTextDestinationQuery, 1);
    }

    protected void showError(String error) {
        Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();
    }

    @Subscribe
    public void onNavigationRouteResponse(NavigationRouteResponse event) {
        if (isAttached()) {
            sLogger.e(event.status + CrashlyticsAppender.SEPARATOR + event.statusDetail);
            if (event.status != RequestStatus.REQUEST_SUCCESS) {
                sLogger.e("Unable to get routes.");
                return;
            }
            List<NavigationRouteResult> results = event.results;
            if (results == null || results.size() == 0) {
                sLogger.e("No results.");
                return;
            }
            RemoteNavControlFragment navControlFragment = RemoteNavControlFragment.newInstance(results.get(0), event.label);
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.replace(R.id.container, navControlFragment);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.addToBackStack(null);
            ft.commit();
            return;
        }
        sLogger.v("isAttached: false");
    }

    private ActionBar getActionBar() {
        return getActivity().getActionBar();
    }
}
