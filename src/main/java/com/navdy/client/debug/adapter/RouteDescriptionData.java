package com.navdy.client.debug.adapter;

import com.navdy.service.library.events.navigation.NavigationRouteResult;

public class RouteDescriptionData {
    public String diff;
    public int duration;
    public int length;

    public RouteDescriptionData(String diff, int duration, int length, NavigationRouteResult navigationRouteResult) {
        this.diff = diff;
        this.duration = duration;
        this.length = length;
    }
}
