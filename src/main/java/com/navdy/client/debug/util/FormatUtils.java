package com.navdy.client.debug.util;

import io.fabric.sdk.android.services.common.IdManager;
import java.text.DecimalFormat;

public class FormatUtils {
    private static final DecimalFormat FILE_SIZE_FORMAT = new DecimalFormat("#,##0.#");
    private static final String SPACE = " ";

    public static String formatLengthFromMetersToMiles(int valueInMeters) {
        return new DecimalFormat(IdManager.DEFAULT_VERSION_NAME).format(((double) valueInMeters) / 1609.344d) + " miles";
    }

    public static String formatDurationFromSecondsToSecondsMinutesHours(int valueInSeconds) {
        String result = "";
        int numberOfHours = valueInSeconds / 3600;
        int numberOfMinutes = (valueInSeconds - ((numberOfHours * 60) * 60)) / 60;
        int numberOfSeconds = (valueInSeconds - ((numberOfHours * 60) * 60)) - (numberOfMinutes * 60);
        if (numberOfHours > 0) {
            result = numberOfHours + " h";
        }
        if (numberOfMinutes > 0) {
            if (numberOfSeconds > 30) {
                numberOfMinutes++;
            }
            result = result + SPACE + numberOfMinutes + " min";
        }
        if (numberOfHours == 0 && numberOfMinutes == 0 && numberOfSeconds > 0) {
            result = result + SPACE + numberOfSeconds + " sec";
        }
        return result.trim();
    }

    public static String addPrefixForRouteDifference(String diff) {
        return "via " + diff;
    }

    public static String readableFileSize(long size) {
        if (size <= 0) {
            return "0";
        }
        String[] units = new String[]{"B", "KB", "MB", "GB", "TB"};
        int digitGroups = (int) (Math.log10((double) size) / Math.log10(1024.0d));
        return String.format("%s %s", new Object[]{FILE_SIZE_FORMAT.format(((double) size) / Math.pow(1024.0d, (double) digitGroups)), units[digitGroups]});
    }
}
