package com.navdy.client.debug.util;

import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.service.library.events.notification.NotificationCategory;
import com.navdy.service.library.events.notification.NotificationEvent;

public class NotificationBuilder {
    public static NotificationEvent buildSmsNotification(String contactName, String phoneNumber, String message, boolean cannotReplyBack) {
        String str;
        Integer valueOf = Integer.valueOf(0);
        NotificationCategory notificationCategory = NotificationCategory.CATEGORY_SOCIAL;
        String str2 = contactName != null ? contactName : phoneNumber;
        String str3 = contactName != null ? phoneNumber : "";
        if (StringUtils.isEmptyAfterTrim(message)) {
            str = "";
        } else {
            str = message;
        }
        return new NotificationEvent(valueOf, notificationCategory, str2, str3, str, null, null, null, null, null, phoneNumber, Boolean.valueOf(cannotReplyBack), null);
    }
}
