package com.navdy.client.debug.view;

import android.content.Intent;
import android.widget.Toast;
import com.here.odnp.util.OdnpConstants;
import com.navdy.client.debug.util.S3Constants;
import com.navdy.client.debug.videoplayer.VideoPlayerActivity;
import com.navdy.service.library.log.Logger;
import java.io.File;
import java.net.URL;
import java.util.Date;

public class VideoS3BrowserFragment extends S3BrowserFragment {
    protected static final Date URL_EXPIRATION = new Date(System.currentTimeMillis() + OdnpConstants.ONE_HOUR_IN_MS);
    private static final Logger sLogger = new Logger(VideoS3BrowserFragment.class);

    protected String getS3BucketName() {
        return S3Constants.BUCKET_NAME;
    }

    protected void onFileSelected(String fileS3Key) {
        if (fileS3Key.endsWith(".mp4")) {
            String sessionPath = new File(fileS3Key).getParent() + S3Constants.S3_FILE_DELIMITER;
            Intent intent = new Intent(getActivity(), VideoPlayerActivity.class);
            URL videoUrl = this.mClient.generatePresignedUrl(getS3BucketName(), fileS3Key, URL_EXPIRATION);
            sLogger.d("Generated signed url " + videoUrl);
            intent.putExtra(VideoPlayerActivity.VIDEO_URL_EXTRA, videoUrl);
            intent.putExtra(VideoPlayerActivity.SESSION_PATH_EXTRA, sessionPath);
            intent.putExtra(VideoPlayerActivity.BUCKET_NAME_EXTRA, getS3BucketName());
            startActivity(intent);
            return;
        }
        Toast.makeText(getActivity(), "Please select a video file", 0).show();
    }
}
