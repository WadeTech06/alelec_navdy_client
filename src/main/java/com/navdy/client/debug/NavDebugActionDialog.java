package com.navdy.client.debug;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AlertDialog.Builder;
import android.widget.Toast;
import com.alelec.navdyclient.R;
import com.navdy.client.app.framework.models.Destination;
import com.navdy.client.app.ui.search.SearchConstants;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class NavDebugActionDialog extends Dialog {
    private static final int COORD_TEST_ADDRESS = 0;
    private static final int COORD_TEST_NAME = 1;
    private static final int GEO_INTENT_CALENDAR = 7;
    private static final int GEO_INTENT_HANGOUT = 8;
    private static final int GOOGLE_MAPS_INTENT_ADDRESS = 4;
    private static final int GOOGLE_MAPS_INTENT_COORD = 5;
    private static final int GOOGLE_MAPS_INTENT_URL_WITH_BOTH = 6;
    private static final int GOOGLE_NAV_INTENT_ADDRESS = 2;
    private static final int GOOGLE_NAV_INTENT_COORD = 3;
    private static final int SHARE_INTENT_ASSISTANT = 10;
    private static final int SHARE_INTENT_MAPS = 9;
    private static final int SHARE_INTENT_YELP = 11;
    private static final int STREET_VIEW = 12;

    public NavDebugActionDialog(@NonNull Context context) {
        super(context);
    }

    public AlertDialog createAndShow(final Destination destination) {
        final Context context = getContext();
        CharSequence[] options = context.getResources().getStringArray(R.array.nav_debug_actions);
        Builder builder = new Builder(context);
        builder.setTitle((int) R.string.actions);
        builder.setItems(options, new OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent("android.intent.action.VIEW");
                try {
                    String encodedAddress = URLEncoder.encode(destination.rawAddressNotForDisplay, "UTF-8");
                    switch (which) {
                        case 1:
                            intent = null;
                            NavCoordTestSuiteActivity.startNavCoordTestSuiteActivity(context, destination.name);
                            Toast.makeText(context, "Test Running. Check logs for output", 0).show();
                            break;
                        case 2:
                            intent.setData(Uri.parse("google.navigation:q=" + encodedAddress));
                            break;
                        case 3:
                            intent.setData(Uri.parse("google.navigation:q=" + destination.displayLat + "%2C" + destination.displayLong));
                            break;
                        case 4:
                            intent.setData(Uri.parse("maps.google.com:q=" + encodedAddress));
                            break;
                        case 5:
                            intent.setData(Uri.parse("maps.google.com:q=" + destination.displayLat + "%2C" + destination.displayLong));
                            break;
                        case 6:
                            intent.setData(Uri.parse("https://maps.google.com:?q=" + encodedAddress + "&ll=" + destination.displayLat + "," + destination.displayLong));
                            break;
                        case 7:
                            intent.setData(Uri.parse("geo:" + destination.displayLat + "," + destination.displayLong + "?q=" + destination.name));
                            break;
                        case 8:
                            intent.setData(Uri.parse("geo:" + destination.displayLat + "," + destination.displayLong + "?q=" + destination.displayLat + "," + destination.displayLong + "(" + destination.name + ")"));
                            break;
                        case 9:
                            intent.setAction("android.intent.action.SEND");
                            intent.setType("text/plain");
                            intent.putExtra("android.intent.extra.TEXT", destination.name + "\n" + destination.rawAddressNotForDisplay + "\n" + "\n" + "https://goo.gl/maps/SOMESHORTURL");
                            break;
                        case 10:
                            intent.setAction("android.intent.action.SEND");
                            intent.setType("text/plain");
                            intent.putExtra("android.intent.extra.TEXT", destination.name + "\n" + "\n" + destination.rawAddressNotForDisplay + "\n" + "\n" + "https://g.co/kgs/SOMESHORTURL");
                            break;
                        case 11:
                            intent.setAction("android.intent.action.SEND");
                            intent.setType("text/plain");
                            String encodedName = destination.name.replaceAll(" ", "-");
                            try {
                                encodedName = URLEncoder.encode(destination.name, "UTF-8");
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                            intent.putExtra("android.intent.extra.TEXT", destination.name + "\n" + "\n" + SearchConstants.YELP_MAPS_URL + encodedName + "?utm_source=ashare&utm_campaign=status_quo_sheet&ref=yelp-android");
                            break;
                        case 12:
                            intent.setData(Uri.parse("google.streetview:cbll=" + destination.displayLat + "," + destination.displayLong));
                            break;
                        default:
                            intent = null;
                            NavCoordTestSuiteActivity.startNavCoordTestSuiteActivity(context, destination.rawAddressNotForDisplay);
                            Toast.makeText(context, "Test Running. Check logs for output", 0).show();
                            break;
                    }
                    if (intent != null) {
                        Toast.makeText(context, "Sending " + intent, 1).show();
                        context.startActivity(intent);
                    }
                } catch (UnsupportedEncodingException e2) {
                    e2.printStackTrace();
                }
            }
        });
        return builder.show();
    }
}
