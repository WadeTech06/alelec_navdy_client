package com.navdy.client.debug;

import android.os.Bundle;
import android.preference.PreferenceFragment;
import com.alelec.navdyclient.R;

public class SavedPreferencesFragment extends PreferenceFragment {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
    }
}
