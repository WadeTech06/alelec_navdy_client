package com.navdy.client.app.providers;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.util.Pair;

import androidx.annotation.CheckResult;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.WorkerThread;

import com.alelec.navdyclient.R;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.models.Address;
import com.navdy.client.app.framework.models.ContactModel;
import com.navdy.client.app.framework.models.Destination;
import com.navdy.client.app.framework.models.Destination.Precision;
import com.navdy.client.app.framework.models.Destination.SearchType;
import com.navdy.client.app.framework.models.Destination.Type;
import com.navdy.client.app.framework.models.DestinationCacheEntry;
import com.navdy.client.app.framework.models.GoogleTextSearchDestinationResult;
import com.navdy.client.app.framework.models.Trip;
import com.navdy.client.app.framework.util.ContactsManager;
import com.navdy.client.app.framework.util.MusicDbUtils;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.framework.util.SystemUtils;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.util.IOUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class NavdyContentProvider extends ContentProvider {
    public static final String CREATE_DESTINATIONS_TABLE = " CREATE TABLE IF NOT EXISTS destinations ( _id INTEGER PRIMARY KEY, place_id TEXT, last_place_id_refresh INTEGER, displat REAL, displong REAL, navlat REAL, navlong REAL, place_name TEXT NOT NULL, address TEXT NOT NULL, street_number TEXT, street_name TEXT, city TEXT, state TEXT, zip_code TEXT, country TEXT, countryCode TEXT, do_not_suggest INT, last_routed_date INTEGER, favorite_label TEXT, favorite_listing_order INTEGER, is_special INTEGER, place_detail_json TEXT, precision_level INTEGER, type INTEGER, contact_lookup_key TEXT, last_known_contact_id INTEGER, last_contact_lookup INTEGER);";
    public static final String CREATE_DESTINATION_CACHE_TABLE = " CREATE TABLE IF NOT EXISTS destination_cache ( _id INTEGER PRIMARY KEY, last_response_date INTEGER, location TEXT, destination_id INTEGER);";
    public static final String CREATE_PLAYLISTS_TABLE = " CREATE TABLE IF NOT EXISTS playlists ( playlist_id INTEGER, playlist_name STRING );";
    public static final String CREATE_PLAYLIST_MEMBERS_TABLE = " CREATE TABLE IF NOT EXISTS playlist_members ( playlist_id INTEGER, SourceId STRING, artist STRING, album STRING, title STRING );";
    public static final String CREATE_SEARCH_HISTORY_TABLE = " CREATE TABLE IF NOT EXISTS search_history ( _id INTEGER PRIMARY KEY, last_searched_on INTEGER, query TEXT);";
    public static final String CREATE_TRIPS_TABLE = " CREATE TABLE IF NOT EXISTS trips ( _id INTEGER PRIMARY KEY, trip_number INTEGER, start_time INTEGER, start_time_zone_n_dst INTEGER, start_odometer INTEGER, start_lat REAL, start_long REAL, end_time INTEGER, end_odometer INTEGER, end_lat REAL, end_long REAL, destination_id INTEGER, arrived_at_destination INTEGER);";
    private static HashMap<String, String> destinationCacheValues;
    private static HashMap<String, String> destinationsValues;
    private static final Logger logger = new Logger(NavdyContentProvider.class);
    private static HashMap<String, String> playlistMembersValues;
    private static HashMap<String, String> playlistsValues;
    private static HashMap<String, String> searchHistoryValues;
    private static SQLiteDatabase sqlDB;
    private static HashMap<String, String> tripsValues;
    public static final UriMatcher uriMatcher = new UriMatcher(-1);

    private interface CacheQuerier {
        Cursor runQuery(String[] strArr, String str, String[] strArr2);
    }

    private static class DatabaseHelper extends SQLiteOpenHelper {
        Context context;

        DatabaseHelper(Context context) {
            super(context, NavdyContentProviderConstants.DB_NAME, null, 19);
            this.context = context;
        }

        public void onCreate(SQLiteDatabase db) {
            db.execSQL(NavdyContentProvider.CREATE_DESTINATIONS_TABLE);
            db.execSQL(NavdyContentProvider.CREATE_TRIPS_TABLE);
            db.execSQL(NavdyContentProvider.CREATE_SEARCH_HISTORY_TABLE);
            db.execSQL(NavdyContentProvider.CREATE_DESTINATION_CACHE_TABLE);
            db.execSQL(NavdyContentProvider.CREATE_PLAYLISTS_TABLE);
            db.execSQL(NavdyContentProvider.CREATE_PLAYLIST_MEMBERS_TABLE);
        }

        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            NavdyContentProvider.logger.v("onUpgrade oldVersion: " + oldVersion + " newVersion: " + newVersion);
            if (oldVersion <= 7) {
                db.execSQL("ALTER TABLE destinations ADD COLUMN place_detail_json TEXT");
            }
            if (oldVersion <= 8) {
                db.execSQL(NavdyContentProvider.CREATE_SEARCH_HISTORY_TABLE);
            }
            if (oldVersion <= 9) {
                db.execSQL(NavdyContentProvider.CREATE_DESTINATION_CACHE_TABLE);
                db.execSQL("ALTER TABLE destinations ADD COLUMN precision_level INTEGER DEFAULT " + Precision.UNKNOWN.getValue());
            }
            if (oldVersion <= 10) {
                db.execSQL("ALTER TABLE destinations ADD COLUMN type INTEGER DEFAULT " + Type.UNKNOWN.getValue());
                NavdyContentProvider.goThroughAllDestinationBlobsAndSetType(db);
            }
            if (oldVersion <= 11) {
                db.execSQL("ALTER TABLE trips ADD COLUMN start_time_zone_n_dst INTEGER DEFAULT " + SystemUtils.getTimeZoneAndDaylightSavingOffset(Long.valueOf(new Date().getTime())));
            }
            if (oldVersion <= 12) {
                db.execSQL(NavdyContentProvider.CREATE_PLAYLISTS_TABLE);
                db.execSQL(NavdyContentProvider.CREATE_PLAYLIST_MEMBERS_TABLE);
            }
            if (oldVersion <= 13) {
                db.execSQL("ALTER TABLE destinations ADD COLUMN countryCode TEXT");
                NavdyContentProvider.goThroughAllDestinationBlobsAndSetCountryCode(db);
            }
            if (oldVersion <= 14) {
                db.execSQL("ALTER TABLE destinations ADD COLUMN contact_lookup_key TEXT");
                db.execSQL("ALTER TABLE destinations ADD COLUMN last_known_contact_id INTEGER");
                db.execSQL("ALTER TABLE destinations ADD COLUMN last_contact_lookup INTEGER");
            }
            if (oldVersion <= 18) {
                NavdyContentProvider.goThroughAllFavoriteDestinationsAndLinkContacts(db, this.context);
            }
        }

        public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            dumpAndRecreateDb(db);
        }

        private void dumpAndRecreateDb(SQLiteDatabase db) {
            db.execSQL("DROP TABLE IF EXISTS destinations");
            db.execSQL("DROP TABLE IF EXISTS trips");
            db.execSQL("DROP TABLE IF EXISTS search_history");
            db.execSQL("DROP TABLE IF EXISTS destination_cache");
            db.execSQL("DROP TABLE IF EXISTS playlists");
            db.execSQL("DROP TABLE IF EXISTS playlist_members");
            onCreate(db);
        }
    }

    public interface QueryResultCallback {
        void onQueryCompleted(int i, @Nullable Uri uri);
    }

    static {
        uriMatcher.addURI(NavdyContentProviderConstants.PROVIDER_NAME, "destinations", 1);
        uriMatcher.addURI(NavdyContentProviderConstants.PROVIDER_NAME, "trips", 2);
        uriMatcher.addURI(NavdyContentProviderConstants.PROVIDER_NAME, "search_history", 3);
        uriMatcher.addURI(NavdyContentProviderConstants.PROVIDER_NAME, "destination_cache", 4);
        uriMatcher.addURI(NavdyContentProviderConstants.PROVIDER_NAME, "playlists", 5);
        uriMatcher.addURI(NavdyContentProviderConstants.PROVIDER_NAME, "playlist_members", 6);
    }

    public String getType(@NonNull Uri uri) {
        switch (uriMatcher.match(uri)) {
            case 1:
                return "vnd.android.cursor.dir/destinations";
            case 2:
                return "vnd.android.cursor.dir/trips";
            case 3:
                return "vnd.android.cursor.dir/search_history";
            case 4:
                return "vnd.android.cursor.dir/destination_cache";
            case 5:
                return "vnd.android.cursor.dir/playlists";
            case 6:
                return "vnd.android.cursor.dir/playlist_members";
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
    }

    @NonNull
    private String getTableName(@NonNull Uri uri) {
        switch (uriMatcher.match(uri)) {
            case 1:
                return "destinations";
            case 2:
                return "trips";
            case 3:
                return "search_history";
            case 4:
                return "destination_cache";
            case 5:
                return "playlists";
            case 6:
                return "playlist_members";
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
    }

    @NonNull
    private String getTableNameForQuery(@NonNull Uri uri) {
        switch (uriMatcher.match(uri)) {
            case 1:
                return "destinations";
            case 2:
                return "trips";
            case 3:
                return "search_history";
            case 4:
                return "destination_cache";
            case 5:
                return "playlists";
            case 6:
                return "playlist_members";
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
    }

    @NonNull
    private Uri getContentUri(@NonNull Uri baseUri) {
        switch (uriMatcher.match(baseUri)) {
            case 1:
                return NavdyContentProviderConstants.DESTINATIONS_CONTENT_URI;
            case 2:
                return NavdyContentProviderConstants.TRIPS_CONTENT_URI;
            case 3:
                return NavdyContentProviderConstants.SEARCH_HISTORY_CONTENT_URI;
            case 4:
                return NavdyContentProviderConstants.DESTINATION_CACHE_CONTENT_URI;
            case 5:
                return NavdyContentProviderConstants.PLAYLISTS_CONTENT_URI;
            case 6:
                return NavdyContentProviderConstants.PLAYLIST_MEMBERS_CONTENT_URI;
            default:
                throw new IllegalArgumentException("Unknown URI: " + baseUri);
        }
    }

    @NonNull
    private HashMap<String, String> getValues(@NonNull Uri uri) {
        switch (uriMatcher.match(uri)) {
            case 1:
                return destinationsValues;
            case 2:
                return tripsValues;
            case 3:
                return searchHistoryValues;
            case 4:
                return destinationCacheValues;
            case 5:
                return playlistsValues;
            case 6:
                return playlistMembersValues;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
    }

    public static synchronized SQLiteDatabase getSqlDb(Context context) {
        SQLiteDatabase sQLiteDatabase;
        synchronized (NavdyContentProvider.class) {
            if (sqlDB == null) {
                sqlDB = new DatabaseHelper(context).getWritableDatabase();
            }
            sQLiteDatabase = sqlDB;
        }
        return sQLiteDatabase;
    }

    public boolean onCreate() {
        return getSqlDb(getContext()) != null;
    }

    public Cursor query(@NonNull Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        queryBuilder.setTables(getTableNameForQuery(uri));
        queryBuilder.setProjectionMap(getValues(uri));
        Cursor cursor = queryBuilder.query(sqlDB, projection, selection, selectionArgs, null, null, sortOrder);
        cursor.setNotificationUri(getContentResolverSafely(), uri);
        return cursor;
    }

    private ContentResolver getContentResolverSafely() {
        Context context = getContext();
        if (context == null) {
            return null;
        }
        return context.getContentResolver();
    }

    private void notifyChangeSafely(Uri _uri) {
        ContentResolver contentResolver = getContentResolverSafely();
        if (contentResolver != null) {
            contentResolver.notifyChange(_uri, null);
        }
    }

    public Uri insert(@NonNull Uri uri, ContentValues values) {
        long rowId = sqlDB.insert(getTableName(uri), null, values);
        if (rowId > 0) {
            Uri uri_to_new_data = ContentUris.withAppendedId(getContentUri(uri), rowId);
            notifyChangeSafely(uri_to_new_data);
            return uri_to_new_data;
        }
        logger.e("Insert failed !");
        return null;
    }

    public int delete(@NonNull Uri uri, String selection, String[] selectionArgs) {
        int rowsDeleted = sqlDB.delete(getTableName(uri), selection, selectionArgs);
        notifyChangeSafely(uri);
        return rowsDeleted;
    }

    public int update(@NonNull Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        int rowsUpdated = sqlDB.update(getTableName(uri), values, selection, selectionArgs);
        notifyChangeSafely(uri);
        return rowsUpdated;
    }

    private static void goThroughAllDestinationBlobsAndSetType(SQLiteDatabase db) {
        Cursor cursor = null;
        try {
            SQLiteDatabase sQLiteDatabase = db;
            cursor = sQLiteDatabase.query("destinations", new String[]{"_id", NavdyContentProviderConstants.DESTINATIONS_PLACE_DETAIL_JSON}, "place_detail_json <> ''", null, null, null, null);
            if (cursor == null) {
                logger.e("Unable to get a destination cursor for destination type upgrade!");
                IOUtils.closeStream(cursor);
                return;
            }
            while (cursor.moveToNext()) {
                int id = DbUtils.getInt(cursor, "_id");
                Type destinationType = GoogleTextSearchDestinationResult.parseTypes(new JSONObject(DbUtils.getString(cursor, NavdyContentProviderConstants.DESTINATIONS_PLACE_DETAIL_JSON)).getJSONArray("types"));
                ContentValues contentValues = new ContentValues();
                contentValues.put("type", Integer.valueOf(destinationType.getValue()));
                SQLiteDatabase sQLiteDatabase2 = db;
                sQLiteDatabase2.update("destinations", contentValues, "_id=?", new String[]{String.valueOf(id)});
            }
            IOUtils.closeStream(cursor);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Throwable th) {
            IOUtils.closeStream(cursor);
        }
    }

    private static void goThroughAllDestinationBlobsAndSetCountryCode(SQLiteDatabase db) {
        Cursor cursor = null;
        try {
            SQLiteDatabase sQLiteDatabase = db;
            cursor = sQLiteDatabase.query("destinations", new String[]{"_id", NavdyContentProviderConstants.DESTINATIONS_PLACE_DETAIL_JSON}, "place_detail_json <> ''", null, null, null, null);
            if (cursor == null) {
                logger.e("Unable to get a destination cursor for country code upgrade!");
                IOUtils.closeStream(cursor);
                return;
            }
            while (cursor.moveToNext()) {
                int i = DbUtils.getInt(cursor, "_id");
                JSONArray addressComponents = new JSONObject(DbUtils.getString(cursor, NavdyContentProviderConstants.DESTINATIONS_PLACE_DETAIL_JSON)).optJSONArray("address_components");
                String countryCode = null;
                if (addressComponents != null) {
                    for (int aci = 0; aci < addressComponents.length(); aci++) {
                        try {
                            JSONObject obj = addressComponents.getJSONObject(aci);
                            JSONArray types = obj.getJSONArray("types");
                            for (int ti = 0; ti < types.length(); ti++) {
                                if (NavdyContentProviderConstants.DESTINATIONS_COUNTRY.equals(types.getString(ti))) {
                                    countryCode = obj.getString("short_name");
                                    break;
                                }
                            }
                        } catch (Exception e) {
                        }
                    }
                }
                if (!StringUtils.isEmptyAfterTrim(countryCode)) {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(NavdyContentProviderConstants.DESTINATIONS_COUNTRY_CODE, countryCode);
                    SQLiteDatabase sQLiteDatabase2 = db;
                    sQLiteDatabase2.update("destinations", contentValues, "_id=?", new String[]{String.valueOf(i)});
                }
            }
            IOUtils.closeStream(cursor);
        } catch (JSONException e2) {
            e2.printStackTrace();
        } catch (Throwable th) {
            IOUtils.closeStream(cursor);
        }
    }

    private static void goThroughAllFavoriteDestinationsAndLinkContacts(@NonNull SQLiteDatabase db, @NonNull Context context) {
        logger.d("goThroughAllFavoriteDestinationsAndLinkContacts");
        ArrayList<Destination> destinations = getContactsWithoutLookupKey(db);
        if (destinations == null || destinations.size() <= 0) {
            logger.d("Found no contact destination without contact lookup key");
            return;
        }
        ContentResolver contentResolver = context.getContentResolver();
        Iterator it = destinations.iterator();
        while (it.hasNext()) {
            Destination d = (Destination) it.next();
            List<ContactModel> contacts = ContactsManager.getContactsPreferablyWithPhoneNumber(d.name, contentResolver);
            if (contacts == null || contacts.size() == 0) {
                logger.d("Looking up " + d.name + " returned 0 contacts");
            } else {
                ContactModel matchedContact;
                logger.d("Looking up " + d.name + " returned " + contacts.size() + " contacts");
                if (contacts.size() == 1) {
                    matchedContact = (ContactModel) contacts.get(0);
                } else {
                    matchedContact = getMatchingContact(d.rawAddressNotForDisplay, d.streetNumber, d.streetName, contentResolver, contacts, getCacheKeysForDestinationId(db, d.id));
                }
                if (matchedContact != null) {
                    logger.d("Matching contact for " + d.name + " is " + matchedContact);
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(NavdyContentProviderConstants.DESTINATIONS_CONTACT_LOOKUP_KEY, matchedContact.lookupKey);
                    contentValues.put(NavdyContentProviderConstants.DESTINATIONS_LAST_KNOWN_CONTACT_ID, Long.valueOf(matchedContact.id));
                    contentValues.put(NavdyContentProviderConstants.DESTINATIONS_LAST_CONTACT_LOOKUP, Long.valueOf(new Date().getTime()));
                    db.update("destinations", contentValues, "_id=?", new String[]{String.valueOf(d.id)});
                } else {
                    logger.d("No matching contact for " + d.name);
                }
            }
        }
    }

    @Nullable
    private static ArrayList<Destination> getContactsWithoutLookupKey(@NonNull SQLiteDatabase db) {
        Exception e;
        Throwable th;
        ArrayList<Destination> destinations = null;
        Cursor cursor = null;
        try {
            SQLiteDatabase sQLiteDatabase = db;
            cursor = sQLiteDatabase.query("destinations", new String[]{"_id", NavdyContentProviderConstants.DESTINATIONS_NAME, NavdyContentProviderConstants.DESTINATIONS_ADDRESS, NavdyContentProviderConstants.DESTINATIONS_STREET_NUMBER, NavdyContentProviderConstants.DESTINATIONS_STREET_NAME}, "type = ? AND contact_lookup_key is null", new String[]{String.valueOf(Type.CONTACT.getValue())}, null, null, null);
            if (cursor == null || cursor.getCount() <= 0) {
                logger.e("Unable to get a destination cursor for contact linking upgrade!");
                IOUtils.closeStream(cursor);
                return null;
            }
            logger.d("cursor has " + cursor.getCount() + " destination(s)");
            ArrayList<Destination> destinations2 = new ArrayList(cursor.getCount());
            while (cursor.moveToNext()) {
                try {
                    Destination d = new Destination();
                    d.id = DbUtils.getInt(cursor, "_id");
                    d.name = DbUtils.getString(cursor, NavdyContentProviderConstants.DESTINATIONS_NAME);
                    d.rawAddressNotForDisplay = DbUtils.getString(cursor, NavdyContentProviderConstants.DESTINATIONS_ADDRESS);
                    d.streetNumber = DbUtils.getString(cursor, NavdyContentProviderConstants.DESTINATIONS_STREET_NUMBER);
                    d.streetName = DbUtils.getString(cursor, NavdyContentProviderConstants.DESTINATIONS_STREET_NAME);
                    destinations2.add(d);
                } catch (Exception e2) {
                    e = e2;
                    destinations = destinations2;
                    try {
                        logger.e("Something went wrong while trying to get a destination cursor for contact linking upgrade!", e);
                        IOUtils.closeStream(cursor);
                        return destinations;
                    } catch (Throwable th2) {
                        th = th2;
                        IOUtils.closeStream(cursor);
                        th.printStackTrace();
                    }
                } catch (Throwable th3) {
                    th = th3;
                    destinations = destinations2;
                    IOUtils.closeStream(cursor);
                    th.printStackTrace();
                }
            }
            IOUtils.closeStream(cursor);
            destinations = destinations2;
            return destinations;
        } catch (Exception e3) {
            e = e3;
            logger.e("Something went wrong while trying to get a destination cursor for contact linking upgrade!", e);
            IOUtils.closeStream(cursor);
            return destinations;
        }
    }

    @Nullable
    @CheckResult
    public static ArrayList<String> getCacheKeysForDestinationId(int destinationId) {
        return getCacheKeysForDestinationId(NavdyApplication.getAppContext().getContentResolver(), destinationId);
    }

    @Nullable
    @CheckResult
    private static ArrayList<String> getCacheKeysForDestinationId(@NonNull final SQLiteDatabase db, int destinationId) {
        return getCacheKeysForDestinationId(new CacheQuerier() {
            public Cursor runQuery(String[] projection, String selection, String[] selectionArgs) {
                return db.query("destination_cache", projection, selection, selectionArgs, null, null, null);
            }
        }, destinationId);
    }

    @Nullable
    @CheckResult
    private static ArrayList<String> getCacheKeysForDestinationId(@NonNull final ContentResolver cr, int destinationId) {
        return getCacheKeysForDestinationId(new CacheQuerier() {
            public Cursor runQuery(String[] projection, String selection, String[] selectionArgs) {
                return cr.query(NavdyContentProviderConstants.DESTINATION_CACHE_CONTENT_URI, projection, selection, selectionArgs, null, null);
            }
        }, destinationId);
    }

    @Nullable
    @CheckResult
    private static ArrayList<String> getCacheKeysForDestinationId(@NonNull CacheQuerier cq, int destinationId) {
        Exception e;
        Throwable th;
        if (destinationId < 0) {
            logger.e("CACHE: Can't lookup entries for a negative ID!");
            return null;
        }
        ArrayList<String> addresses = null;
        Cursor cursor = null;
        try {
            String selection = "destination_id=?";
            cursor = cq.runQuery(new String[]{"location"}, "destination_id=?", new String[]{String.valueOf(destinationId)});
            if (cursor != null) {
                ArrayList<String> addresses2 = new ArrayList(cursor.getCount());
                while (cursor.moveToNext()) {
                    try {
                        addresses2.add(DbUtils.getString(cursor, "location"));
                    } catch (Exception e2) {
                        e = e2;
                        addresses = addresses2;
                        try {
                            logger.e("CACHE: Something went wrong while trying to query the HERE cache!", e);
                            IOUtils.closeStream(cursor);
                            return addresses;
                        } catch (Throwable th2) {
                            th = th2;
                            IOUtils.closeStream(cursor);
                            th.printStackTrace();
                        }
                    } catch (Throwable th3) {
                        th = th3;
                        addresses = addresses2;
                        IOUtils.closeStream(cursor);
                        th.printStackTrace();
                    }
                }
                addresses = addresses2;
            }
            IOUtils.closeStream(cursor);
            return addresses;
        } catch (Exception e3) {
            e = e3;
            logger.e("CACHE: Something went wrong while trying to query the HERE cache!", e);
            IOUtils.closeStream(cursor);
            return addresses;
        }
    }

    @Nullable
    private static ContactModel getMatchingContact(@Nullable String address, @Nullable String streetNumber, @Nullable String streetName, @NonNull ContentResolver contentResolver, @NonNull List<ContactModel> contacts, @Nullable ArrayList<String> cachedAddresses) {
        for (ContactModel contact : contacts) {
            List<Address> addresses = ContactsManager.getAddressFromLookupKey(contentResolver, contact.lookupKey, contact.id);
            contact.addAddresses(addresses);
            for (Address contactAddress : addresses) {
                if (isMatchingContact(address, streetNumber, streetName, contactAddress)) {
                    return contact;
                }
                if (cachedAddresses != null) {
                    String fullAddress = cleanUpAddressForComparison(contactAddress.getFullAddress());
                    String contactStreet = cleanUpAddressForComparison(contactAddress.street);
                    Iterator it = cachedAddresses.iterator();
                    while (it.hasNext()) {
                        if (isSomewhatMatchingAddress(cleanUpAddressForComparison((String) it.next()), fullAddress, contactStreet)) {
                            return contact;
                        }
                    }
                    continue;
                }
            }
        }
        return null;
    }

    private static boolean isMatchingContact(@Nullable String address, @Nullable String streetNumber, @Nullable String streetName, @NonNull Address contactAddress) {
        String fullAddress = contactAddress.getFullAddress();
        String contactStreet = contactAddress.street;
        logger.d("Comparing " + address + " with " + fullAddress);
        logger.d("Comparing " + streetNumber + " " + streetName + " with " + contactStreet);
        address = cleanUpAddressForComparison(address);
        fullAddress = cleanUpAddressForComparison(fullAddress);
        contactStreet = cleanUpAddressForComparison(contactStreet);
        return isSomewhatMatchingAddress(address, fullAddress, contactStreet) || ((StringUtils.containsAfterTrim(contactStreet, streetNumber) && StringUtils.containsAfterTrim(contactStreet, streetName)) || (StringUtils.containsAfterTrim(fullAddress, streetNumber) && StringUtils.containsAfterTrim(fullAddress, streetName)));
    }

    @Nullable
    private static String cleanUpAddressForComparison(@Nullable String address) {
        return StringUtils.isEmptyAfterTrim(address) ? address : address.replaceAll("[,\n\t]", " ").replaceAll(" +", " ").trim();
    }

    private static boolean isSomewhatMatchingAddress(@Nullable String cachedAddress, @Nullable String fullAddress, @Nullable String contactStreet) {
        logger.d("Comparing " + cachedAddress + " with " + fullAddress + " and " + contactStreet);
        return StringUtils.containsAfterTrim(fullAddress, cachedAddress) || StringUtils.containsAfterTrim(cachedAddress, fullAddress) || StringUtils.containsAfterTrim(cachedAddress, contactStreet);
    }

    public static Cursor getDestinationCursor() {
        return getDestinationCursor(new Pair(null, null));
    }

    public static Cursor getDestinationCursor(@NonNull Pair<String, String[]> selection) {
        return getDestinationCursor(selection, "_id ASC");
    }

    public static Cursor getDestinationCursor(@NonNull Pair<String, String[]> selection, String sortOrder) {
        ContentResolver contentResolver = NavdyApplication.getAppContext().getContentResolver();
        if (contentResolver == null) {
            return null;
        }
        return contentResolver.query(NavdyContentProviderConstants.DESTINATIONS_CONTENT_URI, NavdyContentProviderConstants.DESTINATIONS_PROJECTION, (String) selection.first, (String[]) selection.second, sortOrder);
    }

    public static Destination getDestinationItemAt(Cursor cursor, int position) {
        if (cursor == null || !cursor.moveToPosition(position)) {
            return null;
        }
        int id = cursor.getInt(cursor.getColumnIndex("_id"));
        String placeId = cursor.getString(cursor.getColumnIndex(NavdyContentProviderConstants.DESTINATIONS_PLACE_ID));
        long lastPlaceIdRefresh = cursor.getLong(cursor.getColumnIndex(NavdyContentProviderConstants.DESTINATIONS_LAST_PLACE_ID_REFRESH));
        double displayLat = cursor.getDouble(cursor.getColumnIndex(NavdyContentProviderConstants.DESTINATIONS_DISPLAY_LAT));
        double displayLong = cursor.getDouble(cursor.getColumnIndex(NavdyContentProviderConstants.DESTINATIONS_DISPLAY_LONG));
        double navigationLat = cursor.getDouble(cursor.getColumnIndex(NavdyContentProviderConstants.DESTINATIONS_NAVIGATION_LAT));
        double navigationLong = cursor.getDouble(cursor.getColumnIndex(NavdyContentProviderConstants.DESTINATIONS_NAVIGATION_LONG));
        String name = cursor.getString(cursor.getColumnIndex(NavdyContentProviderConstants.DESTINATIONS_NAME));
        String address = cursor.getString(cursor.getColumnIndex(NavdyContentProviderConstants.DESTINATIONS_ADDRESS));
        String streetNumber = cursor.getString(cursor.getColumnIndex(NavdyContentProviderConstants.DESTINATIONS_STREET_NUMBER));
        String streetName = cursor.getString(cursor.getColumnIndex(NavdyContentProviderConstants.DESTINATIONS_STREET_NAME));
        String city = cursor.getString(cursor.getColumnIndex(NavdyContentProviderConstants.DESTINATIONS_CITY));
        String state = cursor.getString(cursor.getColumnIndex("state"));
        String zipCode = cursor.getString(cursor.getColumnIndex(NavdyContentProviderConstants.DESTINATIONS_ZIP_CODE));
        String country = cursor.getString(cursor.getColumnIndex(NavdyContentProviderConstants.DESTINATIONS_COUNTRY));
        String countryCode = cursor.getString(cursor.getColumnIndex(NavdyContentProviderConstants.DESTINATIONS_COUNTRY_CODE));
        boolean doNotSuggest = cursor.getInt(cursor.getColumnIndex(NavdyContentProviderConstants.DESTINATIONS_DO_NOT_SUGGEST)) == 1;
        Destination d = new Destination(id, placeId, lastPlaceIdRefresh, displayLat, displayLong, navigationLat, navigationLong, name, address, streetNumber, streetName, city, state, zipCode, country, countryCode, doNotSuggest, cursor.getLong(cursor.getColumnIndex(NavdyContentProviderConstants.DESTINATIONS_LAST_ROUTED_DATE)), cursor.getString(cursor.getColumnIndex(NavdyContentProviderConstants.DESTINATIONS_LABEL)), cursor.getInt(cursor.getColumnIndex(NavdyContentProviderConstants.DESTINATIONS_ORDER)), cursor.getInt(cursor.getColumnIndex(NavdyContentProviderConstants.DESTINATIONS_FAVORITE_TYPE)), cursor.getString(cursor.getColumnIndex(NavdyContentProviderConstants.DESTINATIONS_PLACE_DETAIL_JSON)), Precision.get(cursor.getInt(cursor.getColumnIndex(NavdyContentProviderConstants.DESTINATIONS_PRECISION_LEVEL))), Type.get(cursor.getInt(cursor.getColumnIndex("type"))), cursor.getString(cursor.getColumnIndex(NavdyContentProviderConstants.DESTINATIONS_CONTACT_LOOKUP_KEY)), cursor.getLong(cursor.getColumnIndex(NavdyContentProviderConstants.DESTINATIONS_LAST_KNOWN_CONTACT_ID)), cursor.getLong(cursor.getColumnIndex(NavdyContentProviderConstants.DESTINATIONS_LAST_CONTACT_LOOKUP)));
        d.rawAddressVariations = getCacheKeysForDestinationId(id);
        return d;
    }

    public static Destination getThisDestination(int destinationId) {
        try {
            return getDestinationItemFromSelection(new Pair("_id = ?", new String[]{String.valueOf(destinationId)}));
        } catch (NumberFormatException e) {
            logger.e("Exception while parsing destination ID", e);
            return null;
        }
    }

    public static Destination getThisDestination(String destinationId) {
        try {
            return getDestinationItemFromSelection(new Pair("_id = ?", new String[]{destinationId}));
        } catch (NumberFormatException e) {
            logger.e("Exception while parsing destination ID", e);
            return null;
        }
    }

    public static Destination getThisDestination(Destination destination) {
        if (destination == null) {
            return null;
        }
        try {
            ContentResolver contentResolver = NavdyApplication.getAppContext().getContentResolver();
            Pair<String, String[]> comparisonSelectionClause = destination.getComparisonSelectionClause();
            if (comparisonSelectionClause == null) {
                return destination;
            }
            Cursor c = contentResolver.query(NavdyContentProviderConstants.DESTINATIONS_CONTENT_URI, NavdyContentProviderConstants.DESTINATIONS_PROJECTION, (String) comparisonSelectionClause.first, (String[]) comparisonSelectionClause.second, null);
            if (c != null && c.moveToFirst()) {
                destination = getDestinationItemAt(c, 0);
            }
            IOUtils.closeStream(c);
            return destination;
        } catch (Exception e) {
            logger.i("Destination not found in db for: " + destination);
            return destination;
        } finally {
            IOUtils.closeStream(null);
        }
    }

    public static Destination getThisDestinationWithPlaceId(String placeId) {
        return getDestinationItemFromSelection(new Pair("place_id = ?", new String[]{placeId}));
    }

    public static ArrayList<Destination> getDestinationsFromSearchQuery(@NonNull String query) {
        logger.v("query: " + query);
        query = "%" + query.trim().replaceAll(" ", "%") + "%";
        ArrayList<Destination> destinations = new ArrayList();
        try {
            ContentResolver contentResolver = NavdyApplication.getAppContext().getContentResolver();
            if (contentResolver == null) {
                destinations = null;
                return destinations;
            }
            Cursor cursor = contentResolver.query(NavdyContentProviderConstants.DESTINATIONS_CONTENT_URI, NavdyContentProviderConstants.DESTINATIONS_PROJECTION, "place_name like ? OR favorite_label like ? OR address like ?", new String[]{query, query, query}, "place_name ASC");
            if (cursor != null) {
                while (cursor.moveToNext()) {
                    Destination destination = getThisDestination(cursor.getInt(cursor.getColumnIndex("_id")));
                    if (destination != null) {
                        destination.searchResultType = SearchType.RECENT_PLACES.getValue();
                        destinations.add(destination);
                    }
                }
            }
            IOUtils.closeStream(cursor);
            logger.d("getDestinationsFromSearchQuery: " + destinations);
            return destinations;
        } finally {
            IOUtils.closeStream(null);
        }
    }

    public static ArrayList<String> getSearchHistoryFromSearchQuery(String constraint) {
        ArrayList<String> pastQueries = new ArrayList();
        try {
            ContentResolver cr = NavdyApplication.getAppContext().getContentResolver();
            if (cr == null) {
                pastQueries = null;
                return pastQueries;
            }
            Cursor c = cr.query(NavdyContentProviderConstants.SEARCH_HISTORY_CONTENT_URI, new String[]{"query"}, "query like ?", new String[]{constraint + "%"}, "last_searched_on DESC");
            while (c != null && c.moveToNext()) {
                pastQueries.add(c.getString(0));
            }
            IOUtils.closeStream(c);
            return pastQueries;
        } finally {
            IOUtils.closeStream(null);
        }
    }

    @Nullable
    public static Destination getDestinationItemFromSelection(@NonNull Pair<String, String[]> selection) {
        Destination destinationItem = null;
        Cursor cursor = null;
        try {
            cursor = getDestinationCursor(selection);
            destinationItem = getDestinationItemAt(cursor, 0);
            return destinationItem;
        } finally {
            IOUtils.closeStream(cursor);
        }
    }

    @Nullable
    public static String getNonStalePlaceDetailInfoFor(String placeId) {
        Destination destination = getThisDestinationWithPlaceId(placeId);
        if (destination == null || destination.isPlaceDetailsInfoStale()) {
            return null;
        }
        return destination.placeDetailJson;
    }

    public static Cursor getRecentsCursor() {
        return getRecentsCursor(new Pair(null, null));
    }

    public static Cursor getRecentsCursor(@NonNull Pair<String, String[]> selection) {
        selection = restrictSelectionToRecents(selection);
        ContentResolver contentResolver = NavdyApplication.getAppContext().getContentResolver();
        if (contentResolver == null) {
            return null;
        }
        return contentResolver.query(NavdyContentProviderConstants.DESTINATIONS_CONTENT_URI, NavdyContentProviderConstants.DESTINATIONS_PROJECTION, (String) selection.first, (String[]) selection.second, "last_routed_date DESC");
    }

    @NonNull
    public static Pair<String, String[]> restrictSelectionToRecents(Pair<String, String[]> selection) {
        if (selection == null || StringUtils.isEmptyAfterTrim((CharSequence) selection.first)) {
            return new Pair("last_routed_date <> 0", null);
        }
        return new Pair("(" + ((String) selection.first) + ") AND (" + NavdyContentProviderConstants.DESTINATIONS_LAST_ROUTED_DATE + " <> 0)", selection.second);
    }

    public static Destination getRecentItemFromSelection(@NonNull Pair<String, String[]> selection) {
        return getDestinationItemFromSelection(restrictSelectionToRecents(selection));
    }

    public static Cursor getFavoritesCursor() {
        return getFavoritesCursor(new Pair(null, null));
    }

    public static Cursor getFavoritesCursor(@NonNull Pair<String, String[]> selection) {
        selection = restrictSelectionToFavorites(selection);
        ContentResolver contentResolver = NavdyApplication.getAppContext().getContentResolver();
        if (contentResolver == null) {
            return null;
        }
        return contentResolver.query(NavdyContentProviderConstants.DESTINATIONS_CONTENT_URI, NavdyContentProviderConstants.DESTINATIONS_PROJECTION, (String) selection.first, (String[]) selection.second, "destinations.favorite_listing_order ASC");
    }

    @NonNull
    public static Pair<String, String[]> restrictSelectionToFavorites(Pair<String, String[]> selection) {
        String isFavoriteClause = "is_special <> 0";
        if (selection == null || StringUtils.isEmptyAfterTrim((CharSequence) selection.first)) {
            return new Pair("is_special <> 0", null);
        }
        return new Pair("(" + ((String) selection.first) + ") AND (" + "is_special <> 0" + ")", selection.second);
    }

    public static Destination getHome() {
        return getFavoriteItemFromSelection(new Pair("is_special ==  ?", new String[]{String.valueOf(-3)}));
    }

    public static Destination getWork() {
        return getFavoriteItemFromSelection(new Pair("is_special ==  ?", new String[]{String.valueOf(-2)}));
    }

    public static boolean isHomeSet() {
        return getHome() != null;
    }

    public static boolean isWorkSet() {
        return getWork() != null;
    }

    public static Destination getFavoriteItemFromSelection(@NonNull Pair<String, String[]> selection) {
        return getDestinationItemFromSelection(restrictSelectionToFavorites(selection));
    }

    public static Cursor getTripsCursor() {
        return getTripsCursor(new Pair(null, null));
    }

    public static Cursor getTripsCursor(@NonNull Pair<String, String[]> selection) {
        ContentResolver contentResolver = NavdyApplication.getAppContext().getContentResolver();
        if (contentResolver == null) {
            return null;
        }
        return contentResolver.query(NavdyContentProviderConstants.TRIPS_CONTENT_URI, NavdyContentProviderConstants.TRIPS_PROJECTION, (String) selection.first, (String[]) selection.second, "start_time DESC");
    }

    public static long getMaxTripId() {
        long j = -1;
        ContentResolver contentResolver = NavdyApplication.getAppContext().getContentResolver();
        if (contentResolver != null) {
            Cursor cursor = null;
            try {
                cursor = contentResolver.query(NavdyContentProviderConstants.TRIPS_CONTENT_URI, new String[]{"MAX(_id)"}, null, null, null);
                if (cursor == null || !cursor.moveToFirst()) {
                    IOUtils.closeStream(cursor);
                } else {
                    j = cursor.getLong(0);
                    cursor.close();
                }
            } finally {
                IOUtils.closeStream(cursor);
            }
        }
        return j;
    }

    public static Cursor getGroupedTripsCursor() {
        ContentResolver contentResolver = NavdyApplication.getAppContext().getContentResolver();
        if (contentResolver == null) {
            return null;
        }
        return contentResolver.query(NavdyContentProviderConstants.TRIPS_CONTENT_URI, new String[]{"count(_id) as occurrences", NavdyContentProviderConstants.TRIPS_DESTINATION_ID}, "destination_id != 0) group by (destination_id", null, "occurrences DESC");
    }

    public static Trip getTripsItemAt(Cursor cursor, int position) {
        if (cursor == null || !cursor.moveToPosition(position)) {
            return null;
        }
        return new Trip(cursor.getInt(cursor.getColumnIndex("_id")), cursor.getLong(cursor.getColumnIndex(NavdyContentProviderConstants.TRIPS_TRIP_NUMBER)), cursor.getLong(cursor.getColumnIndex(NavdyContentProviderConstants.TRIPS_START_TIME)), cursor.getInt(cursor.getColumnIndex(NavdyContentProviderConstants.TRIPS_START_TIME_ZONE_N_DST)), cursor.getInt(cursor.getColumnIndex(NavdyContentProviderConstants.TRIPS_START_ODOMETER)), cursor.getDouble(cursor.getColumnIndex(NavdyContentProviderConstants.TRIPS_START_LAT)), cursor.getDouble(cursor.getColumnIndex(NavdyContentProviderConstants.TRIPS_START_LONG)), cursor.getLong(cursor.getColumnIndex(NavdyContentProviderConstants.TRIPS_END_TIME)), cursor.getInt(cursor.getColumnIndex(NavdyContentProviderConstants.TRIPS_END_ODOMETER)), cursor.getDouble(cursor.getColumnIndex(NavdyContentProviderConstants.TRIPS_END_LAT)), cursor.getDouble(cursor.getColumnIndex(NavdyContentProviderConstants.TRIPS_END_LONG)), cursor.getLong(cursor.getColumnIndex(NavdyContentProviderConstants.TRIPS_ARRIVED_AT_DESTINATION)), cursor.getInt(cursor.getColumnIndex(NavdyContentProviderConstants.TRIPS_DESTINATION_ID)));
    }

    public static Trip getThisTrip(String tripNumber) {
        return getTripFromSelection(new Pair("trip_number=?", new String[]{String.valueOf(tripNumber)}));
    }

    @Nullable
    public static Trip getTripFromSelection(@NonNull Pair<String, String[]> selection) {
        Trip tripsItem = null;
        Cursor cursor = null;
        try {
            cursor = getTripsCursor(selection);
            tripsItem = getTripsItemAt(cursor, 0);
            return tripsItem;
        } finally {
            IOUtils.closeStream(cursor);
        }
    }

    public static Cursor getSearchHistoryCursor() {
        return getSearchHistoryCursor(new Pair(null, null));
    }

    @WorkerThread
    public static Cursor getSearchHistoryCursor(@NonNull Pair<String, String[]> selection) {
        SystemUtils.ensureNotOnMainThread();
        ContentResolver contentResolver = NavdyApplication.getAppContext().getContentResolver();
        if (contentResolver == null) {
            return null;
        }
        return contentResolver.query(NavdyContentProviderConstants.SEARCH_HISTORY_CONTENT_URI, NavdyContentProviderConstants.SEARCH_HISTORY_PROJECTION, (String) selection.first, (String[]) selection.second, "last_searched_on DESC");
    }

    @WorkerThread
    public static int addToSearchHistory(String query) {
        SystemUtils.ensureNotOnMainThread();
        Context context = NavdyApplication.getAppContext();
        ContentResolver contentResolver = context.getContentResolver();
        if (contentResolver == null || StringUtils.isEmptyAfterTrim(query) || query.equals(context.getString(R.string.gas)) || query.equals(context.getString(R.string.parking)) || query.equals(context.getString(R.string.food)) || query.equals(context.getString(R.string.atm))) {
            return -1;
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("last_searched_on", Long.valueOf(new Date().getTime()));
        Cursor cursor = null;
        try {
            cursor = contentResolver.query(NavdyContentProviderConstants.SEARCH_HISTORY_CONTENT_URI, NavdyContentProviderConstants.SEARCH_HISTORY_PROJECTION, "lower(query)=?", new String[]{query.toLowerCase()}, null);
            if (cursor == null || !cursor.moveToFirst()) {
                IOUtils.closeStream(cursor);
                contentValues.put("query", query);
                return contentResolver.insert(NavdyContentProviderConstants.SEARCH_HISTORY_CONTENT_URI, contentValues) != null ? 1 : -1;
            } else {
                int id = cursor.getInt(cursor.getColumnIndex("_id"));
                int update = contentResolver.update(NavdyContentProviderConstants.SEARCH_HISTORY_CONTENT_URI, contentValues, "_id=?", new String[]{String.valueOf(id)});
                return update;
            }
        } finally {
            IOUtils.closeStream(cursor);
        }
    }

    @WorkerThread
    public static int removeFromSearchHistory(int id) {
        SystemUtils.ensureNotOnMainThread();
        ContentResolver contentResolver = NavdyApplication.getAppContext().getContentResolver();
        if (contentResolver == null) {
            return -1;
        }
        return contentResolver.delete(NavdyContentProviderConstants.SEARCH_HISTORY_CONTENT_URI, "_id=?", new String[]{String.valueOf(id)});
    }

    @Nullable
    @WorkerThread
    public static DestinationCacheEntry getCacheEntryIfExists(@Nullable String location) {
        logger.d("CACHE: Checking the cache for " + location);
        if (StringUtils.isEmptyAfterTrim(location)) {
            logger.e("CACHE: Can't lookup entries for a null location!");
            return null;
        }
        SystemUtils.ensureNotOnMainThread();
        ContentResolver contentResolver = NavdyApplication.getAppContext().getContentResolver();
        if (contentResolver == null) {
            logger.e("CACHE: Unable to get a content resolver!");
            return null;
        }
        removeOutOfDateCacheEntries();
        Cursor cursor = null;
        try {
            cursor = contentResolver.query(NavdyContentProviderConstants.DESTINATION_CACHE_CONTENT_URI, NavdyContentProviderConstants.DESTINATION_CACHE_PROJECTION, "location=?", new String[]{location}, null);
            if (cursor == null || !cursor.moveToFirst()) {
                IOUtils.closeStream(cursor);
                logger.d("CACHE: Couldn't find any cache entry for this location: " + location);
                return null;
            }
            int id = cursor.getInt(cursor.getColumnIndex("_id"));
            long lastResponseDate = cursor.getLong(cursor.getColumnIndex("last_response_date"));
            String locationString = cursor.getString(cursor.getColumnIndex("location"));
            int destinationId = cursor.getInt(cursor.getColumnIndex(NavdyContentProviderConstants.TRIPS_DESTINATION_ID));
            DestinationCacheEntry dce = new DestinationCacheEntry(id, lastResponseDate, locationString, destinationId);
            dce.destination = getThisDestination(destinationId);
            logger.d("CACHE: Found this cache entry for this location: " + dce);
            return dce;
        } finally {
            IOUtils.closeStream(cursor);
        }
    }

    @WorkerThread
    private static void removeOutOfDateCacheEntries() {
        SystemUtils.ensureNotOnMainThread();
        ContentResolver contentResolver = NavdyApplication.getAppContext().getContentResolver();
        if (contentResolver != null) {
            long limit = new Date().getTime() - NavdyContentProviderConstants.MAX_DESTINATION_CACHE_AGE;
            int nbDeleted = contentResolver.delete(NavdyContentProviderConstants.DESTINATION_CACHE_CONTENT_URI, "last_response_date<? AND destination_id = -1", new String[]{String.valueOf(limit)});
            if (nbDeleted > 0) {
                logger.d("CACHE: Removed " + nbDeleted + " old entry(s) from the cache");
            }
        }
    }

    @WorkerThread
    public static void addToCacheIfNotAlreadyIn(@NonNull String address, @Nullable Destination destination) {
        if (!StringUtils.isEmptyAfterTrim(address)) {
            SystemUtils.ensureNotOnMainThread();
            if (NavdyApplication.getAppContext().getContentResolver() != null) {
                if (!(destination == null || destination.hasValidNavCoordinates())) {
                    destination = null;
                }
                logger.d("CACHE: addToCacheIfNotAlreadyIn: address = " + address + " destination = " + destination);
                if (getCacheEntryIfExists(address) == null) {
                    insertNewDestinationInCache(address, destination);
                } else {
                    logger.d("CACHE: Location already present in the cache so will not add it.");
                }
            }
        }
    }

    @WorkerThread
    private static void insertNewDestinationInCache(@NonNull String location, @Nullable Destination destination) {
        if (!StringUtils.isEmptyAfterTrim(location)) {
            logger.d("CACHE: Inserting the following destination in the cache: location = " + location + " | destination = " + destination);
            if (destination == null || destination.id > 0) {
                SystemUtils.ensureNotOnMainThread();
                ContentResolver contentResolver = NavdyApplication.getAppContext().getContentResolver();
                if (contentResolver != null) {
                    removeOldestEntryIfCacheMaxSizeExceeded();
                    int destinationId = destination != null ? destination.id : -1;
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("last_response_date", Long.valueOf(new Date().getTime()));
                    contentValues.put("location", location);
                    contentValues.put(NavdyContentProviderConstants.TRIPS_DESTINATION_ID, Integer.valueOf(destinationId));
                    contentResolver.insert(NavdyContentProviderConstants.DESTINATION_CACHE_CONTENT_URI, contentValues);
                    return;
                }
                return;
            }
            throw new RuntimeException("Trying to insert a non null destination that has no ID! This should never happen");
        }
    }

    @WorkerThread
    private static void removeOldestEntryIfCacheMaxSizeExceeded() {
        SystemUtils.ensureNotOnMainThread();
        ContentResolver contentResolver = NavdyApplication.getAppContext().getContentResolver();
        if (contentResolver != null) {
            try {
                Cursor cursor = contentResolver.query(NavdyContentProviderConstants.DESTINATION_CACHE_CONTENT_URI, NavdyContentProviderConstants.DESTINATION_CACHE_PROJECTION, null, null, "last_response_date ASC");
                if (cursor != null) {
                    int count = cursor.getCount();
                    long nbEntriesToRemove = ((long) count) - 1000;
                    if (nbEntriesToRemove > 0) {
                        logger.d("CACHE: Too many entries in the cache. Removing " + nbEntriesToRemove + " out of " + count);
                    }
                    for (int i = 0; ((long) i) < nbEntriesToRemove; i++) {
                        if (cursor.moveToPosition(i)) {
                            removeThisCacheEntry(cursor.getInt(cursor.getColumnIndex("_id")));
                        }
                    }
                }
                IOUtils.closeStream(cursor);
            } catch (Throwable th) {
                IOUtils.closeStream(null);
            }
        }
    }

    @WorkerThread
    private static void removeThisCacheEntry(int id) {
        SystemUtils.ensureNotOnMainThread();
        ContentResolver contentResolver = NavdyApplication.getAppContext().getContentResolver();
        if (contentResolver != null) {
            contentResolver.delete(NavdyContentProviderConstants.DESTINATION_CACHE_CONTENT_URI, "_id=?", new String[]{String.valueOf(id)});
        }
    }

    public static void clearCache() {
        SQLiteDatabase database = getSqlDb(NavdyApplication.getAppContext());
        database.delete("destination_cache", null, null);
        database.execSQL(CREATE_DESTINATION_CACHE_TABLE);
    }

    @WorkerThread
    public static Cursor getPlaylistsCursor() {
        SystemUtils.ensureNotOnMainThread();
        return NavdyApplication.getAppContext().getContentResolver().query(NavdyContentProviderConstants.PLAYLISTS_CONTENT_URI, NavdyContentProviderConstants.PLAYLISTS_PROJECTION, null, null, MusicDbUtils.getOrderString("playlist_name"));
    }

    @WorkerThread
    public static Cursor getPlaylistMembersCursor(int playlistId) {
        SystemUtils.ensureNotOnMainThread();
        ContentResolver contentResolver = NavdyApplication.getAppContext().getContentResolver();
        if (contentResolver == null) {
            return null;
        }
        String[] args = new String[]{String.valueOf(playlistId)};
        return contentResolver.query(NavdyContentProviderConstants.PLAYLIST_MEMBERS_CONTENT_URI, NavdyContentProviderConstants.PLAYLIST_MEMBERS_PROJECTION, "playlist_id = ?", args, null);
    }

    public static void addPlaylistToDb(int playlistId, String playlistName) {
        Context context = NavdyApplication.getAppContext();
        ContentValues playlistContentValues = new ContentValues(2);
        playlistContentValues.put("playlist_id", Integer.valueOf(playlistId));
        playlistContentValues.put("playlist_name", playlistName);
        context.getContentResolver().insert(NavdyContentProviderConstants.PLAYLISTS_CONTENT_URI, playlistContentValues);
    }

    public static void addPlaylistMemberToDb(int playlistId, String sourceId, String artist, String album, String title) {
        Context context = NavdyApplication.getAppContext();
        ContentValues memberContentValues = new ContentValues(5);
        memberContentValues.put("playlist_id", Integer.valueOf(playlistId));
        memberContentValues.put("SourceId", sourceId);
        memberContentValues.put("artist", artist);
        memberContentValues.put("album", album);
        memberContentValues.put("title", title);
        context.getContentResolver().insert(NavdyContentProviderConstants.PLAYLIST_MEMBERS_CONTENT_URI, memberContentValues);
    }

    @WorkerThread
    public static void deleteAllPlaylists() {
        SystemUtils.ensureNotOnMainThread();
        ContentResolver contentResolver = NavdyApplication.getAppContext().getContentResolver();
        contentResolver.delete(NavdyContentProviderConstants.PLAYLISTS_CONTENT_URI, null, null);
        contentResolver.delete(NavdyContentProviderConstants.PLAYLIST_MEMBERS_CONTENT_URI, null, null);
    }

    @WorkerThread
    public static void deletePlaylist(int playlistId) {
        SystemUtils.ensureNotOnMainThread();
        ContentResolver contentResolver = NavdyApplication.getAppContext().getContentResolver();
        String query = "playlist_id = ?";
        String[] args = new String[]{String.valueOf(playlistId)};
        contentResolver.delete(NavdyContentProviderConstants.PLAYLISTS_CONTENT_URI, query, args);
        contentResolver.delete(NavdyContentProviderConstants.PLAYLIST_MEMBERS_CONTENT_URI, query, args);
    }
}
