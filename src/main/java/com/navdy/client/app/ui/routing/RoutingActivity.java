package com.navdy.client.app.ui.routing;

import android.app.Activity;
import android.content.Intent;
import android.graphics.PointF;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.here.android.mpa.mapping.Map;
import com.here.android.mpa.mapping.Map.Animation;
import com.here.android.mpa.mapping.MapGesture.OnGestureListener.OnGestureListenerAdapter;
import com.here.android.mpa.mapping.MapMarker;
import com.alelec.navdyclient.R;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.AppInstance;
import com.navdy.client.app.framework.DeviceConnection.DeviceDisconnectedEvent;
import com.navdy.client.app.framework.map.MapUtils;
import com.navdy.client.app.framework.models.Destination;
import com.navdy.client.app.framework.navigation.NavdyRouteHandler;
import com.navdy.client.app.framework.navigation.NavdyRouteHandler.Error;
import com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteInfo;
import com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteListener;
import com.navdy.client.app.framework.servicehandler.NetworkStatusManager;
import com.navdy.client.app.framework.servicehandler.NetworkStatusManager.ReachabilityEvent;
import com.navdy.client.app.tracking.Tracker;
import com.navdy.client.app.tracking.TrackerConstants.Screen;
import com.navdy.client.app.ui.base.BaseHereMapFragment;
import com.navdy.client.app.ui.base.BaseHereMapFragment.OnHereMapFragmentReady;
import com.navdy.client.app.ui.base.BaseToolbarActivity;
import com.navdy.client.app.ui.customviews.FluctuatorAnimatorView;
import com.navdy.client.app.ui.customviews.Gauge;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Subscribe;
import javax.inject.Inject;

import static android.view.View.GONE;
import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;

public class RoutingActivity extends BaseToolbarActivity implements NavdyRouteListener {
    private static final int HERE_MAP_PADDING = MapUtils.hereMapSidePadding;
    private static final int HERE_MAP_TOP_PADDING = (NavdyApplication.getAppContext().getResources().getDimensionPixelSize(R.dimen.search_bar_margin) + MapUtils.hereMapTopDownPadding);
    private static final Logger logger = new Logger(RoutingActivity.class);
    private RelativeLayout activeTripCardFrameLayoutContainer;
    private boolean canRetry;
    private MapMarker destinationMarker;
    private FluctuatorAnimatorView fluctuatorAnimatorView;
    private Gauge gauge;
    private BaseHereMapFragment hereMapFragment;
    private final NavdyRouteHandler navdyRouteHandler = NavdyRouteHandler.getInstance();
    @Inject
    NetworkStatusManager networkStatusManager = null;
    private RelativeLayout offlineBanner;
    private TextView routeName;

    public static void startRoutingActivity(Activity activity) {
        if (activity != null) {
            activity.startActivity(new Intent(activity.getApplicationContext(), RoutingActivity.class));
        } else {
            logger.e("could not start routing activity, origin activity is null");
        }
    }

    public void retryRoute(View view) {
        if (this.canRetry) {
            this.canRetry = false;
            this.navdyRouteHandler.retryRoute();
        }
    }

    @Subscribe
    public void appInstanceDeviceDisconnectedEvent(DeviceDisconnectedEvent deviceDisconnectedEvent) {
        logger.v("App Disconnected");
        this.handler.removeCallbacks(null);
        finish();
    }

    protected void onCreate(Bundle savedInstanceState) {
        logger.v("onCreate");
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.routing_activity);
//        Injector.inject(NavdyApplication.getAppContext(), this);
        NavdyApplication.deps.inject(this);
        this.activeTripCardFrameLayoutContainer = (RelativeLayout) findViewById(R.id.trip_overview_estimates);
        this.gauge = (Gauge) findViewById(R.id.gauge);
        this.fluctuatorAnimatorView = (FluctuatorAnimatorView) findViewById(R.id.fluctuator);
        this.offlineBanner = (RelativeLayout) findViewById(R.id.offline_banner);
        this.routeName = (TextView) findViewById(R.id.route_name);
        initUi();
    }

    public void onResume() {
        logger.v("onResume");
        super.onResume();
        this.hereMapFragment.onResume();
        Tracker.tagScreen(Screen.ROUTING);
        this.navdyRouteHandler.addListener(this);
        updateOfflineBannerVisibility();
    }

    public void onPause() {
        logger.v("onPause");
        this.navdyRouteHandler.removeListener(this);
        super.onPause();
    }

    protected void onDestroy() {
        logger.v("onDestroy");
        this.handler.removeCallbacksAndMessages(null);
        this.fluctuatorAnimatorView.stop();
        super.onDestroy();
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 16908332:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onBackPressed() {
        this.navdyRouteHandler.stopRouting();
        super.onBackPressed();
    }

    public void onPendingRouteCalculating(@NonNull Destination destination) {
        finish();
    }

    public void onPendingRouteCalculated(@NonNull Error error, @Nullable NavdyRouteInfo pendingRoute) {
        finish();
    }

    public void onRouteCalculating(@NonNull final Destination destination) {
        logger.v("onRouteCalculating");
        this.routeName.setText(R.string.calculating_dot_dot_dot);
        this.gauge.setVisibility(INVISIBLE);
        this.fluctuatorAnimatorView.setVisibility(VISIBLE);
        this.fluctuatorAnimatorView.start();
        this.fluctuatorAnimatorView.animate();
        this.hereMapFragment.whenReady(new OnHereMapFragmentReady() {
            public void onReady(@NonNull Map hereMap) {
                RoutingActivity.this.destinationMarker = destination.getHereMapMarker();
                if (RoutingActivity.this.destinationMarker != null) {
                    hereMap.addMapObject(RoutingActivity.this.destinationMarker);
                }
                RoutingActivity.this.hereMapFragment.centerOnUserLocationAndDestination(destination, Animation.LINEAR);
            }

            public void onError(@NonNull BaseHereMapFragment.Error error) {
            }
        });
    }

    public void onRouteCalculated(@NonNull Error error, @NonNull NavdyRouteInfo route) {
        if (error == Error.NONE) {
            finish();
            return;
        }
        this.fluctuatorAnimatorView.stop();
        this.fluctuatorAnimatorView.setVisibility(INVISIBLE);
        this.routeName.setText(R.string.error_please_retry_route);
        this.canRetry = true;
    }

    public void onRouteStarted(@NonNull NavdyRouteInfo route) {
        finish();
    }

    public void onTripProgress(@NonNull NavdyRouteInfo progress) {
        finish();
    }

    public void onReroute() {
        finish();
    }

    public void onRouteArrived(@NonNull Destination destination) {
    }

    public void onStopRoute() {
        finish();
    }

    private void initUi() {
        setUpHereMap();
        new ToolbarBuilder().title((int) R.string.calculating_route).build();
        this.activeTripCardFrameLayoutContainer.setVisibility(INVISIBLE);
    }

    private void setUpHereMap() {
        this.hereMapFragment = (BaseHereMapFragment) getFragmentManager().findFragmentById(R.id.map_fragment);
        if (this.hereMapFragment == null) {
            logger.e("Calling setUpHereMap with a null hereMapFragment");
            return;
        }
        this.hereMapFragment.hide();
        this.hereMapFragment.setUsableArea(HERE_MAP_TOP_PADDING, HERE_MAP_PADDING, HERE_MAP_PADDING, HERE_MAP_PADDING);
        this.hereMapFragment.centerOnUserLocation();
        this.hereMapFragment.setMapGesture(new OnGestureListenerAdapter() {
            public boolean onTapEvent(PointF pointF) {
                RoutingActivity.this.retryRoute(null);
                return super.onTapEvent(pointF);
            }
        });
        this.hereMapFragment.show();
    }

    @Subscribe
    public void handleReachabilityStateChange(ReachabilityEvent reachabilityEvent) {
        if (reachabilityEvent != null) {
            updateOfflineBannerVisibility();
        }
    }

    private void updateOfflineBannerVisibility() {
        if (this.offlineBanner != null) {
            this.offlineBanner.setVisibility(AppInstance.getInstance().canReachInternet() ? GONE : VISIBLE);
        }
    }

    public void onRefreshConnectivityClick(View view) {
        AppInstance.getInstance().checkForNetwork();
    }
}
