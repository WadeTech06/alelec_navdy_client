package com.navdy.client.app.ui;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;

public class SimpleDialogFragment extends DialogFragment {
    public static final String EXTRA_ID = "_id";
    public static final String EXTRA_MESSAGE = "message";
    public static final String EXTRA_POSITIVE_BUTTON_TITLE = "positive_button_title";
    public static final String EXTRA_TITLE = "title";

    public interface DialogProvider {
        Dialog createDialog(int i, Bundle bundle);
    }

    public static SimpleDialogFragment newInstance(int id, Bundle args) {
        if (args == null) {
            args = new Bundle();
        }
        args.putInt("_id", id);
        SimpleDialogFragment simpleDialogFragment = new SimpleDialogFragment();
        simpleDialogFragment.setArguments(args);
        return simpleDialogFragment;
    }

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Activity activity = getActivity();
        Bundle arguments = getArguments();
        if (arguments != null) {
            int id = arguments.getInt("_id");
            if (activity != null && (activity instanceof DialogProvider)) {
                return ((DialogProvider) activity).createDialog(id, arguments);
            }
        }
        return super.onCreateDialog(savedInstanceState);
    }
}
