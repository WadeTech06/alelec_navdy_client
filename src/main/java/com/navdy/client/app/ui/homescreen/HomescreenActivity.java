package com.navdy.client.app.ui.homescreen;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.net.UrlQuerySanitizer;
import android.os.AsyncTask;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.view.ActionMode;
import androidx.appcompat.view.ActionMode.Callback;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import androidx.viewpager.widget.ViewPager.OnPageChangeListener;

import com.alelec.navdyclient.R;
import com.google.android.gms.actions.SearchIntents;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.floatingactionbutton.FloatingActionButton.OnVisibilityChangedListener;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.navigation.NavigationView.OnNavigationItemSelectedListener;
import com.google.android.material.tabs.TabLayout;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.AppInstance;
import com.navdy.client.app.framework.DeviceConnection.DeviceConnectedEvent;
import com.navdy.client.app.framework.DeviceConnection.DeviceDisconnectedEvent;
import com.navdy.client.app.framework.DeviceConnection.DeviceInfoEvent;
import com.navdy.client.app.framework.map.MapUtils;
import com.navdy.client.app.framework.map.NavCoordsAddressProcessor;
import com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Error;
import com.navdy.client.app.framework.map.NavCoordsAddressProcessor.OnCompleteCallback;
import com.navdy.client.app.framework.models.Destination;
import com.navdy.client.app.framework.models.Destination.Type;
import com.navdy.client.app.framework.models.UserAccountInfo;
import com.navdy.client.app.framework.navigation.NavdyRouteHandler;
import com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteInfo;
import com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteListener;
import com.navdy.client.app.framework.util.CredentialsUtils;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.framework.util.SuggestionManager;
import com.navdy.client.app.framework.util.SystemUtils;
import com.navdy.client.app.providers.NavdyContentProvider;
import com.navdy.client.app.providers.NavdyContentProvider.QueryResultCallback;
import com.navdy.client.app.tracking.SetDestinationTracker;
import com.navdy.client.app.tracking.Tracker;
import com.navdy.client.app.tracking.TrackerConstants.Attributes.DestinationAttributes.SOURCE_VALUES;
import com.navdy.client.app.tracking.TrackerConstants.Attributes.DestinationAttributes.TYPE_VALUES;
import com.navdy.client.app.tracking.TrackerConstants.Event;
import com.navdy.client.app.tracking.TrackerConstants.Screen.Home;
import com.navdy.client.app.ui.base.BaseActivity;
import com.navdy.client.app.ui.customviews.DestinationImageView;
import com.navdy.client.app.ui.details.DetailsActivity;
import com.navdy.client.app.ui.favorites.FavoritesFragment;
import com.navdy.client.app.ui.firstlaunch.AppSetupActivity;
import com.navdy.client.app.ui.firstlaunch.AppSetupPagerAdapter;
import com.navdy.client.app.ui.firstlaunch.AppSetupScreen;
import com.navdy.client.app.ui.glances.GlancesFragment;
import com.navdy.client.app.ui.routing.RoutingActivity;
import com.navdy.client.app.ui.search.SearchActivity;
import com.navdy.client.app.ui.search.SearchConstants;
import com.navdy.client.app.ui.search.SearchConstants.SEARCH_TYPES;
import com.navdy.client.app.ui.settings.AudioSettingsActivity;
import com.navdy.client.app.ui.settings.CalendarSettingsActivity;
import com.navdy.client.app.ui.settings.CarSettingsActivity;
import com.navdy.client.app.ui.settings.GeneralSettingsActivity;
import com.navdy.client.app.ui.settings.LegalSettingsActivity;
import com.navdy.client.app.ui.settings.MessagingSettingsActivity;
import com.navdy.client.app.ui.settings.NavigationSettingsActivity;
import com.navdy.client.app.ui.settings.OtaSettingsActivity;
import com.navdy.client.app.ui.settings.ProfileSettingsActivity;
import com.navdy.client.app.ui.settings.SettingsConstants;
import com.navdy.client.app.ui.settings.SettingsUtils;
import com.navdy.client.app.ui.settings.SupportActivity;
import com.navdy.client.debug.MainDebugActivity;
import com.navdy.service.library.device.RemoteDevice;
import com.navdy.service.library.events.DeviceInfo;
import com.navdy.service.library.events.connection.DisconnectRequest;
import com.navdy.service.library.task.TaskManager;
import com.squareup.otto.Subscribe;

import net.hockeyapp.android.UpdateManager;
import net.hockeyapp.android.UpdateManagerListener;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

//import com.navdy.client.ota.OTAUpdateService;

public class HomescreenActivity extends BaseActivity implements OnPageChangeListener, NavdyRouteListener {
    public static final int EASTER_EGG_HIDE_ALL_TIPS = 15;
    public static final int EASTER_EGG_SHOW_ALL_TIPS = 20;
    public static final int EASTER_EGG_SHOW_TRIP_TAB = 10;
    public static final String EXTRA_DESTINATION = "extra_destination";
    private static final String EXTRA_HOMESCREEN_EXPLICITLY_ROUTING = "extra_homescreen_explicitly_routing";
    private static final String GOOGLE_MAPS_HOST = "maps.google.com";
    private static final String GOOGLE_NAVIGATION_SCHEME = "google.navigation";
    private static final int SUCCESS_BANNER_DISMISS_DELAY = 2000;
    private static final long VERSION_CHECK_INTERVAL = 30000;
    private TextView addFavoriteLabel;
    private FloatingActionButton addHome;
    private TextView addHomeLabel;
    private FloatingActionButton addWork;
    private TextView addWorkLabel;
    private FloatingActionButton fab;
    private View film;
    private boolean highlightGlanceSwitchOnResume = false;
    private boolean isFabShowing = false;
    private long lastCheck = 0;
    private Intent lastExternalIntentHandled = null;
    private AppInstance mAppInstance;
    private ActionMode mode;
    private final NavdyRouteHandler navdyRouteHandler = NavdyRouteHandler.getInstance();
    private NavigationView navigationView;
    HomescreenPagerAdapter pagerAdapter;
    private boolean reloadProfile = false;
    private TabLayout tabLayout;
    private Toolbar toolbar;
    private String trackerEvent = Event.NAVIGATE_USING_SEARCH_RESULTS;
    private ViewPager viewPager;

    protected void onCreate(Bundle savedInstanceState) {
        this.logger.v("HomescreenActivity::onCreate" + System.identityHashCode(this));
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.homescreen_container);
        this.mAppInstance = AppInstance.getInstance();
        this.mAppInstance.mHomescreenActivity = this;
        this.toolbar = (Toolbar) findViewById(R.id.homescreen_toolbar);
        setSupportActionBar(this.toolbar);
        setConnectionIndicator();
        setUpLogoEasterEgg();
        View navdyDisplay = findViewById(R.id.navdy_display);
        if (navdyDisplay != null) {
            navdyDisplay.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    RelativeLayout successBanner = (RelativeLayout) HomescreenActivity.this.findViewById(R.id.connection_success_banner);
                    RelativeLayout failureBanner = (RelativeLayout) HomescreenActivity.this.findViewById(R.id.connection_failure_banner);
                    if (successBanner != null && failureBanner != null) {
                        if (HomescreenActivity.this.mAppInstance.isDeviceConnected()) {
                            if (successBanner.getVisibility() == VISIBLE) {
                                HomescreenActivity.this.hideConnectionBanner(null);
                            } else {
                                HomescreenActivity.this.showConnectivityBanner();
                            }
                        } else if (failureBanner.getVisibility() == VISIBLE) {
                            HomescreenActivity.this.hideConnectionBanner(null);
                        } else {
                            HomescreenActivity.this.showConnectivityBanner();
                        }
                    }
                }
            });
        }
        setupBurgerMenu();
        this.viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(this.viewPager);
        this.tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        if (this.tabLayout != null) {
            this.tabLayout.setupWithViewPager(this.viewPager);
        }
        this.fab = (FloatingActionButton) findViewById(R.id.fab);
        this.fab.startAnimation(AnimationUtils.loadAnimation(this, R.anim.simple_grow));
        this.fab.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                HomescreenActivity.this.pickFabAction();
            }
        });
        setUpFloatingActionMenu();
        Intent i = getIntent();
        String action = i.getAction();
        String type = i.getType();
        boolean isComingFromGoogleNow = SearchIntents.ACTION_SEARCH.equals(action) || SearchConstants.ACTION_SEARCH.equals(action);
        this.logger.d("Search is coming from Google Now = " + isComingFromGoogleNow);
        if (isComingFromGoogleNow && i.hasExtra("query")) {
            Intent intent = new Intent(getApplicationContext(), SearchActivity.class);
            intent.setAction(action);
            intent.putExtra("query", i.getStringExtra("query"));
            startActivityForResult(intent, SEARCH_TYPES.SEARCH.getCode());
            this.trackerEvent = Event.NAVIGATE_USING_GOOGLE_NOW;
        }
    }

    protected void onResume() {
        super.onResume();
        SharedPreferences sharedPreferences = SettingsUtils.getSharedPreferences();
        boolean usesOreoPermissions;
        if (sharedPreferences.getInt(SettingsConstants.PERMISSIONS_VERSION, 1) > 25) {
            usesOreoPermissions = true;
        } else {
            usesOreoPermissions = false;
        }
        if (VERSION.SDK_INT > 25 && !usesOreoPermissions) {
            if (weHaveThisPermission("android.permission.ACCESS_FINE_LOCATION")) {
                requestLocationPermission(null, null);
            }
            if (weHaveThisPermission("android.permission.RECEIVE_SMS")) {
                requestSmsPermission(null, null);
            }
            if (weHaveThisPermission("android.permission.CALL_PHONE")) {
                requestPhonePermission(null, null);
            }
            if (weHaveThisPermission("android.permission.WRITE_EXTERNAL_STORAGE")) {
                requestStoragePermission(null, null);
            }
            sharedPreferences.edit().putInt(SettingsConstants.PERMISSIONS_VERSION, 26).apply();
        }
        for (int i = 0; i < AppSetupPagerAdapter.getScreensCount(); i++) {
            AppSetupScreen screen = AppSetupPagerAdapter.getScreen(i);
            if (!(screen == null || !screen.isMandatory || AppSetupPagerAdapter.weHavePermissionForThisScreen(screen))) {
                this.logger.e("Screen " + i + "(" + screen + ") is mandatory and we seem to be missing this permission.");
                AppSetupActivity.goToAppSetup(this);
                finish();
            }
        }
        if (this.reloadProfile) {
            this.reloadProfile = false;
            setUsernameAndAddress();
        }
        updateHudVersionString();
        updateMessagingVisibility();
        Intent callingIntent = getIntent();
        if (callingIntent != this.lastExternalIntentHandled && handleIntent(callingIntent)) {
            this.lastExternalIntentHandled = callingIntent;
        }
        if (this.viewPager.getCurrentItem() != 1) {
            this.fab.setVisibility(GONE);
        }
        if (!NavdyApplication.isDeveloperBuild()) {
            checkForUpdates(null);
        }
        this.navdyRouteHandler.addListener(this);
        if (this.highlightGlanceSwitchOnResume) {
            this.highlightGlanceSwitchOnResume = false;
            if (!(this.viewPager == null || isFinishing())) {
                this.viewPager.setCurrentItem(2);
                Fragment fragment = this.pagerAdapter.getItem(2);
                if (fragment instanceof GlancesFragment) {
                    ((GlancesFragment) fragment).highlightGlanceSwitch();
                }
            }
        }
        showObdDialogIfCarHasBeenAddedToBlacklist();
        setConnectionIndicator();
    }

    protected void onPause() {
        this.navdyRouteHandler.removeListener(this);
        if (this.mode != null) {
            stopSelectionActionMode();
        }
        super.onPause();
    }

    protected void onDestroy() {
        super.onDestroy();
        this.logger.v("HomescreenActivity::onDestroy" + System.identityHashCode(this));
    }

    private void setupBurgerMenu() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer != null) {
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, this.toolbar, R.string.menu_drawer_open, R.string.menu_drawer_close);
            drawer.addDrawerListener(toggle);
            toggle.syncState();
            this.navigationView = (NavigationView) findViewById(R.id.nav_view);
            if (this.navigationView != null) {
                this.navigationView.setItemIconTintList(null);
                final Context applicationContext = getApplicationContext();
                setUsernameAndAddress();
                Menu menu = this.navigationView.getMenu();
                if (menu != null) {
                    if (menu.findItem(R.id.menu_debug) != null) {
                    }
                    MenuItem messagingMenuItem = menu.findItem(R.id.menu_messaging);
                    if (messagingMenuItem != null) {
                        messagingMenuItem.setVisible(SettingsUtils.getPermissionsSharedPreferences().getBoolean(SettingsConstants.HUD_CANNED_RESPONSE_CAPABLE, false));
                    }
                    MenuItem versionItem = menu.findItem(R.id.menu_app_version);
                    if (versionItem != null) {
                        TextView appVersion = (TextView) versionItem.getActionView().findViewById(R.id.menu_version);
                        if (appVersion != null) {
                            String appVersionRes = getString(R.string.menu_app_version);
                            String appVersionString = getAppVersionString();
                            if (!StringUtils.equalsOrBothEmptyAfterTrim(appVersionString, getString(R.string.unknown))) {
                                appVersionString = "v." + appVersionString;
                            }
                            appVersion.setText(String.format(appVersionRes, new Object[]{appVersionString}));
                        }
                        updateHudVersionString();
                    }
                }
                this.navigationView.setNavigationItemSelectedListener(new OnNavigationItemSelectedListener() {
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.menu_car /*2131756042*/:
                                HomescreenActivity.this.startActivity(new Intent(applicationContext, CarSettingsActivity.class));
                                break;
                            case R.id.menu_audio /*2131756043*/:
                                HomescreenActivity.this.startActivity(new Intent(applicationContext, AudioSettingsActivity.class));
                                break;
                            case R.id.menu_calendar /*2131756044*/:
                                HomescreenActivity.this.startActivity(new Intent(applicationContext, CalendarSettingsActivity.class));
                                break;
                            case R.id.menu_navigation /*2131756045*/:
                                HomescreenActivity.this.startActivity(new Intent(applicationContext, NavigationSettingsActivity.class));
                                break;
                            case R.id.menu_messaging /*2131756046*/:
                                HomescreenActivity.this.startActivity(new Intent(applicationContext, MessagingSettingsActivity.class));
                                break;
                            case R.id.menu_software_update /*2131756047*/:
                                HomescreenActivity.this.startActivity(new Intent(applicationContext, OtaSettingsActivity.class));
                                break;
                            case R.id.menu_general /*2131756048*/:
                                HomescreenActivity.this.startActivity(new Intent(applicationContext, GeneralSettingsActivity.class));
                                break;
                            case R.id.menu_support /*2131756049*/:
                                HomescreenActivity.this.startActivity(new Intent(applicationContext, SupportActivity.class));
                                break;
                            case R.id.menu_legal /*2131756050*/:
                                HomescreenActivity.this.startActivity(new Intent(applicationContext, LegalSettingsActivity.class));
                                break;
                            case R.id.menu_debug /*2131756051*/:
                                HomescreenActivity.this.startActivity(new Intent(HomescreenActivity.this, MainDebugActivity.class));
                                break;
                            default:
                                return true;
                        }
                        HomescreenActivity.this.closeDrawer();
                        return true;
                    }
                });
            }
        }
    }

    private void updateHudVersionString() {
        Context applicationContext = getApplicationContext();
        Menu menu = this.navigationView.getMenu();
        if (menu != null) {
            MenuItem versionItem = menu.findItem(R.id.menu_hud_version);
            if (versionItem != null) {
                View actionView = versionItem.getActionView();
                TextView hudVersion = (TextView) actionView.findViewById(R.id.menu_version);
                if (hudVersion != null) {
                    String hudVersionRes = getString(R.string.menu_display_version);
                    String displayVersionString = getDisplayVersionString(applicationContext);
                    if (!StringUtils.equalsOrBothEmptyAfterTrim(displayVersionString, getString(R.string.unknown))) {
                        displayVersionString = "v." + displayVersionString;
                    }
                    hudVersion.setText(String.format(hudVersionRes, new Object[]{displayVersionString}));
                }
                TextView hudSerial = (TextView) actionView.findViewById(R.id.menu_serial);
                if (hudSerial != null) {
                    hudSerial.setText(String.format(getString(R.string.menu_display_serial), new Object[]{getDisplaySerialString(applicationContext)}));
                }
            }
        }
    }

    private void closeDrawer() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer != null) {
            drawer.closeDrawer((int) GravityCompat.START);
        }
    }

    public static String getAppVersionString() {
        PackageInfo pInfo = null;
        Context context = NavdyApplication.getAppContext();
        try {
            pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
        } catch (NameNotFoundException e) {
            e.printStackTrace();
        }
        if (pInfo == null || pInfo.versionName == null) {
            return context.getString(R.string.unknown);
        }
        return pInfo.versionName.split("-")[0];
    }

    private static String getDisplayVersionString(Context context) {
        RemoteDevice remoteDevice = AppInstance.getInstance().getRemoteDevice();
        if (remoteDevice != null) {
            DeviceInfo deviceInfo = remoteDevice.getDeviceInfo();
            if (!(deviceInfo == null || deviceInfo.clientVersion == null)) {
                String[] clientVersionParts = deviceInfo.clientVersion.split("-");
                if (clientVersionParts.length > 0) {
                    return clientVersionParts[0];
                }
            }
        }
        String lastDeviceVersionText = ""; //OTAUpdateService.getLastDeviceVersionText();
        return StringUtils.isEmptyAfterTrim(lastDeviceVersionText) ? context.getString(R.string.unknown) : lastDeviceVersionText;
    }

    private static String getDisplaySerialString(Context context) {
        RemoteDevice remoteDevice = AppInstance.getInstance().getRemoteDevice();
        if (remoteDevice != null) {
            DeviceInfo deviceInfo = remoteDevice.getDeviceInfo();
            if (deviceInfo != null) {
                return deviceInfo.deviceUuid;
            }
        }
        return context.getString(R.string.unknown);
    }

    private void setUsernameAndAddress() {
        if (this.navigationView != null) {
            View headerView = this.navigationView.getHeaderView(0);
            if (headerView != null) {
                headerView.setOnClickListener(new OnClickListener() {
                    public void onClick(View v) {
                        HomescreenActivity.this.startActivity(new Intent(HomescreenActivity.this.getApplicationContext(), ProfileSettingsActivity.class));
                        HomescreenActivity.this.reloadProfile = true;
                        HomescreenActivity.this.closeDrawer();
                    }
                });
                UserAccountInfo accountInfo = UserAccountInfo.getUserAccountInfo();
                DestinationImageView accountPhoto = (DestinationImageView) headerView.findViewById(R.id.account_photo);
                if (accountPhoto != null) {
                    if (accountInfo.photo != null) {
                        accountPhoto.setImage(accountInfo.photo);
                    } else {
                        accountPhoto.setImage(R.drawable.icon_profile_user_blue, accountInfo.fullName);
                    }
                }
                TextView accountName = (TextView) headerView.findViewById(R.id.account_name);
                if (accountName != null) {
                    if (StringUtils.isEmptyAfterTrim(accountInfo.fullName)) {
                        accountName.setText(R.string.go_to_profile_to_set_name);
                    } else {
                        accountName.setText(accountInfo.fullName);
                    }
                }
                TextView accountEmail = (TextView) headerView.findViewById(R.id.account_email);
                if (accountEmail == null) {
                    return;
                }
                if (!StringUtils.isEmptyAfterTrim(accountInfo.email)) {
                    accountEmail.setText(accountInfo.email);
                } else if (StringUtils.isEmptyAfterTrim(accountInfo.fullName)) {
                    accountEmail.setText(R.string.and_email);
                } else {
                    accountEmail.setText(R.string.go_to_profile_to_set_email);
                }
            }
        }
    }

    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (BaseActivity.isEnding(this)) {
            this.logger.w("Activity already ending so ignoring onBackPressed.");
        } else if (drawer != null && drawer.isDrawerOpen((int) GravityCompat.START)) {
            drawer.closeDrawer((int) GravityCompat.START);
        } else if (this.viewPager.getCurrentItem() != 0) {
            this.viewPager.setCurrentItem(0);
            this.tabLayout.setupWithViewPager(this.viewPager);
            hideOtherFabs();
        } else {
            super.onBackPressed();
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        this.logger.v("onActivityResult: requestCode = " + requestCode + " resultCode = " + resultCode);
        if (resultCode == -1) {
            switch (requestCode) {
                case 0:
                    Tracker.tagEvent(this.trackerEvent);
                    this.trackerEvent = Event.NAVIGATE_USING_SEARCH_RESULTS;
                    sendDestinationToNavdy(resultCode, data);
                    return;
                case 1:
                case 2:
                case 3:
                    saveDestinationToFavorites(requestCode, resultCode, data);
                    return;
                case 5:
                    if (data.getBooleanExtra(DetailsActivity.EXTRA_UPDATED_DESTINATION, false)) {
                        this.logger.d("Details Code reached");
                        Destination destination = NavdyRouteHandler.getInstance().getCurrentDestination();
                        if (destination != null) {
                            this.logger.d("destination contents: " + destination.isFavoriteDestination());
                            destination.reloadSelfFromDatabaseAsync(new Runnable() {
                                public void run() {
                                    SuggestionManager.forceSuggestionFullRefresh();
                                    HomescreenActivity.this.rebuildSuggestions();
                                }
                            });
                            return;
                        }
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    private void sendDestinationToNavdy(int resultCode, Intent data) {
        if (resultCode == -1) {
            final Destination destination = (Destination) data.getParcelableExtra(SearchConstants.SEARCH_RESULT);
            this.logger.v("destination address: " + destination.rawAddressNotForDisplay);
            Runnable requestRouteRunnable = new Runnable() {
                public void run() {
                    SetDestinationTracker.getInstance().tagSetDestinationEvent(destination);
                    HomescreenActivity.this.navdyRouteHandler.requestNewRoute(destination);
                }
            };
            if (data.getBooleanExtra(SearchConstants.EXTRA_IS_VOICE_SEARCH, false)) {
                requestRouteRunnable.run();
            } else {
                showRequestNewRouteDialog(requestRouteRunnable);
            }
        }
    }

    private void saveDestinationToFavorites(int requestCode, int resultCode, Intent data) {
        if (resultCode == -1) {
            int specialType;
            Destination d = (Destination) data.getParcelableExtra(SearchConstants.SEARCH_RESULT);
            this.logger.v("saveDestinationToFavorites: " + d.rawAddressNotForDisplay);
            showProgressDialog();
            d.setFavoriteLabel(d.name);
            switch (requestCode) {
                case 2:
                    specialType = -3;
                    d.setFavoriteLabel(getString(R.string.home));
                    Tracker.tagEvent(Event.SET_HOME);
                    break;
                case 3:
                    specialType = -2;
                    d.setFavoriteLabel(getString(R.string.work));
                    Tracker.tagEvent(Event.SET_WORK);
                    break;
                default:
                    if (d.type != Type.CONTACT) {
                        specialType = -1;
                        break;
                    } else {
                        specialType = -4;
                        break;
                    }
            }
            final int finalSpecialType = specialType;
            NavCoordsAddressProcessor.processDestination(d, new OnCompleteCallback() {
                public void onSuccess(Destination destination) {
                    updateOrSaveFavoriteToDbAsync(destination);
                }

                public void onFailure(Destination destination, Error error) {
                    HomescreenActivity.this.logger.v("navigationHelper failed to get latLng for ContactModel Destination");
                    updateOrSaveFavoriteToDbAsync(destination);
                }

                private void updateOrSaveFavoriteToDbAsync(Destination destination) {
                    destination.saveDestinationAsFavoritesAsync(finalSpecialType, new QueryResultCallback() {
                        public void onQueryCompleted(int nbRows, @Nullable Uri uri) {
                            if (nbRows <= 0) {
                                HomescreenActivity.this.logger.e("Something went wrong while trying to save the favorite to the db.");
                            }
                            HomescreenActivity.this.onFavoriteListChanged();
                            HomescreenActivity.this.hideProgressDialog();
                        }
                    });
                }
            });
        }
    }

    public void onFavoriteListChanged() {
        runOnUiThread(new Runnable() {
            public void run() {
                if (HomescreenActivity.this.viewPager != null && HomescreenActivity.this.pagerAdapter != null && HomescreenActivity.this.pagerAdapter.getCount() >= 2) {
                    FavoritesFragment favoritesFragment = (FavoritesFragment) HomescreenActivity.this.pagerAdapter.getItem(1);
                    if (favoritesFragment != null) {
                        if (favoritesFragment.favoritesAdapter != null) {
                            favoritesFragment.favoritesAdapter.notifyDbChanged();
                        }
                        favoritesFragment.showOrHideSplashScreen();
                    }
                    HomescreenActivity.this.rebuildSuggestions();
                }
            }
        });
    }

    public void rebuildSuggestions() {
        if (this.pagerAdapter != null) {
            SuggestionsFragment suggestionsFragment = (SuggestionsFragment) this.pagerAdapter.getItem(0);
            if (suggestionsFragment != null && suggestionsFragment.suggestionAdapter != null) {
                suggestionsFragment.suggestionAdapter.rebuildSuggestions();
            }
        }
    }

    private void setupViewPager(ViewPager viewPager) {
        this.pagerAdapter = new HomescreenPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(this.pagerAdapter);
        viewPager.setOffscreenPageLimit(3);
        viewPager.addOnPageChangeListener(this);
    }

    private void setUpLogoEasterEgg() {
        ImageView logo = (ImageView) findViewById(R.id.navdy_logo);
        if (logo != null) {
            logo.setOnClickListener(new OnClickListener() {
                int count = 0;

                public void onClick(View v) {
                    HomescreenActivity.this.logger.d("Clicked on the logo " + (this.count + 1) + " time(s)");
                    int i = this.count + 1;
                    this.count = i;
                    if (i == 10) {
                        if (HomescreenActivity.this.pagerAdapter != null) {
                            HomescreenActivity.this.pagerAdapter.showHiddenTabs();
                            HomescreenActivity.this.tabLayout.setupWithViewPager(HomescreenActivity.this.viewPager);
                        }
                        HomescreenActivity.this.navigationView = (NavigationView) HomescreenActivity.this.findViewById(R.id.nav_view);
                        if (HomescreenActivity.this.navigationView != null) {
                            HomescreenActivity.this.updateMenuVisibility(true, R.id.menu_debug);
                        }
                        SuggestionManager.forceSuggestionFullRefresh();
                        HomescreenActivity.this.rebuildSuggestions();
                        NavdyContentProvider.clearCache();
                    }
                    if (this.count >= 15) {
                        BaseActivity.showShortToast(R.string.hide_all_tips, new Object[0]);
                        HomescreenActivity.hideAllTips();
                        SuggestionManager.forceSuggestionFullRefresh();
                        HomescreenActivity.this.rebuildSuggestions();
                    }
                    if (this.count == 20) {
                        BaseActivity.showShortToast(R.string.show_all_tips, new Object[0]);
                        this.count = 0;
                        HomescreenActivity.showAllTips();
                        SuggestionManager.forceSuggestionFullRefresh();
                        HomescreenActivity.this.rebuildSuggestions();
                    }
                }
            });
        }
    }

    private void updateMenuVisibility(boolean visible, @IdRes int menuItemId) {
        Menu menu = this.navigationView.getMenu();
        if (menu != null) {
            MenuItem messagingMenuItem = menu.findItem(menuItemId);
            if (messagingMenuItem != null) {
                messagingMenuItem.setVisible(visible);
            }
        }
    }

    private static void showAllTips() {
        Editor editor = SettingsUtils.getSharedPreferences().edit();
        editor.putBoolean(SettingsConstants.USER_WATCHED_THE_DEMO, false);
        editor.putBoolean(SettingsConstants.USER_ENABLED_GLANCES_ONCE_BEFORE, false);
        editor.putBoolean(SettingsConstants.USER_TRIED_GESTURES_ONCE_BEFORE, false);
        editor.putBoolean(SettingsConstants.USER_ALREADY_SAW_MICROPHONE_TIP, false);
        editor.putBoolean(SettingsConstants.USER_ALREADY_SAW_GOOGLE_NOW_TIP, false);
        editor.putBoolean(SettingsConstants.USER_ALREADY_SAW_GESTURE_TIP, false);
        editor.putBoolean(SettingsConstants.USER_ALREADY_SAW_ADD_HOME_TIP, false);
        editor.putBoolean(SettingsConstants.USER_ALREADY_SAW_ADD_WORK_TIP, false);
        editor.putBoolean(SettingsConstants.USER_ALREADY_SAW_LOCAL_MUSIC_BROWSER_TIP, false);
        editor.apply();
    }

    public static void hideAllTips() {
        Editor editor = SettingsUtils.getSharedPreferences().edit();
        editor.putBoolean(SettingsConstants.USER_WATCHED_THE_DEMO, true);
        editor.putBoolean(SettingsConstants.USER_ENABLED_GLANCES_ONCE_BEFORE, true);
        editor.putBoolean(SettingsConstants.USER_TRIED_GESTURES_ONCE_BEFORE, true);
        editor.putBoolean(SettingsConstants.USER_ALREADY_SAW_MICROPHONE_TIP, true);
        editor.putBoolean(SettingsConstants.USER_ALREADY_SAW_GOOGLE_NOW_TIP, true);
        editor.putBoolean(SettingsConstants.USER_ALREADY_SAW_GESTURE_TIP, true);
        editor.putBoolean(SettingsConstants.USER_ALREADY_SAW_ADD_HOME_TIP, true);
        editor.putBoolean(SettingsConstants.USER_ALREADY_SAW_ADD_WORK_TIP, true);
        editor.putBoolean(SettingsConstants.USER_ALREADY_SAW_LOCAL_MUSIC_BROWSER_TIP, true);
        editor.apply();
    }

    private void setConnectionIndicator() {
        ImageView indicator = (ImageView) findViewById(R.id.navdy_display);
        if (indicator != null) {
            if (this.mAppInstance.isDeviceConnected()) {
                indicator.setImageResource(R.drawable.icon_display_connected);
            } else {
                indicator.setImageResource(R.drawable.icon_display_disconnected);
            }
            showConnectivityBanner();
        }
    }

    public void pulseConnectionIndicator() {
        this.handler.post(new Runnable() {
            public void run() {

                final ImageView indicator = (ImageView) findViewById(R.id.navdy_display);
                if (indicator != null) {
//                    indicator.setBackgroundColor(getResources().getColor(R.color.grey));
                    indicator.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.rounded_grey));

                    HomescreenActivity.this.handler.postDelayed(new Runnable() {
                        public void run() {
                            indicator.setBackground(null);
//                            indicator.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                    }, 150);
                }
            }});
    }

    public void hideConnectionBanner(View v) {
        RelativeLayout banner = (RelativeLayout) findViewById(R.id.connection_success_banner);
        if (banner != null) {
            banner.setVisibility(GONE);
        }
        banner = (RelativeLayout) findViewById(R.id.connection_failure_banner);
        if (banner != null) {
            banner.setVisibility(GONE);
        }
    }

    private void showConnectivityBanner() {
        RelativeLayout successBanner = (RelativeLayout) findViewById(R.id.connection_success_banner);
        RelativeLayout failureBanner = (RelativeLayout) findViewById(R.id.connection_failure_banner);
        if (successBanner != null && failureBanner != null) {
            if (this.mAppInstance.isDeviceConnected()) {
                successBanner.setVisibility(VISIBLE);
                failureBanner.setVisibility(GONE);
                TextView text = (TextView) findViewById(R.id.connection_success_banner_text);
                if (text != null) {
                    RemoteDevice remoteDevice = this.mAppInstance.getRemoteDevice();
                    String deviceName = null;
                    if (remoteDevice != null) {
                        DeviceInfo deviceInfo = remoteDevice.getDeviceInfo();
                        if (deviceInfo != null) {
                            deviceName = deviceInfo.deviceName;
                        }
                    }
                    if (StringUtils.isEmptyAfterTrim(deviceName)) {
                        text.setText(R.string.navdy_connected);
                    } else {
                        text.setText(getString(R.string.connected_to, new Object[]{deviceName}));
                    }
                }
                this.handler.postDelayed(new Runnable() {
                    public void run() {
                        HomescreenActivity.this.hideConnectionBanner(null);
                    }
                }, 2000);
                return;
            }
            successBanner.setVisibility(GONE);
            failureBanner.setVisibility(VISIBLE);
        }
    }

    public void onFailureBannerClick(View v) {
        if (!this.mAppInstance.isDeviceConnected()) {
            openBtConnectionDialog();
        }
    }

    public void onSuccessBannerClick(View v) {
        this.handler.removeCallbacksAndMessages(null);
        showQuestionDialog(R.string.disconnect_from_display, R.string.disconnect_from_display_desc, R.string.yes_button, R.string.cancel_button, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (which == -1) {
                    RemoteDevice device = AppInstance.getInstance().getRemoteDevice();
                    if (device != null) {
                        device.postEvent(new DisconnectRequest(Boolean.valueOf(false)));
                    }
                }
            }
        }, null);
    }

    public void onHelpConnectClick(View v) {
        startActivity(new Intent(getApplicationContext(), SupportActivity.class));
    }

    @Subscribe
    public void onDeviceConnectedEvent(DeviceConnectedEvent deviceConnectedEvent) {
        this.logger.v("onDeviceConnectedEvent: " + deviceConnectedEvent);
        setConnectionIndicator();
        updateMessagingVisibility();
    }

    @Subscribe
    public void onDeviceDisconnectedEvent(DeviceDisconnectedEvent deviceDisconnectedEvent) {
        this.logger.v("onDeviceDisconnectedEvent: " + deviceDisconnectedEvent);
        setConnectionIndicator();
    }

    @Subscribe
    public void onDeviceInfoEvent(DeviceInfoEvent deviceInfoEvent) {
        this.logger.v("onDeviceInfoEvent: " + deviceInfoEvent);
        updateHudVersionString();
        updateMessagingVisibility();
    }

    private void updateMessagingVisibility() {
        updateMenuVisibility(SettingsUtils.getSharedPreferences().getBoolean(SettingsConstants.MESSAGING_HAS_SEEN_CAPABLE_HUD, false), R.id.menu_messaging);
    }

    private void pickFabAction() {
        int position = this.viewPager.getCurrentItem();
        this.logger.v("pickFabAction: " + position);
        switch (position) {
            case 1:
                favoritesFragmentFabAction();
                return;
            default:
                this.logger.e("Unsupported viewPager position for fab: " + position);
                return;
        }
    }

    private void favoritesFragmentFabAction() {
        this.logger.v("favoritesFragmentFabAction");
        if (this.isFabShowing || (NavdyContentProvider.isHomeSet() && NavdyContentProvider.isWorkSet())) {
            callSearchFor(SEARCH_TYPES.FAVORITE);
        } else {
            showOtherFabs();
        }
    }

    private void setUpFloatingActionMenu() {
        this.addFavoriteLabel = (TextView) findViewById(R.id.addFavoriteLabel);
        this.addHome = (FloatingActionButton) findViewById(R.id.addHome);
        this.addHomeLabel = (TextView) findViewById(R.id.addHomeLabel);
        this.addWork = (FloatingActionButton) findViewById(R.id.addWork);
        this.addWorkLabel = (TextView) findViewById(R.id.addWorkLabel);
        this.film = findViewById(R.id.film);
        this.addHome.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                HomescreenActivity.this.callSearchFor(SEARCH_TYPES.HOME);
            }
        });
        this.addWork.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                HomescreenActivity.this.callSearchFor(SEARCH_TYPES.WORK);
            }
        });
        this.film.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                HomescreenActivity.this.hideOtherFabs();
            }
        });
    }

    private void callSearchFor(SEARCH_TYPES searchType) {
        callSearchFor(searchType, false);
    }

    private void callSearchFor(SEARCH_TYPES searchType, boolean searchMic) {
        hideOtherFabs();
        SearchActivity.startSearchActivityFor(searchType, searchMic, this);
    }

    private void showOtherFabs() {
        this.isFabShowing = true;
        setOtherFabsVisibility(0);
    }

    private void hideOtherFabs() {
        this.isFabShowing = false;
        setOtherFabsVisibility(8);
    }

    private void setOtherFabsVisibility(int visibility) {
        this.addFavoriteLabel.setVisibility(visibility);
        if (!NavdyContentProvider.isHomeSet()) {
            this.addHome.setVisibility(visibility);
            this.addHomeLabel.setVisibility(visibility);
        }
        if (!NavdyContentProvider.isWorkSet()) {
            this.addWork.setVisibility(visibility);
            this.addWorkLabel.setVisibility(visibility);
        }
        this.film.setVisibility(visibility);
    }

    public void onPageScrollStateChanged(int state) {
    }

    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    public void onPageSelected(int position) {
        this.logger.v("onPageSelected: " + position);
        switch (position) {
            case 1:
                this.fab.setVisibility(VISIBLE);
                this.fab.hide(new OnVisibilityChangedListener() {
                    public void onHidden(FloatingActionButton fab) {
                        fab.show();
                    }
                });
                break;
            default:
                this.fab.hide();
                break;
        }
        if (position != 0) {
            stopSelectionActionMode();
        }
        hideConnectionBanner(null);
        Tracker.tagScreen(Home.tag(position));
    }

    @SuppressLint("StaticFieldLeak")
    private boolean handleIntent(@NonNull final Intent callingIntent) {
//        try {
            boolean isActionView = "android.intent.action.VIEW".equals(callingIntent.getAction());
            if (isActionView && callingIntent.hasExtra(EXTRA_DESTINATION)) {
                new AsyncTask<Void, Void, Destination>() {
                    protected Destination doInBackground(Void... voids) {
                        return NavdyContentProvider.getThisDestination(callingIntent.getIntExtra(HomescreenActivity.EXTRA_DESTINATION, -1));
                    }

                    protected void onPostExecute(Destination destination) {
                        if (destination != null) {
                            NavdyRouteHandler.getInstance().requestNewRoute(destination);
                        }
                    }
                }.execute();
                return true;
            }
            this.logger.d("handleIntent: " + callingIntent.getAction());
            SystemUtils.logIntentExtras(callingIntent, this.logger);
            SetDestinationTracker setDestinationTracker = SetDestinationTracker.getInstance();
            setDestinationTracker.setSourceValue(SOURCE_VALUES.THIRD_PARTY_INTENT);
            setDestinationTracker.setDestinationType(TYPE_VALUES.INTENT);
            if (isActionView) {
                this.logger.v("ACTION_VIEW Intent found");
                Uri uri = callingIntent.getData();
                String host = uri.getHost();
                this.logger.v("uri: " + uri);
                this.logger.v("uri host: " + host);
                this.logger.v("uri scheme: " + uri.getScheme());
                this.logger.v("uri schemeSpecificPart: " + uri.getSchemeSpecificPart());
                if (StringUtils.equalsOrBothEmptyAfterTrim(host, GOOGLE_MAPS_HOST) || StringUtils.equalsOrBothEmptyAfterTrim(uri.getScheme(), GOOGLE_NAVIGATION_SCHEME)) {
                    routeToUri(uri);
                    return true;
                } else if (StringUtils.equalsOrBothEmptyAfterTrim(uri.getScheme(), "geo")) {
                    routeToGeoUri(uri);
                    return true;
                }
            } else if ("android.intent.action.SEND".equals(callingIntent.getAction()) && ("text/plain").equals(callingIntent.getType())) {
                String sharedText = callingIntent.getStringExtra("android.intent.extra.TEXT");
                if (StringUtils.isEmptyAfterTrim(sharedText)) {
                    this.logger.d("ACTION_SEND Intent had bad text");
                    return false;
                }
                this.logger.i("ACTION_SEND Intent found: " + sharedText);
                searchOrRouteToSharedText(sharedText, callingIntent);
                return true;
            }
            return false;
//        } catch (Throwable e) {
//            this.logger.e(e);
//        }
    }

    private void routeToUri(@NonNull Uri uri) {
        this.logger.v("routeToUri: " + uri);
        String address = null;
        String latLngStr = null;
        String daddr = null;
        String query = null;
        if (uri.isHierarchical()) {
            daddr = uri.getQueryParameter("daddr");
            query = uri.getQueryParameter("q");
        }
        String schemeSpecificPart = uri.getSchemeSpecificPart();
        if (daddr != null) {
            String[] parts = daddr.split(" +?@");
            if (parts.length > 1) {
                address = parts[0];
                latLngStr = parts[1];
            } else if (parts[0].matches(StringUtils.LAT_LNG_REGEXP)) {
                latLngStr = parts[0];
            } else {
                address = parts[0];
            }
        } else if (schemeSpecificPart != null && schemeSpecificPart.startsWith("q=")) {
            query = schemeSpecificPart.substring(2, schemeSpecificPart.length()).trim();
            try {
                query = URLDecoder.decode(query, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                query = query.replaceAll("%2C", ",");
                e.printStackTrace();
            }
            if (AppInstance.getInstance().canReachInternet()) {
                routeToFirstResultFor(query);
                return;
            }
            address = null;
            LatLng latLng = MapUtils.parseLatLng(query);
            if (latLng == null) {
                address = query;
            }
            routeToAddressOrCoords(null, address, latLng);
            return;
        } else if (query != null) {
            Matcher matcher = Pattern.compile(StringUtils.LAT_LNG_REGEXP).matcher(query);
            if (matcher.find()) {
                routeToAddressOrCoords(query.substring(matcher.end(), query.length()).replace("(", "").replace(")", "").trim(), null, MapUtils.parseLatLng(matcher.group()));
                return;
            }
        }
        if (uri.isHierarchical()) {
            if (StringUtils.isEmptyAfterTrim(address)) {
                address = uri.getQueryParameter("q");
            }
            if (StringUtils.isEmptyAfterTrim(latLngStr)) {
                latLngStr = uri.getQueryParameter("ll");
            }
        }
        routeToAddressOrCoords(null, address, MapUtils.parseLatLng(latLngStr));
    }

    private void routeToGeoUri(Uri uri) {
        sLogger.d("routeToGeoUri: " + uri);
        if (uri == null) {
            this.logger.e("Unable to route to a null URI");
            return;
        }
        String partsString = Uri.decode(uri.getSchemeSpecificPart());
        if (!StringUtils.isEmptyAfterTrim(partsString)) {
            String[] parts = partsString.split("\\?");
            String address = null;
            LatLng latLng = null;
            if (parts.length > 0) {
                latLng = MapUtils.parseLatLng(parts[0]);
            }
            String q = parseParamValue(uri, "q");
            if (!StringUtils.isEmptyAfterTrim(q)) {
                if (latLng == null) {
                    latLng = MapUtils.parseLatLng(q);
                }
                address = parseLabel(q);
                if (StringUtils.isEmptyAfterTrim(address)) {
                    address = q;
                }
            }
            routeToAddressOrCoords(null, address, latLng);
        }
    }

    private void searchOrRouteToSharedText(final String sharedText, final Intent intent) {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                if (StringUtils.isEmptyAfterTrim(sharedText)) {
                    HomescreenActivity.this.logger.d("shared text is empty");
                    return;
                }
                String displayName = "";
                String address = "";
                if (sharedText.contains(SearchConstants.GOOGLE_MAPS_SHORT_URL)) {
                    if (sharedText.contains(SearchConstants.GOOGLE_MAPS_TO_SYNTAX) && sharedText.contains(SearchConstants.GOOGLE_MAPS_VIA_SYNTAX)) {
                        displayName = sharedText.substring(sharedText.indexOf(SearchConstants.GOOGLE_MAPS_TO_SYNTAX) + 2, sharedText.indexOf(SearchConstants.GOOGLE_MAPS_VIA_SYNTAX)).trim();
                    } else {
                        displayName = intent.getStringExtra("android.intent.extra.SUBJECT");
                        String lines[] = sharedText.split("\\r?\\n");
                        address = lines[1];
                    }
                } else if (sharedText.contains(SearchConstants.GOOGLE_ASSISTANT_SHORT_URL)) {
                    displayName = sharedText.substring(0, sharedText.indexOf(SearchConstants.GOOGLE_ASSISTANT_SHORT_URL));
                } else if (sharedText.contains(SearchConstants.GOOGLE_SEARCH_SHORT_URL)) {
                    displayName = sharedText.substring(0, sharedText.indexOf(SearchConstants.GOOGLE_SEARCH_SHORT_URL));
                } else if (sharedText.contains(SearchConstants.YELP_MAPS_URL)) {
                    displayName = sharedText.substring(0, sharedText.indexOf(SearchConstants.YELP_MAPS_URL)).trim();
                }

                HomescreenActivity.this.logger.d("display name: " + displayName);
                if (!StringUtils.isEmptyAfterTrim(address)) {
                    Destination d = new Destination();
                    d.setName(displayName);
                    d.setRawAddressNotForDisplay(address);
                    NavCoordsAddressProcessor.processDestination(NavdyContentProvider.getThisDestination(d), new OnCompleteCallback() {
                        public void onSuccess(Destination destination) {
                            HomescreenActivity.this.routeToDestination(destination);
                        }

                        public void onFailure(Destination destination, Error error) {
                            HomescreenActivity.this.logger.e("Failure: " + error.name());
                        }
                    });
                } else if (StringUtils.isEmptyAfterTrim(displayName)) {
                    HomescreenActivity.this.logger.d("display name and address are both empty so we can't navigate anywhere");
                } else {
                    HomescreenActivity.this.routeToFirstResultFor(displayName);
                }
            }
        }, 3);
    }

    public void routeToFirstResultFor(String searchTerm) {
        final Intent searchIntent = new Intent(getApplicationContext(), SearchActivity.class);
        searchIntent.setAction(SearchConstants.ACTION_SEND);
        searchIntent.putExtra("query", searchTerm);
        runOnUiThread(new Runnable() {
            public void run() {
                HomescreenActivity.this.startActivityForResult(searchIntent, SEARCH_TYPES.SEARCH.getCode());
            }
        });
    }

    private void routeToAddressOrCoords(@Nullable String name, @Nullable String address, @Nullable LatLng latLng) {
        this.logger.i("routeToAddressOrCoords, address: " + address + "\n " + "lat/lng: " + latLng);
        if (StringUtils.isEmptyAfterTrim(name) && StringUtils.isEmptyAfterTrim(address) && !MapUtils.isValidSetOfCoordinates(latLng)) {
            BaseActivity.showLongToast(R.string.could_not_route_to_intent, new Object[0]);
            return;
        }
        final Destination destination = new Destination();
        if (!StringUtils.isEmptyAfterTrim(name)) {
            destination.name = name;
        }
        if (!StringUtils.isEmptyAfterTrim(address)) {
            destination.rawAddressNotForDisplay = address;
        }
        if (MapUtils.isValidSetOfCoordinates(latLng)) {
            destination.displayLat = latLng.latitude;
            destination.displayLong = latLng.longitude;
        }
        if (StringUtils.isEmptyAfterTrim(address) && MapUtils.isValidSetOfCoordinates(latLng)) {
            destination.setCoordsToSame(latLng);
            MapUtils.doReverseGeocodingFor(latLng, destination, new Runnable() {
                public void run() {
                    TaskManager.getInstance().execute(new Runnable() {
                        public void run() {
                            destination.updateAllColumnsOrInsertNewEntryInDb();
                            HomescreenActivity.this.navdyRouteHandler.requestNewRoute(destination);
                        }
                    }, 1);
                }
            });
            return;
        }
        showProgressDialog();
        NavCoordsAddressProcessor.processDestination(destination, new OnCompleteCallback() {
            public void onSuccess(Destination destination) {
                HomescreenActivity.this.hideProgressDialog();
                HomescreenActivity.this.routeToDestination(destination);
            }

            public void onFailure(Destination destination, Error error) {
                HomescreenActivity.this.logger.w("routeToAddressOrCoords, couldn't get coords either from here or google, trying with just address");
                HomescreenActivity.this.hideProgressDialog();
                HomescreenActivity.this.routeToDestination(destination);
            }
        });
    }

    private void routeToDestination(final Destination destination) {
        showRequestNewRouteDialog(new Runnable() {
            public void run() {
                NavdyRouteHandler.getInstance().requestNewRoute(destination);
            }
        });
    }

    @Nullable
    private String parseLabel(String str) {
        Matcher matcher = Pattern.compile(StringUtils.LAT_LNG_REGEXP).matcher(str);
        if (!matcher.find()) {
            return str;
        }
        String latLngStr = matcher.group();
        if (matcher.end() == str.length()) {
            return latLngStr;
        }
        return str.substring(matcher.end(), str.length()).replace("(", "").replace(")", "").trim();
    }

    private String parseParamValue(Uri uri, String param) {
        UrlQuerySanitizer querySanitizer = new UrlQuerySanitizer(uri.toString());
        querySanitizer.registerParameter(param, UrlQuerySanitizer.getAllButNulAndAngleBracketsLegal());
        try {
            String value = querySanitizer.getValue(param);
            if (value != null) {
                return URLDecoder.decode(value.replaceAll("_", " "), Charset.defaultCharset().name());
            }
            return null;
        } catch (UnsupportedEncodingException e) {
            this.logger.e("Couldn't decode parameter '" + param + "' in uri: " + uri);
            return null;
        }
    }

    private void checkForUpdates(UpdateManagerListener listener) {
        long now = new Date().getTime();
        if (now - this.lastCheck > 30000) {
            this.logger.v("Checking Hockey app for updates.");
            this.lastCheck = now;
            UpdateManager.register((Activity) this, CredentialsUtils.getCredentials(NavdyApplication.getAppContext().getString(R.string.metadata_hockey_app_credentials)), listener, true);
            return;
        }
        this.logger.v("Hasn't been long enough to be checking Hockey app for updates. now = " + now + " lastCheck = " + this.lastCheck + " now - lastCheck = " + (now - this.lastCheck));
    }

    public void onSearchClick(View view) {
        callSearchFor(SEARCH_TYPES.SEARCH);
    }

    public void onSearchMicClick(View view) {
        callSearchFor(SEARCH_TYPES.SEARCH, true);
    }

    void startSelectionActionMode(Callback actionModeCallback) {
        this.mode = startSupportActionMode(actionModeCallback);
    }

    private void stopSelectionActionMode() {
        if (this.mode != null) {
            this.mode.finish();
        }
    }

    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        if (intent.getBooleanExtra(EXTRA_HOMESCREEN_EXPLICITLY_ROUTING, false)) {
            this.viewPager.setCurrentItem(0);
        }
    }

    public void onPendingRouteCalculating(@NonNull Destination destination) {
    }

    public void onPendingRouteCalculated(@NonNull NavdyRouteHandler.Error error, @NonNull NavdyRouteInfo pendingRoute) {
    }

    public void onRouteCalculating(@NonNull Destination destination) {
        RoutingActivity.startRoutingActivity(this);
    }

    public void onRouteCalculated(@NonNull NavdyRouteHandler.Error error, @NonNull NavdyRouteInfo route) {
        openStartTab();
    }

    public void onRouteStarted(@NonNull NavdyRouteInfo route) {
        openStartTab();
    }

    private void openStartTab() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer != null && drawer.isDrawerOpen((int) GravityCompat.START)) {
            drawer.closeDrawer((int) GravityCompat.START);
        }
        if (this.viewPager.getCurrentItem() != 0) {
            this.viewPager.setCurrentItem(0);
            this.tabLayout.setupWithViewPager(this.viewPager);
            hideOtherFabs();
        }
    }

    public void onTripProgress(@NonNull NavdyRouteInfo progress) {
    }

    public void onReroute() {
    }

    public void onRouteArrived(@NonNull Destination destination) {
    }

    public void onStopRoute() {
    }

    void goToFavoritesTab() {
        if (this.viewPager != null && !isFinishing()) {
            this.viewPager.setCurrentItem(1);
        }
    }

    void goToGlancesTab() {
        if (this.viewPager != null && !isFinishing()) {
            this.viewPager.setCurrentItem(2);
            this.highlightGlanceSwitchOnResume = true;
        }
    }

    void goToGesture() {
        startActivity(new Intent(this, GeneralSettingsActivity.class));
    }

    public void onRefreshConnectivityClick(View view) {
        AppInstance.getInstance().checkForNetwork();
    }
}
