package com.navdy.client.app.ui.settings;

import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.Adapter;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;
import androidx.recyclerview.widget.ItemTouchHelper.Callback;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.alelec.navdyclient.R;
import com.navdy.client.app.framework.util.ImageCache;
import com.navdy.client.app.framework.util.ImageUtils;
import com.navdy.client.app.ui.base.BaseActivity;
import com.navdy.client.app.ui.settings.MessagingSettingsActivity.ReplyClickListener;
import java.util.ArrayList;
import java.util.Collections;

class RepliesAdapter extends Adapter<ReplyViewHolder> {
    private static final int TYPE_FOOTER = 2;
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_REPLY = 1;
    private ImageCache imageCache = new ImageCache();
    private Runnable itemMovedCallback;
    private Callback itemTouchHelperCallback = new Callback() {
        public int getMovementFlags(RecyclerView recyclerView, ViewHolder viewHolder) {
            if (viewHolder.getItemViewType() == 1) {
                return Callback.makeMovementFlags(3, 48);
            }
            return 0;
        }

        public boolean onMove(RecyclerView recyclerView, ViewHolder viewHolder, ViewHolder target) {
            if (viewHolder.getItemViewType() != 1 || RepliesAdapter.this.replies == null) {
                return false;
            }
            int fromPos = viewHolder.getAdapterPosition() - 1;
            int toPos = target.getAdapterPosition() - 1;
            if (toPos < 0 || fromPos < 0 || toPos >= RepliesAdapter.this.replies.size() || fromPos >= RepliesAdapter.this.replies.size()) {
                return false;
            }
            if (fromPos == toPos) {
                return true;
            }
            if (toPos >= fromPos) {
                Collections.rotate(RepliesAdapter.this.replies.subList(fromPos, toPos + 1), -1);
            } else {
                Collections.rotate(RepliesAdapter.this.replies.subList(toPos, fromPos + 1), 1);
            }
            RepliesAdapter.this.notifyItemMoved(fromPos + 1, toPos + 1);
            return true;
        }

        public void onMoved(RecyclerView recyclerView, ViewHolder viewHolder, int fromPos, ViewHolder target, int toPos, int x, int y) {
            super.onMoved(recyclerView, viewHolder, fromPos, target, toPos, x, y);
            if (RepliesAdapter.this.itemMovedCallback != null) {
                RepliesAdapter.this.itemMovedCallback.run();
            }
        }

        public void onSwiped(ViewHolder viewHolder, int direction) {
        }

        public boolean isItemViewSwipeEnabled() {
            return false;
        }

        public boolean isLongPressDragEnabled() {
            return true;
        }
    };
    private final ArrayList<String> replies;
    private ReplyClickListener replyClickListener;

    RepliesAdapter(ArrayList<String> replies, ReplyClickListener replyClickListener, Runnable itemMovedCallback, ImageCache imageCache) {
        this.replies = replies;
        this.replyClickListener = replyClickListener;
        this.itemMovedCallback = itemMovedCallback;
        this.imageCache = imageCache;
    }

    public int getItemViewType(int position) {
        if (position == 0) {
            return 0;
        }
        if (this.replies == null || position <= this.replies.size()) {
            return 1;
        }
        return 2;
    }

    public ReplyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v;
        if (viewType == 0) {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.settings_messaging_header, parent, false);
            ImageView img = (ImageView) v.findViewById(R.id.illustration);
            if (img != null) {
                ImageUtils.loadImage(img, R.drawable.image_quick_messages, this.imageCache);
            }
        } else {
            v = viewType == 2 ? LayoutInflater.from(parent.getContext()).inflate(R.layout.padding_footer_layout, parent, false) : LayoutInflater.from(parent.getContext()).inflate(R.layout.settings_messaging_list_item, parent, false);
        }
        return new ReplyViewHolder(v);
    }

    public void onBindViewHolder(final ReplyViewHolder holder, int position) {
        if (holder.getItemViewType() == 1) {
            final String reply = (String) this.replies.get(position - 1);
            if (reply != null) {
                OnClickListener clickListener = new OnClickListener() {
                    public void onClick(View v) {
                        RepliesAdapter.this.replyClickListener.onReplyClick(reply, holder.more);
                    }
                };
                if (holder.text != null) {
                    holder.text.setText(reply);
                    holder.text.setOnClickListener(clickListener);
                }
                if (holder.more != null) {
                    holder.more.setOnClickListener(clickListener);
                }
            }
        }
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public int getItemCount() {
        return (this.replies != null ? this.replies.size() : 0) + 2;
    }

    public ArrayList<String> getList() {
        return this.replies;
    }

    public void replace(String oldMessage, String newMessage) {
        int i = this.replies.indexOf(oldMessage);
        this.replies.set(i, newMessage);
        notifyItemChanged(i + 1);
    }

    public void add(String message) {
        if (this.replies.indexOf(message) >= 0) {
            BaseActivity.showShortToast(R.string.message_already_exists, new Object[0]);
            return;
        }
        this.replies.add(message);
        notifyDataSetChanged();
    }

    public void delete(String message) {
        int i = this.replies.indexOf(message);
        if (i >= 0) {
            this.replies.remove(i);
            notifyItemRemoved(i + 1);
        }
    }

    Callback getItemTouchHelperCallback() {
        return this.itemTouchHelperCallback;
    }
}
