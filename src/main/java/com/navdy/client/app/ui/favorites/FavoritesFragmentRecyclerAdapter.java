package com.navdy.client.app.ui.favorites;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ShortcutInfo;
import android.content.pm.ShortcutInfo.Builder;
import android.content.pm.ShortcutManager;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.graphics.drawable.Icon;
import android.os.Build.VERSION;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;
import androidx.recyclerview.widget.ItemTouchHelper.Callback;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import com.alelec.navdyclient.R;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.models.Destination;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.providers.NavdyContentProvider;
import com.navdy.client.app.ui.RecyclerViewCursorAdapter;
import com.navdy.client.app.ui.homescreen.HomescreenActivity;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import java.util.ArrayList;

public class FavoritesFragmentRecyclerAdapter extends RecyclerViewCursorAdapter<ViewHolder> implements ItemTouchHelperAdapter {
    private static final boolean VERBOSE = false;
    private static final Logger logger = new Logger(FavoritesFragmentRecyclerAdapter.class);
    private static int maxShortcutCountPerActivity;
    private OnClickListener listener;

    static class ItemTouchHelperCallback extends Callback {
        private final ItemTouchHelperAdapter mAdapter;

        ItemTouchHelperCallback(ItemTouchHelperAdapter adapter) {
            this.mAdapter = adapter;
        }

        public int getMovementFlags(RecyclerView recyclerView, ViewHolder viewHolder) {
            return Callback.makeMovementFlags(3, 48);
        }

        public boolean onMove(RecyclerView recyclerView, ViewHolder viewHolder, ViewHolder target) {
            this.mAdapter.onItemMove(viewHolder.getAdapterPosition(), target.getAdapterPosition());
            return true;
        }

        public void onMoved(RecyclerView recyclerView, ViewHolder viewHolder, int fromPos, ViewHolder target, int toPos, int x, int y) {
            super.onMoved(recyclerView, viewHolder, fromPos, target, toPos, x, y);
            this.mAdapter.setupDynamicShortcuts();
        }

        public void onSwiped(ViewHolder viewHolder, int direction) {
        }

        public boolean isItemViewSwipeEnabled() {
            return false;
        }

        public boolean isLongPressDragEnabled() {
            return true;
        }
    }

    static {
        if (VERSION.SDK_INT >= 25) {
            maxShortcutCountPerActivity = NavdyApplication.getAppContext().getSystemService(ShortcutManager.class).getMaxShortcutCountPerActivity();
        }
    }

    FavoritesFragmentRecyclerAdapter(Context context) {
        super(context, false, true);
    }

    public Cursor getNewCursor() {
        return NavdyContentProvider.getFavoritesCursor();
    }

    public long getItemId(int position) {
        if (this.dataIsValid && this.cursor != null && this.cursor.moveToPosition(position)) {
            return this.cursor.getLong(this.cursor.getColumnIndex("_id"));
        }
        return 0;
    }

    public FavoriteViewHolder onCreateNormalViewHolder(ViewGroup parent) {
        return new FavoriteViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.favorites_item_layout, parent, false));
    }

    public ViewHolder onCreateHeaderViewHolder(ViewGroup parent) {
        return null;
    }

    public PaddingFooterViewHolder onCreateFooterViewHolder(ViewGroup parent) {
        return new PaddingFooterViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.padding_footer_layout, parent, false));
    }

    @UiThread
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        if (viewHolder instanceof FavoriteViewHolder) {
            FavoriteViewHolder favoriteViewHolder = (FavoriteViewHolder) viewHolder;
            if (!this.dataIsValid) {
                throw new IllegalStateException("this should only be called when the cursor is valid");
            } else if (this.cursor == null || !this.cursor.moveToPosition(position)) {
                throw new IllegalStateException("couldn't move cursor to position " + position + " cursor = " + this.cursor);
            } else {
                try {
                    Destination destination = getItem(position);
                    Pair<String, String> titleAndSubtitle = destination.getTitleAndSubtitle();
                    if (favoriteViewHolder.firstLine != null) {
                        favoriteViewHolder.firstLine.setText((CharSequence) titleAndSubtitle.first);
                    }
                    if (favoriteViewHolder.secondLine != null) {
                        favoriteViewHolder.secondLine.setText((CharSequence) titleAndSubtitle.second);
                    }
                    if (favoriteViewHolder.icon != null) {
                        favoriteViewHolder.icon.setImageResource(destination.getBadgeAsset());
                    }
                    if (favoriteViewHolder.edit != null) {
                        favoriteViewHolder.edit.setTag(position);
                        favoriteViewHolder.edit.setOnClickListener(this.listener);
                    }
                    if (favoriteViewHolder.row != null) {
                        favoriteViewHolder.row.setTag(position);
                        favoriteViewHolder.row.setOnClickListener(this.listener);
                    }
                } catch (CursorIndexOutOfBoundsException e) {
                    logger.e("Error happened while reading the destination cursor: ", e);
                }
            }
        }
    }

    void setClickListener(OnClickListener onClickListener) {
        this.listener = onClickListener;
    }

    public Destination getItem(int position) {
        return NavdyContentProvider.getDestinationItemAt(this.cursor, position);
    }

    public void onItemMove(int fromPosition, int toPosition) {
        Destination fromDestination = getItem(fromPosition);
        Destination toDestination = getItem(toPosition);
        if (fromDestination != null && toDestination != null) {
            fromDestination.setFavoriteOrder(toPosition);
            toDestination.setFavoriteOrder(fromPosition);
            fromDestination.updateOnlyFavoriteFieldsInDb(false);
            toDestination.updateOnlyFavoriteFieldsInDb();
            notifyDataSetChanged();
        }
    }

    public void setupDynamicShortcuts() {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                if (VERSION.SDK_INT >= 25) {
                    ShortcutManager shortcutManager = (ShortcutManager) FavoritesFragmentRecyclerAdapter.this.context.getSystemService(ShortcutManager.class);
                    FavoritesFragmentRecyclerAdapter.logger.d("favoritesAdapter.getItemCount() = " + FavoritesFragmentRecyclerAdapter.this.getItemCount());
                    int itemCount = FavoritesFragmentRecyclerAdapter.this.getItemCount();
                    ArrayList<ShortcutInfo> shortcuts = new ArrayList<>(itemCount);
                    FavoritesFragmentRecyclerAdapter.logger.d("max shortcut count is " + FavoritesFragmentRecyclerAdapter.maxShortcutCountPerActivity);
                    int i = 0;
                    while (i < itemCount && i < FavoritesFragmentRecyclerAdapter.maxShortcutCountPerActivity) {
                        Destination destination = FavoritesFragmentRecyclerAdapter.this.getItem(i);
                        if (destination != null) {
                            Intent intent = new Intent(FavoritesFragmentRecyclerAdapter.this.context, HomescreenActivity.class);
                            intent.putExtra(HomescreenActivity.EXTRA_DESTINATION, destination.id);
                            intent.setAction("android.intent.action.VIEW");
                            String favoriteLabel = destination.getFavoriteLabel().trim();
                            if (StringUtils.isEmptyAfterTrim(favoriteLabel)) {
                                favoriteLabel = destination.getTitleAndSubtitle().first;
                            }
                            shortcuts.add(new Builder(FavoritesFragmentRecyclerAdapter.this.context, String.valueOf(destination.id)).setShortLabel(favoriteLabel).setLongLabel(FavoritesFragmentRecyclerAdapter.this.context.getString(R.string.navigate_to_x, new Object[]{favoriteLabel})).setIcon(Icon.createWithResource(FavoritesFragmentRecyclerAdapter.this.context, destination.getBadgeAsset())).setIntent(intent).build());
                        }
                        i++;
                    }
                    FavoritesFragmentRecyclerAdapter.logger.d("shortcuts.size() = " + shortcuts.size());
                    if (shortcutManager != null) {
                        shortcutManager.setDynamicShortcuts(shortcuts);
                    }
                }
            }
        }, 1);
    }

    public void onItemDismiss(int position) {
    }
}
