package com.navdy.client.app.ui.customviews;

import android.content.Context;
import androidx.annotation.NonNull;
import android.util.AttributeSet;
import android.view.View;

import com.alelec.navdyclient.R;
import com.navdy.client.app.framework.navigation.NavdyRouteHandler.Error;
import com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteInfo;
import com.navdy.client.app.ui.details.DetailsActivity;
import com.navdy.service.library.log.Logger;

public class PendingTripCardView extends TripCardView {
    private static final Logger logger = new Logger(PendingTripCardView.class);
    private Context callingActivity;
    private final Context context;
    private OnClickListener currentClickListener;

    public PendingTripCardView(Context context) {
        this(context, null, 0);
    }

    public PendingTripCardView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public PendingTripCardView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.callingActivity = null;
        this.context = context;
    }

    public void onPendingRouteCalculated(@NonNull Error error, @NonNull NavdyRouteInfo pendingRoute) {
        super.onPendingRouteCalculated(error, pendingRoute);
        setUpClickListener(error, pendingRoute);
    }

    private void setUpClickListener(@NonNull Error error, @NonNull final NavdyRouteInfo pendingRoute) {
        if (error == Error.NONE) {
            this.currentClickListener = new OnClickListener() {
                public void onClick(View v) {
                    PendingTripCardView.logger.v("onClick, starting details activity");
                    if (PendingTripCardView.this.callingActivity != null) {
                        DetailsActivity.startDetailsActivityForResult(pendingRoute.getDestination(), PendingTripCardView.this.callingActivity);
                        PendingTripCardView.this.callingActivity = null;
                        return;
                    }
                    DetailsActivity.startDetailsActivity(pendingRoute.getDestination(), PendingTripCardView.this.context);
                }
            };
        } else {
            this.currentClickListener = new OnClickListener() {
                public void onClick(View v) {
                    PendingTripCardView.logger.v("onClick, retrying route");
                    PendingTripCardView.this.navdyRouteHandler.retryRoute();
                }
            };
        }
    }

    public void handleOnClick() {
        if (this.currentClickListener != null) {
            this.currentClickListener.onClick(this);
        }
    }

    public void handleOnClick(Context context) {
        this.callingActivity = context;
        if (this.currentClickListener != null && this.callingActivity != null) {
            this.currentClickListener.onClick(this);
        }
    }

    protected String appendPrefix(String name) {
        return getResources().getString(R.string.pending_trip_prefix, new Object[]{name});
    }
}
