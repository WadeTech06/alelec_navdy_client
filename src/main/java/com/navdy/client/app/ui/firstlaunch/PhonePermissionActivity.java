package com.navdy.client.app.ui.firstlaunch;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.view.View;
import android.widget.ImageView;
import com.alelec.navdyclient.R;
import com.navdy.client.app.framework.util.SystemUtils;
import com.navdy.client.app.framework.util.ZendeskJWT;
import com.navdy.client.app.ui.WebViewActivity;
import com.navdy.client.app.ui.base.BaseActivity;
import com.navdy.client.app.ui.base.BaseToolbarActivity;
import com.navdy.client.app.ui.homescreen.HomescreenActivity;
import com.navdy.client.app.ui.settings.ContactUsActivity;

public class PhonePermissionActivity extends BaseToolbarActivity {
    AppSetupScreen appSetupScreen = AppSetupPagerAdapter.getScreen(8);
    private AppSetupBottomCardGenericFragment bottomCard;
    private ImageView hud;
    private boolean showingFail = false;

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.fle_app_setup_permission);
        new ToolbarBuilder().title((int) R.string.app_setup).build();
        this.hud = (ImageView) findViewById(R.id.hud);
        if (this.hud != null) {
            this.hud.setImageResource(this.appSetupScreen.hudRes);
        }
        FragmentManager fm = getSupportFragmentManager();
        if (fm != null) {
            FragmentTransaction ft = fm.beginTransaction();
            if (ft != null) {
                this.bottomCard = new AppSetupBottomCardGenericFragment();
                Bundle bundle = new Bundle();
                bundle.putParcelable("screen", this.appSetupScreen);
                this.bottomCard.setArguments(bundle);
                ft.replace(R.id.bottom_card, this.bottomCard);
                ft.commit();
            }
        }
    }

    private void goToHomeScreen() {
        Intent i = new Intent(getApplicationContext(), HomescreenActivity.class);
        i.setFlags(268468224);
        startActivity(i);
        finish();
    }

    private void showFailureForCurrentScreen() {
        this.logger.d("showFailureForCurrentScreen");
        if (this.bottomCard != null) {
            this.showingFail = true;
            this.bottomCard.showFail();
        }
        if (this.hud != null) {
            this.hud.setImageResource(this.appSetupScreen.hudResFail);
        }
    }

    public void onPrivacyPolicyClick(View view) {
        Intent browserIntent = new Intent(getApplicationContext(), WebViewActivity.class);
        browserIntent.putExtra("type", WebViewActivity.PRIVACY);
        startActivity(browserIntent);
    }

    public void onContactSupportClick(View view) {
        startActivity(new Intent(getApplicationContext(), ContactUsActivity.class));
    }

    public void onDescriptionClick(View view) {
        if (this.showingFail) {
            SystemUtils.goToSystemSettingsAppInfoForOurApp(this);
        }
    }

    public void onHelpCenterClick(View view) {
        openBrowserFor(ZendeskJWT.getZendeskUri());
    }

    public void onButtonClick(View v) {
        if (this.appSetupScreen != null && !BaseActivity.isEnding(this)) {
            if (this.appSetupScreen.isMandatory || !this.showingFail) {
                requestPhonePermission(new Runnable() {
                    public void run() {
                        PhonePermissionActivity.this.goToHomeScreen();
                    }
                }, new Runnable() {
                    public void run() {
                        PhonePermissionActivity.this.showFailureForCurrentScreen();
                    }
                });
            } else {
                goToHomeScreen();
            }
        }
    }
}
