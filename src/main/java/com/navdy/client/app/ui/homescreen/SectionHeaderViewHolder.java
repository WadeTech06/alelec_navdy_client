package com.navdy.client.app.ui.homescreen;

import androidx.recyclerview.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.alelec.navdyclient.R;

class SectionHeaderViewHolder extends ViewHolder {
    ImageView chevron;
    TextView title;

    SectionHeaderViewHolder(View itemView) {
        super(itemView);
        this.title = (TextView) itemView.findViewById(R.id.title);
        this.chevron = (ImageView) itemView.findViewById(R.id.chevron);
    }
}
