package com.navdy.client.app.ui.firstlaunch;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import com.alelec.navdyclient.R;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.tracking.Tracker;
import com.navdy.client.app.tracking.TrackerConstants.Screen.FirstLaunch.Install;
import com.navdy.client.app.ui.base.BaseToolbarActivity;
import com.navdy.client.app.ui.base.BaseToolbarActivity.ToolbarBuilder;
import com.navdy.client.app.ui.settings.SettingsConstants;
import com.navdy.client.app.ui.settings.SettingsUtils;

public class InstallIntroActivity extends BaseToolbarActivity {
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.fle_install_intro);
        if (getIntent().getBooleanExtra(SettingsConstants.EXTRA_WAS_SKIPPED, false) && InstallActivity.userHasFinishedInstall() && !AppSetupActivity.userHasFinishedAppSetup()) {
            AppSetupActivity.goToAppSetup(this);
        }
        new ToolbarBuilder().title((int) R.string.installation).build();
        TextView alreadyInstalled = (TextView) findViewById(R.id.already_installed);
        if (alreadyInstalled != null) {
            alreadyInstalled.setText(StringUtils.fromHtml((int) R.string.already_installed_skip_to_app_setup));
            alreadyInstalled.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    SharedPreferences sharedPreferences = SettingsUtils.getSharedPreferences();
                    if (sharedPreferences != null) {
                        sharedPreferences.edit().putBoolean(SettingsConstants.HAS_SKIPPED_INSTALL, true).apply();
                    }
                    InstallIntroActivity.this.startActivity(new Intent(InstallIntroActivity.this.getApplicationContext(), InstallCompleteActivity.class));
                }
            });
        }
    }

    public void onNextClick(View v) {
        SharedPreferences sharedPreferences = SettingsUtils.getSharedPreferences();
        if (sharedPreferences != null) {
            sharedPreferences.edit().putBoolean(SettingsConstants.HAS_SKIPPED_INSTALL, false).apply();
        }
        startActivity(new Intent(getApplicationContext(), BoxPickerActivity.class));
    }

    protected void onResume() {
        super.onResume();
        Tracker.tagScreen(Install.READY_CHECK);
    }
}
