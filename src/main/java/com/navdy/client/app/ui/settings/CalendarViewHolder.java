package com.navdy.client.app.ui.settings;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.GradientDrawable;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import com.alelec.navdyclient.R;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.models.Calendar;

public class CalendarViewHolder extends ViewHolder {
    private TextView calendarName;
    private TextView calendarOwner;
    private ImageView checkMark;
    private View color;
    private View rootView;
    private final SharedPreferences sharedPrefs = SettingsUtils.getSharedPreferences();

    CalendarViewHolder(View v) {
        super(v);
        this.rootView = v;
        this.color = v.findViewById(R.id.color);
        this.calendarName = (TextView) v.findViewById(R.id.calendar_name);
        this.calendarOwner = (TextView) v.findViewById(R.id.calendar_owner);
        this.checkMark = (ImageView) v.findViewById(R.id.checkmark);
    }

    public void setCalendar(final Calendar calendar) {
        int i = 0;
        if (this.color != null && this.calendarName != null && this.calendarOwner != null && this.checkMark != null) {
            Context context = NavdyApplication.getAppContext();
            ((GradientDrawable) this.color.getBackground()).setColor(calendar.calendarColor);
            boolean globalSwitchIsEnabled = this.sharedPrefs.getBoolean(SettingsConstants.CALENDARS_ENABLED, true);
            this.calendarName.setText(calendar.displayName);
            this.calendarOwner.setText(context.getString(R.string.shared_by, new Object[]{calendar.accountName}));
            boolean enabled = isEnabled(calendar);
            ImageView imageView = this.checkMark;
            if (!enabled) {
                i = 8;
            }
            imageView.setVisibility(i);
            this.rootView.setAlpha(globalSwitchIsEnabled ? 1.0f : 0.3f);
            this.rootView.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    boolean enabled = true;
                    int i = 0;
                    if (CalendarViewHolder.this.sharedPrefs.getBoolean(SettingsConstants.CALENDARS_ENABLED, true)) {
                        String sharedPrefKey = SettingsConstants.CALENDAR_PREFIX + calendar.id;
                        if (CalendarViewHolder.this.sharedPrefs.getBoolean(sharedPrefKey, calendar.visible)) {
                            enabled = false;
                        }
                        CalendarViewHolder.this.sharedPrefs.edit().putBoolean(sharedPrefKey, enabled).apply();
                        ImageView access$100 = CalendarViewHolder.this.checkMark;
                        if (!enabled) {
                            i = 8;
                        }
                        access$100.setVisibility(i);
                    }
                }
            });
        }
    }

    private boolean isEnabled(Calendar calendar) {
        return this.sharedPrefs.getBoolean(SettingsConstants.CALENDAR_PREFIX + calendar.id, calendar.visible);
    }
}
