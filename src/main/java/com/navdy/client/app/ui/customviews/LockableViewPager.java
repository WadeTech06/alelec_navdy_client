package com.navdy.client.app.ui.customviews;

import android.content.Context;
import androidx.viewpager.widget.ViewPager;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;

public class LockableViewPager extends ViewPager {
    private boolean swipeable = false;

    public LockableViewPager(Context context) {
        super(context);
    }

    public LockableViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public boolean getSwipeable() {
        return this.swipeable;
    }

    public void setSwipeable(boolean swipeable) {
        this.swipeable = swipeable;
    }

    public boolean onTouchEvent(MotionEvent event) {
        return this.swipeable && super.onTouchEvent(event);
    }

    public boolean onInterceptTouchEvent(MotionEvent event) {
        return this.swipeable && super.onInterceptTouchEvent(event);
    }

    public boolean canScrollHorizontally(int direction) {
        return this.swipeable && super.canScrollHorizontally(direction);
    }

    public boolean executeKeyEvent(KeyEvent event) {
        return this.swipeable && super.executeKeyEvent(event);
    }
}
