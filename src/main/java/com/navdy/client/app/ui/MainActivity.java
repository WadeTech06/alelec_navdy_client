package com.navdy.client.app.ui;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.Toast;
//import com.localytics.android.Localytics;
import com.alelec.navdyclient.R;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.AppInstance;
import com.navdy.client.app.framework.util.GmsUtils;
import com.navdy.client.app.framework.util.ImageUtils;
import com.navdy.client.app.tracking.Tracker;
import com.navdy.client.app.tracking.TrackerConstants.Screen;
import com.navdy.client.app.ui.base.BaseActivity;
import com.navdy.client.app.ui.firstlaunch.AppSetupActivity;
import com.navdy.client.app.ui.firstlaunch.MarketingFlowActivity;
import com.navdy.client.app.ui.homescreen.HomescreenActivity;
import com.navdy.service.library.task.TaskManager;
import java.util.Date;

public class MainActivity extends BaseActivity {
    private static final int SPLASH_TIMEOUT = 1000;
    private AppInstance appInstance;
    private Runnable launchRunnable = new Runnable() {
        public void run() {
            if (!MainActivity.this.isActivityDestroyed()) {
                Intent i;
                if (AppSetupActivity.userHasFinishedAppSetup()) {
                    i = new Intent(MainActivity.this, HomescreenActivity.class);
                    i.setFlags(67108864);
                } else {
                    i = new Intent(MainActivity.this, MarketingFlowActivity.class);
                }
                MainActivity.this.startActivity(i);
                MainActivity.this.finish();
            }
        }
    };

    public boolean requiresBus() {
        return false;
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main_lyt);
        ImageView imageView = (ImageView) findViewById(R.id.navdy_logo);
        if (imageView != null) {
            ImageUtils.loadImage(imageView, R.drawable.image_launch_background, null);
        }
        this.appInstance = AppInstance.getInstance();
        final long timeBeforeGmsCheck = new Date().getTime();
        switch (GmsUtils.checkIfMissingOrOutOfDate()) {
            case MISSING:
                showQuestionDialog(R.string.no_google_play_services, R.string.dude_you_need_google_play_services, R.string.install_now, R.string.later, new OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int whichButton) {
                        if (whichButton == -1) {
                            MainActivity.this.openMarketAppFor("com.google.android.gms");
                            MainActivity.this.finish();
                            return;
                        }
                        MainActivity.this.initAndStartTheApp(timeBeforeGmsCheck);
                    }
                }, null);
                return;
            case OUT_OF_DATE:
                showQuestionDialog(R.string.outdated_gms_title, R.string.outdated_gms_message, R.string.update_now, R.string.later, new OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int whichButton) {
                        if (whichButton == -1) {
                            MainActivity.this.openMarketAppFor("com.google.android.gms");
                            MainActivity.this.finish();
                            return;
                        }
                        MainActivity.this.initAndStartTheApp(timeBeforeGmsCheck);
                    }
                }, null);
                return;
            case UP_TO_DATE:
                initAndStartTheApp(timeBeforeGmsCheck);
                return;
            default:
                return;
        }
    }

    private void initAndStartTheApp(long timeBeforeGmsCheck) {
        int remainingTime = 1000 - ((int) (new Date().getTime() - timeBeforeGmsCheck));
        if (remainingTime < 0) {
            remainingTime = 0;
        }
        final int finalRemainingTime = remainingTime;
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                try {
                    MainActivity.this.appInstance.initializeApp();
                    MainActivity.this.handler.postDelayed(MainActivity.this.launchRunnable, (long) finalRemainingTime);
                } catch (Throwable t) {
                    MainActivity.this.logger.e(t);
                    MainActivity.this.handler.post(new Runnable() {
                        public void run() {
                            if (!MainActivity.this.isActivityDestroyed()) {
                                Toast.makeText(NavdyApplication.getAppContext(), "App Instance error", 0).show();
                                MainActivity.this.finish();
                            }
                        }
                    });
                }
            }
        }, 1);
    }

    protected void onDestroy() {
        this.handler.removeCallbacks(this.launchRunnable);
        super.onDestroy();
    }

    protected void onResume() {
        super.onResume();
        Tracker.tagScreen(Screen.SPLASH);
    }

//    protected void onNewIntent(Intent intent) {
//        super.onNewIntent(intent);
//        Localytics.onNewIntent(this, intent);
//    }
}
