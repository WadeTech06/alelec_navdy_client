package com.navdy.client.app.ui.settings;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;
import android.widget.Switch;
import android.widget.EditText;
import com.alelec.navdyclient.R;
import com.navdy.client.app.tracking.Tracker;
import com.navdy.client.app.tracking.TrackerConstants.Event;
import com.navdy.client.app.tracking.TrackerConstants.Screen.Settings;
import com.navdy.client.app.ui.base.BaseActivity;
import com.navdy.client.app.ui.base.BaseEditActivity;
import com.navdy.client.app.ui.base.BaseToolbarActivity.ToolbarBuilder;

public class NavigationSettingsActivity extends BaseEditActivity {
    private Switch autoRecalc;
    private Switch autoTrains;
    private Switch ferries;
    private Switch highways;
    private RadioButton routeCalculationFastest;
    private RadioButton routeCalculationShortest;
    private SharedPreferences sharedPrefs;
    private Switch tollRoads;
    private Switch tunnels;
    private Switch unpavedRoads;
    private EditText googleApiKey;
    private EditText hereAppId;
    private EditText hereAppCode;
    private EditText hereLicence;
    private String initial_googleApiKey;
    private String initial_hereAppId;
    private String initial_hereAppCode;
    private String initial_hereLicence;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.settings_navigation);
        new ToolbarBuilder().title((int) R.string.menu_navigation).build();
        this.routeCalculationFastest = (RadioButton) findViewById(R.id.route_calculation_fastest);
        this.routeCalculationShortest = (RadioButton) findViewById(R.id.route_calculation_shortest);
        this.autoRecalc = (Switch) findViewById(R.id.auto_recalc);
        this.highways = (Switch) findViewById(R.id.highways);
        this.tollRoads = (Switch) findViewById(R.id.toll_roads);
        this.ferries = (Switch) findViewById(R.id.ferries);
        this.tunnels = (Switch) findViewById(R.id.tunnels);
        this.unpavedRoads = (Switch) findViewById(R.id.unpaved_roads);
        this.autoTrains = (Switch) findViewById(R.id.auto_trains);
        this.sharedPrefs = SettingsUtils.getSharedPreferences();
        boolean calculateShortestRouteIsOn = this.sharedPrefs.getBoolean(SettingsConstants.NAVIGATION_ROUTE_CALCULATION, false);
        boolean autoRecalcIsOn = this.sharedPrefs.getBoolean(SettingsConstants.NAVIGATION_AUTO_RECALC, false);
        boolean highwaysIsOn = this.sharedPrefs.getBoolean(SettingsConstants.NAVIGATION_HIGHWAYS, true);
        boolean tollRoadsIsOn = this.sharedPrefs.getBoolean(SettingsConstants.NAVIGATION_TOLL_ROADS, true);
        boolean ferriesIsOn = this.sharedPrefs.getBoolean(SettingsConstants.NAVIGATION_FERRIES, true);
        boolean tunnelsIsOn = this.sharedPrefs.getBoolean(SettingsConstants.NAVIGATION_TUNNELS, true);
        boolean unpavedRoadsIsOn = this.sharedPrefs.getBoolean(SettingsConstants.NAVIGATION_UNPAVED_ROADS, true);
        boolean autoTrainsIsOn = this.sharedPrefs.getBoolean(SettingsConstants.NAVIGATION_AUTO_TRAINS, true);
        initCompoundButton(this.routeCalculationFastest, !calculateShortestRouteIsOn);
        initCompoundButton(this.routeCalculationShortest, calculateShortestRouteIsOn);
        initCompoundButton(this.autoRecalc, autoRecalcIsOn);
        initCompoundButton(this.highways, highwaysIsOn);
        initCompoundButton(this.tollRoads, tollRoadsIsOn);
        initCompoundButton(this.ferries, ferriesIsOn);
        initCompoundButton(this.tunnels, tunnelsIsOn);
        initCompoundButton(this.unpavedRoads, unpavedRoadsIsOn);
        initCompoundButton(this.autoTrains, autoTrainsIsOn);

        googleApiKey = findViewById(R.id.google_api_key);
        hereAppId = findViewById(R.id.here_maps_appid);
        hereAppCode = findViewById(R.id.here_maps_appcode);
        hereLicence = findViewById(R.id.here_maps_licence);
        initial_googleApiKey = this.sharedPrefs.getString(SettingsConstants.NAVIGATION_GOOGLE_API_KEY, "");
        initial_hereAppId = this.sharedPrefs.getString(SettingsConstants.NAVIGATION_HERE_APPID, "");
        initial_hereAppCode = this.sharedPrefs.getString(SettingsConstants.NAVIGATION_HERE_APPCODE, "");
        initial_hereLicence = this.sharedPrefs.getString(SettingsConstants.NAVIGATION_HERE_LICENCE, "");

        googleApiKey.setText(initial_googleApiKey);
        hereAppId.setText(initial_hereAppId);
        hereAppCode.setText(initial_hereAppCode);
        hereLicence.setText(initial_hereLicence);
    }

    protected void onPause() {
        if (!googleApiKey.getText().toString().equals(initial_googleApiKey) ||
            !hereAppId.getText().toString().equals(initial_hereAppId) ||
            !hereAppCode.getText().toString().equals(initial_hereAppCode) ||
            !hereLicence.getText().toString().equals(initial_hereLicence)) {
            somethingChanged = true;
        }
        super.onPause();
    }

    protected void onResume() {
        super.onResume();
        Tracker.tagScreen(Settings.NAVIGATION);
    }

    public void onAutoRecalcClick(View view) {
        if (this.autoRecalc != null && this.autoRecalc.isEnabled()) {
            this.autoRecalc.performClick();
        }
    }

    protected void saveChanges() {
        if (this.sharedPrefs != null) {
            long serialNumber = this.sharedPrefs.getLong("nav_serial_number", 0) + 1;

            this.sharedPrefs.edit()
                    .putLong("nav_serial_number", serialNumber)
                    .putBoolean(SettingsConstants.NAVIGATION_ROUTE_CALCULATION, this.routeCalculationShortest.isChecked())
                    .putBoolean(SettingsConstants.NAVIGATION_AUTO_RECALC, this.autoRecalc.isChecked())
                    .putBoolean(SettingsConstants.NAVIGATION_HIGHWAYS, this.highways.isChecked())
                    .putBoolean(SettingsConstants.NAVIGATION_TOLL_ROADS, this.tollRoads.isChecked())
                    .putBoolean(SettingsConstants.NAVIGATION_FERRIES, this.ferries.isChecked())
                    .putBoolean(SettingsConstants.NAVIGATION_TUNNELS, this.tunnels.isChecked())
                    .putBoolean(SettingsConstants.NAVIGATION_UNPAVED_ROADS, this.unpavedRoads.isChecked())
                    .putBoolean(SettingsConstants.NAVIGATION_AUTO_TRAINS, this.autoTrains.isChecked())
                    .putString(SettingsConstants.NAVIGATION_GOOGLE_API_KEY, googleApiKey.getText().toString())
                    .putString(SettingsConstants.NAVIGATION_HERE_APPID, hereAppId.getText().toString())
                    .putString(SettingsConstants.NAVIGATION_HERE_APPCODE, hereAppCode.getText().toString())
                    .putString(SettingsConstants.NAVIGATION_HERE_LICENCE, hereLicence.getText().toString())
                    .apply();

            if (SettingsUtils.sendNavSettingsToTheHud(SettingsUtils.buildNavigationPreferences(
                    serialNumber,
                    this.routeCalculationShortest.isChecked(),
                    this.autoRecalc.isChecked(),
                    this.highways.isChecked(),
                    this.tollRoads.isChecked(),
                    this.ferries.isChecked(),
                    this.tunnels.isChecked(),
                    this.unpavedRoads.isChecked(),
                    this.autoTrains.isChecked(),
                    this.sharedPrefs.getBoolean(SettingsConstants.AUDIO_SPEED_WARNINGS, false),
                    this.sharedPrefs.getBoolean(SettingsConstants.AUDIO_CAMERA_WARNINGS, true),
                    this.sharedPrefs.getBoolean(SettingsConstants.AUDIO_TURN_BY_TURN_INSTRUCTIONS, true)))
            ) {
                BaseActivity.showShortToast(R.string.settings_navigation_succeeded, new Object[0]);
                Tracker.tagEvent(Event.Settings.NAVIGATION_SETTINGS_CHANGED);
                return;
            }
            BaseActivity.showShortToast(R.string.settings_need_to_be_connected_to_hud, new Object[0]);
            return;
        }
        BaseActivity.showShortToast(R.string.settings_navigation_failed, new Object[0]);
    }
}
