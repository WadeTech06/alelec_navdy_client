package com.navdy.client.app.ui.customviews;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.RectF;
import androidx.core.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import com.here.odnp.config.OdnpConfigStatic;
import com.alelec.navdyclient.R;
import com.navdy.client.app.NavdyApplication;
import com.navdy.service.library.log.Logger;

public class ProgressCircle extends View {
    private float endAngleNaturalized;
    private final Logger logger;
    private RectF oval;
    private Paint paint;
    private float startAngle;
    private float strokeWidth;
    private float sweepAngle;

    public ProgressCircle(Context context) {
        this(context, null, 0);
    }

    public ProgressCircle(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
        this.logger.v("ProgressCircle(Context, Attrs)");
    }

    public ProgressCircle(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.logger = new Logger(getClass());
        this.paint = new Paint();
        this.strokeWidth = 30.0f;
        this.oval = new RectF();
        this.startAngle = 90.0f;
        this.sweepAngle = 270.0f;
        this.endAngleNaturalized = 1.0f;
        this.logger.v("progressCircle initialized");
        this.paint.setColor(ContextCompat.getColor(NavdyApplication.getAppContext(), R.color.blue));
        this.paint.setStrokeWidth(this.strokeWidth);
        this.paint.setStyle(Style.STROKE);
        this.paint.setFlags(1);
    }

    public void setProgressColor(int color) {
        this.logger.v("setProgressColor");
        this.paint.setColor(color);
    }

    public void setProgress(float progress) {
        this.logger.v("setProgress to: " + progress);
        if (progress > 1.0f || progress < 0.0f) {
            throw new RuntimeException("Value must be between 0 and 1: " + progress);
        }
        this.endAngleNaturalized = progress;
        invalidate();
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        this.logger.v("onDraw");
        this.oval.set(this.strokeWidth / 2.0f, this.strokeWidth / 2.0f, ((float) getWidth()) - (this.strokeWidth / 2.0f), ((float) getWidth()) - (this.strokeWidth / 2.0f));
        canvas.drawArc(this.oval, this.startAngle, this.sweepAngle, true, this.paint);
    }

    public void startAnimation() {
        this.logger.v("startAnimation");
        ValueAnimator valueAnimator = ValueAnimator.ofFloat(new float[]{this.sweepAngle, this.endAngleNaturalized});
        valueAnimator.addUpdateListener(new AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                ProgressCircle.this.logger.v("animationupdate: " + valueAnimator.getAnimatedValue());
                ProgressCircle.this.sweepAngle = ((Float) valueAnimator.getAnimatedValue()).floatValue();
                ProgressCircle.this.invalidate();
            }
        });
        valueAnimator.setDuration(OdnpConfigStatic.MIN_ALARM_TIMER_INTERVAL);
        valueAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
        valueAnimator.start();
    }
}
