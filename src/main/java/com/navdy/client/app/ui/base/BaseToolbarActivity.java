package com.navdy.client.app.ui.base;

import androidx.annotation.IdRes;
import androidx.annotation.StringRes;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import com.alelec.navdyclient.R;
import com.navdy.client.app.framework.util.StringUtils;

public abstract class BaseToolbarActivity extends BaseActivity {

    public class ToolbarBuilder {
        String title = "";

        public ToolbarBuilder title(String title) {
            if (!StringUtils.isEmptyAfterTrim(title)) {
                this.title = title;
            }
            return this;
        }

        public ToolbarBuilder title(@StringRes int title) {
            String titleString = "";
            if (title > 0) {
                titleString = BaseToolbarActivity.this.getResources().getString(title);
            }
            return title(titleString);
        }

        public Toolbar build() {
            BaseToolbarActivity.this.setTitle(this.title);
            return BaseToolbarActivity.this.initActionBar(R.id.my_toolbar);
        }
    }

    private Toolbar initActionBar(@IdRes int toolbarResId) {
        Toolbar myToolbar = (Toolbar) findViewById(toolbarResId);
        setSupportActionBar(myToolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        return myToolbar;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 16908332:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
