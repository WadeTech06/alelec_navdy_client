package com.navdy.client.app.ui.firstlaunch;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;
import androidx.viewpager.widget.ViewPager.OnPageChangeListener;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;
import com.alelec.navdyclient.R;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.AppInstance;
import com.navdy.client.app.framework.DeviceConnection.DeviceConnectedEvent;
import com.navdy.client.app.framework.DeviceConnection.DeviceDisconnectedEvent;
import com.navdy.client.app.framework.models.UserAccountInfo;
import com.navdy.client.app.framework.servicehandler.ContactServiceHandler;
import com.navdy.client.app.framework.servicehandler.MusicServiceHandler;
import com.navdy.client.app.framework.util.ContactsManager;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.framework.util.SystemUtils;
import com.navdy.client.app.framework.util.ZendeskJWT;
import com.navdy.client.app.tracking.Tracker;
import com.navdy.client.app.tracking.TrackerConstants.Attributes;
import com.navdy.client.app.tracking.TrackerConstants.Attributes.InstallAttributes;
import com.navdy.client.app.tracking.TrackerConstants.Event.Install;
import com.navdy.client.app.ui.WebViewActivity;
import com.navdy.client.app.ui.base.BaseActivity;
import com.navdy.client.app.ui.firstlaunch.AppSetupScreen.ScreenType;
import com.navdy.client.app.ui.homescreen.HomescreenActivity;
import com.navdy.client.app.ui.settings.ContactUsActivity;
import com.navdy.client.app.ui.settings.ProfileSettingsActivity;
import com.navdy.client.app.ui.settings.SettingsConstants;
import com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences;
import com.navdy.client.app.ui.settings.SettingsUtils;
import com.navdy.service.library.device.RemoteDevice;
import com.navdy.service.library.task.TaskManager;
import com.squareup.otto.Subscribe;
import java.lang.ref.WeakReference;
import java.util.HashMap;

public class AppSetupActivity extends ProfileSettingsActivity {
    private ViewPager bottomCard;
    private boolean hasNeverShownBtSuccess = true;
    public static boolean hasOpenedBluetoothPairingDialog = false;
    private int hiddenButtonClickCount = 0;
    private ImageView hud;
    private AppSetupPagerAdapter setupPagerAdapter;
    private boolean showingFail = false;

    public static void goToAppSetup(Activity activity) {
        activity.startActivity(new Intent(activity, AppSetupActivity.class));
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.fle_app_setup);
        new ToolbarBuilder().title((int) R.string.app_setup).build();
        this.hud = (ImageView) findViewById(R.id.hud);
        this.bottomCard = (ViewPager) findViewById(R.id.bottom_card);
        if (this.hud == null || this.bottomCard == null) {
            this.logger.e("Ui element missing !");
            return;
        }
        this.userPhoto = Tracker.getUserProfilePhoto();
        if (this.customerPrefs != null) {
            this.name = this.customerPrefs.getString(ProfilePreferences.FULL_NAME, "");
            this.email = this.customerPrefs.getString("email", "");
        }
        if (!(StringUtils.isValidName(this.name) && StringUtils.isValidEmail(this.email))) {
            requestContactsPermission(new Runnable() {
                public void run() {
                    UserAccountInfo userAccountInfo = ContactsManager.lookUpUserProfileInfo();
                    if (userAccountInfo != null) {
                        boolean incrementAndSendToHud = false;
                        if (AppSetupActivity.this.userPhoto == null && userAccountInfo.photo != null) {
                            Tracker.saveUserPhotoToInternalStorage(userAccountInfo.photo);
                            incrementAndSendToHud = true;
                        }
                        if (AppSetupActivity.this.customerPrefs != null) {
                            Editor editor = AppSetupActivity.this.customerPrefs.edit();
                            if (!StringUtils.isValidName(AppSetupActivity.this.name) && StringUtils.isValidName(userAccountInfo.fullName)) {
                                editor.putString(ProfilePreferences.FULL_NAME, userAccountInfo.fullName);
                                incrementAndSendToHud = true;
                            }
                            if (!StringUtils.isValidEmail(AppSetupActivity.this.email)) {
                                if (!StringUtils.isValidEmail(userAccountInfo.email)) {
                                    for (Account account : ((AccountManager) AppSetupActivity.this.getSystemService("account")).getAccounts()) {
                                        if (account.type.equalsIgnoreCase("com.google") && StringUtils.isValidEmail(account.name)) {
                                            editor.putString("email", account.name);
                                            incrementAndSendToHud = true;
                                            break;
                                        }
                                    }
                                } else {
                                    editor.putString("email", userAccountInfo.email);
                                    incrementAndSendToHud = true;
                                }
                            }
                            editor.apply();
                        }
                        if (incrementAndSendToHud) {
                            TaskManager.getInstance().execute(new Runnable() {
                                public void run() {
                                    SettingsUtils.incrementDriverProfileSerial();
                                    SettingsUtils.sendDriverProfileToTheHudBasedOnSharedPrefValue();
                                }
                            }, 1);
                        }
                    }
                }
            }, null);
        }
        setUpViewPager();
    }

    private void setUpViewPager() {
        this.bottomCard.setOffscreenPageLimit(1);
        this.setupPagerAdapter = new AppSetupPagerAdapter(getSupportFragmentManager(), this.imageCache);
        this.bottomCard.addOnPageChangeListener(new OnPageChangeListener() {
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            public void onPageSelected(int position) {
                AppSetupActivity.this.showingFail = false;
                SystemUtils.dismissKeyboard(AppSetupActivity.this);
                AppSetupScreen currentScreen = AppSetupActivity.this.getCurrentScreen();
                if (currentScreen == null || currentScreen.screenType != ScreenType.PROFILE) {
                    AppSetupActivity.this.hideProfileStuff();
                    AppSetupActivity.this.updateIllustration(position);
                    if (AppSetupActivity.this.setupPagerAdapter != null) {
                        AppSetupBottomCardGenericFragment frag = (AppSetupBottomCardGenericFragment) AppSetupActivity.this.setupPagerAdapter.getItem(position);
                        if (frag != null) {
                            frag.showNormal();
                        }
                    }
                } else {
                    AppSetupActivity.this.showProfileStuff();
                }
                if (currentScreen != null && AppSetupActivity.this.hasNeverShownBtSuccess && currentScreen.screenType == ScreenType.BLUETOOTH && AppSetupActivity.this.isDeviceConnected(AppSetupActivity.this.getApplicationContext())) {
                    AppSetupActivity.this.hasNeverShownBtSuccess = false;
                    AppSetupActivity.this.moveToNextScreen();
                }
            }

            public void onPageScrollStateChanged(int state) {
            }
        });
        this.bottomCard.setAdapter(this.setupPagerAdapter);
    }

    protected void onPause() {
        super.onPause();
    }

    public void onResume() {
        super.onResume();
        if (!Tracker.weHaveCarInfo()) {
            startActivity(new Intent(getApplicationContext(), EditCarInfoActivity.class));
        }
        this.setupPagerAdapter.recalculateScreenCount();
        AppSetupScreen currentScreen = getCurrentScreen();
        if (currentScreen != null) {
            if (currentScreen.screenType == ScreenType.BLUETOOTH) {
                if (isDeviceConnected(getApplicationContext())) {
                    moveToNextScreen();
                } else if (this.hasOpenedBluetoothPairingDialog) {
                    showFailureForCurrentScreen();
                }
            }
            if (currentScreen.screenType == ScreenType.NOTIFICATIONS) {
                if (BaseActivity.weHaveNotificationPermission()) {
                    moveToNextScreen();
                } else {
                    showFailureForCurrentScreen();
                }
            }
        }
        Tracker.tagScreen("First_Launch");
    }

    protected void onDestroy() {
        if (this.setupPagerAdapter != null) {
            this.setupPagerAdapter.clearCache();
        }
        super.onDestroy();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (this.setupPagerAdapter != null) {
            this.setupPagerAdapter.recalculateScreenCount();
        }
    }

    @Subscribe
    public void appInstanceDeviceConnectedEvent(DeviceConnectedEvent deviceConnectedEvent) {
        handleConnectionChange(true);
    }

    @Subscribe
    public void appInstanceDeviceDisconnectedEvent(DeviceDisconnectedEvent deviceDisconnectedEvent) {
        handleConnectionChange(false);
    }

    private void handleConnectionChange(boolean connected) {
        if (this.setupPagerAdapter != null) {
            this.setupPagerAdapter.recalculateScreenCount();
        }
        if (connected) {
            AppSetupScreen currentScreen = getCurrentScreen();
            if (currentScreen != null && currentScreen.screenType == ScreenType.BLUETOOTH) {
                moveToNextScreen();
                return;
            }
            return;
        }
        this.bottomCard.setCurrentItem(1);
    }

    public boolean isDeviceConnected(Context applicationContext) {
        return AppInstance.getInstance().isDeviceConnected();
    }

    public void onPrivacyPolicyClick(View view) {
        Intent browserIntent = new Intent(getApplicationContext(), WebViewActivity.class);
        browserIntent.putExtra("type", WebViewActivity.PRIVACY);
        startActivity(browserIntent);
    }

    public void onContactSupportClick(View view) {
        startActivity(new Intent(getApplicationContext(), ContactUsActivity.class));
    }

    public void onDescriptionClick(View view) {
        if (this.showingFail) {
            AppSetupScreen currentScreen = getCurrentScreen();
            if (currentScreen != null && currentScreen.screenType == ScreenType.BLUETOOTH) {
                onHelpCenterClick(null);
            } else if (currentScreen == null || !(currentScreen.screenType == ScreenType.PROFILE || currentScreen.screenType == ScreenType.NOTIFICATIONS)) {
                SystemUtils.goToSystemSettingsAppInfoForOurApp(this);
            }
        }
    }

    public void onHelpCenterClick(View view) {
        openBrowserFor(ZendeskJWT.getZendeskUri());
    }

    public void onButtonClick(View v) {
        AppSetupScreen currentScreen = getCurrentScreen();
        if (currentScreen != null && !BaseActivity.isEnding(this)) {
            Runnable doThingsThatRequirePermission = new Runnable() {
                public void run() {
                    AppSetupActivity.this.moveToNextScreen();
                }
            };
            Runnable handlePermissionDenial = new Runnable() {
                public void run() {
                    AppSetupActivity.this.showFailureForCurrentScreen();
                }
            };
            Context applicationContext = NavdyApplication.getAppContext();
            switch (currentScreen.screenType) {
                case BLUETOOTH:
                    if (isDeviceConnected(applicationContext)) {
                        moveToNextScreen();
                        return;
                    }
                    requestLocationPermission(new Runnable() {
                        public void run() {
                            AppSetupActivity.this.hasNeverShownBtSuccess = true;
                            openBtConnectionDialog();
                        }
                    }, handlePermissionDenial, AppSetupActivity.this);
                    return;
                case NOTIFICATIONS:
                    if (BaseActivity.weHaveNotificationPermission()) {
                        moveToNextScreen();
                        return;
                    } else {
                        startActivity(new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS"));
                        return;
                    }
                case ACCESS_FINE_LOCATION:
                    if (currentScreen.isMandatory || !this.showingFail) {
                        requestLocationPermission(new Runnable() {
                            public void run() {
                                AppSetupActivity.this.moveToNextScreen();
                                AppInstance appInstance = AppInstance.getInstance();
                                if (appInstance.mLocationTransmitter != null) {
                                    appInstance.mLocationTransmitter.start();
                                }
                            }
                        }, handlePermissionDenial);
                        return;
                    } else {
                        moveToNextScreen();
                        return;
                    }
                case USE_MICROPHONE:
                    if (currentScreen.isMandatory || !this.showingFail) {
                        requestMicrophonePermission(new Runnable() {
                            public void run() {
                                AppSetupActivity.this.moveToNextScreen();
                            }
                        }, handlePermissionDenial);
                        return;
                    } else {
                        moveToNextScreen();
                        return;
                    }
                case READ_CONTACTS:
                    if (currentScreen.isMandatory || !this.showingFail) {
                        requestContactsPermission(doThingsThatRequirePermission, handlePermissionDenial);
                        return;
                    } else {
                        moveToNextScreen();
                        return;
                    }
                case RECEIVE_SMS:
                    if (currentScreen.isMandatory || !this.showingFail) {
                        requestSmsPermission(doThingsThatRequirePermission, handlePermissionDenial);
                        return;
                    } else {
                        moveToNextScreen();
                        return;
                    }
                case CALL_PHONE:
                    if (currentScreen.isMandatory || !this.showingFail) {
                        requestPhonePermission(doThingsThatRequirePermission, handlePermissionDenial);
                        return;
                    } else {
                        moveToNextScreen();
                        return;
                    }
                case READ_CALENDAR:
                    if (currentScreen.isMandatory || !this.showingFail) {
                        requestCalendarPermission(doThingsThatRequirePermission, handlePermissionDenial);
                        return;
                    } else {
                        moveToNextScreen();
                        return;
                    }
                case WRITE_EXTERNAL_STORAGE:
                    if (currentScreen.isMandatory || !this.showingFail) {
                        requestStoragePermission(new Runnable() {
                            public void run() {
                                AppSetupActivity.this.moveToNextScreen();
                                RemoteDevice remoteDevice = AppInstance.getInstance().getRemoteDevice();
                                if (remoteDevice != null) {
                                    remoteDevice.postEvent(MusicServiceHandler.getMusicCapabilities());
                                }
                                MusicServiceHandler musicServiceHandler = MusicServiceHandler.getInstance();
                                if (musicServiceHandler != null && musicServiceHandler.shouldPerformFullPlaylistIndex()) {
                                    musicServiceHandler.indexPlaylists();
                                }
                            }
                        }, handlePermissionDenial);
                        return;
                    } else {
                        moveToNextScreen();
                        return;
                    }
                case END:
                    SettingsUtils.getSharedPreferences().edit().putBoolean(SettingsConstants.FINISHED_APP_SETUP, true).apply();
                    moveToNextScreen();
                    return;
                default:
                    moveToNextScreen();
                    return;
            }
        }
    }

    public void onProfileInfoSet() {
        onButtonClick(null);
    }

    private void showProfileStuff() {
        changeProfileStuffVisibility(0);
    }

    private void hideProfileStuff() {
        changeProfileStuffVisibility(8);
    }

    private void changeProfileStuffVisibility(int visibility) {
        if (this.photo != null) {
            this.photo.setVisibility(visibility);
        }
        if (this.photoHint != null) {
            this.photoHint.setVisibility(visibility);
        }
        if (this.hud != null && visibility == 0) {
            this.hud.setImageResource(R.drawable.image_navdy_profile_empty);
        }
    }

    protected boolean validateAndSaveName(boolean success) {
        success = super.validateAndSaveName(success);
        if (this.setupPagerAdapter != null) {
            this.setupPagerAdapter.recalculateScreenCount();
        }
        return success;
    }

    protected boolean validateAndSaveEmail(boolean success) {
        success = super.validateAndSaveEmail(success);
        if (this.setupPagerAdapter != null) {
            this.setupPagerAdapter.recalculateScreenCount();
        }
        return success;
    }

    public void updateIllustration(int position) {
        this.setupPagerAdapter.updateIllustration(new WeakReference(this.hud), position, this.showingFail);
    }

    private void showFailureForCurrentScreen() {
        this.logger.d("showFailureForCurrentScreen");
        if (this.setupPagerAdapter != null && this.bottomCard != null) {
            int currentPosition = this.bottomCard.getCurrentItem();
            AppSetupBottomCardGenericFragment fragment = (AppSetupBottomCardGenericFragment) this.setupPagerAdapter.getItem(currentPosition);
            this.showingFail = true;
            fragment.showFail();
            updateIllustration(currentPosition);
        }
    }

    @Nullable
    public AppSetupScreen getCurrentScreen() {
        return this.setupPagerAdapter != null ? AppSetupPagerAdapter.getScreen(this.bottomCard != null ? this.bottomCard.getCurrentItem() : 0) : null;
    }

    private void moveToNextScreen() {
        this.setupPagerAdapter.recalculateScreenCount();
        int nextIndex = this.bottomCard.getCurrentItem() + 1;
        AppSetupScreen currentScreen = getCurrentScreen();
        if (currentScreen != null && currentScreen.screenType == ScreenType.END) {
            goToHomeScreen();
        } else if (nextIndex < this.setupPagerAdapter.getCount()) {
            this.bottomCard.setCurrentItem(nextIndex);
        }
    }

    private void goToHomeScreen() {
        Tracker.tagEvent(Install.FIRST_LAUNCH_COMPLETED);
        SharedPreferences customerPrefs = SettingsUtils.getCustomerPreferences();
        String carYear = customerPrefs.getString(ProfilePreferences.CAR_YEAR, "");
        String carMake = customerPrefs.getString(ProfilePreferences.CAR_MAKE, "");
        String carModel = customerPrefs.getString(ProfilePreferences.CAR_MODEL, "");
        SharedPreferences sharedPreferences = SettingsUtils.getSharedPreferences();
        String box = sharedPreferences.getString(SettingsConstants.BOX, "Old_Box");
        int nbConfig = sharedPreferences.getInt(SettingsConstants.NB_CONFIG, 0);
        String mount = InstallPagerAdapter.getCurrentMountType(sharedPreferences).getValue();
        HashMap<String, String> attributes = new HashMap(5);
        attributes.put(Attributes.CAR_YEAR, carYear);
        attributes.put(Attributes.CAR_MAKE, carMake);
        attributes.put(Attributes.CAR_MODEL, carModel);
        attributes.put(InstallAttributes.SELECTED_MOUNT, mount);
        attributes.put(InstallAttributes.SELECTED_BOX, box);
        attributes.put(InstallAttributes.NB_CONFIGURATIONS, String.valueOf(nbConfig));
        Tracker.tagEvent(Install.CONFIGURATION_AT_COMPLETION, attributes);
        ContactServiceHandler.getInstance().forceSendFavoriteContactsToHud();
        Intent i = new Intent(getApplicationContext(), HomescreenActivity.class);
        i.setFlags(268468224);
        startActivity(i);
        finish();
    }

    public void onBackPressed() {
        int nextIndex = this.bottomCard.getCurrentItem() - 1;
        if (nextIndex < 0) {
            super.onBackPressed();
        } else {
            this.bottomCard.setCurrentItem(nextIndex);
        }
    }

    public static boolean userHasFinishedAppSetup() {
        return SettingsUtils.getSharedPreferences().getBoolean(SettingsConstants.FINISHED_APP_SETUP, false);
    }

    public void onHiddenButtonClick(View view) {
        AppSetupScreen currentScreen = getCurrentScreen();
        if (currentScreen != null && currentScreen.screenType == ScreenType.BLUETOOTH) {
            this.hiddenButtonClickCount++;
            if (this.hiddenButtonClickCount >= 10) {
                this.hiddenButtonClickCount = 0;
                this.setupPagerAdapter.pretendBtConnected();
                moveToNextScreen();
            }
        }
    }
}
