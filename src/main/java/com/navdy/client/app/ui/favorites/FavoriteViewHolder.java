package com.navdy.client.app.ui.favorites;

import androidx.recyclerview.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.alelec.navdyclient.R;

public class FavoriteViewHolder extends ViewHolder {
    protected ImageView edit;
    protected TextView firstLine;
    protected ImageView icon;
    protected View row;
    protected TextView secondLine;

    public FavoriteViewHolder(View itemView) {
        super(itemView);
        this.row = itemView.findViewById(R.id.card_row);
        this.icon = (ImageView) itemView.findViewById(R.id.icon);
        this.firstLine = (TextView) itemView.findViewById(R.id.card_first_line);
        this.secondLine = (TextView) itemView.findViewById(R.id.card_second_line);
        this.edit = (ImageView) itemView.findViewById(R.id.edit);
    }
}
