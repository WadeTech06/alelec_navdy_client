package com.navdy.client.app.ui.firstlaunch;

import android.content.SharedPreferences;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import android.view.ViewGroup;
import com.alelec.navdyclient.R;
import com.navdy.client.app.framework.models.MountInfo.MountType;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.tracking.Tracker;
import com.navdy.client.app.ui.settings.SettingsConstants;
import com.navdy.client.app.ui.settings.SettingsUtils;
import com.navdy.service.library.log.Logger;

public class InstallPagerAdapter extends FragmentStatePagerAdapter {
    @LayoutRes
    private static final int[] MEDIUM_TALL_MOUNT_FLOW = new int[]{R.layout.fle_install_overview, R.layout.fle_install_medium_or_tall_mount, R.layout.fle_install_lens_check, R.layout.fle_install_locate_obd, R.layout.fle_install_tidying_up, R.layout.fle_install_turn_on, R.layout.fle_install_dial};
    @LayoutRes
    private static final int[] SHORT_MOUNT_FLOW = new int[]{R.layout.fle_install_overview, R.layout.fle_install_short_mount, R.layout.fle_install_lens_check, R.layout.fle_install_secure_mount, R.layout.fle_install_locate_obd, R.layout.fle_install_tidying_up, R.layout.fle_install_turn_on, R.layout.fle_install_dial};
    static final int STEP_LENS_CHECK = 2;
    private static final int STEP_MEDIUM_TALL_MOUNT_LOCATE_OBD = 3;
    private static final int STEP_SHORT_MOUNT_LOCATE_OBD = 4;
    private int[] currentFlow;
    private boolean hasPickedMount;
    private final Logger logger;
    private final SharedPreferences sharedPreferences;
    private boolean weHaveCarInfo;

    InstallPagerAdapter(FragmentManager manager) {
        super(manager);
        this.logger = new Logger(InstallPagerAdapter.class);
        this.currentFlow = SHORT_MOUNT_FLOW;
        this.hasPickedMount = false;
        this.weHaveCarInfo = Tracker.weHaveCarInfo();
        this.sharedPreferences = SettingsUtils.getSharedPreferences();
        this.currentFlow = getInstallFlow(getCurrentMountType());
        notifyDataSetChanged();
    }

    int getLocateObdStepIndex() {
        if (getCurrentMountType() == MountType.SHORT) {
            return 4;
        }
        return 3;
    }

    public int getCount() {
        boolean hasSkippedInstall = false;
        if (this.sharedPreferences != null) {
            hasSkippedInstall = this.sharedPreferences.getBoolean(SettingsConstants.HAS_SKIPPED_INSTALL, false);
        }
        if (!this.hasPickedMount && !hasSkippedInstall) {
            return 3;
        }
        if (this.weHaveCarInfo || hasSkippedInstall) {
            return this.currentFlow.length;
        }
        return getLocateObdStepIndex() + 1;
    }

    static int getStepForCurrentPosition(int currentPosition, MountType currentMountType) {
        if (currentMountType == MountType.SHORT) {
            return SHORT_MOUNT_FLOW[currentPosition];
        }
        return MEDIUM_TALL_MOUNT_FLOW[currentPosition];
    }

    static int getPositionForStep(int step, int[] currentFlow) {
        if (currentFlow != null) {
            for (int i = 0; i < currentFlow.length; i++) {
                if (step == currentFlow[i]) {
                    return i;
                }
            }
        }
        return 0;
    }

    @NonNull
    public InstallCardFragment getItem(int position) {
        int layout = this.currentFlow[position];
        InstallCardFragment f;
        switch (layout) {
            case R.layout.fle_install_lens_check /*2130903152*/:
                f = new InstallLensCheckFragment();
                f.setMountType(getCurrentMountType());
                return f;
            case R.layout.fle_install_locate_obd /*2130903153*/:
                return new InstallLocateObdFragment();
            case R.layout.fle_install_medium_or_tall_mount /*2130903154*/:
            case R.layout.fle_install_mounts /*2130903155*/:
            case R.layout.fle_install_short_mount /*2130903158*/:
                f = new InstallMountFragment();
                f.setMountType(getCurrentMountType());
                return f;
            default:
                f = new InstallCardFragment();
                f.setLayoutId(layout);
                f.setMountType(getCurrentMountType());
                return f;
        }
    }

    public void destroyItem(ViewGroup container, int position, Object object) {
        super.destroyItem(container, position, object);
        this.logger.v("Destroying item at position " + position);
    }

    void setHasPickedMount(boolean hasPickedMount) {
        this.hasPickedMount = hasPickedMount;
        notifyDataSetChanged();
    }

    MountType getCurrentMountType() {
        return getCurrentMountType(this.sharedPreferences);
    }

    static MountType getCurrentMountType(SharedPreferences sharedPreferences) {
        if (sharedPreferences != null) {
            try {
                return MountType.getMountTypeForValue(sharedPreferences.getString(SettingsConstants.MOUNT_TYPE, MountType.SHORT.getValue()));
            } catch (ClassCastException e) {
            }
        }
        return MountType.SHORT;
    }

    void setCurrentMountType(MountType currentMountType) {
        if (this.sharedPreferences != null) {
            this.sharedPreferences.edit().putString(SettingsConstants.MOUNT_TYPE, currentMountType.getValue()).apply();
        }
        this.currentFlow = getInstallFlow(currentMountType);
        setHasPickedMount(true);
    }

    static int[] getInstallFlow(MountType mountType) {
        if (mountType == MountType.SHORT) {
            return SHORT_MOUNT_FLOW;
        }
        return MEDIUM_TALL_MOUNT_FLOW;
    }

    public void notifyDataSetChanged() {
        this.weHaveCarInfo = Tracker.weHaveCarInfo();
        super.notifyDataSetChanged();
    }

    String getScreenAtPosition(int position) {
        return getItem(position).getScreen();
    }

    boolean isOnStepBeforeCablePicker(int currentItem) {
        return currentItem == getLocateObdStepIndex() + -1;
    }

    boolean isOnStepAfterCablePicker(int currentItem) {
        if (StringUtils.equalsOrBothEmptyAfterTrim(SettingsUtils.getSharedPreferences().getString(SettingsConstants.POWER_CABLE_SELECTION, "OBD"), SettingsConstants.POWER_CABLE_SELECTION_CLA)) {
            if (currentItem == getLocateObdStepIndex() + 1) {
                return true;
            }
            return false;
        } else if (currentItem != getLocateObdStepIndex()) {
            return false;
        } else {
            return true;
        }
    }

    boolean isOnStepAfterLocatingObd(int currentItem) {
        if (!StringUtils.equalsOrBothEmptyAfterTrim(SettingsUtils.getSharedPreferences().getString(SettingsConstants.POWER_CABLE_SELECTION, "OBD"), SettingsConstants.POWER_CABLE_SELECTION_CLA) && currentItem == getLocateObdStepIndex() + 1) {
            return true;
        }
        return false;
    }
}
