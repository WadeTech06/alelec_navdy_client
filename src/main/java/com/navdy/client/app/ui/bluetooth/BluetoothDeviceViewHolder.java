package com.navdy.client.app.ui.bluetooth;

import androidx.recyclerview.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.TextView;
import com.alelec.navdyclient.R;

class BluetoothDeviceViewHolder extends ViewHolder {
    TextView deviceMainTitle;
    protected View row;

    BluetoothDeviceViewHolder(View itemView) {
        super(itemView);
        this.row = itemView.findViewById(R.id.bluetooth_device_list_row);
        this.deviceMainTitle = (TextView) itemView.findViewById(R.id.device_main_title);
    }
}
