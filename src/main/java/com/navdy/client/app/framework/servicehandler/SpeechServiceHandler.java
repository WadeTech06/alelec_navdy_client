package com.navdy.client.app.framework.servicehandler;

import android.content.SharedPreferences;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.DeviceConnection;
import com.navdy.client.app.framework.util.BusProvider;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.framework.util.TTSAudioRouter;
import com.navdy.client.app.ui.settings.SettingsConstants;
import com.navdy.service.library.events.audio.CancelSpeechRequest;
import com.navdy.service.library.events.audio.SpeechRequest;
import com.navdy.service.library.events.audio.SpeechRequest.Category;
import com.navdy.service.library.events.audio.SpeechRequestStatus;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Subscribe;
import com.squareup.wire.Message;
import javax.inject.Inject;

public class SpeechServiceHandler {
    public static final Logger sLogger = new Logger(SpeechServiceHandler.class);
    @Inject
    TTSAudioRouter audioRouter;
    @Inject
    SharedPreferences sharedPreferences;

    public SpeechServiceHandler() {
//        Injector.inject(NavdyApplication.getAppContext(), this);
        NavdyApplication.deps.inject(this);
        this.audioRouter.setSpeechServiceHadler(this);
        BusProvider.getInstance().register(this);
    }

    @Subscribe
    public void onSpeechRequest(SpeechRequest request) {
        String words = request.words;
        if (!StringUtils.isEmptyAfterTrim(words) && shouldRequestBeProcessed(request)) {
            sLogger.v("[tts-play] category[" + request.category + "] id [" + request.id + "] text[" + words + "]");
            this.audioRouter.processTTSRequest(request.words, request.id, request.sendStatus != null && request.sendStatus);
        }
    }

    @Subscribe
    public void onCancelSpeechRequest(CancelSpeechRequest request) {
        String id = request.id;
        sLogger.v("[tts-cancel] id[" + id + "]");
        if (!StringUtils.isEmptyAfterTrim(id)) {
            this.audioRouter.cancelTTSRequest(id);
        }
    }

    private boolean shouldRequestBeProcessed(SpeechRequest request) {
        return request.category != Category.SPEECH_WELCOME_MESSAGE || this.sharedPreferences.getBoolean(SettingsConstants.AUDIO_PLAY_WELCOME_MESSAGE, false);
    }

    public void sendSpeechNotification(SpeechRequestStatus event) {
        try {
            sLogger.v("send speech notification id[" + event.id + "] type[" + event.status + "]");
            DeviceConnection.postEvent(event);
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }
}
