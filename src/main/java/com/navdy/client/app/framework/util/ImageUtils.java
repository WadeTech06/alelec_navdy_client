package com.navdy.client.app.framework.util;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.os.Handler;
import android.os.Looper;
import androidx.annotation.CheckResult;
import androidx.annotation.Nullable;
import androidx.annotation.UiThread;
import android.widget.ImageView;
import com.navdy.client.app.NavdyApplication;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.util.IOUtils;
import com.navdy.service.library.util.ScalingUtilities;
import com.navdy.service.library.util.ScalingUtilities.ScalingLogic;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

public class ImageUtils {
    private static final boolean VERBOSE = false;
    private static final Handler handler = new Handler(Looper.getMainLooper());
    private static final Logger logger = new Logger(ImageUtils.class);

    public interface StreamFactory {
        InputStream getInputStream() throws FileNotFoundException;
    }

    @Nullable
    @CheckResult
    public static Bitmap getScaledBitmap(StreamFactory streamFactory, int width, int height) {
        Bitmap bitmap = null;
        if (streamFactory != null) {
            InputStream inputStream = null;
            try {
                inputStream = streamFactory.getInputStream();
                if (inputStream == null) {
                    logger.w("Failed to get input stream to scale");
                    if (inputStream != null) {
                        IOUtils.closeStream(inputStream);
                    }
                } else {
                    Options options = new Options();
                    options.inJustDecodeBounds = true;
                    BitmapFactory.decodeStream(inputStream, null, options);
                    IOUtils.closeStream(inputStream);
                    options.inSampleSize = calculateInSampleSize(options, width, height);
                    options.inJustDecodeBounds = false;
                    inputStream = streamFactory.getInputStream();
                    Bitmap bitmap2 = BitmapFactory.decodeStream(inputStream, null, options);
                    if (bitmap2 != null) {
                        bitmap = ScalingUtilities.createScaledBitmapAndRecycleOriginalIfScaled(bitmap2, width, height, ScalingLogic.FIT);
                        if (inputStream != null) {
                            IOUtils.closeStream(inputStream);
                        }
                    } else {
                        logger.e("Fetching compressed bitmap failed");
                        if (inputStream != null) {
                            IOUtils.closeStream(inputStream);
                        }
                    }
                }
            } catch (Throwable th) {
                if (inputStream != null) {
                    IOUtils.closeStream(inputStream);
                }
            }
        }
        return bitmap;
    }

    @Nullable
    @CheckResult
    public static Bitmap getScaledBitmap(final File file, int width, int height) {
        return getScaledBitmap(new StreamFactory() {
            public InputStream getInputStream() throws FileNotFoundException {
                return new FileInputStream(file);
            }
        }, width, height);
    }

    @CheckResult
    private static int calculateInSampleSize(Options options, int reqWidth, int reqHeight) {
        int height = options.outHeight;
        int width = options.outWidth;
        int inSampleSize = 1;
        if (height > reqHeight || width > reqWidth) {
            int halfHeight = height / 2;
            int halfWidth = width / 2;
            while (halfHeight / inSampleSize > reqHeight && halfWidth / inSampleSize > reqWidth) {
                inSampleSize *= 2;
            }
        }
        return inSampleSize;
    }

    @CheckResult
    public static Bitmap resizeBitmap(Bitmap bitmap, int maxSize) {
        if (bitmap == null || maxSize < 0) {
            return null;
        }
        int width;
        int height;
        float ratio = ((float) bitmap.getWidth()) / ((float) bitmap.getHeight());
        if (ratio > 1.0f) {
            width = maxSize;
            height = (int) (((float) width) / ratio);
        } else {
            height = maxSize;
            width = (int) (((float) height) / ratio);
        }
        Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap, width, height, true);
        bitmap.recycle();
        return scaledBitmap;
    }

    @UiThread
    @CheckResult
    private static Bitmap getBitmap(int resId) {
        Bitmap bitmap = BitmapFactory.decodeResource(NavdyApplication.getAppContext().getResources(), resId);
        if (bitmap != null) {
        }
        return bitmap;
    }

    @UiThread
    @CheckResult
    public static Bitmap getBitmap(int resId, int width, int height) {
        try {
            Options options = new Options();
            options.inJustDecodeBounds = true;
            Resources res = NavdyApplication.getAppContext().getResources();
            BitmapFactory.decodeResource(res, resId, options);
            options.inSampleSize = calculateInSampleSize(options, width, height);
            options.inJustDecodeBounds = false;
            Bitmap bitmap = BitmapFactory.decodeResource(res, resId, options);
            if (bitmap != null) {
            }
            return bitmap;
        } catch (Throwable t) {
            logger.e("Fetching compressed Bitmap failed: " + t);
            return null;
        }
    }

    @UiThread
    public static void loadImage(ImageView imageView, int resId, ImageCache imageCache) {
        Bitmap bitmap;
        if (imageCache != null) {
            bitmap = imageCache.getBitmap(String.valueOf(resId));
            if (bitmap != null) {
                imageView.setImageBitmap(bitmap);
                return;
            }
        }
        imageView.setTag(Integer.valueOf(resId));
        bitmap = getBitmap(resId);
        if (bitmap != null) {
            Object tag = imageView.getTag();
            if (tag != null && (tag instanceof Integer) && ((Integer) tag).intValue() == resId) {
                imageView.setTag(null);
                imageView.setImageBitmap(bitmap);
                if (imageCache != null) {
                    imageCache.putBitmap(String.valueOf(resId), bitmap);
                    return;
                }
                return;
            }
            return;
        }
        logger.e("bitmap not loaded:" + resId);
    }
}
