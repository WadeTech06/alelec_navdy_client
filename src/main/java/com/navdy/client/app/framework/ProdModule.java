package com.navdy.client.app.framework;

import android.content.Context;
import android.content.SharedPreferences;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.servicehandler.NetworkStatusManager;
import com.navdy.client.app.framework.servicehandler.SpeechServiceHandler;
import com.navdy.client.app.framework.servicehandler.VoiceServiceHandler;
import com.navdy.client.app.framework.util.CarMdClient;
import com.navdy.client.app.framework.util.TTSAudioRouter;
import com.navdy.client.app.service.ActivityRecognizedCallbackService;
import com.navdy.client.app.ui.favorites.FavoritesEditActivity;
import com.navdy.client.app.ui.homescreen.SuggestionsFragment;
import com.navdy.client.app.ui.routing.RoutingActivity;
import com.navdy.client.app.ui.search.SearchActivity;
import com.navdy.client.app.ui.settings.AudioSettingsActivity;
import com.navdy.client.app.ui.settings.HFPAudioDelaySettingsActivity;
import com.navdy.client.app.ui.settings.SettingsConstants;
import com.navdy.client.debug.SubmitTicketFragment;
import com.navdy.service.library.network.http.HttpManager;
import com.navdy.service.library.network.http.IHttpManager;
import dagger.Module;
import dagger.Provides;
import javax.inject.Singleton;

@Module
public class ProdModule {
    private TTSAudioRouter audioRouter;
    private Context context;
//    private IHttpManager httpManager;
    private NetworkStatusManager networkStatusManager;
    private SharedPreferences settingsSharedPreference;

    public ProdModule(Context context) {
        this.context = context;
    }

    @Provides
    IHttpManager provideHttpManager(HttpManager httpManager) {
        return httpManager;
    }
//    @Singleton
//    @Provides
//    public IHttpManager provideHttpManager() {
//        if (this.httpManager == null) {
//            this.httpManager = new HttpManager();
//        }
//        return this.httpManager;
//    }

//    @Provides
//    TTSAudioRouter provideTTSAudioRouter(TTSAudioRouter instance) {
//        return instance;
//    }
    @Singleton
    @Provides
    public TTSAudioRouter provideTTSAudioRouter() {
        if (this.audioRouter == null) {
            this.audioRouter = new TTSAudioRouter();
        }
        return this.audioRouter;
    }

    @Singleton
    @Provides
    public SharedPreferences provideSharedPreferences() {
        if (this.settingsSharedPreference == null) {
            this.settingsSharedPreference = NavdyApplication.getAppContext().getSharedPreferences(SettingsConstants.SETTINGS, 0);
        }
        return this.settingsSharedPreference;
    }

    @Singleton
    @Provides
    public NetworkStatusManager provideNetworkStatusManager() {
        if (this.networkStatusManager == null) {
            this.networkStatusManager = new NetworkStatusManager();
            this.networkStatusManager.register();
        }
        return this.networkStatusManager;
    }
}
