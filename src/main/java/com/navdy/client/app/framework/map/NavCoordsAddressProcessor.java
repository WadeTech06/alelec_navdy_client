package com.navdy.client.app.framework.map;

import android.content.Context;
import android.location.Location;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.util.Pair;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.model.LatLng;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.search.Address;
import com.here.android.mpa.search.ErrorCode;
import com.alelec.navdyclient.R;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.AppInstance;
import com.navdy.client.app.framework.glances.GlanceConstants;
import com.navdy.client.app.framework.i18n.AddressUtils;
import com.navdy.client.app.framework.location.NavdyLocationManager;
import com.navdy.client.app.framework.map.HereGeocoder.OnGeocodeCompleteCallback;
import com.navdy.client.app.framework.models.Destination;
import com.navdy.client.app.framework.models.Destination.Precision;
import com.navdy.client.app.framework.models.Destination.SearchType;
import com.navdy.client.app.framework.models.GoogleTextSearchDestinationResult;
import com.navdy.client.app.framework.search.GooglePlacesSearch;
import com.navdy.client.app.framework.search.GooglePlacesSearch.GoogleSearchListener;
import com.navdy.client.app.framework.search.GooglePlacesSearch.Query;
import com.navdy.client.app.framework.util.CredentialsUtils;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.providers.NavdyContentProvider;
import com.navdy.client.app.providers.NavdyContentProviderConstants;
import com.navdy.service.library.events.location.Coordinate;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.util.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;
import javax.net.ssl.HttpsURLConnection;
import org.droidparts.contract.SQL.DDL;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class NavCoordsAddressProcessor {
    private static final float MAX_DISTANCE_BETWEEN_COORDINATES_WITHOUT_STREET_NUMBER = 2000.0f;
    private static final float MAX_DISTANCE_BETWEEN_COORDINATES_WITH_STREET_NUMBER = 1000.0f;
    private static final boolean VERBOSE = false;
    private static final HandlerThread bgHandlerThread;
    private static final Handler bgThreadHandler;
    private static final Logger logger;
    private static final Handler uiThreadHandler;

    static {
        logger = new Logger(NavCoordsAddressProcessor.class);
        uiThreadHandler = new Handler(Looper.getMainLooper());
        (bgHandlerThread = new HandlerThread("BgHandlerThread")).start();
        bgThreadHandler = new Handler(NavCoordsAddressProcessor.bgHandlerThread.getLooper());
    }

    public interface OnCompleteCallback {
        void onFailure(Destination destination, Error error);

        void onSuccess(Destination destination);
    }

    public interface NavigationCoordinatesRetrievalCallback {
        void onFailure(String str);

        void onSuccess(@NonNull Coordinate coordinate, @NonNull Coordinate coordinate2, @Nullable Address address, Precision precision);
    }

    public enum Error {
        NONE,
        INVALID_REQUEST,
        BAD_COORDS,
        SERVICE_ERROR,
        NO_INTERNET,
        CACHE_NO_COORDS
    }

    public static void processDestination(@Nullable final Destination destination, @Nullable final OnCompleteCallback callback) {
        if (destination == null) {
            logger.w("processDestination, destination is null, calling back with error");
            callbackWithError(callback, null, Error.INVALID_REQUEST);
            return;
        }
        TaskManager.getInstance().execute(new RunProcessDestination(destination, callback), 13);
    }

    private static void tryHerePlacesSearchNativeApiWithNameAndAddress(final Destination processedDestination, final OnCompleteCallback callback) {
        String query = NavdyApplication.getAppContext().getString(R.string.name_plus_address, processedDestination.name, processedDestination.rawAddressNotForDisplay);
        logger.v("tryHerePlacesSearchNativeApiWithNameAndAddress, query is: " + query);
        HereGeocoder.makeRequest(query, new OnGeocodeCompleteCallback() {
            public void onComplete(@NonNull GeoCoordinate displayCoords, @Nullable GeoCoordinate navigationCoords, @Nullable Address structuredAddress, Precision precision) {
                final GeoCoordinate geoCoordinate = navigationCoords;
                final GeoCoordinate geoCoordinate2 = displayCoords;
                final Address address = structuredAddress;
                final Precision precision2 = precision;
                TaskManager.getInstance().execute(new Runnable() {
                    public void run() {
                        if (geoCoordinate == null || NavCoordsAddressProcessor.isInvalidOrExceedsThreshold(new LatLng(geoCoordinate.getLatitude(), geoCoordinate.getLongitude()), processedDestination)) {
                            if (geoCoordinate != null) {
                                NavCoordsAddressProcessor.logger.v("tryHerePlacesSearchNativeApiWithNameAndAddress returned bad navCoords: " + geoCoordinate.getLatitude() + "," + geoCoordinate.getLongitude() + "; trying just with address now");
                            } else {
                                NavCoordsAddressProcessor.logger.v("tryHerePlacesSearchNativeApiWithNameAndAddress returned null navCoords; trying just with address now");
                            }
                            NavCoordsAddressProcessor.tryHerePlacesSearchNativeApiWithAddress(processedDestination, callback);
                            return;
                        }
                        NavCoordsAddressProcessor.logger.v("tryHerePlacesSearchNativeApiWithNameAndAddress returned good navCoords: " + geoCoordinate.getLatitude() + "," + geoCoordinate.getLongitude() + "; saving and calling back success");
                        NavCoordsAddressProcessor.saveToCacheAndDb(processedDestination, MapUtils.buildNewCoordinate(geoCoordinate2.getLatitude(), geoCoordinate2.getLongitude()), MapUtils.buildNewCoordinate(geoCoordinate.getLatitude(), geoCoordinate.getLongitude()), address, precision2, callback);
                    }
                }, 13);
            }

            public void onError(@NonNull final ErrorCode error) {
                TaskManager.getInstance().execute(new Runnable() {
                    public void run() {
                        NavCoordsAddressProcessor.logger.v("tryHerePlacesSearchNativeApiWithNameAndAddress returned HERE error[" + error.name() + "];" + " trying just with address now");
                        NavCoordsAddressProcessor.tryHerePlacesSearchNativeApiWithAddress(processedDestination, callback);
                    }
                }, 13);
            }
        });
    }

    private static void tryHerePlacesSearchNativeApiWithAddress(final Destination processedDestination, final OnCompleteCallback callback) {
        HereGeocoder.makeRequest(processedDestination.rawAddressNotForDisplay, new OnGeocodeCompleteCallback() {
            public void onComplete(@NonNull GeoCoordinate displayCoords, @Nullable GeoCoordinate navigationCoords, @Nullable Address address, Precision precision) {
                final GeoCoordinate geoCoordinate = navigationCoords;
                final Address address2 = address;
                final GeoCoordinate geoCoordinate2 = displayCoords;
                final Precision precision2 = precision;
                TaskManager.getInstance().execute(new Runnable() {
                    public void run() {
                        if (geoCoordinate == null || NavCoordsAddressProcessor.isInvalidOrExceedsThreshold(new LatLng(geoCoordinate.getLatitude(), geoCoordinate.getLongitude()), processedDestination)) {
                            if (geoCoordinate != null) {
                                NavCoordsAddressProcessor.logger.v("tryHerePlacesSearchNativeApiWithAddress returned bad navCoords: " + geoCoordinate.getLatitude() + "," + geoCoordinate.getLongitude() + "; trying with Google now");
                            } else {
                                NavCoordsAddressProcessor.logger.v("tryHerePlacesSearchNativeApiWithAddress returned null navCoords; trying with Google now");
                            }
                            NavCoordsAddressProcessor.callGoogleDirectionsWebApi(processedDestination, callback);
                            return;
                        }
                        NavCoordsAddressProcessor.logger.v("tryHerePlacesSearchNativeApiWithAddress returned good navCoords: " + geoCoordinate.getLatitude() + "," + geoCoordinate.getLongitude() + "; saving and calling back success. New address is: " + address2);
                        NavCoordsAddressProcessor.saveToCacheAndDb(processedDestination, MapUtils.buildNewCoordinate(geoCoordinate2.getLatitude(), geoCoordinate2.getLongitude()), MapUtils.buildNewCoordinate(geoCoordinate.getLatitude(), geoCoordinate.getLongitude()), address2, precision2, callback);
                    }
                }, 13);
            }

            public void onError(@NonNull final ErrorCode error) {
                TaskManager.getInstance().execute(new Runnable() {
                    public void run() {
                        NavCoordsAddressProcessor.logger.v("tryHerePlacesSearchNativeApiWithAddress returned HERE error[" + error.name() + "];" + " trying with Google now");
                        NavCoordsAddressProcessor.callGoogleDirectionsWebApi(processedDestination, callback);
                    }
                }, 13);
            }
        });
    }

    private static void callGoogleDirectionsWebApi(final Destination processedDestination, final OnCompleteCallback callback) {
        getCoordinatesFromGoogleDirectionsWebApi(processedDestination, new NavigationCoordinatesRetrievalCallback() {
            public void onSuccess(@NonNull Coordinate displayCoords, @NonNull Coordinate navCoords, @Nullable Address address, Precision precisionLevel) {
                final Coordinate coordinate = navCoords;
                final Coordinate coordinate2 = displayCoords;
                final Address address2 = address;
                final Precision precision = precisionLevel;
                TaskManager.getInstance().execute(new Runnable() {
                    public void run() {
                        if (NavCoordsAddressProcessor.isInvalidOrExceedsThreshold(new LatLng(coordinate.latitude, coordinate.longitude), processedDestination)) {
                            NavCoordsAddressProcessor.logger.v("callGoogleDirectionsWebApi returned bad navCoords: " + coordinate.latitude + "," + coordinate.longitude + "; can't do anything else, saving and calling back success");
                            NavCoordsAddressProcessor.saveToCacheAndDb(processedDestination, coordinate2, null, address2, precision, callback);
                            return;
                        }
                        NavCoordsAddressProcessor.logger.v("callGoogleDirectionsWebApi returned good navCoords: " + coordinate.latitude + "," + coordinate.longitude + "; saving and calling back success");
                        NavCoordsAddressProcessor.saveToCacheAndDb(processedDestination, coordinate2, coordinate, address2, precision, callback);
                    }
                }, 13);
            }

            public void onFailure(final String error) {
                TaskManager.getInstance().execute(new Runnable() {
                    public void run() {
                        if (StringUtils.equalsOrBothEmptyAfterTrim(error, Error.SERVICE_ERROR.name())) {
                            NavCoordsAddressProcessor.logger.v("callGoogleDirectionsWebApi returned Google service error; calling back failure");
                            NavCoordsAddressProcessor.callbackWithError(callback, processedDestination, Error.BAD_COORDS);
                            return;
                        }
                        NavCoordsAddressProcessor.logger.v("callGoogleDirectionsWebApi returned Google error; can't do anything else, saving and calling back success");
                        NavCoordsAddressProcessor.saveToCacheAndDb(processedDestination, null, null, null, Precision.UNKNOWN, callback);
                    }
                }, 13);
            }
        });
    }

    public static void callWebHereGeocoderApi(String address, OnGeocodeCompleteCallback callback) {
        String HERE_GEOCODER_ENDPOINT1 = "https://geocoder.cit.api.here.com/6.2/geocode.json?app_id=";
        String HERE_GEOCODER_ENDPOINT2 = "&app_code=";
        String HERE_GEOCODER_ENDPOINT3 = "&gen=9&searchtext=";
        String HERE_RESPONSE_STR = "Response";
        String HERE_VIEW_STR = "View";
        String HERE_RESULT_STR = "Result";
        String HERE_LOCATION_STR = "Location";
        String HERE_DISPLAY_STR = "DisplayPosition";
        String HERE_NAVIGATION_STR = "NavigationPosition";
        String HERE_LATITUDE_STR = "Latitude";
        String HERE_LONGITUDE_STR = "Longitude";
        String HERE_ADDRESS_STR = "Address";
        String HERE_LABEL_STR = "Label";
        String HERE_COUNTRY_STR = "Country";
        String HERE_STATE_STR = "State";
        String HERE_COUNTY_STR = "County";
        String HERE_CITY_STR = "City";
        String HERE_POSTALCODE_STR = "PostalCode";
        String HERE_ADDNL_DATA_STR = "AdditionalData";
        String HERE_COUNTRY_NAME_STR = "CountryName";
        String HERE_COUNTY_NAME_STR = "CountyName";
        String HERE_STATE_NAME_STR = "StateName";
        Context context = NavdyApplication.getAppContext();
        String HERE_GEO_APP_ID = CredentialsUtils.getCredentials(context.getString(R.string.metadata_here_geo_app_id));
        String HERE_GEO_APP_TOKEN = CredentialsUtils.getCredentials(context.getString(R.string.metadata_here_geo_app_token));
        final String HERE_GEOCODER_BASE_URL = "https://geocoder.cit.api.here.com/6.2/geocode.json?app_id=" + HERE_GEO_APP_ID + "&app_code=" + HERE_GEO_APP_TOKEN + "&gen=9&searchtext=";
        String HERE_GEOCODER_BASE_URL_4_LOGS = "https://geocoder.cit.api.here.com/6.2/geocode.json?app_id=HERE_GEO_APP_ID&app_code=HERE_GEO_APP_TOKEN&gen=9&searchtext=";
        if (HERE_GEO_APP_ID == null || HERE_GEO_APP_TOKEN == null) {
            logger.e("no here token in manifest");
            callback.onError(ErrorCode.INVALID_CREDENTIALS);
            return;
        }
        logger.v("using here geocoder to get nav position");
        final String str = address;
        final OnGeocodeCompleteCallback onGeocodeCompleteCallback = callback;
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                InputStream inputStream = null;
                try {
                    String urlStr = HERE_GEOCODER_BASE_URL + URLEncoder.encode(str, "UTF-8");
                    NavCoordsAddressProcessor.logger.d("Calling HERE with URL: https://geocoder.cit.api.here.com/6.2/geocode.json?app_id=HERE_GEO_APP_ID&app_code=HERE_GEO_APP_TOKEN&gen=9&searchtext=" + URLEncoder.encode(str, "UTF-8"));
                    HttpsURLConnection urlConnection = (HttpsURLConnection) new URL(urlStr).openConnection();
                    int httpResponse = urlConnection.getResponseCode();
                    Double dispLat = 0.0d;
                    Double dispLong = 0.0d;
                    Double navLat = 0.0d;
                    Double navLong = 0.0d;
                    Address hereAddress = new Address();
                    if (httpResponse < 200 || httpResponse >= 300) {
                        NavCoordsAddressProcessor.logger.d("callWebHereGeocoderApi: response code:" + httpResponse);
                        onGeocodeCompleteCallback.onError(ErrorCode.BAD_REQUEST);
                    } else {
                        inputStream = urlConnection.getInputStream();
                        JSONObject location = ((JSONObject) new JSONObject(IOUtils.convertInputStreamToString(inputStream, "UTF-8")).getJSONObject("Response").getJSONArray("View").get(0)).getJSONArray("Result").getJSONObject(0).getJSONObject("Location");
                        JSONObject dispPos = location.getJSONObject("DisplayPosition");
                        String dispLatStr = dispPos.optString("Latitude");
                        String dispLonStr = dispPos.optString("Longitude");
                        dispLat = Double.valueOf(dispLatStr);
                        dispLong = Double.valueOf(dispLonStr);
                        JSONObject navPos = location.getJSONArray("NavigationPosition").getJSONObject(0);
                        String navLatStr = navPos.optString("Latitude");
                        String navLonStr = navPos.optString("Longitude");
                        navLat = Double.valueOf(navLatStr);
                        navLong = Double.valueOf(navLonStr);
                        JSONObject address = location.getJSONObject("Address");
                        hereAddress.setText(address.optString("Label"));
                        hereAddress.setCountryName(address.optString("Country"));
                        hereAddress.setState(address.optString("State"));
                        hereAddress.setCounty(address.optString("County"));
                        hereAddress.setCity(address.optString("City"));
                        hereAddress.setPostalCode(address.optString("PostalCode"));
                        JSONArray additionalData = address.optJSONArray("AdditionalData");
                        if (additionalData != null) {
                            for (int i = 0; i < additionalData.length(); i++) {
                                JSONObject data = additionalData.getJSONObject(i);
                                if (data.has("CountryName")) {
                                    hereAddress.setCountryName(data.optString("CountryName"));
                                }
                                if (data.has("CountyName")) {
                                    hereAddress.setCounty(data.optString("CountyName"));
                                }
                                if (data.has("StateName")) {
                                    hereAddress.setState(data.optString("StateName"));
                                }
                            }
                        }
                    }
                    if (navLat == 0.0d || navLong == 0.0d) {
                        onGeocodeCompleteCallback.onError(ErrorCode.BAD_LOCATION);
                    } else {
                        onGeocodeCompleteCallback.onComplete(new GeoCoordinate(dispLat.doubleValue(), dispLong.doubleValue()), new GeoCoordinate(navLat.doubleValue(), navLong.doubleValue()), hereAddress, Precision.UNKNOWN);
                    }
                    IOUtils.closeStream(inputStream);
                } catch (Throwable th) {
                    IOUtils.closeStream(inputStream);
                    try {
                        throw th;
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, 3);
    }

    public static void callWebHerePlacesSearchApi(String address, OnGeocodeCompleteCallback callback) {
        String HERE_SEARCH_ENDPOINT1 = "https://places.cit.api.here.com/places/v1/discover/search?app_id=";
        String HERE_SEARCH_ENDPOINT2 = "&app_code=";
        String HERE_SEARCH_ENDPOINT3 = "&at=";
        String HERE_SEARCH_ENDPOINT4 = "&q=";
        String HERE_RESULTS_STR = "results";
        String HERE_ITEMS_STR = "items";
        String HERE_LOCATION_STR = "location";
        String HERE_POSITION_STR = "position";
        String HERE_TITLE_STR = "title";
        String HERE_VICINITY_STR = "vicinity";
        String HERE_HREF_STR = "href";
        String HERE_ADDRESS_STR = NavdyContentProviderConstants.DESTINATIONS_ADDRESS;
        String HERE_TEXT_STR = GlanceConstants.CAR_EXT_CONVERSATION_MESSAGES_TEXT;
        String HERE_HOUSE_STR = "house";
        String HERE_STREET_STR = "street";
        String HERE_CITY_STR = NavdyContentProviderConstants.DESTINATIONS_CITY;
        String HERE_STATE_STR = "stateCode";
        String HERE_COUNTRY_STR = NavdyContentProviderConstants.DESTINATIONS_COUNTRY;
        String HERE_POSTALCODE_STR = "postalCode";
        String HERE_VIEW_STR = "view";
        Context context = NavdyApplication.getAppContext();
        String HERE_GEO_APP_ID = CredentialsUtils.getCredentials(context.getString(R.string.metadata_here_geo_app_id));
        String HERE_GEO_APP_TOKEN = CredentialsUtils.getCredentials(context.getString(R.string.metadata_here_geo_app_token));
        String HERE_SEARCH_BASE_URL = "https://places.cit.api.here.com/places/v1/discover/search?app_id=" + HERE_GEO_APP_ID + "&app_code=" + HERE_GEO_APP_TOKEN + "&at=";
        String HERE_SEARCH_BASE_URL_4_LOGS = "https://places.cit.api.here.com/places/v1/discover/search?app_id=HERE_GEO_APP_ID&app_code=HERE_GEO_APP_TOKEN&at=";
        if (HERE_GEO_APP_ID == null || HERE_GEO_APP_TOKEN == null) {
            logger.e("no here token in manifest");
            callback.onError(ErrorCode.INVALID_CREDENTIALS);
            return;
        }
        logger.v("using here geocoder to get nav position");
        final OnGeocodeCompleteCallback onGeocodeCompleteCallback = callback;
        final String str = address;
        final String str2 = HERE_SEARCH_BASE_URL;
        final Context context2 = context;
        TaskManager.getInstance().execute(new RunCallWebHerePlacesSearchApi(onGeocodeCompleteCallback, str, str2, context2), 3);
    }

    private static void saveToCacheAndDb(@Nullable Destination destination, @Nullable Coordinate displayCoords, @Nullable Coordinate navigationCoords, @Nullable Address address, @Nullable Precision precision, @Nullable OnCompleteCallback callback) {
        if (destination == null) {
            logger.w("saveToCacheAndDb, destination is null, no-op");
            callbackWithError(callback, null, Error.INVALID_REQUEST);
        } else if (AppInstance.getInstance().canReachInternet()) {
            double displayLat = 0.0d;
            double displayLng = 0.0d;
            if (MapUtils.isValidSetOfCoordinates(displayCoords)) {
                displayLat = displayCoords.latitude.doubleValue();
                displayLng = displayCoords.longitude.doubleValue();
            }
            double navigationLat = 0.0d;
            double navigationLng = 0.0d;
            if (MapUtils.isValidSetOfCoordinates(navigationCoords)) {
                navigationLat = navigationCoords.latitude.doubleValue();
                navigationLng = navigationCoords.longitude.doubleValue();
            }
            String originalAddress = destination.rawAddressNotForDisplay;
            destination.handleNewCoordsAndAddress(displayLat, displayLng, navigationLat, navigationLng, address, precision);
            NavdyContentProvider.addToCacheIfNotAlreadyIn(originalAddress, destination);
            callbackWithSuccess(callback, destination);
        } else {
            logger.v("saveToCacheAndDb, no internet so not saving to cache, no-op");
            callbackWithError(callback, destination, Error.SERVICE_ERROR);
        }
    }

    private static void callbackWithSuccess(final OnCompleteCallback callback, final Destination destination) {
        uiThreadHandler.post(new Runnable() {
            public void run() {
                if (callback != null) {
                    callback.onSuccess(destination);
                }
            }
        });
    }

    private static void callbackWithError(final OnCompleteCallback callback, final Destination destination, final Error error) {
        uiThreadHandler.post(new Runnable() {
            public void run() {
                if (callback != null) {
                    callback.onFailure(destination, error);
                }
            }
        });
    }

    public static void getCoordinatesFromGoogleDirectionsWebApi(final Destination destination, final NavigationCoordinatesRetrievalCallback callback) {
        if (!AppInstance.getInstance().canReachInternet()) {
            callback.onFailure(Error.NO_INTERNET.name());
        }
        new GooglePlacesSearch(new GoogleSearchListener() {
            public void onGoogleSearchResult(final List<GoogleTextSearchDestinationResult> destinations, Query queryType, final com.navdy.client.app.framework.search.GooglePlacesSearch.Error error) {
                TaskManager.getInstance().execute(new Runnable() {
                    public void run() {
                        boolean foundValidNavCoordinates = false;
                        if (destinations != null && destinations.size() > 0) {
                            for (int i = 0; i < destinations.size() && !foundValidNavCoordinates; i++) {
                                GoogleTextSearchDestinationResult result = (GoogleTextSearchDestinationResult) destinations.get(i);
                                if (result != null) {
                                    final String lat = result.lat;
                                    final String lng = result.lng;
                                    Precision precision = Precision.PRECISE;
                                    if (result.types != null) {
                                        for (String type : result.types) {
                                            if (StringUtils.equalsOrBothEmptyAfterTrim(type, "locality") || StringUtils.equalsOrBothEmptyAfterTrim(type, "postal_code") || StringUtils.equalsOrBothEmptyAfterTrim(type, NavdyContentProviderConstants.DESTINATIONS_COUNTRY) || StringUtils.equalsOrBothEmptyAfterTrim(type, "administrative_area_level_1") || StringUtils.equalsOrBothEmptyAfterTrim(type, "neighborhood") || StringUtils.equalsOrBothEmptyAfterTrim(type, "route")) {
                                                precision = Precision.IMPRECISE;
                                                break;
                                            }
                                        }
                                    }
                                    if (!(StringUtils.isEmptyAfterTrim(lat) || StringUtils.isEmptyAfterTrim(lng))) {
                                        foundValidNavCoordinates = true;
                                        final Precision finalPrecision = precision;
                                        NavCoordsAddressProcessor.uiThreadHandler.post(new Runnable() {
                                            public void run() {
                                                Coordinate latLngCoord = MapUtils.buildNewCoordinate(Double.parseDouble(lat), Double.parseDouble(lng));
                                                if (error != com.navdy.client.app.framework.search.GooglePlacesSearch.Error.NONE) {
                                                    callback.onFailure(error.name());
                                                } else {
                                                    callback.onSuccess(destination.getDisplayCoordinate(), latLngCoord, null, finalPrecision);
                                                }
                                            }
                                        });
                                    }
                                }
                            }
                        }
                        if (!foundValidNavCoordinates) {
                            NavCoordsAddressProcessor.logger.e("Google Directions API failed.");
                            NavCoordsAddressProcessor.logger.e("Could not find valid coordinates out of: " + destinations);
                            NavCoordsAddressProcessor.uiThreadHandler.post(new Runnable() {
                                public void run() {
                                    callback.onFailure(Error.BAD_COORDS.name());
                                }
                            });
                        }
                    }
                }, 13);
            }
        }).runDirectionsSearchWebApi(destination);
    }

    private static boolean isInvalidOrExceedsThreshold(LatLng latLng, Destination destination) {
        if (!MapUtils.isValidSetOfCoordinates(latLng)) {
            return true;
        }
        if (!destination.hasValidDisplayCoordinates()) {
            return false;
        }
        float[] results = new float[1];
        Location.distanceBetween(destination.displayLat, destination.displayLong, latLng.latitude, latLng.longitude, results);
        if (((double) results[0]) <= getDistanceThresholdFor(destination)) {
            return false;
        }
        logger.v("Max Distance exceeded between display and navigation coordinates. Nav coords are: " + latLng + " Display coords are: " + destination.displayLat + DDL.SEPARATOR + destination.displayLong);
        return true;
    }

    public static double getDistanceThresholdFor(Destination destination) {
        float coordinateThreshold = MAX_DISTANCE_BETWEEN_COORDINATES_WITHOUT_STREET_NUMBER;
        if (destination.hasStreetNumber()) {
            coordinateThreshold = MAX_DISTANCE_BETWEEN_COORDINATES_WITH_STREET_NUMBER;
        }
        return (double) coordinateThreshold;
    }

    private static class RunProcessDestination implements Runnable {
        private final Destination destination;
        private final OnCompleteCallback callback;

        public RunProcessDestination(Destination destination, OnCompleteCallback callback) {
            this.destination = destination;
            this.callback = callback;
        }

        public void run() {
            destination.reloadSelfFromDatabase();
            if (!destination.isPlaceDetailsInfoStale() || destination.placeId == null) {
                doCoordAndAddressLookup();
            } else {
                new GooglePlacesSearch(new GoogleSearchListener() {
                    public void onGoogleSearchResult(List<GoogleTextSearchDestinationResult> destinations, Query queryType, GooglePlacesSearch.Error error) {
                        if (!(destinations == null || destinations.isEmpty())) {
                            ((GoogleTextSearchDestinationResult) destinations.listIterator().next()).toModelDestinationObject(SearchType.DETAILS, destination);
                        }
                        RunProcessDestination.this.doCoordAndAddressLookup();
                    }
                }, NavCoordsAddressProcessor.bgThreadHandler).runDetailsSearchWebApi(destination.placeId);
            }
        }

        private void doCoordAndAddressLookup() {
            if (destination.hasAlreadyBeenProcessed() || (destination.hasValidNavCoordinates() && destination.hasDetailedAddress())) {
                NavCoordsAddressProcessor.logger.v("destination has good data but it's not stored in cache or DB, saving and calling back with success ");
                NavCoordsAddressProcessor.saveToCacheAndDb(destination, null, null, null, destination.precisionLevel, callback);
            } else if (StringUtils.isEmptyAfterTrim(destination.rawAddressNotForDisplay)) {
                LatLng latLng;
                if (destination.hasValidDisplayCoordinates()) {
                    NavCoordsAddressProcessor.logger.v("processDestination, destination does not have address, assigning display coordinates as address");
                    destination.setRawAddressNotForDisplay(destination.displayLat + "," + destination.displayLong);
                    latLng = new LatLng(destination.displayLat, destination.displayLong);
                } else if (destination.hasValidNavCoordinates()) {
                    NavCoordsAddressProcessor.logger.v("processDestination, destination does not have address, assigning nav coordinates as address");
                    destination.setRawAddressNotForDisplay(destination.navigationLat + "," + destination.navigationLong);
                    latLng = new LatLng(destination.navigationLat, destination.navigationLong);
                } else {
                    NavCoordsAddressProcessor.logger.w("processDestination, destination does not have address or coords, calling back with error");
                    NavCoordsAddressProcessor.callbackWithError(callback, destination, Error.INVALID_REQUEST);
                    return;
                }
                MapUtils.doReverseGeocodingFor(latLng, destination, new Runnable() {
                    public void run() {
                        if (callback != null) {
                            callback.onSuccess(destination);
                        }
                    }
                });
            } else {
                boolean shouldAddNameToAddress = (destination.isFavoriteDestination() || StringUtils.isEmptyAfterTrim(destination.name) || AddressUtils.isTitleIsInTheAddress(destination.name, destination.rawAddressNotForDisplay) || destination.isCalendarEvent() || destination.isContact()) ? false : true;
                if (shouldAddNameToAddress) {
                    NavCoordsAddressProcessor.logger.v("destination is not favorite, trying adding name with address");
                    NavCoordsAddressProcessor.tryHerePlacesSearchNativeApiWithNameAndAddress(destination, callback);
                    return;
                }
                NavCoordsAddressProcessor.logger.v("destination is favorite/contact, trying just with address");
                NavCoordsAddressProcessor.tryHerePlacesSearchNativeApiWithAddress(destination, callback);
            }
        }
    }

    private static class RunCallWebHerePlacesSearchApi implements Runnable {
        private final OnGeocodeCompleteCallback onGeocodeCompleteCallback;
        private final String str;
        private final String str2;
        private final Context context2;

        public RunCallWebHerePlacesSearchApi(OnGeocodeCompleteCallback onGeocodeCompleteCallback, String str, String str2, Context context2) {
            this.onGeocodeCompleteCallback = onGeocodeCompleteCallback;
            this.str = str;
            this.str2 = str2;
            this.context2 = context2;
        }

        public void run() {
            InputStream inputStream = null;
            try {
                Coordinate currentLocation = NavdyLocationManager.getInstance().getSmartStartCoordinates();
                if (currentLocation == null) {
                    onGeocodeCompleteCallback.onError(ErrorCode.BAD_LOCATION);
                    return;
                }
                String params = currentLocation.latitude + "," + currentLocation.longitude + "&q=" + URLEncoder.encode(str, "UTF-8");
                String urlStr = str2 + params;
                NavCoordsAddressProcessor.logger.d("Calling HERE with URL: https://places.cit.api.here.com/places/v1/discover/search?app_id=HERE_GEO_APP_ID&app_code=HERE_GEO_APP_TOKEN&at=" + params);
                HttpsURLConnection urlConnection = (HttpsURLConnection) new URL(urlStr).openConnection();
                int httpResponse = urlConnection.getResponseCode();
                Address hereAddress = new Address();
                if (httpResponse < 200 || httpResponse >= 300) {
                    NavCoordsAddressProcessor.logger.d("callWebHereGeocoderApi: response code:" + httpResponse);
                    onGeocodeCompleteCallback.onError(ErrorCode.BAD_REQUEST);
                } else {
                    inputStream = urlConnection.getInputStream();
                    JSONObject item = new JSONObject(IOUtils.convertInputStreamToString(inputStream, "UTF-8")).getJSONObject("results").getJSONArray("items").getJSONObject(0);
                    Pair<Double, Double> navCoords = parseLocationJson(item);
                    Double navLat = navCoords.first;
                    Double navLong = navCoords.second;
                    String title = item.optString("title");
                    hereAddress.setText(title + item.optString("vicinity"));
                    String metaDataUrl = item.optString("href");
                    if (StringUtils.isEmptyAfterTrim(metaDataUrl)) {
                        finalizeWebPlaceSearch(Double.valueOf(0.0d), Double.valueOf(0.0d), navLat, navLong, hereAddress, onGeocodeCompleteCallback);
                    } else {
                        fetchMetaData(metaDataUrl, navLat, navLong, hereAddress, onGeocodeCompleteCallback);
                    }
                }
                IOUtils.closeStream(inputStream);
            } catch (Throwable t) {
                NavCoordsAddressProcessor.logger.e("getNavigationCoordinate", t);
                onGeocodeCompleteCallback.onError(ErrorCode.SERVER_INTERNAL);
            } finally {
                IOUtils.closeStream(inputStream);
            }
        }

        private Pair<Double, Double> parseLocationJson(JSONObject item) throws JSONException {
            JSONArray position = item.getJSONArray("position");
            return new Pair(Double.valueOf(position.get(0).toString()), Double.valueOf(position.get(1).toString()));
        }

        private Address parseAddressJson(JSONObject location) throws JSONException {
            Address hereAddress = new Address();
            JSONObject address = location.getJSONObject(NavdyContentProviderConstants.DESTINATIONS_ADDRESS);
            hereAddress.setText(address.optString(GlanceConstants.CAR_EXT_CONVERSATION_MESSAGES_TEXT));
            hereAddress.setHouseNumber(address.optString("house"));
            hereAddress.setStreet(address.optString("street"));
            hereAddress.setCity(address.optString(NavdyContentProviderConstants.DESTINATIONS_CITY));
            hereAddress.setState(address.optString("stateCode"));
            hereAddress.setPostalCode(address.optString("postalCode"));
            hereAddress.setCounty(address.optString(NavdyContentProviderConstants.DESTINATIONS_COUNTRY));
            return hereAddress;
        }

        private void fetchMetaData(String metaDataUrl, Double navLat, Double navLong, Address hereAddress, OnGeocodeCompleteCallback callback) {
            if (StringUtils.isEmptyAfterTrim(metaDataUrl)) {
                finalizeWebPlaceSearch(Double.valueOf(0.0d), Double.valueOf(0.0d), navLat, navLong, hereAddress, callback);
                return;
            }
            final Double d = navLat;
            final Double d2 = navLong;
            final Address address = hereAddress;
            final OnGeocodeCompleteCallback onGeocodeCompleteCallback = callback;
            Listener anonymousClass1 = new Listener<JSONObject>() {
                public void onResponse(JSONObject response) {
                    RunCallWebHerePlacesSearchApi.this.parseMetaDataResponse(response, d, d2, address, onGeocodeCompleteCallback);
                }
            };
//            d2 = navLat;
            final Double d3 = navLong;
            final Address address2 = hereAddress;
            final OnGeocodeCompleteCallback onGeocodeCompleteCallback2 = callback;
            Volley.newRequestQueue(context2).add(new JsonObjectRequest(0, metaDataUrl, null, anonymousClass1, new ErrorListener() {
                public void onErrorResponse(VolleyError error) {
                    NavCoordsAddressProcessor.logger.e("Error occured while trying to retrieve more info about a HERE places search result. ", error);
                    RunCallWebHerePlacesSearchApi.this.finalizeWebPlaceSearch(Double.valueOf(0.0d), Double.valueOf(0.0d), d2, d3, address2, onGeocodeCompleteCallback2);
                }
            }));
        }

        private void parseMetaDataResponse(JSONObject response, Double navLat, Double navLong, Address hereAddress, OnGeocodeCompleteCallback callback) {
            try {
                JSONObject location = response.getJSONObject("location");
                Pair<Double, Double> navCoords = parseLocationJson(location);
                navLat = (Double) navCoords.first;
                navLong = (Double) navCoords.second;
                hereAddress = parseAddressJson(location);
                NavCoordsAddressProcessor.logger.d("Here returned the result with URL: " + response.optString("view"));
                finalizeWebPlaceSearch(Double.valueOf(0.0d), Double.valueOf(0.0d), navLat, navLong, hereAddress, callback);
            } catch (JSONException e) {
                NavCoordsAddressProcessor.logger.e("Json Error occured while trying to retrieve more info about a HERE places search result. ", e);
                finalizeWebPlaceSearch(Double.valueOf(0.0d), Double.valueOf(0.0d), navLat, navLong, hereAddress, callback);
            } catch (Throwable th) {
                Throwable th2 = th;
                finalizeWebPlaceSearch(Double.valueOf(0.0d), Double.valueOf(0.0d), navLat, navLong, hereAddress, callback);
            }
        }

        private void finalizeWebPlaceSearch(Double dispLat, Double dispLong, Double navLat, Double navLong, Address hereAddress, OnGeocodeCompleteCallback callback) {
            if (navLat.doubleValue() == 0.0d || navLong.doubleValue() == 0.0d) {
                callback.onError(ErrorCode.BAD_LOCATION);
            } else {
                callback.onComplete(new GeoCoordinate(dispLat.doubleValue(), dispLong.doubleValue()), new GeoCoordinate(navLat.doubleValue(), navLong.doubleValue()), hereAddress, Precision.UNKNOWN);
            }
        }
    }
}
