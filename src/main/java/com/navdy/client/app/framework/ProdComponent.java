package com.navdy.client.app.framework;

import android.app.Application;
import android.content.Context;

import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.servicehandler.NetworkStatusManager;
import com.navdy.client.app.framework.servicehandler.SpeechServiceHandler;
import com.navdy.client.app.framework.servicehandler.VoiceServiceHandler;
import com.navdy.client.app.framework.util.CarMdClient;
import com.navdy.client.app.framework.util.TTSAudioRouter;
import com.navdy.client.app.service.ActivityRecognizedCallbackService;
import com.navdy.client.app.ui.favorites.FavoritesEditActivity;
import com.navdy.client.app.ui.homescreen.SuggestionsFragment;
import com.navdy.client.app.ui.routing.RoutingActivity;
import com.navdy.client.app.ui.search.SearchActivity;
import com.navdy.client.app.ui.settings.AudioSettingsActivity;
import com.navdy.client.app.ui.settings.HFPAudioDelaySettingsActivity;
import com.navdy.client.debug.SubmitTicketFragment;

import dagger.BindsInstance;
import dagger.Component;
import dagger.Provides;
import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;
import dagger.android.support.AndroidSupportInjectionModule;

import javax.inject.Singleton;

@Singleton
@Component(modules = {AndroidSupportInjectionModule.class, ProdModule.class})
public interface ProdComponent extends AndroidInjector<DaggerApplication> {
    void inject(NavdyApplication application);
    void inject(AudioSettingsActivity audio_settings_activity);
    void inject(SpeechServiceHandler speech_service_handler);
    void inject(TTSAudioRouter tts_audio_router);
    void inject(CarMdClient car_md_client);
    void inject(NetworkStatusManager network_status_manager);
    void inject(AppInstance app_instance);
    void inject(SearchActivity search_activity);
    void inject(SuggestionsFragment suggestions_fragment);
    void inject(RoutingActivity routing_activity);
    void inject(FavoritesEditActivity favorites_edit_activity);
    void inject(SubmitTicketFragment submod_ticket_fragment);
    void inject(VoiceServiceHandler voice_service_handler);
    void inject(HFPAudioDelaySettingsActivity hpf_audio_delay_settings_activity);
    void inject(ActivityRecognizedCallbackService activity_recognized_callback_service);

}

//@Component.Builder
//interface Builder {
//    @BindsInstance
//    Builder application(Application application);
//    AppComponent build();
//}
