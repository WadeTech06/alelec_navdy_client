package com.navdy.client.app.framework.suggestion;

//import com.navdy.client.app.framework.suggestion.SuggestedDestination;

import android.content.Context;

import java.io.IOException;
import java.util.Calendar;

public class DestinationSuggestionService extends android.app.IntentService {
    public static final String PLACE_SUGGESTION_LIBRARY_NAME = "placesuggestion";
    private static final java.lang.String BUCKET_NAME = "navdy-trip-data";
    public static final java.lang.String COMMAND_AUTO_UPLOAD_TRIP_DATA = "COMMAND_AUTO_UPLOAD";
    public static final java.lang.String COMMAND_POPULATE = "COMMAND_POPULATE";
    public static final java.lang.String COMMAND_RESET = "COMMAND_RESET";
    public static final java.lang.String COMMAND_SUGGEST = "COMMAND_SUGGEST";
    public static final java.lang.String COMMAND_UPLOAD_TRIP_DATA = "COMMAND_UPLOAD";
    private static final java.lang.String EXTRA_LOCAL = "EXTRA_LOCAL";
    private static final long MINIMUM_INTERVAL_BETWEEN_UPLOADS = java.util.concurrent.TimeUnit.DAYS.toMillis(1);
    private static com.navdy.client.app.framework.models.Destination NO_SUGGESTION = null;
//    public static final java.lang.String PLACE_SUGGESTION_LIBRARY_NAME = "placesuggestion";
    private static final java.lang.String PREFERENCE_LAST_UPLOAD_ID = "PREF_LAST_UPLOAD_ID";
    private static final java.lang.String PREFERENCE_LAST_UPLOAD_TIME = "PREF_LAST_UPLOAD_TIME";
    private static final java.lang.String SERVICE_NAME = "SERVICE_NAME";
    public static final int SUGGESTED_DESTINATION_TRESHOLD = 200;
    public static final int TRIP_TRESHOLD_FOR_UPLOAD = 10;
    public static final int TRIP_UPLOAD_SERVICE_REQ = 256;
    private static boolean sColdStart = true;
    /* access modifiers changed from: private|static|final */
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(DestinationSuggestionService.class);
    private static long sMaxIdentifier = -1;

    private enum UploadResult {
        SUCCESS,
        FAILURE
    }

    static {
        System.loadLibrary(PLACE_SUGGESTION_LIBRARY_NAME);

        NO_SUGGESTION = new com.navdy.client.app.framework.models.Destination();
//        java.lang.System.loadLibrary(PLACE_SUGGESTION_LIBRARY_NAME);
        NO_SUGGESTION = new com.navdy.client.app.framework.models.Destination();
        NO_SUGGESTION.setId(-1);
    }

    public static native void learn(double d, double d2, long j, double d3, double d4, int i, int i2, int i3);

    public static native void reset();

    public static native SuggestedDestination suggestDestination(double d, double d2, int i, int i2, int i3);


    public DestinationSuggestionService() {
        super(SERVICE_NAME);
    }

    /* access modifiers changed from: private */
    public boolean firstSuggestionHasValidDestination(java.util.ArrayList<com.navdy.client.app.framework.models.Suggestion> arrayList) {
        return arrayList != null && arrayList.size() > 0 && arrayList.get(0) != null && arrayList.get(0).destination != null && arrayList.get(0).destination.hasOneValidSetOfCoordinates();
    }

    private android.database.Cursor getTripCursor() {
        if (sColdStart) {
            sLogger.d("cold start, getting all the trips for learning");
            return com.navdy.client.app.providers.NavdyContentProvider.getTripsCursor();
        }
        sLogger.d("warm start, getting trips greater than " + sMaxIdentifier);
        return getTripCursor(sMaxIdentifier);
    }

    private android.database.Cursor getTripCursor(long j) {
        return com.navdy.client.app.providers.NavdyContentProvider.getTripsCursor(new android.util.Pair("_id > ?", new java.lang.String[]{java.lang.Long.toString(j)}));
    }

    private void handleAutoTripUpload() {
        java.lang.Throwable th;
        android.database.Cursor tripsCursor = null;
        sLogger.d("handleAutoTripUpload : checking if we need an auto upload");
        final android.content.SharedPreferences sharedPreferences = com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences();
        long j = sharedPreferences.getLong(PREFERENCE_LAST_UPLOAD_TIME, MINIMUM_INTERVAL_BETWEEN_UPLOADS);
        long currentTimeMillis = java.lang.System.currentTimeMillis();
        long j2 = sharedPreferences.getLong(PREFERENCE_LAST_UPLOAD_ID, -1);
        final long maxTripId = com.navdy.client.app.providers.NavdyContentProvider.getMaxTripId();
        boolean z = maxTripId < j2;
        if (z) {
            sLogger.d("Database has changed");
        }
        if (z || ((currentTimeMillis - j > MINIMUM_INTERVAL_BETWEEN_UPLOADS && (j2 == -1 || j2 < maxTripId)) || maxTripId > TRIP_TRESHOLD_FOR_UPLOAD + j2)) {
            sLogger.d("Uploading the trip DB");
            android.database.Cursor cursor = null;
            if (j2 > MINIMUM_INTERVAL_BETWEEN_UPLOADS) {
                try {
                    tripsCursor = getTripCursor(j2);
                } catch (Throwable th2) {
                    th = th2;
                }
            } else {
                tripsCursor = com.navdy.client.app.providers.NavdyContentProvider.getTripsCursor();
            }
            try {
                java.lang.String str = (com.navdy.client.app.ui.settings.SettingsUtils.getCustomerPreferences().getString("fname", "UNKNOWN") + com.navdy.service.library.util.MusicDataUtils.ALTERNATE_SEPARATOR + android.os.Build.MANUFACTURER + com.navdy.service.library.util.MusicDataUtils.ALTERNATE_SEPARATOR + android.os.Build.MODEL).replaceAll("[^a-zA-Z0-9.-]", com.navdy.service.library.util.MusicDataUtils.ALTERNATE_SEPARATOR) + new java.text.SimpleDateFormat("_MM_dd_yyyy_HH_mm_ss_SSS", java.util.Locale.US).format(new java.util.Date(java.lang.System.currentTimeMillis())) + "_Android_V" + 1718 + ".csv";
                sLogger.d("Uploading the file to S3, Key : " + str);
                final com.squareup.otto.Bus bus = new com.squareup.otto.Bus(com.squareup.otto.ThreadEnforcer.ANY);
                bus.register(new java.lang.Object() {
                    @com.squareup.otto.Subscribe
                    public void onProgress(DestinationSuggestionService.UploadResult uploadResult) {
                        if (uploadResult == DestinationSuggestionService.UploadResult.SUCCESS) {
                            sharedPreferences.edit().putLong(DestinationSuggestionService.PREFERENCE_LAST_UPLOAD_TIME, java.lang.System.currentTimeMillis()).putLong(DestinationSuggestionService.PREFERENCE_LAST_UPLOAD_ID, maxTripId).apply();
                            DestinationSuggestionService.this.scheduleNextCheckForUpdate();
                        }
                        bus.unregister(this);
                    }
                });
                uploadToS3(str, tripsCursor, bus);
                com.navdy.service.library.util.IOUtils.closeStream(tripsCursor);
                scheduleNextCheckForUpdate();
            } catch (Throwable th3) {
                cursor = tripsCursor;
                com.navdy.service.library.util.IOUtils.closeStream(cursor);
                throw th3;
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:31:0x01a6 A[SYNTHETIC, Splitter:B:31:0x01a6] */
    /* JADX WARNING: Removed duplicated region for block: B:95:? A[SYNTHETIC, RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x01ab A[Catch:{ IOException -> 0x0259 }] */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x026d A[SYNTHETIC, Splitter:B:70:0x026d] */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x0272 A[Catch:{ IOException -> 0x0276 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void handlePopulate() {
        java.io.BufferedReader bufferedReader;
        java.io.FileReader fileReader;
        java.lang.Throwable th;
        java.io.File file = new java.io.File(android.os.Environment.getExternalStorageDirectory() + java.io.File.separator + "data.csv");
        if (file.exists()) {
            try {
                fileReader = new java.io.FileReader(file);
                try {
                    bufferedReader = new java.io.BufferedReader(fileReader);
                    while (true) {
                        try {
                            java.lang.String readLine = bufferedReader.readLine();
                            if (readLine == null) {
                                break;
                            } else if (!readLine.startsWith("Z_PK")) {
                                java.lang.String[] split = readLine.split(com.navdy.client.app.framework.glances.GlanceConstants.COMMA);
                                long parseLong = java.lang.Long.parseLong(split[0]);
                                long parseLong2 = java.lang.Long.parseLong(split[5]);
                                long j = !com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(split[6]) ? java.lang.Long.parseLong(split[6]) : -1;
                                long j2 = !com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(split[7]) ? java.lang.Long.parseLong(split[7]) : -1;
                                long j3 = !com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(split[8]) ? java.lang.Long.parseLong(split[8]) : -1;
                                long j4 = j != -1 ? j : j2 != -1 ? j2 : j3;
                                double parseDouble = java.lang.Double.parseDouble(split[9]);
                                double parseDouble2 = java.lang.Double.parseDouble(split[10]);
                                long parseDouble3 = (long) (java.lang.Double.parseDouble(split[11]) + 9.783072E8d);
                                double parseDouble4 = java.lang.Double.parseDouble(split[12]);
                                double parseDouble5 = java.lang.Double.parseDouble(split[13]);
                                long parseDouble6 = (long) (java.lang.Double.parseDouble(split[14]) + 9.783072E8d);
                                sLogger.d("Start Time " + parseDouble6);
                                int timeZoneAndDaylightSavingOffset = com.navdy.client.app.framework.util.SystemUtils.getTimeZoneAndDaylightSavingOffset(parseDouble6);
                                java.util.Calendar instance = java.util.Calendar.getInstance(java.util.Locale.getDefault());
                                instance.setTime(new java.util.Date(1000 * parseDouble6));
                                int i = instance.get(11);
                                int i2 = instance.get(12);
                                int i3 = instance.get(7);
                                sLogger.d("MM/DD/YYY " + instance.get(2) + "/" + instance.get(5) + "/" + instance.get(1) + " , Day :" + i3);
                                new com.navdy.client.app.framework.models.Trip((int) parseLong, parseLong2, parseDouble6, timeZoneAndDaylightSavingOffset, 0, parseDouble4, parseDouble5, parseDouble3, 0, parseDouble, parseDouble2, j, (int) j4).saveToDb(this);
                                DestinationSuggestionService.learn(parseDouble, parseDouble2, j4, parseDouble4, parseDouble5, i, i2, i3);
                            }
                        } catch (Throwable th3) {
                            th = th3;
                            throw th;
                        }
                    }
                    for (int i4 = 1; i4 <= 7; i4++) {
                        for (int i5 = 0; i5 < 24; i5++) {
                            sLogger.d("Day :" + i4 + ", Hour :" + i5);
                            SuggestedDestination suggestDestination = DestinationSuggestionService.suggestDestination(0.0d, 0.0d, i5, 0, i4);
                            if (suggestDestination != null) {
                                sLogger.d("Destination :" + suggestDestination.latitude + ", " + suggestDestination.longitude + " ; ID :" + suggestDestination.uniqueIdentifier);
                            }
                        }
                    }
                    try {
                        bufferedReader.close();
                    } catch (IOException e) {
                        sLogger.e("Error closing the buffered reader");
                        return;
                    }
                    fileReader.close();
                } catch (Throwable th4) {
                    th = th4;
                    throw th;
                }
            } catch (Throwable th5) {
                th5.printStackTrace();
            }
        }
    }

    private void handleSuggest(android.content.Intent intent) {
        boolean booleanExtra = intent.getBooleanExtra(EXTRA_LOCAL, true);
        sLogger.d("onHandleIntent : Command suggest, forThePhoneSuggestionList = " + booleanExtra);
        try {
            learnAllTrips();
            if (booleanExtra) {
                suggestDestinationBasedOnContext(true);
                return;
            }
            com.navdy.service.library.device.RemoteDevice remoteDevice = com.navdy.client.app.framework.AppInstance.getInstance().getRemoteDevice();
            if (remoteDevice == null || !remoteDevice.isConnected()) {
                sLogger.d("Remote device is not connected, so not making a suggestion");
            } else if (com.navdy.client.app.framework.navigation.NavdyRouteHandler.getInstance().getCurrentState() == com.navdy.client.app.framework.navigation.NavdyRouteHandler.State.EN_ROUTE) {
                sLogger.d("There is an active trip going on in HUD, so not sending suggestion");
            } else {
                com.navdy.client.app.framework.util.SuggestionManager.buildCalendarSuggestions(new com.navdy.client.app.framework.util.SuggestionManager.SuggestionBuildListener() {
                    public void onSuggestionBuildComplete(java.util.ArrayList<com.navdy.client.app.framework.models.Suggestion> arrayList) {
                        if (DestinationSuggestionService.this.firstSuggestionHasValidDestination(arrayList)) {
                            DestinationSuggestionService.this.sendSuggestionToHud(new com.navdy.client.app.framework.models.Suggestion(((com.navdy.client.app.framework.models.Suggestion) arrayList.get(0)).destination, com.navdy.client.app.framework.models.Suggestion.SuggestionType.CALENDAR), com.navdy.service.library.events.places.SuggestedDestination.SuggestionType.SUGGESTION_CALENDAR);
                            return;
                        }
                        DestinationSuggestionService.this.suggestDestinationBasedOnContext(false);
                    }
                });
            }
        } catch (Throwable th) {
            sLogger.e("Error during destination suggestion ", th);
        }
    }

    private void handleUploadTripData() {
        sLogger.d("Uploading the trip data base to the S3");
        java.lang.String replaceAll = com.navdy.client.app.ui.settings.SettingsUtils.getCustomerPreferences().getString("fname", "UNKNOWN").replaceAll("[^a-zA-Z0-9.-]", com.navdy.service.library.util.MusicDataUtils.ALTERNATE_SEPARATOR);
        uploadToS3(replaceAll + com.navdy.service.library.util.MusicDataUtils.ALTERNATE_SEPARATOR + android.os.Build.SERIAL + new java.text.SimpleDateFormat("_MM_dd_yyyy_HH_mm_ss_SSS", java.util.Locale.US).format(new java.util.Date(java.lang.System.currentTimeMillis())) + ".csv", getTripCursor(), null);
    }

//    public static native void learn(double d, double d2, long j, double d3, double d4, int i, int i2, int i3);

    private void learn(com.navdy.client.app.framework.models.Trip trip) {
        if (((long) trip.id) > sMaxIdentifier) {
            sMaxIdentifier = (long) trip.id;
        }
        long j = trip.destinationId > 0 ? (long) trip.destinationId : -1;
        com.navdy.client.app.framework.models.Destination thisDestination = com.navdy.client.app.providers.NavdyContentProvider.getThisDestination((int) j);
        if ((thisDestination == null || !thisDestination.doNotSuggest) && (trip.endLat != 0) && (trip.endLong != 0)) {
            java.util.Calendar instance = java.util.Calendar.getInstance(java.util.Locale.getDefault());
            instance.setTime(new java.util.Date(trip.startTime));
            sLogger.i("Learn: " + trip.endLat + ", " + trip.endLong + ", " + j + ", " + trip.startLat + ", " + trip.startLong + ", " + instance.get(Calendar.HOUR_OF_DAY) + ", " + instance.get(Calendar.MINUTE) + ", " + instance.get(Calendar.DAY_OF_WEEK));
            learn(trip.endLat, trip.endLong, j, trip.startLat, trip.startLong, instance.get(Calendar.HOUR_OF_DAY), instance.get(Calendar.MINUTE), instance.get(Calendar.DAY_OF_WEEK));
        }
    }

    private void learnAllTrips() {
        android.database.Cursor cursor = null;
        try {
            cursor = getTripCursor();
            if (cursor != null) {
                while (cursor.moveToNext()) {
                    com.navdy.client.app.framework.models.Trip tripsItemAt = com.navdy.client.app.providers.NavdyContentProvider.getTripsItemAt(cursor, cursor.getPosition());
                    if (tripsItemAt != null) {
                        learn(tripsItemAt);
                    }
                }
                sColdStart = false;
            }
        } finally {
            com.navdy.service.library.util.IOUtils.closeStream(cursor);
        }
    }

//    public static native void reset();

    public static void reset(android.content.Context context) {
        android.content.Intent intent = new android.content.Intent(context, DestinationSuggestionService.class);
        intent.setAction(COMMAND_RESET);
        context.startService(intent);
    }

    /* access modifiers changed from: private */
    public void scheduleNextCheckForUpdate() {
        sLogger.d("Scheduling for next check for update");
        android.content.Intent intent = new android.content.Intent(this, DestinationSuggestionService.class);
        intent.setAction(COMMAND_AUTO_UPLOAD_TRIP_DATA);
        ((android.app.AlarmManager) getSystemService(Context.ALARM_SERVICE)).set(1, java.lang.System.currentTimeMillis() + MINIMUM_INTERVAL_BETWEEN_UPLOADS, android.app.PendingIntent.getService(this, TRIP_UPLOAD_SERVICE_REQ, intent, 268435456));
    }

    /* access modifiers changed from: private */
    public void sendSuggestion(com.navdy.service.library.events.places.SuggestedDestination suggestedDestination) {
        com.navdy.service.library.device.RemoteDevice remoteDevice = com.navdy.client.app.framework.AppInstance.getInstance().getRemoteDevice();
        if (remoteDevice != null && remoteDevice.isConnected()) {
            remoteDevice.postEvent((com.squareup.wire.Message) suggestedDestination);
        }
    }

    /* access modifiers changed from: private */
    public void sendSuggestionToHud(@androidx.annotation.NonNull com.navdy.client.app.framework.models.Suggestion suggestion, final com.navdy.service.library.events.places.SuggestedDestination.SuggestionType suggestionType) {
        double d;
        double d2;
        final com.navdy.service.library.events.destination.Destination protobufDestinationObject = suggestion.toProtobufDestinationObject();
        if (protobufDestinationObject != null) {
            sLogger.d("DestinationEngine , destination message,  Title :" + protobufDestinationObject.destination_title + ", Subtitle :" + protobufDestinationObject.destination_subtitle + ", Type :" + protobufDestinationObject.suggestion_type + ", Is Recommendation :" + protobufDestinationObject.is_recommendation + ", Address :" + protobufDestinationObject.full_address + ", Unique ID: " + protobufDestinationObject.identifier);
            if (protobufDestinationObject.navigation_position.latitude == 0.0d && protobufDestinationObject.navigation_position.longitude == 0.0d) {
                d2 = protobufDestinationObject.display_position.latitude;
                d = protobufDestinationObject.display_position.longitude;
            } else {
                d2 = protobufDestinationObject.navigation_position.latitude;
                d = protobufDestinationObject.navigation_position.longitude;
            }
            com.navdy.client.app.framework.navigation.HereRouteManager.getInstance().calculateRoute(d2, d, new com.navdy.client.app.framework.navigation.HereRouteManager.Listener() {
                public void onPreCalculation(@androidx.annotation.NonNull com.navdy.client.app.framework.navigation.HereRouteManager.RouteHandle routeHandle) {
                }

                public void onRouteCalculated(@androidx.annotation.NonNull com.navdy.client.app.framework.navigation.HereRouteManager.Error error, com.here.android.mpa.routing.Route route) {
                    int i = -1;
                    if (error == com.navdy.client.app.framework.navigation.HereRouteManager.Error.NONE && route.getTta(com.here.android.mpa.routing.Route.TrafficPenaltyMode.OPTIMAL, 268435455) != null) {
                        i = route.getTta(com.here.android.mpa.routing.Route.TrafficPenaltyMode.OPTIMAL, 268435455).getDuration();
                    }
                    DestinationSuggestionService.this.sendSuggestion(new com.navdy.service.library.events.places.SuggestedDestination(protobufDestinationObject, java.lang.Integer.valueOf(i), suggestionType));
                }
            });
        }
    }

//    public static native com.navdy.client.app.framework.suggestion.SuggestedDestination suggestDestination(double d, double d2, int i, int i2, int i3);

    public static void suggestDestination(android.content.Context context, boolean z) {
        android.content.Intent intent = new android.content.Intent(context, DestinationSuggestionService.class);
        intent.setAction(COMMAND_SUGGEST);
        intent.putExtra(EXTRA_LOCAL, z);
        context.startService(intent);
    }

    /* access modifiers changed from: private */
    public void suggestDestinationBasedOnContext(boolean z) {
        SuggestedDestination suggestedDestination;
        com.navdy.client.app.framework.models.Destination destination;
        sLogger.d("Suggest Destination based on the context For suggestion list :" + z);
        java.util.Date date = new java.util.Date(java.lang.System.currentTimeMillis());
        java.util.Calendar instance = java.util.Calendar.getInstance(java.util.Locale.getDefault());
        instance.setTime(date);
        int i = instance.get(11);
        int i2 = instance.get(12);
        int i3 = instance.get(7);
        com.navdy.service.library.events.location.Coordinate smartStartCoordinates = com.navdy.client.app.framework.location.NavdyLocationManager.getInstance().getSmartStartCoordinates();
        if (smartStartCoordinates != null) {
            sLogger.d("Current location is known: " + smartStartCoordinates.latitude + ", " + smartStartCoordinates.longitude);
            suggestedDestination = DestinationSuggestionService.suggestDestination(smartStartCoordinates.latitude, smartStartCoordinates.longitude, i, i2, i3);
        } else {
            sLogger.d("Current location is unknown");
            suggestedDestination = z ? DestinationSuggestionService.suggestDestination(0.0d, 0.0d, i, i2, i3) : null;
        }
        if (suggestedDestination != null) {
            destination = com.navdy.client.app.providers.NavdyContentProvider.getThisDestination((int) suggestedDestination.uniqueIdentifier);
            if (destination != null) {
                android.location.Location location = new android.location.Location("");
                if (smartStartCoordinates != null) {
                    location.setLatitude(smartStartCoordinates.latitude);
                    location.setLongitude(smartStartCoordinates.longitude);
                }
                android.location.Location location2 = new android.location.Location("");
                location2.setLatitude(destination.displayLat);
                location2.setLongitude(destination.displayLong);
                if (((double) location2.distanceTo(location)) <= SUGGESTED_DESTINATION_TRESHOLD) {
                    if (z) {
                        com.navdy.client.app.framework.util.BusProvider.getInstance().post(NO_SUGGESTION);
                    }
                    sLogger.d("Not suggesting as the destination with the identifier is very close to the current location");
                    sLogger.d("DestinationEngine , suggested destiantion,  ID: " + suggestedDestination.uniqueIdentifier + ", Lat: " + suggestedDestination.latitude + ", Long: " + suggestedDestination.longitude + ", Frequency: " + suggestedDestination.frequency + ", Probability: " + suggestedDestination.probability);
                    return;
                } else if (z) {
                    com.navdy.client.app.framework.util.BusProvider.getInstance().post(destination);
                } else {
                    sLogger.d("DestinationEngine: suggested destiantion: ID = " + suggestedDestination.uniqueIdentifier + ", Lat = " + suggestedDestination.latitude + ", Long = " + suggestedDestination.longitude + ", Frequency = " + suggestedDestination.frequency + ", Probability = " + suggestedDestination.probability);
                    sendSuggestionToHud(new com.navdy.client.app.framework.models.Suggestion(destination, com.navdy.client.app.framework.models.Suggestion.SuggestionType.RECOMMENDATION), com.navdy.service.library.events.places.SuggestedDestination.SuggestionType.SUGGESTION_RECOMMENDATION);
                }
            }
        } else {
            destination = null;
        }
        if (z && destination == null) {
            com.navdy.client.app.framework.util.BusProvider.getInstance().post(NO_SUGGESTION);
        }
    }

    private void uploadFileToS3(final java.io.File file, final com.squareup.otto.Bus bus) {
        if (file != null && file.exists()) {
            sLogger.d("Length of the file being uploaded: " + file.length());
            if (file.length() != MINIMUM_INTERVAL_BETWEEN_UPLOADS) {
                new com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility(com.navdy.client.ota.impl.OTAUpdateManagerImpl.createS3Client(), this).upload(BUCKET_NAME, file.getName(), file).setTransferListener(new com.amazonaws.mobileconnectors.s3.transferutility.TransferListener() {
                    public void onError(int i, java.lang.Exception exc) {
                        DestinationSuggestionService.sLogger.e("Uploading File " + file.getName() + ", onError : " + i, exc);
                        if (bus != null) {
                            bus.post(DestinationSuggestionService.UploadResult.FAILURE);
                        }
                    }

                    public void onProgressChanged(int i, long j, long j2) {
                        DestinationSuggestionService.sLogger.d("Uploading File " + file.getName() + ", onProgressChanged : " + i + ", Size :" + j + ", of " + j2);
                    }

                    public void onStateChanged(int i, com.amazonaws.mobileconnectors.s3.transferutility.TransferState transferState) {
                        DestinationSuggestionService.sLogger.d("Uploading File " + file.getName() + ", onStatChanged : " + i + ", " + transferState.name());
                        if (transferState == com.amazonaws.mobileconnectors.s3.transferutility.TransferState.COMPLETED) {
                            if (bus != null) {
                                bus.post(DestinationSuggestionService.UploadResult.SUCCESS);
                            }
                        } else if (transferState == com.amazonaws.mobileconnectors.s3.transferutility.TransferState.FAILED && bus != null) {
                            bus.post(DestinationSuggestionService.UploadResult.FAILURE);
                        }
                    }
                });
            } else if (bus != null) {
                bus.post(DestinationSuggestionService.UploadResult.FAILURE);
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:43:0x0148 A[SYNTHETIC, Splitter:B:43:0x0148] */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0156 A[SYNTHETIC, Splitter:B:47:0x0156] */
    /* JADX WARNING: Removed duplicated region for block: B:115:? A[SYNTHETIC, RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x015b A[SYNTHETIC, Splitter:B:50:0x015b] */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x01a7 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:15:0x0051] */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x01aa A[SYNTHETIC, Splitter:B:79:0x01aa] */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x01af A[SYNTHETIC, Splitter:B:82:0x01af] */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x0197, code lost:
            r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x0198, code lost:
            if (r15 != null) goto L_0x019a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:?, code lost:
            r15.post(com.navdy.client.app.framework.suggestion.DestinationSuggestionService.UploadResult.FAILURE);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x019f, code lost:
            sLogger.e("Error closing the cursor ", r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x01a7, code lost:
            r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:?, code lost:
            r14.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:?, code lost:
            r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:102:0x01ec, code lost:
            r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:103:0x01ed, code lost:
            sLogger.e("Error closing the cursor ", r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:104:0x01f5, code lost:
            r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:105:0x01f6, code lost:
            sLogger.e("Error closing the file writer ", r1);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void uploadToS3(java.lang.String str, android.database.Cursor cursor, com.squareup.otto.Bus bus) {
        java.io.FileWriter fileWriter;
        try {
            java.io.File file = new java.io.File(getFilesDir() + java.io.File.separator + str);
            if (file.exists()) {
                com.navdy.service.library.util.IOUtils.deleteFile(com.navdy.client.app.NavdyApplication.getAppContext(), file.getAbsolutePath());
            }
            if (file.createNewFile()) {
                fileWriter = new java.io.FileWriter(file, true);
                if (cursor != null) {
                    try {
                        if (cursor.getCount() > 0) {
                            fileWriter.write("TRIP_ID,TRIP_NUMBER,TRIP_START_TIME,TRIP_START_TIME_ZONE_N_DST,TRIP_ODOMETER,TRIP_LATITUDE,TRIP_LONGITUDE,TRIP_END_TIME,TRIP_END_ODOMETER,TRIP_END_LATITUDE,TRIP_END_LONGITUDE,TRIP_ARRIVED_AT_DESTINATION,TRIP_DESTINATION_ID,TRIP_DESTINATION_LAT,TRIP_DESTINATION_LONG\n");
                            while (cursor.moveToNext()) {
                                com.navdy.client.app.framework.models.Trip tripsItemAt = com.navdy.client.app.providers.NavdyContentProvider.getTripsItemAt(cursor, cursor.getPosition());
                                if (tripsItemAt != null) {
                                    com.navdy.client.app.framework.models.Destination thisDestination = com.navdy.client.app.providers.NavdyContentProvider.getThisDestination(tripsItemAt.destinationId);
                                    fileWriter.write(tripsItemAt.id + com.navdy.client.app.framework.glances.GlanceConstants.COMMA + tripsItemAt.tripNumber + com.navdy.client.app.framework.glances.GlanceConstants.COMMA + tripsItemAt.startTime + com.navdy.client.app.framework.glances.GlanceConstants.COMMA + tripsItemAt.offset + com.navdy.client.app.framework.glances.GlanceConstants.COMMA + tripsItemAt.startOdometer + com.navdy.client.app.framework.glances.GlanceConstants.COMMA + tripsItemAt.startLat + com.navdy.client.app.framework.glances.GlanceConstants.COMMA + tripsItemAt.startLong + com.navdy.client.app.framework.glances.GlanceConstants.COMMA + tripsItemAt.endTime + com.navdy.client.app.framework.glances.GlanceConstants.COMMA + tripsItemAt.endOdometer + com.navdy.client.app.framework.glances.GlanceConstants.COMMA + tripsItemAt.endLat + com.navdy.client.app.framework.glances.GlanceConstants.COMMA + tripsItemAt.endLong + com.navdy.client.app.framework.glances.GlanceConstants.COMMA + (tripsItemAt.arrivedAtDestination == MINIMUM_INTERVAL_BETWEEN_UPLOADS ? "false" : "true") + com.navdy.client.app.framework.glances.GlanceConstants.COMMA + (tripsItemAt.destinationId <= 0 ? "NA" : java.lang.Integer.valueOf(tripsItemAt.destinationId)) + com.navdy.client.app.framework.glances.GlanceConstants.COMMA + (thisDestination != null ? thisDestination.navigationLat : 0.0d) + com.navdy.client.app.framework.glances.GlanceConstants.COMMA + (thisDestination != null ? thisDestination.navigationLong : 0.0d) + com.navdy.client.app.framework.glances.GlanceConstants.NEW_LINE);
                                }
                            }
                            cursor.close();
                            fileWriter.flush();
                            uploadFileToS3(file, bus);
                            try {
                                cursor.close();
                            } catch (Throwable th) {
                                sLogger.e("Error closing the cursor ", th);
                            }
                            try {
                                fileWriter.close();
                                return;
                            } catch (IOException e) {
                                sLogger.e("Error closing the file writer ", e);
                                return;
                            }
                        }
                    } catch (Throwable ignored) {
                    }
                }
                if (bus != null) {
                    bus.post(DestinationSuggestionService.UploadResult.FAILURE);
                }
                if (cursor != null) {
                    try {
                        cursor.close();
                    } catch (Throwable th3) {
                        sLogger.e("Error closing the cursor ", th3);
                    }
                }
                try {
                    fileWriter.close();
                } catch (IOException e2) {
                    sLogger.e("Error closing the file writer ", e2);
                }
            } else if (cursor != null) {
                try {
                    cursor.close();
                } catch (Throwable th4) {
                    sLogger.e("Error closing the cursor ", th4);
                }
            }
        } catch (Throwable th5) {
            if (bus != null) bus.post(UploadResult.FAILURE);
            sLogger.e("Error while dumping the trip database to the csv file");
            if (cursor != null) {
                try {
                    cursor.close();
                } catch (Throwable th7) {
                    sLogger.e("Error closing the cursor ", th7);
                }
            }
        }
    }

    public static void uploadTripDatabase(android.content.Context context, boolean z) {
        if (z) {
            android.content.Intent intent = new android.content.Intent(context, DestinationSuggestionService.class);
            intent.setAction(COMMAND_UPLOAD_TRIP_DATA);
            context.startService(intent);
            return;
        }
        sLogger.d("UploadTripDatabase :Not a debug build so skipping");
    }

    public void onCreate() {
        super.onCreate();
    }

    /* access modifiers changed from: protected */
    public void onHandleIntent(android.content.Intent intent) {
        if (intent == null) {
            sLogger.e("received a call for onHandleIntent with a null intent!");
            return;
        }
        java.lang.String action = intent.getAction();
        if (COMMAND_SUGGEST.equals(action)) {
            handleSuggest(intent);
        } else if (COMMAND_POPULATE.equals(action)) {
            handlePopulate();
        } else if (COMMAND_UPLOAD_TRIP_DATA.equals(action)) {
            handleUploadTripData();
        } else if (COMMAND_AUTO_UPLOAD_TRIP_DATA.equals(action)) {
            handleAutoTripUpload();
        } else if (COMMAND_RESET.equals(action)) {
            DestinationSuggestionService.reset();
            sColdStart = true;
        }
    }
}