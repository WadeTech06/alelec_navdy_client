package com.navdy.client.app.framework.search;

import android.os.AsyncTask;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.alelec.navdyclient.R;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.AppInstance;
import com.navdy.client.app.framework.DeviceConnection;
import com.navdy.client.app.framework.models.ContactModel;
import com.navdy.client.app.framework.models.Destination;
import com.navdy.client.app.framework.models.Destination.SearchType;
import com.navdy.client.app.framework.models.GoogleTextSearchDestinationResult;
import com.navdy.client.app.framework.search.GooglePlacesSearch.Error;
import com.navdy.client.app.framework.search.GooglePlacesSearch.GoogleSearchListener;
import com.navdy.client.app.framework.search.GooglePlacesSearch.Query;
import com.navdy.client.app.framework.search.GooglePlacesSearch.ServiceTypes;
import com.navdy.client.app.framework.util.BusProvider;
import com.navdy.client.app.framework.util.ContactsManager;
import com.navdy.client.app.framework.util.ContactsManager.Attributes;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.framework.util.VoiceCommand;
import com.navdy.client.app.framework.util.VoiceCommandUtils;
import com.navdy.client.app.providers.NavdyContentProvider;
import com.navdy.service.library.events.RequestStatus;
import com.navdy.service.library.events.places.PlacesSearchRequest.Builder;
import com.navdy.service.library.events.places.PlacesSearchResponse;
import com.navdy.service.library.events.places.PlacesSearchResult;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Subscribe;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class NavdySearch {
    private static final long HUD_SEARCH_TIMEOUT = TimeUnit.MINUTES.toMillis(1);
    public static final int MAX_RESULTS_FROM_HUD = 30;
    public static final Integer SEARCH_AREA_VALUE = null;
    private Handler handler;
    private String lastHudQuery;
    private Logger logger;
    private SearchCallback searchCallback;
    private SearchResults searchResults;
    private boolean withContactPhotos;

    public interface SearchCallback {
        void onSearchCompleted(SearchResults searchResults);

        void onSearchStarted();
    }

    public NavdySearch(SearchCallback searchCallback) {
        this.logger = new Logger(NavdySearch.class);
        this.searchResults = new SearchResults();
        this.searchCallback = null;
        this.withContactPhotos = false;
        this.handler = new Handler();
        this.searchCallback = searchCallback;
    }

    public NavdySearch(SearchCallback searchCallback, boolean withContactPhotos) {
        this(searchCallback);
        this.withContactPhotos = withContactPhotos;
    }

    @Subscribe
    public void onPlacesSearchResponse(PlacesSearchResponse placesSearchResponse) {
        this.handler.removeCallbacksAndMessages(null);
        if (placesSearchResponse == null) {
            this.logger.d("Receiving search results from HUD that is null!");
            finishSearch();
            return;
        }
        this.logger.d("Receiving search results from HUD for \"" + placesSearchResponse.searchQuery + "\": " + placesSearchResponse.results);
        handleHudSearchResponse(placesSearchResponse);
        BusProvider.getInstance().unregister(this);
    }

    public void runSearch(String query) {
        if (this.searchCallback == null) {
            throw new RuntimeException("NoSearchCallBackException", new Throwable("No point running a search if there is no callback specified."));
        }
        this.searchResults.setQuery(query);
        this.searchCallback.onSearchStarted();
        AppInstance appInstance = AppInstance.getInstance();
        if (appInstance.canReachInternet()) {
            runOnlineSearch(query);
        } else if (appInstance.isDeviceConnected()) {
            BusProvider.getInstance().register(this);
            query = cleanUpNearestQuery(query);
            this.lastHudQuery = query;
            this.logger.d("Requesting search from HUD for \"" + query + "\"" + " with a " + SEARCH_AREA_VALUE + " search area" + " and " + 30 + " max results");
            DeviceConnection.postEvent(new Builder().searchQuery(query).searchArea(SEARCH_AREA_VALUE).maxResults(30).build());
            this.handler.postDelayed(new Runnable() {
                public void run() {
                    NavdySearch.this.onPlacesSearchResponse(new PlacesSearchResponse.Builder().searchQuery(NavdySearch.this.lastHudQuery).status(RequestStatus.REQUEST_UNKNOWN_ERROR).statusDetail("Hud Search timed out").results(new ArrayList(0)).build());
                }
            }, HUD_SEARCH_TIMEOUT);
        } else {
            runOfflineDisconnectSearch(query);
        }
    }

    private void finishSearch() {
        if (this.searchCallback != null) {
            this.searchCallback.onSearchCompleted(this.searchResults);
        }
    }

    private void addMatchingContactsAndDbDestinationsToSearchResults(@NonNull String query) {
        List<ContactModel> contacts;
        Iterator it;
        ContactsManager contactsManager = ContactsManager.getInstance();
        String contactQuery = cleanUpContactQuery(query);
        this.logger.d("The cleaned up query for contact is: " + contactQuery);
        if (this.withContactPhotos) {
            contacts = contactsManager.getContactsAndLoadPhotos(contactQuery, EnumSet.of(Attributes.ADDRESS));
        } else {
            contacts = contactsManager.getContactsWithoutLoadingPhotos(contactQuery, EnumSet.of(Attributes.ADDRESS));
        }
        String and = " " + NavdyApplication.getAppContext().getString(R.string.this_and_that) + " ";
        if (contactQuery.contains(and)) {
            contactQuery = contactQuery.replaceAll(and, " & ");
            List<ContactModel> contacts2;
            if (this.withContactPhotos) {
                contacts2 = contactsManager.getContactsAndLoadPhotos(contactQuery, EnumSet.of(Attributes.ADDRESS));
            } else {
                contacts2 = contactsManager.getContactsWithoutLoadingPhotos(contactQuery, EnumSet.of(Attributes.ADDRESS));
            }
            for (ContactModel contact2 : contacts2) {
                boolean found = false;
                for (ContactModel contact1 : contacts) {
                    if (contact1.equals(contact2)) {
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    contacts.add(contact2);
                }
            }
        }
        this.searchResults.setContacts(contacts);
        ArrayList<Destination> destinationsFromDatabase = NavdyContentProvider.getDestinationsFromSearchQuery(query);
        if (query.contains(and)) {
            ArrayList<Destination> destinationsFromDatabase2 = NavdyContentProvider.getDestinationsFromSearchQuery(query.replaceAll(and, " & "));
            if (destinationsFromDatabase != null) {
                destinationsFromDatabase = (ArrayList) Destination.mergeAndDeduplicateLists(destinationsFromDatabase, destinationsFromDatabase2);
            } else {
                destinationsFromDatabase = destinationsFromDatabase2;
            }
        }
        if (destinationsFromDatabase != null) {
            it = destinationsFromDatabase.iterator();
            while (it.hasNext()) {
                Destination destination = (Destination) it.next();
                destination.rawAddressVariations = NavdyContentProvider.getCacheKeysForDestinationId(destination.id);
            }
        }
        this.searchResults.setDestinationsFromDatabase(destinationsFromDatabase);
    }

    @NonNull
    private String cleanUpContactQuery(@NonNull String query) {
        VoiceCommand vc = VoiceCommandUtils.getMatchingVoiceCommand(R.raw.contact_home_strings, query);
        return (vc == null || StringUtils.isEmptyAfterTrim(vc.query)) ? query : vc.query;
    }

    @NonNull
    private String cleanUpNearestQuery(@NonNull String query) {
        VoiceCommand vc = VoiceCommandUtils.getMatchingVoiceCommand(R.raw.nearest_strings, query);
        return (vc == null || StringUtils.isEmptyAfterTrim(vc.query)) ? query : vc.query;
    }

    private void runOnlineSearch(final String query) {
        new AsyncTask<Void, Void, Void>() {
            protected Void doInBackground(Void... voids) {
                NavdySearch.this.addMatchingContactsAndDbDestinationsToSearchResults(query);
                return null;
            }

            protected void onPostExecute(Void aVoid) {
                GooglePlacesSearch googlePlacesSearch = new GooglePlacesSearch(new GoogleSearchListener() {
                    public void onGoogleSearchResult(List<GoogleTextSearchDestinationResult> googleResults, Query queryType, Error error) {
                        if (error != Error.NONE) {
                            NavdySearch.this.logger.e("received Google places search error while running online search: " + error.name());
                            NavdySearch.this.searchResults.setGoogleResults(null);
                        } else {
                            NavdySearch.this.searchResults.setGoogleResults(googleResults);
                        }
                        NavdySearch.this.finishSearch();
                    }
                }, new Handler());
                String newQuery = NavdySearch.this.cleanUpNearestQuery(query);
                if (StringUtils.equalsOrBothEmptyAfterTrim(newQuery, query)) {
                    googlePlacesSearch.runTextSearchWebApi(query);
                    return;
                }
                NavdySearch.this.logger.v("Query was a 'the nearest' kind of query. We will seach for the closest: " + newQuery);
                googlePlacesSearch.runNearbySearchWebApi(newQuery);
            }
        }.execute(new Void[0]);
    }

    private void handleHudSearchResponse(@NonNull final PlacesSearchResponse placesSearchResponse) {
        if (this.lastHudQuery != null && this.lastHudQuery.equals(placesSearchResponse.searchQuery)) {
            this.lastHudQuery = null;
            new AsyncTask<Void, Void, Void>() {
                protected Void doInBackground(Void... voids) {
                    NavdySearch.this.addMatchingContactsAndDbDestinationsToSearchResults(placesSearchResponse.searchQuery);
                    return null;
                }

                protected void onPostExecute(Void aVoid) {
                    List<PlacesSearchResult> hudSearchResults;
                    if (placesSearchResponse.status == RequestStatus.REQUEST_SUCCESS) {
                        hudSearchResults = placesSearchResponse.results;
                    } else {
                        hudSearchResults = null;
                        NavdySearch.this.logger.v("Request failed: " + placesSearchResponse.status);
                    }
                    NavdySearch.this.searchResults.setHudSearchResults(hudSearchResults);
                    NavdySearch.this.finishSearch();
                }
            }.execute(new Void[0]);
        }
    }

    private void runOfflineDisconnectSearch(final String query) {
        new AsyncTask<Void, Void, Void>() {
            protected Void doInBackground(Void... voids) {
                NavdySearch.this.addMatchingContactsAndDbDestinationsToSearchResults(query);
                return null;
            }

            protected void onPostExecute(Void aVoid) {
                NavdySearch.this.finishSearch();
            }
        }.execute(new Void[0]);
    }

    @Nullable
    static ArrayList<Destination> getDestinationListFromPlaceSearchResults(@Nullable List<PlacesSearchResult> placesSearchResults) {
        ArrayList<Destination> destinations = new ArrayList();
        if (placesSearchResults != null) {
            for (PlacesSearchResult placesSearchResult : placesSearchResults) {
                Destination destination = new Destination();
                Destination.placesSearchResultToDestinationObject(placesSearchResult, destination);
                destinations.add(destination);
            }
        }
        return destinations;
    }

    @Nullable
    public static ArrayList<Destination> getDestinationListFromGoogleTextSearchResults(@Nullable List<GoogleTextSearchDestinationResult> googleTextSearchResults) {
        ArrayList<Destination> destinations = new ArrayList();
        if (googleTextSearchResults != null) {
            for (GoogleTextSearchDestinationResult googleTextSearchDestinationResult : googleTextSearchResults) {
                Destination destination = new Destination();
                googleTextSearchDestinationResult.toModelDestinationObject(SearchType.TEXT_SEARCH, destination);
                destinations.add(destination);
            }
        }
        return destinations;
    }

    public static void runServiceSearch(GooglePlacesSearch googlePlacesSearch, ServiceTypes serviceType) {
        switch (serviceType) {
            case FOOD:
            case STORE:
                googlePlacesSearch.runTextSearchWebApi(serviceType.getHudServiceType());
                return;
            default:
                googlePlacesSearch.runServiceSearchWebApi(serviceType);
                return;
        }
    }
}
