package com.navdy.client.app.framework.util;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore.Audio.Albums;
import android.provider.MediaStore.Audio.Artists;
import android.provider.MediaStore.Audio.Media;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.providers.NavdyContentProvider;
import com.navdy.client.app.providers.NavdyContentProviderConstants;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.util.IOUtils;
import java.util.HashSet;

public class MusicDbUtils {
    private static final String CHARACTER_MAP_SELECTION = "1=1) GROUP BY (letter";
    public static final String CHARACTER_MAP_SORT = "CASE\n  WHEN letter GLOB '[A-Z]' THEN 0\n  WHEN letter GLOB '[0-9]' THEN 1\n  ELSE 2\nEND ASC";
    private static final String COUNT = "count(*) AS count";
    public static final String COUNT_COLUMN = "count";
    private static final String FIRST_LETTER_FORMAT = "SUBSTR(UPPER(%s), 1, 1) AS letter";
    private static final String GPM_PLAYLISTS_BASE_URI = "content://com.google.android.music.MusicContent/playlists";
    public static final String GPM_PLAYLISTS_COLUMN_PLAYLIST_NAME = "playlist_name";
    private static final String GPM_PLAYLISTS_MEMBERS_PATH = "members";
    public static final String GPM_PLAYLIST_MEMBERS_COLUMN_ALBUM = "album";
    public static final String GPM_PLAYLIST_MEMBERS_COLUMN_ARTIST = "artist";
    public static final String GPM_PLAYLIST_MEMBERS_COLUMN_SOURCE_ID = "SourceId";
    public static final String GPM_PLAYLIST_MEMBERS_COLUMN_TITLE = "title";
    public static final String LETTER_COLUMN = "letter";
    public static final String ORDER_FORMAT = "CASE%n  WHEN UPPER(%1$s) GLOB '[A-Z]*' THEN 0%n  WHEN UPPER(%1$s) GLOB '[0-9]*' THEN 1%n  ELSE 2%nEND ASC, UPPER(%1$s);";
    private static Logger logger = new Logger(MusicDbUtils.class);

    public static Cursor getAlbumMembersCursor(String str) {
        return NavdyApplication.getAppContext().getContentResolver().query(Media.EXTERNAL_CONTENT_URI, new String[]{"title", "artist", "album", "_id"}, "album_id = ?", new String[]{str}, null);
    }

    public static Cursor getAlbumsCharacterMapCursor() {
        return NavdyApplication.getAppContext().getContentResolver().query(Albums.EXTERNAL_CONTENT_URI, getCharacterMapProjection("album"), CHARACTER_MAP_SELECTION, null, CHARACTER_MAP_SORT);
    }

    public static Cursor getAlbumsCursor() {
        return NavdyApplication.getAppContext().getContentResolver().query(Albums.EXTERNAL_CONTENT_URI, new String[]{"_id", "album", "numsongs", "artist"}, null, null, getOrderString("album"));
    }

    public static Cursor getArtistMembersCursor(String str) {
        return NavdyApplication.getAppContext().getContentResolver().query(Media.EXTERNAL_CONTENT_URI, new String[]{"title", "artist", "album", "_id"}, "artist_id = ?", new String[]{str}, null);
    }

    public static int getArtistSongsCount(String str) {
        Cursor cursor;
        int i;
        cursor = NavdyApplication.getAppContext().getContentResolver().query(Media.EXTERNAL_CONTENT_URI, new String[]{COUNT}, "artist_id = ?", new String[]{str}, null);
        if (cursor != null) {
            try {
                if (cursor.moveToFirst()) {
                    i = cursor.getInt(cursor.getColumnIndex(COUNT_COLUMN));
                    IOUtils.closeObject(cursor);
                    return i;
                }
            } catch (Throwable th) {
                IOUtils.closeObject(cursor);
            }
        }
        i = -1;
        IOUtils.closeObject(cursor);
        return i;
    }

    public static Cursor getArtistsAlbums(String str) {
        Cursor cursor;
        ContentResolver contentResolver = NavdyApplication.getAppContext().getContentResolver();
        Uri uri = Media.EXTERNAL_CONTENT_URI;
        HashSet<Integer> hashSet = new HashSet<>();
        cursor = contentResolver.query(uri, new String[]{"DISTINCT album_id"}, "artist_id = ?", new String[]{str}, null);
        if (cursor != null) {
            try {
                if (cursor.moveToFirst()) {
                    do {
                        hashSet.add(cursor.getInt(cursor.getColumnIndex("album_id")));
                    } while (cursor.moveToNext());
                }
            } catch (Throwable th) {
                cursor.close();
                throw th;
            }
        }
        if (cursor != null) {
            cursor.close();
        }
        StringBuilder sb = new StringBuilder();
        sb.append("_id IN (");
        int size = hashSet.size();
        String[] strArr = new String[size];
        int i = 0;
        for (Integer num : hashSet) {
            int i2 = i + 1;
            strArr[i] = num.toString();
            if (i2 <= size - 1) {
                sb.append("?,");
                i = i2;
            } else if (i2 == size) {
                sb.append("?)");
                i = i2;
            } else {
                i = i2;
            }
        }
        String sb2 = sb.toString();
        return contentResolver.query(Albums.EXTERNAL_CONTENT_URI, new String[]{"_id", "album", "numsongs", "artist"}, sb2, strArr, getOrderString("album"));
    }

    public static Cursor getArtistsCharacterMapCursor() {
        return NavdyApplication.getAppContext().getContentResolver().query(Artists.EXTERNAL_CONTENT_URI, getCharacterMapProjection("artist"), CHARACTER_MAP_SELECTION, null, CHARACTER_MAP_SORT);
    }

    public static Cursor getArtistsCursor() {
        return NavdyApplication.getAppContext().getContentResolver().query(Artists.EXTERNAL_CONTENT_URI, new String[]{"_id", "artist"}, null, null, getOrderString("artist"));
    }

    public static String[] getCharacterMapProjection(String str) {
        return new String[]{COUNT, String.format(FIRST_LETTER_FORMAT, str)};
    }

    public static Cursor getGpmPlaylistCursor(int i) {
        try {
            return NavdyApplication.getAppContext().getContentResolver().query(getGpmPlaylistUri(i), new String[]{"playlist_name"}, null, null, null);
        } catch (Throwable th) {
            logger.e("Couldn't get GPM playlist cursor for playlist " + i + ": " + th);
            return null;
        }
    }

    public static Cursor getGpmPlaylistMembersCursor(int i) {
        try {
            return NavdyApplication.getAppContext().getContentResolver().query(getGpmPlaylistMembersUri(i), new String[]{"title", "artist", "album", "SourceId"}, null, null, null);
        } catch (Throwable th) {
            logger.e("Couldn't get GPM playlist members cursor for playlist " + i + ": " + th);
            return null;
        }
    }

    private static Uri getGpmPlaylistMembersUri(int i) {
        return Uri.parse("content://com.google.android.music.MusicContent/playlists/" + i + "/" + GPM_PLAYLISTS_MEMBERS_PATH);
    }

    private static Uri getGpmPlaylistUri(int i) {
        return Uri.parse("content://com.google.android.music.MusicContent/playlists/" + i);
    }

    public static Cursor getGpmPlaylistsCursor() {
        try {
            return NavdyApplication.getAppContext().getContentResolver().query(getGpmPlaylistsUri(), new String[]{"_id", "playlist_name"}, null, null, "playlist_name");
        } catch (Throwable th) {
            logger.e("Couldn't get GPM playlists cursor: " + th);
            return null;
        }
    }

    public static Uri getGpmPlaylistsUri() {
        return Uri.parse(GPM_PLAYLISTS_BASE_URI);
    }

    public static String getOrderString(String str) {
        return String.format(ORDER_FORMAT, str);
    }

    public static Cursor getPlaylistMembersCursor(int i) {
        return NavdyContentProvider.getPlaylistMembersCursor(i);
    }

    public static Cursor getPlaylistMembersCursor(String str) {
        try {
            return getPlaylistMembersCursor(Integer.parseInt(str));
        } catch (NumberFormatException e) {
            logger.e("Invalid collection ID: " + str);
            return null;
        }
    }

    public static Cursor getPlaylistsCharacterMapCursor() {
        return NavdyApplication.getAppContext().getContentResolver().query(NavdyContentProviderConstants.PLAYLISTS_CONTENT_URI, getCharacterMapProjection("playlist_name"), CHARACTER_MAP_SELECTION, null, CHARACTER_MAP_SORT);
    }

    public static Cursor getPlaylistsCursor() {
        return NavdyContentProvider.getPlaylistsCursor();
    }
}