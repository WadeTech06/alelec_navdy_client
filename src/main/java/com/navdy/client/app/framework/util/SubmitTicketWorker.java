package com.navdy.client.app.framework.util;

import android.content.Context;
import android.os.Build;
import android.os.Build.VERSION;
import com.alelec.navdyclient.BuildConfig;
import com.alelec.navdyclient.R;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.AppInstance;
import com.navdy.client.app.framework.util.SupportTicketService.OnLogCollectedInterface;
import com.navdy.client.app.tracking.Tracker;
import com.navdy.client.app.tracking.TrackerConstants.Event;
import com.navdy.client.app.ui.base.BaseActivity;
import com.navdy.client.app.ui.settings.SettingsConstants;
import com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences;
import com.navdy.client.app.ui.settings.SettingsUtils;
import com.navdy.client.app.ui.settings.ZendeskConstants;
import com.navdy.service.library.device.RemoteDevice;
import com.navdy.service.library.events.DeviceInfo;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.util.IOUtils;
import com.zendesk.sdk.model.request.CreateRequest;
import com.zendesk.sdk.model.request.CustomField;
import com.zendesk.sdk.model.request.UploadResponse;
import com.zendesk.sdk.network.impl.ProviderStore;
import com.zendesk.sdk.network.impl.ZendeskConfig;
import com.zendesk.service.ErrorResponse;
import com.zendesk.service.ZendeskCallback;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONObject;

public class SubmitTicketWorker {
    private static final String ANDROID_BACKGROUND_SUBMISSION = "android_background_submission";
    private static final int INTERNET_ERROR_CODE = -1;
    private static final int MALFORMED_TICKET = 422;
    private static final long TEN_DAYS_IN_MILLIS = TimeUnit.DAYS.toMillis(10);
    private static final boolean VERBOSE = true;
    private static final String ZIP_MIME_TYPE = "application/zip";
    private static final Logger logger = new Logger(SubmitTicketWorker.class);
    private OnFinishSubmittingTickets callback;
    private File displayLog;
    private int failCount = 0;
    private AtomicBoolean isRunning = new AtomicBoolean(false);
    private final ProviderStore providerStore = ZendeskConfig.INSTANCE.provider();
    private boolean showSuccessNotification;
    private int successCount = 0;
    private int ticketCount = 0;

    public interface OnFinishSubmittingTickets {
        void onFinish(int i, boolean z);
    }

    SubmitTicketWorker() {
    }

    public synchronized void start(OnFinishSubmittingTickets onFinishSubmittingTickets, File[] folders) {
        this.ticketCount = folders.length;
        this.callback = onFinishSubmittingTickets;
        if (folders.length > 0) {
            this.isRunning.set(true);
            if (SettingsUtils.getSharedPreferences().getBoolean(SettingsConstants.TICKETS_AWAITING_HUD_LOG_EXIST, false) && AppInstance.getInstance().isDeviceConnected()) {
                fetchHudLogAndProcessTickets(folders);
            } else {
                processAllTickets(folders);
            }
        } else if (this.callback != null) {
            this.callback.onFinish(this.failCount, this.showSuccessNotification);
        }
    }

    private synchronized void processNextTicketFolder(final File ticketFolder) {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                if (ticketFolder.exists()) {
                    SubmitTicketWorker.logger.i("Now processing: " + ticketFolder.getName());
                    try {
                        if (System.currentTimeMillis() - SubmitTicketWorker.TEN_DAYS_IN_MILLIS >= ticketFolder.lastModified()) {
                            SubmitTicketWorker.logger.v("deleting old folder: " + ticketFolder.getAbsolutePath());
                            IOUtils.deleteDirectory(NavdyApplication.getAppContext(), ticketFolder);
                            SubmitTicketWorker.this.incrementSuccessCount();
                            return;
                        }
                        File attachments = new File(ticketFolder.getAbsolutePath() + File.separator + "attachments.zip");
                        SubmitTicketWorker.logger.v("attachment filepath: " + ticketFolder.getAbsolutePath() + File.separator + "attachments.zip");
                        File content = new File(ticketFolder.getAbsolutePath() + File.separator + ZendeskConstants.TICKET_JSON_FILENAME);
                        SubmitTicketWorker.logger.v("content filepath: " + ticketFolder.getAbsolutePath() + File.separator + ZendeskConstants.TICKET_JSON_FILENAME);
                        JSONObject jsonObject = SubmitTicketWorker.getJSONObjectFromFile(content);
                        if (jsonObject == null) {
                            SubmitTicketWorker.logger.d("No JSON Object found in the folder " + ticketFolder.getName());
                            SubmitTicketWorker.this.incrementFailureCount();
                            return;
                        } else if (!jsonObject.optString(ZendeskConstants.TICKET_ACTION, "").equals(ZendeskConstants.ACTION_FETCH_HUD_LOG_UPLOAD_DELETE)) {
                            SubmitTicketWorker.this.submitTicket(jsonObject, attachments, ticketFolder);
                            return;
                        } else if (SubmitTicketWorker.this.displayLog == null || !SubmitTicketWorker.this.displayLog.exists()) {
                            SubmitTicketWorker.this.incrementSuccessCount();
                            return;
                        } else {
                            SubmitTicketWorker.this.attachLogsAndUpdateTicketAction(jsonObject, new File[]{SubmitTicketWorker.this.displayLog}, attachments);
                            SubmitTicketWorker.this.submitTicket(jsonObject, attachments, ticketFolder);
                            return;
                        }
                    } catch (Exception e) {
                        SubmitTicketWorker.logger.e("Exception was found: " + e);
                        SubmitTicketWorker.this.incrementFailureCount();
                        return;
                    }
                }
                SubmitTicketWorker.this.incrementFailureCount();
            }
        }, 9);
    }

    static JSONObject getJSONObjectFromFile(File file) {
        try {
            JSONObject jsonObject = new JSONObject(getStringFromFile(file));
            logger.v("jsonObject status: " + jsonObject);
            return jsonObject;
        } catch (Exception e) {
            logger.e("getting the JSON Object from file failed: " + e);
            return null;
        }
    }

    private static String getStringFromFile(File file) throws Exception {
        try {
            FileInputStream fileInputStream = new FileInputStream(file);
            String fileString = convertStreamToString(fileInputStream);
            fileInputStream.close();
            if (!StringUtils.isEmptyAfterTrim(fileString)) {
                return fileString;
            }
            logger.e("getStringFromFile failed");
            return null;
        } catch (Exception e) {
            logger.e("Exception found: " + e);
            return null;
        }
    }

    private static String convertStreamToString(InputStream inputStream) throws Exception {
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            StringBuilder stringBuilder = new StringBuilder();
            while (true) {
                String line = bufferedReader.readLine();
                if (line == null) {
                    break;
                }
                stringBuilder.append(line).append("\n");
            }
            bufferedReader.close();
            if (!StringUtils.isEmptyAfterTrim(stringBuilder)) {
                return stringBuilder.toString();
            }
            logger.e("convertStreamToString failed");
            return null;
        } catch (Exception e) {
            logger.e("Exception found: " + e);
            return null;
        }
    }

    private synchronized void submitTicket(final JSONObject ticketContents, File ticketAttachment, final File folder) {
        logger.v("submitTicket content status: " + ticketContents + ", attachment status : " + ticketAttachment);
        if (ticketContents == null) {
            logger.e("Ticket contents are null. Deleting folder for invalid support request form:" + folder.getAbsolutePath());
            IOUtils.deleteDirectory(NavdyApplication.getAppContext(), folder);
            incrementFailureCount();
        } else {
            if (ticketAttachment != null) {
                if (ticketAttachment.length() > 0) {
                    logger.v("ticket attachment size: " + ticketAttachment.length());
                    this.providerStore.uploadProvider().uploadAttachment("attachments.zip", ticketAttachment, "application/zip", new ZendeskCallback<UploadResponse>() {
                        public void onSuccess(UploadResponse uploadResponse) {
                            SubmitTicketWorker.logger.v("file successfully uploaded");
                            SubmitTicketWorker.this.sendToZenddesk(folder, ticketContents, uploadResponse.getToken());
                        }

                        public void onError(ErrorResponse errorResponse) {
                            SubmitTicketWorker.logger.v("file failed to upload: " + errorResponse.getReason());
                            SubmitTicketWorker.this.incrementFailureCount();
                        }
                    });
                }
            }
            sendToZenddesk(folder, ticketContents, null);
        }
    }

    private synchronized void sendToZenddesk(File folder, JSONObject ticketContents, String attachmentToken) {
        logger.v("sendToZendesk");
        String ticketType = ticketContents.optString(ZendeskConstants.TICKET_TYPE, "");
        String ticketDescription = ticketContents.optString(ZendeskConstants.TICKET_DESCRIPTION, "");
        String vin = ticketContents.optString(ZendeskConstants.TICKET_VIN, "");
        String email = ticketContents.optString(ZendeskConstants.TICKET_EMAIL, "");
        logger.v("ticketDescription: " + ticketDescription + ", ticketType: " + ticketType);
        if (StringUtils.isEmptyAfterTrim(ticketType) || StringUtils.isEmptyAfterTrim(ticketDescription)) {
            logger.e("ticket contents were null.");
            incrementFailureCount();
        } else {
            ArrayList<CustomField> customFields = new ArrayList();
            CreateRequest request = new CreateRequest();
            request.setDescription(ticketDescription);
            try {
                if (ticketContents.getString(ZendeskConstants.TICKET_ACTION).equals(ZendeskConstants.ACTION_FETCH_HUD_LOG_UPLOAD_DELETE)) {
                    logger.d("adding tag for android background submission");
                    request.setTags(Collections.singletonList(ANDROID_BACKGROUND_SUBMISSION));
                }
            } catch (Exception e) {
                logger.e("JSON Exception: " + e);
            }
            if (!StringUtils.isEmptyAfterTrim(attachmentToken)) {
                ArrayList<String> attachmentTokenList = new ArrayList();
                attachmentTokenList.add(attachmentToken);
                request.setAttachments(attachmentTokenList);
            }
            addCustomField(customFields, ZendeskConstants.PROBLEM_TYPES.longValue(), ticketType);
            if (!StringUtils.isEmptyAfterTrim(email)) {
                request.setEmail(email);
            }
            if (!StringUtils.isEmptyAfterTrim(vin)) {
                addCustomField(customFields, ZendeskConstants.VIN.longValue(), vin);
            }
            final Context appContext = NavdyApplication.getAppContext();
            addCustomFieldFromSharedPref(customFields, ZendeskConstants.VEHICLE_MAKE.longValue(), ProfilePreferences.CAR_MAKE);
            addCustomFieldFromSharedPref(customFields, ZendeskConstants.VEHICLE_MODEL.longValue(), ProfilePreferences.CAR_MODEL);
            addCustomFieldFromSharedPref(customFields, ZendeskConstants.VEHICLE_YEAR.longValue(), ProfilePreferences.CAR_YEAR);
            addCustomField(customFields, ZendeskConstants.MOBILE_APP_VERSION.longValue(), BuildConfig.VERSION_NAME);
            addCustomField(customFields, ZendeskConstants.MOBILE_APP_NAME.longValue(), appContext.getString(R.string.app_name));
            addCustomField(customFields, ZendeskConstants.MOBILE_DEVICE_MODEL.longValue(), Build.MODEL);
            addCustomField(customFields, ZendeskConstants.MOBILE_DEVICE_MAKE.longValue(), Build.MANUFACTURER);
            addCustomField(customFields, ZendeskConstants.MOBILE_OS_VERSION.longValue(), String.valueOf(VERSION.RELEASE));
            RemoteDevice remoteDevice = AppInstance.getInstance().getRemoteDevice();
            if (remoteDevice != null) {
                DeviceInfo info = remoteDevice.getDeviceInfo();
                if (info != null) {
                    addCustomField(customFields, ZendeskConstants.DISPLAY_SOFTWARE_VERSION.longValue(), info.systemVersion);
                    addCustomField(customFields, ZendeskConstants.DISPLAY_SERIAL_NUMBER.longValue(), info.deviceUuid);
                }
            }
            logger.i("setting custom fields: " + customFields);
            request.setCustomFields(customFields);
            ZendeskConfig.INSTANCE.setCustomFields(customFields);
            final JSONObject jSONObject = ticketContents;
            final File file = folder;
            this.providerStore.requestProvider().createRequest(request, new ZendeskCallback<CreateRequest>() {
                public void onSuccess(CreateRequest createRequest) {
                    SubmitTicketWorker.logger.d("ticket request successful");
                    SubmitTicketWorker.this.showSuccessNotification = true;
                    BaseActivity.showShortToast(R.string.zendesk_submit_succeeded, new Object[0]);
                    Tracker.tagEvent(Event.SUPPORT_TICKET_CREATED);
                    try {
                        if (jSONObject.optString(ZendeskConstants.TICKET_ACTION, "").equals(ZendeskConstants.ACTION_UPLOAD_AWAIT_HUD_LOG)) {
                            jSONObject.put(ZendeskConstants.TICKET_ACTION, ZendeskConstants.ACTION_FETCH_HUD_LOG_UPLOAD_DELETE);
                            SupportTicketService.writeJsonObjectToFile(jSONObject, file.getAbsolutePath(), SubmitTicketWorker.logger);
                            SettingsUtils.getSharedPreferences().edit().putBoolean(SettingsConstants.TICKETS_AWAITING_HUD_LOG_EXIST, true).apply();
                        } else {
                            SubmitTicketWorker.logger.d("deleting submitted ticket folder: " + file.getAbsolutePath());
                            IOUtils.deleteDirectory(appContext, file);
                        }
                    } catch (Exception e) {
                        SubmitTicketWorker.logger.e("exception found trying to read JSON object: " + e);
                    }
                    SubmitTicketWorker.this.incrementSuccessCount();
                    SupportTicketService.resetRetryCount();
                }

                public void onError(ErrorResponse errorResponse) {
                    SubmitTicketWorker.logger.e("ticket request failed: " + errorResponse.getReason());
                    switch (errorResponse.getStatus()) {
                        case -1:
                            SubmitTicketWorker.logger.e("Internet error");
                            break;
                        case SubmitTicketWorker.MALFORMED_TICKET /*422*/:
                            SubmitTicketWorker.logger.e("Ticket is malformed");
                            break;
                        default:
                            SubmitTicketWorker.logger.e("Ticket request failed: " + errorResponse.getReason());
                            break;
                    }
                    SubmitTicketWorker.this.incrementFailureCount();
                }
            });
        }
        return;
    }

    private void addCustomField(ArrayList<CustomField> customFields, long field, String value) {
        if (!StringUtils.isEmptyAfterTrim(value)) {
            customFields.add(new CustomField(Long.valueOf(field), value));
        }
    }

    private void addCustomFieldFromSharedPref(ArrayList<CustomField> customFields, long field, String sharedPrefKey) {
        String value = SettingsUtils.getCustomerPreferences().getString(sharedPrefKey, null);
        if (!StringUtils.isEmptyAfterTrim(value)) {
            customFields.add(new CustomField(Long.valueOf(field), value));
        }
    }

    private synchronized void fetchHudLogAndProcessTickets(final File[] folders) {
        logger.d("fetchHudLogAndProcessTickets");
        if (this.displayLog == null) {
            SupportTicketService.collectHudLog(new OnLogCollectedInterface() {
                public void onLogCollectionSucceeded(ArrayList<File> logAttachments) {
                    SubmitTicketWorker.logger.d("fetching hud log succeeded");
                    if (!logAttachments.isEmpty()) {
                        Iterator it = logAttachments.iterator();
                        while (it.hasNext()) {
                            File log = (File) it.next();
                            if (log.getName().contains("display_log")) {
                                SubmitTicketWorker.this.displayLog = log;
                                break;
                            }
                        }
                    }
                    SubmitTicketWorker.this.processAllTickets(folders);
                }

                public void onLogCollectionFailed() {
                    SubmitTicketWorker.logger.d("fetching hud log failed.");
                    SubmitTicketWorker.this.processAllTickets(folders);
                }
            });
        } else {
            logger.d("hud log exists: " + this.displayLog);
            processAllTickets(folders);
        }
    }

    private void processAllTickets(File[] folders) {
        logger.d("processAllTickets");
        for (File folder : folders) {
            processNextTicketFolder(folder);
        }
    }

    private void attachLogsAndUpdateTicketAction(JSONObject ticketContents, File[] logs, File attachmentsZipFile) {
        logger.d("attachLogsAndUpdateTicketAction");
        IOUtils.compressFilesToZip(NavdyApplication.getAppContext(), logs, attachmentsZipFile.getAbsolutePath());
        try {
            ticketContents.put(ZendeskConstants.TICKET_HAS_DISPLAY_LOG, true);
            if (ticketContents.optString(ZendeskConstants.TICKET_ACTION, "").equals(ZendeskConstants.ACTION_UPLOAD_AWAIT_HUD_LOG)) {
                ticketContents.put(ZendeskConstants.TICKET_ACTION, ZendeskConstants.ACTION_UPLOAD_DELETE);
            }
        } catch (Exception e) {
            logger.e("JSON Exception found: " + e);
        }
    }

    public synchronized boolean isRunning() {
        return this.isRunning.get();
    }

    private synchronized void incrementSuccessCount() {
        this.successCount++;
        logger.i("incrementSuccessCount: " + this.successCount);
        finishIfAllTicketsAttemptedSubmission();
    }

    private synchronized void incrementFailureCount() {
        this.failCount++;
        logger.i("incrementFailureCount: " + this.failCount);
        finishIfAllTicketsAttemptedSubmission();
    }

    private synchronized void finishIfAllTicketsAttemptedSubmission() {
        if (this.successCount + this.failCount == this.ticketCount) {
            this.isRunning.set(false);
            this.callback.onFinish(this.failCount, this.showSuccessNotification);
        }
    }
}
