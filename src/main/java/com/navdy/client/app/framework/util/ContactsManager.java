package com.navdy.client.app.framework.util;

import android.annotation.TargetApi;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.CommonDataKinds.StructuredPostal;
import android.provider.ContactsContract.Contacts;
import android.provider.ContactsContract.Data;
import android.provider.ContactsContract.Profile;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.WorkerThread;
import android.util.Pair;
import com.alelec.navdyclient.R;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.models.Address;
import com.navdy.client.app.framework.models.ContactModel;
import com.navdy.client.app.framework.models.PhoneNumberModel;
import com.navdy.client.app.framework.models.UserAccountInfo;
import com.navdy.client.app.framework.util.ImageUtils.StreamFactory;
import com.navdy.client.app.providers.DbUtils;
import com.navdy.client.app.providers.NavdyContentProviderConstants;
import com.navdy.client.debug.util.S3Constants;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.util.IOUtils;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

public class ContactsManager {
    private static final boolean VERBOSE = false;
    private static final String[] addressProjection = new String[]{"contact_id", S3Constants.LOOKUP_FOLDER_PREFIX, "data5", "data4", "data7", "data8", "data9", "data10", "data2", "display_name", "data3"};
    private static final ArrayList<Address> emptyAddressList = new ArrayList();
    private static final ArrayList<ContactModel> emptyContactList = new ArrayList();
    private static final String[] genericProjection = new String[]{"contact_id", S3Constants.LOOKUP_FOLDER_PREFIX, "display_name", "has_phone_number", "is_primary"};
    private static final ContactsManager instance = new ContactsManager();
    private static final Logger logger = new Logger(ContactsManager.class);
    private static final String[] phoneProjection = new String[]{"contact_id", S3Constants.LOOKUP_FOLDER_PREFIX, "data1", "data2", "display_name", "starred", "data3", "is_primary"};

    public enum Attributes {
        ADDRESS,
        PHONE,
        BOTH
    }

    private ContactsManager() {
    }

    public static ContactsManager getInstance() {
        return instance;
    }

    @NonNull
    public List<ContactModel> getContactsWithoutLoadingPhotos(String filter, EnumSet<Attributes> set) {
        return getContacts(filter, set, false);
    }

    public List<ContactModel> getContactsAndLoadPhotos(String filter, EnumSet<Attributes> set) {
        return getContacts(filter, set, true);
    }

    @WorkerThread
    private List<ContactModel> getContacts(String filter, EnumSet<Attributes> set, boolean loadPhotos) {
        SystemUtils.ensureNotOnMainThread();
        List<ContactModel> contacts = new ArrayList();
        ContentResolver contentResolver = NavdyApplication.getAppContext().getContentResolver();
        if (set.contains(Attributes.BOTH)) {
            contacts = getContactsWithBoth(filter, contentResolver);
        } else if (set.contains(Attributes.ADDRESS)) {
            contacts = getContactsWithAddresses(filter, contentResolver);
        } else if (set.contains(Attributes.PHONE)) {
            contacts = getContactsWithPhoneNumbers(filter, contentResolver, false);
        }
        if (loadPhotos) {
            for (ContactModel contact : contacts) {
                contact.photo = loadContactPhoto(contact.id, false);
            }
        }
        return contacts;
    }

    @WorkerThread
    @NonNull
    public List<ContactModel> getFavoriteContactsWithPhone() {
        SystemUtils.ensureNotOnMainThread();
        return getContactsWithPhoneNumbers(null, NavdyApplication.getAppContext().getContentResolver(), true);
    }

    private static List<ContactModel> getContactsWithBoth(String filter, ContentResolver contentResolver) {
        HashMap<String, ContactModel> contacts = new HashMap();
        Cursor dataCursor = getGenericCursor(contentResolver, filter);
        if (dataCursor == null) {
            return emptyContactList;
        }
        while (dataCursor.moveToNext()) {
            try {
                ContactModel contact;
                long id = DbUtils.getLong(dataCursor, "contact_id");
                String lookupKey = DbUtils.getString(dataCursor, S3Constants.LOOKUP_FOLDER_PREFIX);
                String name = DbUtils.getString(dataCursor, "display_name");
                boolean hasPhoneNumber = DbUtils.getBoolean(dataCursor, "has_phone_number");
                List<PhoneNumberModel> phoneNumbers = null;
                List<Address> addresses = getAddressFromLookupKey(contentResolver, lookupKey, id);
                if (hasPhoneNumber) {
                    long contactId = getContactIdFromLookupKey(contentResolver, lookupKey, id);
                    if (contactId < 0) {
                        contactId = id;
                    }
                    phoneNumbers = getPhoneNumbers(contentResolver, contactId);
                }
                if (contacts.get(name) != null) {
                    contact = (ContactModel) contacts.get(name);
                } else {
                    contact = new ContactModel();
                    contact.setName(name);
                    contacts.put(name, contact);
                }
                contact.setID(id);
                contact.setLookupKey(lookupKey);
                contact.setLookupTimestampToNow();
                contact.addAddresses(addresses);
                contact.addPhoneNumbers(phoneNumbers);
            } finally {
                SystemUtils.closeCursor(dataCursor);
            }
        }
        List<ContactModel> contactList = new ArrayList(contacts.values());
        return contactList;
    }

    @Nullable
    public static List<ContactModel> getContactsPreferablyWithPhoneNumber(@Nullable String filter, @NonNull ContentResolver contentResolver) {
        List<ContactModel> list = null;
        if (!StringUtils.isEmptyAfterTrim(filter)) {
            Cursor dataCursor = null;
            try {
                dataCursor = contentResolver.query(Data.CONTENT_URI, genericProjection, "display_name LIKE ?", new String[]{filter.trim() + "%"}, "has_phone_number DESC");
            } catch (Exception e) {
                logger.e("Something went wrong while trying to get a contact cursor", e);
            }
            if (dataCursor != null) {
                try {
                    list = new ArrayList();
                    while (dataCursor.moveToNext()) {
                        long id = DbUtils.getLong(dataCursor, "contact_id");
                        String lookupKey = DbUtils.getString(dataCursor, S3Constants.LOOKUP_FOLDER_PREFIX);
                        String name = DbUtils.getString(dataCursor, "display_name");
                        ContactModel contact = new ContactModel();
                        contact.setName(name);
                        contact.setID(id);
                        contact.setLookupKey(lookupKey);
                        contact.setLookupTimestampToNow();
                        list.add(contact);
                    }
                } finally {
                    SystemUtils.closeCursor(dataCursor);
                }
            }
        }
        return list;
    }

    private List<ContactModel> getContactsWithAddresses(String filter, ContentResolver contentResolver) {
        Cursor addressCursor = null;
        try {
            List<ContactModel> contacts;
            addressCursor = getAddressCursor(contentResolver, filter);
            if (addressCursor == null) {
                contacts = emptyContactList;
            } else {
                contacts = getAddressesFromContactsHelper(addressCursor);
                SystemUtils.closeCursor(addressCursor);
            }
            return contacts;
        } finally {
            SystemUtils.closeCursor(addressCursor);
        }
    }

    private List<ContactModel> getContactsWithPhoneNumbers(String filter, ContentResolver contentResolver, boolean starredOnly) {
        Cursor phoneCursor = null;
        try {
            List<ContactModel> contacts;
            phoneCursor = getPhoneCursor(contentResolver, filter, starredOnly);
            if (phoneCursor == null) {
                contacts = emptyContactList;
            } else {
                contacts = getPhoneNumbersFromContactsHelper(phoneCursor);
                SystemUtils.closeCursor(phoneCursor);
            }
            return contacts;
        } finally {
            SystemUtils.closeCursor(phoneCursor);
        }
    }

    private static List<ContactModel> getAddressesFromContactsHelper(Cursor cursor) {
        HashMap<String, ContactModel> contacts = new HashMap();
        if (cursor == null) {
            return emptyContactList;
        }
        while (cursor.moveToNext()) {
            ContactModel contact;
            String name = DbUtils.getString(cursor, "display_name");
            if (contacts.get(name) != null) {
                contact = (ContactModel) contacts.get(name);
            } else {
                contact = new ContactModel();
                contact.setName(name);
            }
            contact.addAddress(getAddress(cursor));
            contact.setID(Long.valueOf(DbUtils.getLong(cursor, "contact_id")).longValue());
            contact.setLookupKey(DbUtils.getString(cursor, S3Constants.LOOKUP_FOLDER_PREFIX));
            contact.setLookupTimestampToNow();
            contacts.put(name, contact);
        }
        return new ArrayList(contacts.values());
    }

    private static List<ContactModel> getPhoneNumbersFromContactsHelper(Cursor cursor) {
        HashMap<String, ContactModel> contacts = new LinkedHashMap();
        while (cursor.moveToNext()) {
            ContactModel contact;
            String name = DbUtils.getString(cursor, "display_name");
            if (contacts.get(name) != null) {
                contact = (ContactModel) contacts.get(name);
            } else {
                contact = new ContactModel();
                contact.setName(name);
            }
            contact.addPhoneNumber(getPhoneNumber(cursor));
            contact.favorite = DbUtils.getInt(cursor, "starred") > 0;
            contact.setID(Long.valueOf(DbUtils.getLong(cursor, "contact_id")).longValue());
            contact.setLookupKey(DbUtils.getString(cursor, S3Constants.LOOKUP_FOLDER_PREFIX));
            contact.setLookupTimestampToNow();
            contacts.put(name, contact);
        }
        return new ArrayList(contacts.values());
    }

    public static long getContactIdFromLookupKey(ContentResolver contentResolver, String key, long lastKnownContactId) throws IllegalStateException {
        if (StringUtils.isEmptyAfterTrim(key)) {
            return lastKnownContactId;
        }
        Uri uri;
        if (lastKnownContactId > 0) {
            uri = Uri.parse(Contacts.CONTENT_LOOKUP_URI + S3Constants.S3_FILE_DELIMITER + key + S3Constants.S3_FILE_DELIMITER + lastKnownContactId);
        } else {
            uri = Uri.parse(Contacts.CONTENT_LOOKUP_URI + S3Constants.S3_FILE_DELIMITER + key);
        }
        Cursor cursor = null;
        try {
            cursor = contentResolver.query(uri, new String[]{"_id", S3Constants.LOOKUP_FOLDER_PREFIX}, null, null, null);
            if (cursor == null || !cursor.moveToNext()) {
                throw new IllegalStateException("contact no longer exists for key");
            }
            long latestContactId = DbUtils.getLong(cursor, "_id");
            updateContactData(key, lastKnownContactId, DbUtils.getString(cursor, S3Constants.LOOKUP_FOLDER_PREFIX), latestContactId);
            if (latestContactId <= 0) {
                return lastKnownContactId;
            }
            IOUtils.closeStream(cursor);
            return latestContactId;
        } catch (Exception e) {
            logger.e("Something went wrong while trying to get a contact cursor", e);
            return lastKnownContactId;
        } finally {
            IOUtils.closeStream(cursor);
        }
    }

    public static List<Address> getAddressFromLookupKey(ContentResolver contentResolver, String lookupKey, long lastKnownContactId) {
        if (getContactIdFromLookupKey(contentResolver, lookupKey, lastKnownContactId) < 0) {
            return emptyAddressList;
        }
        try {
            Cursor cursor = contentResolver.query(StructuredPostal.CONTENT_URI, addressProjection, "contact_id = ? AND mimetype = ?", new String[]{String.valueOf(getContactIdFromLookupKey(contentResolver, lookupKey, lastKnownContactId)), "vnd.android.cursor.item/postal-address_v2"}, null);
            List<Address> addresses = new ArrayList();
            while (cursor != null && cursor.moveToNext()) {
                addresses.add(getAddress(cursor));
            }
            SystemUtils.closeCursor(cursor);
            return addresses;
        } catch (Exception e) {
            logger.e("Something went wrong while trying to get an address cursor", e);
            SystemUtils.closeCursor(null);
            return emptyAddressList;
        } catch (Throwable th) {
            SystemUtils.closeCursor(null);
            throw th;
        }
    }

    private static Address getAddress(Cursor cursor) {
        return new Address(DbUtils.getString(cursor, "data5"), DbUtils.getString(cursor, "data4"), DbUtils.getString(cursor, "data7"), DbUtils.getString(cursor, "data8"), DbUtils.getString(cursor, "data9"), DbUtils.getString(cursor, "data10"), DbUtils.getInt(cursor, "data2"), DbUtils.getString(cursor, "display_name"), DbUtils.getString(cursor, "data3"));
    }

    @NonNull
    public static List<PhoneNumberModel> getPhoneNumbers(ContentResolver contentResolver, long contactId) {
        ArrayList<PhoneNumberModel> numbers = new ArrayList();
        if (contactId >= 0) {
            try {
                Cursor cursor = contentResolver.query(Phone.CONTENT_URI, phoneProjection, "contact_id = ?", new String[]{String.valueOf(contactId)}, null);
                while (cursor != null && cursor.moveToNext()) {
                    numbers.add(getPhoneNumber(cursor));
                }
                SystemUtils.closeCursor(cursor);
            } catch (Exception e) {
                logger.e("Something went wrong while trying to get a phone number cursor", e);
                SystemUtils.closeCursor(null);
            } catch (Throwable th) {
                SystemUtils.closeCursor(null);
                throw th;
            }
        }
        return numbers;
    }

    @NonNull
    private static PhoneNumberModel getPhoneNumber(Cursor cursor) {
        return new PhoneNumberModel(DbUtils.getString(cursor, "data1"), DbUtils.getInt(cursor, "data2"), DbUtils.getString(cursor, "data3"), DbUtils.getBoolean(cursor, "is_primary"));
    }

    private static Cursor getPhoneCursor(ContentResolver contentResolver, String startsWith, boolean starredOnly) {
        String selection = null;
        String[] args = null;
        if (starredOnly) {
            selection = "starred>?";
            args = new String[]{String.valueOf(0)};
        }
        if (!StringUtils.isEmptyAfterTrim(startsWith)) {
            selection = "display_name LIKE ?";
            args = new String[]{startsWith.trim() + "%"};
        }
        Cursor cursor = null;
        try {
            return contentResolver.query(Phone.CONTENT_URI, phoneProjection, selection, args, "sort_key ASC");
        } catch (Exception e) {
            logger.e("Something went wrong while trying to get a phone number cursor", e);
            return cursor;
        }
    }

    private static Cursor getAddressCursor(ContentResolver contentResolver, String startsWith) {
        String selection = null;
        String[] selectionArgs = null;
        if (!StringUtils.isEmptyAfterTrim(startsWith)) {
            selection = "display_name LIKE ?";
            selectionArgs = new String[]{startsWith.trim() + "%"};
        }
        Cursor cursor = null;
        try {
            return contentResolver.query(StructuredPostal.CONTENT_URI, addressProjection, selection, selectionArgs, "sort_key ASC");
        } catch (Exception e) {
            logger.e("Something went wrong while trying to get an address cursor", e);
            return cursor;
        }
    }

    private static Cursor getGenericCursor(ContentResolver contentResolver, String startsWith) {
        String selection = null;
        String[] selectionArgs = null;
        if (!StringUtils.isEmptyAfterTrim(startsWith)) {
            selection = "display_name LIKE ?";
            selectionArgs = new String[]{startsWith.trim() + "%"};
        }
        Cursor cursor = null;
        try {
            return contentResolver.query(Data.CONTENT_URI, genericProjection, selection, selectionArgs, "sort_key ASC");
        } catch (Exception e) {
            logger.e("Something went wrong while trying to get a contact cursor", e);
            return cursor;
        }
    }

    @Nullable
    public static String getContactName(ContentResolver cr, long id) {
        Cursor cursor = null;
        String string;
        try {
            ContentResolver contentResolver = cr;
            cursor = contentResolver.query(Data.CONTENT_URI, new String[]{"display_name"}, "contact_id=?", new String[]{String.valueOf(id)}, null);
            if (cursor == null || !cursor.moveToFirst()) {
                IOUtils.closeStream(cursor);
                return null;
            }
            string = DbUtils.getString(cursor, "display_name");
            return string;
        } catch (Exception e) {
            logger.e("Something went wrong while trying to get a contact cursor", e);
        } finally {
            IOUtils.closeStream(cursor);
        }
        return null;
    }

    private static Bitmap loadContactPhoto(long id, final boolean highRes) {
        InputStream input = null;
        Bitmap bitmap;
        try {
            final ContentResolver contentResolver = NavdyApplication.getAppContext().getContentResolver();
            final Uri uri = ContentUris.withAppendedId(Contacts.CONTENT_URI, id);
            input = Contacts.openContactPhotoInputStream(contentResolver, uri, highRes);
            if (input == null) {
                bitmap = null;
                return bitmap;
            }
            bitmap = ImageUtils.getScaledBitmap(new StreamFactory() {
                public InputStream getInputStream() throws FileNotFoundException {
                    return Contacts.openContactPhotoInputStream(contentResolver, uri, highRes);
                }
            }, NavdyApplication.getAppContext().getResources().getDimensionPixelSize(R.dimen.contact_image_width), NavdyApplication.getAppContext().getResources().getDimensionPixelSize(R.dimen.contact_image_height));
            IOUtils.closeStream(input);
            return bitmap;
        } catch (Throwable e) {
            logger.e(e);
            bitmap = null;
        } finally {
            IOUtils.closeStream(input);
        }
        return null;
    }

    @TargetApi(14)
    private static String getUserProfileEmail() {
        ContentResolver contentResolver = NavdyApplication.getAppContext().getContentResolver();
        Cursor cursor = null;
        try {
            cursor = contentResolver.query(Uri.withAppendedPath(Profile.CONTENT_URI, CarMdClient.DATA), new String[]{"data1"}, "mimetype=?", new String[]{"vnd.android.cursor.item/email_v2"}, "is_primary DESC, is_primary DESC");
            if (cursor == null || cursor.getCount() <= 0) {
                IOUtils.closeStream(cursor);
                return null;
            } else if (cursor.moveToFirst()) {
                String string = DbUtils.getString(cursor, "data1");
                IOUtils.closeStream(cursor);
                return string;
            } else {
                IOUtils.closeStream(cursor);
                return null;
            }
        } catch (Exception e) {
            logger.e("Something went wrong while trying to get a contact cursor", e);
            IOUtils.closeStream(cursor);
        } catch (Throwable th) {
            IOUtils.closeStream(cursor);
            throw th;
        }
        return null;
    }

    public static UserAccountInfo lookUpUserProfileInfo() {
        Cursor cursor = null;
        UserAccountInfo uai = new UserAccountInfo("", "", null);
        ContentResolver contentResolver = NavdyApplication.getAppContext().getContentResolver();
        try {
            cursor = contentResolver.query(Profile.CONTENT_URI, new String[]{"_id", "display_name"}, "is_user_profile = 1", null, null);
            if (cursor == null || !cursor.moveToFirst()) {
                logger.v("The profile cursor is null or empty!");
            } else {
                String id = DbUtils.getString(cursor, "_id");
                uai.fullName = DbUtils.getString(cursor, "display_name");
                logger.v("***** name = " + uai.fullName);
                if (!StringUtils.isEmptyAfterTrim(id)) {
                    long contactId = Long.parseLong(id);
                    uai.email = getUserProfileEmail();
                    logger.v("***** email = " + uai.email);
                    uai.photo = loadContactPhoto(contactId, true);
                    logger.v("***** photo = " + uai.photo);
                }
            }
            IOUtils.closeStream(cursor);
        } catch (Exception e) {
            logger.e("Something went wrong while trying to get a contact cursor", e);
            IOUtils.closeStream(cursor);
        } catch (Throwable th) {
            IOUtils.closeStream(cursor);
            throw th;
        }
        return uai;
    }

    private static void updateContactData(String oldLookupKey, long oldContactId, String newLookupKey, long newContactId) {
        if (StringUtils.equalsOrBothEmptyAfterTrim(oldLookupKey, newLookupKey)) {
            logger.d("updateContactData: The lookup keys are the same so no-op");
            return;
        }
        Pair<String, String[]> selection;
        if (!StringUtils.isEmptyAfterTrim(oldLookupKey) && oldContactId > 0) {
            selection = new Pair("contact_lookup_key=? OR last_known_contact_id=?", new String[]{oldLookupKey, String.valueOf(oldContactId)});
        } else if (!StringUtils.isEmptyAfterTrim(oldLookupKey) && oldContactId <= 0) {
            selection = new Pair("contact_lookup_key=?", new String[]{oldLookupKey});
        } else if (!StringUtils.isEmptyAfterTrim(oldLookupKey) || oldContactId <= 0) {
            logger.e("Can't update contact data because oldLookupKey = " + oldLookupKey + " and oldContactId = " + oldContactId);
            return;
        } else {
            selection = new Pair("last_known_contact_id=?", new String[]{String.valueOf(oldContactId)});
        }
        logger.d("Updating any destination that matches: selection = " + ((String) selection.first) + " args = " + Arrays.toString((Object[]) selection.second));
        ContentResolver contentResolver = NavdyApplication.getAppContext().getContentResolver();
        ContentValues contentValues = new ContentValues();
        contentValues.put(NavdyContentProviderConstants.DESTINATIONS_CONTACT_LOOKUP_KEY, newLookupKey);
        contentValues.put(NavdyContentProviderConstants.DESTINATIONS_LAST_KNOWN_CONTACT_ID, Long.valueOf(newContactId));
        contentValues.put(NavdyContentProviderConstants.DESTINATIONS_LAST_CONTACT_LOOKUP, Long.valueOf(new Date().getTime()));
        contentResolver.update(NavdyContentProviderConstants.DESTINATIONS_CONTENT_URI, contentValues, (String) selection.first, (String[]) selection.second);
    }
}
